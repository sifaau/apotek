<?php
function get_menu_utama(){
  $general_model = new App\Models\General_model;
  $get = $general_model->select_order('zpb_access_role_group','*',['status'=>1],"order ASC");
  return $get;
}

function get_sub_menu($id_group){
  $general_model = new App\Models\General_model;
  $get = $general_model->select_order('zpb_access_role','*',['id_access_role_group'=>$id_group,'type'=>1,'status'=>1],"order ASC");
  return $get;
}

function check_akses_menu($id){
  $session = session();
  $id_user = $session->get('id_user');
  $general_model = new App\Models\General_model;
  $get = $general_model->count_rows('zpb_access_role_group_user','id',['id_user'=>$id_user,'id_access_role_group'=>$id],"order ASC");
  return $get;
}

function check_akses_sub_menu($id){
  $session = session();
  $id_user = $session->get('id_user');
  $general_model = new App\Models\General_model;
  $get = $general_model->count_rows('zpb_access_role_permission','id',['id_user'=>$id_user,'id_access_role'=>$id],"order ASC");
  return $get;
}

?>
