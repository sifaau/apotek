<?php


function child_code_accounting_for_option($code_parent,$name_code_parent,$proyek=null){
  $option         =   '';
  $array          =   array();
  $get_array  =   child_code_accounting_for_option_array($array,$code_parent,$name_code_parent,$proyek);
  foreach ($get_array as $key => $value) {
    $option = $option.'<option value="'.$value['code'].'">'.$value['code'].' '.$value['name'].'</option>';
  }
  echo $option;
}

function child_code_accounting_for_option_array($array,$code_parent,$name_code_parent,$proyek=null){
  $general_model = new App\Models\General_model;
  // $ci = & get_instance();
  // $general_model = new App\Models\General_model;
  $max_level = code_accounting_max_level();
  // $array = array();
  $min_length = code_accounting_journal_min_length();
  if ( strlen($code_parent) >= $min_length ){
    $x = check_12_digit($code_parent,$proyek);
    if (count($x) > 0){
      array_push($array,$x);
    }
  } else {
    $q3 = $general_model->select_data('zacc_code_accounting','code,name',"code LIKE '".$code_parent."%' AND status IS NULL AND level = '".$max_level."' ");
    //  'XXXX.YYY'
    if ( $q3->getNumRows() > 0 ){
      foreach ($q3->getResult() as $row4) {
        $x = check_12_digit($row4->code,$proyek);
        if (count($x) > 0){
          array_push($array,$x);
        }
      }
    } else {
      $x = check_12_digit($code_parent,$proyek);
      if (count($x) > 0){
        array_push($array,$x);
      }
    }
  }
  return $array;
}

function check_12_digit($code_parent,$proyek=null){
  $general_model = new App\Models\General_model;
  $x = array();
  $min_length = code_accounting_journal_min_length();
  if ( strlen($code_parent) >= $min_length ){
    if ($proyek =='' OR $proyek == null){
      $get_code = $general_model->select_data('zacc_code_accounting','code,name',"code = '".$code_parent."' AND status IS NULL");
      if ( $get_code->getNumRows() > 0 ){
        foreach ($get_code->getResult() as $row1) {
          $x = array(
            'code'  =>  $row1->code,
            'name'  =>  $row1->name,
          );
        }
      }
    } else {
      if ( $proyek == '12'){
        $get_code = $general_model->select_data('zacc_code_accounting','code,name',"code = '".$code_parent."' AND ( project = '".$proyek."' OR project IS NULL OR project = '' OR project = 'ALL' OR project = 'UMUM' ) AND status IS NULL");
      } else {
        $get_code = $general_model->select_data('zacc_code_accounting','code,name',"code = '".$code_parent."' AND project = '".$proyek."' AND status IS NULL");
      }
      if ( $get_code->getNumRows() > 0 ){
        foreach ($get_code->getResult() as $row2) {
          $x = array(
            'code'  =>  $row2->code,
            'name'  =>  $row2->name,
          );
        }
      }
    }
  }
  return $x;
}

function get_name_code_acounting($code)
{

  $general_model = new App\Models\General_model;
  $condition = ['code' => $code];
  $result = $general_model->get_data_by_field('zacc_code_accounting', 'name', $condition);

  return $result;
}

function get_list_code_acount_helper($code_parent)
{

  $ci->load->model('accounting/code_accounting_model');
  $condition = "code_parent = '".$code_parent."' AND status IS NULL";
  $result = $ci->code_accounting_model->list_code_accounting($condition);

  return $result;
}

function detail_journal($id)
{

  $general_model = new App\Models\General_model;
  $result = $general_model->select_order('zacc_journal as j,zacc_journal_detail as d,zacc_code_accounting as c',
  'd.code_accounting,d.debet,d.credit,d.desc,c.name as account_name,d.id_user_update,d.date_update,c.type_report',
  "j.id = d.id_journal
  AND d.code_accounting = code
  AND d.id_journal = '".$id."'
  AND j.status IS NULL
  AND d.status IS NULL
  AND c.status IS NULL",
  'd.line ASC, d.debet DESC');

  return $result;
}

function list_income_statement($code_parent, $id_periode)
{

  $ci->load->model('accounting/saldo_model');
  $q = $ci->db->query("SELECT id FROM zacc_periode WHERE id < '".$id_periode."' ORDER BY id DESC LIMIT 1 ");
  if ($q->getNumRows() > 0) {
    $id_periode_before = $q->row()->id;
  } else {
    $id_periode_before = 0;
  }
  $result = $ci->saldo_model->list_detail_income_statement2($code_parent, $id_periode, $id_periode_before);

  return $result;
}

function sum_income_statement($code_parent, $id_periode)
{

  $ci->load->model('accounting/saldo_model');
  $condition = "zacc_code_accounting.code_parent = '".$code_parent."' AND zacc_code_accounting.status IS NULL AND zacc_saldo.id_periode = '".$id_periode."'";
  $result = $ci->saldo_model->sum_saldo_code_account('saldo', $condition);

  return $result;
}

function sum_saldo_code_accounting($condition)
{

  $ci->load->model('accounting/saldo_model');
  $condition = $condition;
  $result = $ci->saldo_model->sum_saldo_code_account('saldo', $condition);

  return $result;
}

function list_neraca_statement($code_parent, $id_periode)
{

  $ci->load->model('accounting/saldo_model');
  $condition = "zacc_code_accounting.code_parent = '".$code_parent."' AND zacc_code_accounting.status IS NULL AND zacc_saldo.id_periode = '".$id_periode."'";
  $result = $ci->saldo_model->list_detail_neraca_statement($condition);

  return $result;
}

function list_income_statement_by_division($code_parent, $code_division, $id_periode)
{

  $ci->load->model('accounting/saldo_model');
  $q = $ci->db->query("SELECT id FROM zacc_periode WHERE id < '".$id_periode."' ORDER BY id DESC LIMIT 1 ");
  if ($q->getNumRows() > 0) {
    $id_periode_before = $q->row()->id;
  } else {
    $id_periode_before = 0;
  }
  $result = $ci->saldo_model->list_detail_income_statement_by_division($code_parent, $code_division, $id_periode, $id_periode_before);

  return $result;
}

function list_ref_for_ledger($id_journal, $line)
{

  $journal_detail_model = new App\Models\journal_detail_model;
  $condition = "zacc_journal.id='".$id_journal."' AND zacc_journal_detail.line = '".$line."' AND zacc_journal_detail.status IS NULL AND zacc_journal.status IS NULL";

  return $ci->journal_model->list_ref_for_ledger($condition);
}

function list_balance_ref_for_ledger($id_journal, $code_accounting, $balance, $line)
{

  $journal_detail_model = new App\Models\journal_detail_model;
  if ($balance == 'D') {
    $con_balance = " AND zacc_journal_detail.credit > 0 AND line = '".$line."'";
  }
  if ($balance == 'K') {
    $con_balance = " AND zacc_journal_detail.debet  > 0 AND line = '".$line."'";
  }

  $condition = "zacc_journal.id = '".$id_journal."' AND zacc_journal_detail.code_accounting != '".$code_accounting."' ".$con_balance.' AND zacc_journal_detail.status IS NULL AND zacc_journal.status IS NULL';

  return $ci->journal_model->list_ref_for_ledger($condition);
}

function check_line_journal_detail($id_journal_detail)
{
  $general_model = new App\Models\General_model;
  return $line = $general_model->get_data_by_field('zacc_journal_detail','line', ['id' => $id_journal_detail]);
}

function check_sub_ledger_by_id_journal_detail($id_journal_detail)
{

  $ci->load->model('accounting/sub_ledger_model');

  return $ci->sub_ledger_model->count_rows(['id_journal_detail' => $id_journal_detail]);
}

function check_have_a_child($code_parent)
{

  $ci->load->model('accounting/code_accounting_model');
  $condition = "code_parent = '".$code_parent."' AND status IS NULL";

  return $ci->code_accounting_model->count_rows($condition);
}

function get_child_code_accounting($code_parent)
{

  $ci->load->model('accounting/code_accounting_model');
  $condition = "code_parent = '".$code_parent."' AND status IS NULL";
  $q = $general_model->select_data('zacc_code_accounting', 'code,name,balance,satuan_rab,project,division', $condition);

  return $q;
}

function get_child_code_accounting_by_proyek($code_parent, $proyek)
{

  $ci->load->model('accounting/code_accounting_model');
  $condition = "code_parent = '".$code_parent."' AND project = '".$proyek."' AND status IS NULL";
  $q = $general_model->select_data('zacc_code_accounting', 'code,name,balance,satuan_rab,project,division', $condition);

  return $q;
}

function get_child_code_accounting_by_division($code_parent, $division)
{

  $ci->load->model('accounting/code_accounting_model');
  $condition = "code_parent = '".$code_parent."' AND division = '".$division."' AND status IS NULL";
  $q = $general_model->select_data('zacc_code_accounting', 'code,name,balance', $condition);

  return $q;
}

function sum_debet_or_credit_detail_journal($id_journal, $param)
{

  $general_model = new App\Models\General_model;
  if ($param == 'D') {
    $field = 'debet';
  }
  if ($param == 'K') {
    $field = 'credit';
  }

  return $hasil = $general_model->select_sum('zacc_journal_detail', $field, "id_journal = '".$id_journal."' AND status IS NULL");
}

function get_list_code_acount_by_project_helper($code_parent, $project)
{

  $ci->load->model('accounting/code_accounting_model');
  $condition = "code_parent = '".$code_parent."' AND project = '".$project."' AND status IS NULL";
  $result = $ci->code_accounting_model->list_code_accounting($condition);

  return $result;
}

function get_list_code_acount_by_user($id_user, $proyek = null)
{

  $general_model = new App\Models\General_model;
  $min_length = code_accounting_journal_min_length();
  $data = [];
  $check_code_acc_user = $general_model->count_rows('zpy_code_acc_user', 'code_accounting', ['id_user' => $id_user]);
  if ($check_code_acc_user > 0) {
    $code_acc_user = $general_model->get_data_by_field('zpy_code_acc_user', 'code_accounting', ['id_user' => $id_user]);
    $code_acc = $general_model->select_data('zacc_code_accounting', '*', ' code_parent IN ('.$code_acc_user.') AND status IS NULL');
    $data = [];
    foreach ($code_acc->getResult() as $row) {
      $code_acc2 = $general_model->select_data('zacc_code_accounting', '*', "code_parent = '".$row->code."' AND status IS NULL");
      if ($code_acc2->getNumRows() > 0) {
        $item2 = [];
        foreach ($code_acc2->getResult() as $row2) {
          $code_acc3 = $general_model->select_data('zacc_code_accounting', '*', "code_parent = '".$row2->code."' AND status IS NULL");
          if ($code_acc3->getNumRows() > 0) {
            $item3 = [];
            if ($proyek != null) {
              $condition = "code_parent = '".$row2->code."' AND project = '".$proyek."' AND status IS NULL";
            } else {
              $condition = "code_parent = '".$row2->code."' AND status IS NULL";
            }
            $code_acc3 = $general_model->select_data('zacc_code_accounting', '*', $condition);
            foreach ($code_acc3->getResult() as $row3) {
              if (strlen($row3->code) >= $min_length) {
                $item3['code'] = $row3->code;
                $item3['name'] = $row3->name;
                array_push($data, $item3);
              }
            }
          } else {
            if (strlen($row2->code) >= $min_length) {
              $item2['code'] = $row2->code;
              $item2['name'] = $row2->name;
              array_push($data, $item2);
            }
          }
        }
      } else {
        if (strlen($row->code) >= $min_length) {
          $item['code'] = $row->code;
          $item['name'] = $row->name;
          array_push($data, $item);
        }
      }
    }
  }

  return $data;
}

function get_kd_rek_hutang_infra($proyek = null)
{

  $general_model = new App\Models\General_model;
  $min_length = code_accounting_journal_min_length();
  $data = [];
  $code_acc = $general_model->select_data('zacc_code_accounting', '*', "code_parent IN ('2117','2116','2118','2120','2119','2121','2122') AND status IS NULL");
  if ($code_acc->getNumRows() > 0) {
    foreach ($code_acc->getResult() as $row) {
      $code_acc2 = $general_model->select_data('zacc_code_accounting', '*', "code_parent = '".$row->code."' AND status IS NULL");
      if ($code_acc2->getNumRows() > 0) {
        $code_acc2 = $general_model->select_data('zacc_code_accounting', '*', "code_parent = '".$row->code."' AND project = '".$proyek."' AND status IS NULL");
        foreach ($code_acc2->getResult() as $row2) {
          if (strlen($row2->code) >= $min_length) {
            $item2['code'] = $row2->code;
            $item2['name'] = $row2->name;
            array_push($data, $item2);
          }
        }
      } else {
        if (strlen($row->code) >= $min_length) {
          $item['code'] = $row->code;
          $item['name'] = $row->name;
          array_push($data, $item);
        }
      }
    }

    return $data;
  }
}

function list_hpp_by_project($code_parent, $id_periode, $id_proyek)
{

  $ci->load->model('accounting/saldo_model');
  $q = $ci->db->query("SELECT id FROM zacc_periode WHERE id < '".$id_periode."' ORDER BY id DESC LIMIT 1 ");
  if ($q->getNumRows() > 0) {
    $id_periode_before = $q->row()->id;
  } else {
    $id_periode_before = 0;
  }
  $condition = "zacc_code_accounting.code_parent = '".$code_parent."' AND zacc_saldo.id_periode = '".$id_periode."' AND zacc_code_accounting.status IS NULL";
  $result = $ci->saldo_model->list_detail_income_statement_by_project($code_parent, $id_periode, $id_periode_before, $id_proyek);

  return $result;
}

function get_laba_rugi_divisi($code_division, $id_periode)
{
  $total41 = $total41debet = $total41credit = 0;
  $q_level_1 = get_child_code_accounting('41');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting_by_division($row2->code, $code_division);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total41 = $total41 + $saldo;
                $total41debet = $row->balance == 'K' ? $total41debet : ($total41debet + $saldo);
                $total41credit = $row->balance == 'D' ? $total41credit : ($total41credit + $saldo);
              }
            }
          }
        }
      }
    }
  }
  $penjualan_bersih = ($total41credit - $total41debet);
  $total51 = $total51debet = $total51credit = 0;
  $q_level_1 = get_child_code_accounting('51');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting_by_division($row2->code, $code_division);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total51 = $total51 + $saldo;
                $total51debet = $row->balance == 'K' ? $total51debet : $total51debet + $saldo;
                $total51credit = $row->balance == 'D' ? $total51credit : $total51credit + $saldo;
              }
            }
          }
        }
      }
    }
  }

  $laba_kotor = ($total51debet - $total51credit);
  $laba_kotor = $penjualan_bersih - $laba_kotor;
  $total_biaya = $total61 = $total61debet = $total61credit = 0;

  $q_level_1 = get_child_code_accounting('61');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting_by_division($row2->code, $code_division);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total61 = $total61 + $saldo;
                $total_biaya = $total_biaya + $saldo;
                $total61debet = $row->balance == 'K' ? $total61debet : $total61debet + $saldo;
                $total61credit = $row->balance == 'D' ? $total61credit : $total61credit + $saldo;
              }
            }
          }
        }
      }
    }
  }
  $laba_bersih = $total61debet - $total61credit;
  $laba_bersih = $laba_kotor - $laba_bersih;
  $total_pendapatan = $total71 = 0;
  $q_level_1 = get_child_code_accounting('71');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting_by_division($row2->code, $code_division);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total71 = $total71 + $saldo;
                $total_pendapatan = $total_pendapatan + $saldo;
              }
            }
          }
        }
      }
    }
  }

  if ($code_division == 'SU') {
    $laba_sebelum_pajak = $total_biaya - $total_pendapatan;
  } else {
    $laba_sebelum_pajak = $laba_bersih + $total_pendapatan;
  }

  return $laba_sebelum_pajak;
}

function get_total_penjualan_bersih_by_divisi($code_division, $id_periode)
{
  $total41 = $total41debet = $total41credit = 0;
  $q_level_1 = get_child_code_accounting('41');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting_by_division($row2->code, $code_division);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total41 = $total41 + $saldo;
                $total41debet = $row->balance == 'K' ? $total41debet : ($total41debet + $saldo);
                $total41credit = $row->balance == 'D' ? $total41credit : ($total41credit + $saldo);
              }
            }
          }
        }
      }
    }
  }
  $penjualan_bersih = ($total41credit - $total41debet);

  return $penjualan_bersih;
}

function saldo_before_laba_rugi_periode($code, $balance, $id_periode)
{

  $general_model = new App\Models\General_model;
  $journal_detail_model = new App\Models\journal_detail_model;
  $check_periode = $general_model->select_data('zacc_periode', 'month,year', ['id' => $id_periode]);
  $type_report = $general_model->get_data_by_field('zacc_code_accounting', 'type_report', ['code' => $code]);
  if ($check_periode->getNumRows() === 1) {
    $d = $check_periode->row();
    $month = $d->month;
    $year = $d->year;
    $periode_before = date('m-Y', strtotime($year.'-'.$month.'-01 -1 MONTH'));
    $explode_periode_before = explode('-', $periode_before);
    $month_before = $explode_periode_before[0];
    $year_before = $explode_periode_before[1];
    $id_previous_periode = $general_model->get_data_by_field('zacc_periode', 'id', ['month' => $month_before, 'year' => $year_before]);
    // $condition                               = "MONTH(zacc_journal.date_trans) = '".$month."' AND YEAR(zacc_journal.date_trans) = '".$year."' AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
    $condition                              = " ( zacc_journal.date_trans BETWEEN '".$year."-".$month."-01 00:00:00' AND '".$year."-".$month."-31 23:59:59' ) AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
    $sum_debet = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet', $condition);
    $sum_credit = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit', $condition);
    $condition_laba_periode = "zacc_journal.id = zacc_journal_detail.id_journal
    AND ( zacc_journal.date_trans BETWEEN '".$year."-".$month."-01 00:00:00' AND '".$year."-".$month."-31 23:59:59' )
    -- AND MONTH(zacc_journal.date_trans) = '".$month."'
    -- AND YEAR(zacc_journal.date_trans) = '".$year."'
    AND zacc_journal_detail.code_accounting = '".$code."'
    AND zacc_journal_detail.desc = 'LABA RUGI PERIODE'
    AND zacc_journal.status IS NULL
    AND zacc_journal_detail.status IS NULL";
    $q = $general_model->select_data('zacc_journal,zacc_journal_detail', 'zacc_journal_detail.debet,zacc_journal_detail.credit', $condition_laba_periode);
    if ($balance == 'K') {
      //$q = $general_model->select_data('zacc_journal,zacc_journal_detail','zacc_journal_detail.debet,zacc_journal_detail.credit',$condition_laba_periode);
      if ($type_report == 'LR') {
        if ($q->getNumRows() === 1) {
          $d = $q->row();
          $nominal_laba_rugi_periode = $d->debet;
          if ((int) $nominal_laba_rugi_periode === 0) {
            $nominal_laba_rugi_periode = -$d->credit;
          }
        } else {
          $nominal_laba_rugi_periode = 0;
        }
      } else {
        $nominal_laba_rugi_periode = 0;
      }

      $sum_debet = $sum_debet - $nominal_laba_rugi_periode;
    }
    if ($balance == 'D') {
      //$q        = $general_model->select_data('zacc_journal,zacc_journal_detail','zacc_journal_detail.credit,zacc_journal_detail.debet',$condition_laba_periode);
      if ($type_report == 'LR') {
        if ($q->getNumRows() === 1) {
          $d = $q->row();
          $nominal_laba_rugi_periode = $d->credit;
          if ((int) $nominal_laba_rugi_periode === 0) {
            $nominal_laba_rugi_periode = -$d->debet;
          }
        } else {
          $nominal_laba_rugi_periode = 0;
        }
      } else {
        $nominal_laba_rugi_periode = 0;
      }

      $sum_credit = $sum_credit - $nominal_laba_rugi_periode;
    }
    $saldo = action_hitung_saldo_akhir($id_previous_periode, $code, $balance, $sum_debet, $sum_credit);
  } else {
    $saldo = 0;
  }

  return $saldo;
}

function saldo_after_laba_rugi_periode($code, $balance, $id_periode)
{

  $general_model = new App\Models\General_model;
  $journal_detail_model = new App\Models\journal_detail_model;
  $check_periode = $general_model->select_data('zacc_periode', 'month,year', ['id' => $id_periode]);
  $type_report = $general_model->get_data_by_field('zacc_code_accounting', 'type_report', ['code' => $code]);
  if ($check_periode->getNumRows() === 1) {
    $d = $check_periode->row();
    $month = $d->month;
    $year = $d->year;
    $periode_before = date('m-Y', strtotime($year.'-'.$month.'-01 -1 MONTH'));
    $explode_periode_before = explode('-', $periode_before);
    $month_before = $explode_periode_before[0];
    $year_before = $explode_periode_before[1];
    $id_previous_periode = $general_model->get_data_by_field('zacc_periode', 'id', ['month' => $month_before, 'year' => $year_before]);
    $condition = "MONTH(zacc_journal.date_trans) = '".$month."' AND YEAR(zacc_journal.date_trans) = '".$year."' AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
    $sum_debet = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet', $condition);
    $sum_credit = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit', $condition);
    $saldo = action_hitung_saldo_akhir($id_previous_periode, $code, $balance, $sum_debet, $sum_credit);
  } else {
    $saldo = 0;
  }

  return $saldo;
}

function action_hitung_saldo_akhir($id_previous_periode, $code, $balance, $sum_debet, $sum_credit)
{

  $ci->load->model('accounting/saldo_model');
  $saldo = 0;
  $check_saldo_previous_periode = $ci->saldo_model->count_rows(['id_periode' => $id_previous_periode, 'code_accounting' => $code]);
  if ($check_saldo_previous_periode > 0) {
    $previous_saldo = $ci->saldo_model->get_data_by_field('saldo', ['id_periode' => $id_previous_periode, 'code_accounting' => $code]);
  } else {
    $previous_saldo = 0;
  }

  if ($balance == 'D') {
    $saldo = $previous_saldo + ($sum_debet - $sum_credit);
  }
  if ($balance == 'K') {
    $saldo = $previous_saldo + ($sum_credit - $sum_debet);
  }

  return $saldo;
}

function check_income_statement_by_division($code_parent, $code_division)
{

  $general_model = new App\Models\General_model;
  $hasil = 0;
  $result = $general_model->select_data('zacc_code_accounting', 'division', "code_parent = '".$code_parent."' AND type_report = 'LR' AND status IS NULL");
  foreach ($result->getResult() as $row) {
    $hasil = $row->division == $code_division ? $hasil + 1 : $hasil;
    $result = $general_model->select_data('zacc_code_accounting', 'division', "code_parent = '".$code_parent."' AND type_report = 'LR' AND status IS NULL");
    foreach ($result->getResult() as $row2) {
      $hasil = $row2->division == $code_division ? $hasil + 1 : $hasil;
      $result = $general_model->select_data('zacc_code_accounting', 'division', "code_parent = '".$code_parent."' AND division = '".$code_division."' AND type_report = 'LR' AND status IS NULL");
      foreach ($result->getResult() as $row3) {
        $hasil = $row3->division == $code_division ? $hasil + 1 : $hasil;
      }
    }
  }

  return $hasil;
}

function list_code_child($code_parent)
{

  $general_model = new App\Models\General_model;
  $result = $general_model->select_order('zacc_code_accounting', 'code,name,project,balance,type_report', "code_parent = '".$code_parent."' AND status IS NULL", 'code ASC');
  return $result;
}

// helper lama diganti get_saldo_akhir_4_digit
function get_saldo_akhir_level_1($code, $id_periode)
{
  // $code AAAA
  $total = 0;
  $q_level_2 = get_child_code_accounting($code);
  if ($q_level_2->getNumRows() > 0) {
    foreach ($q_level_2->getResult() as $row2) {
      // $code AAAA.BBB
      $q_level_3 = get_child_code_accounting($row2->code);
      if ($q_level_3->getNumRows() > 0) {
        foreach ($q_level_3->getResult() as $row3) {
          // $code AAAA.BBB.CCC
          $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
          if ($saldo != 0) {
            $total = $total + $saldo;
          }
        }
      }
    }
  }

  return $total;
}

// helper lama diganti get_saldo_akhir_2_digit
function get_saldo_akhir_level_2($code, $id_periode)
{
  // $code AA
  $total = 0;
  $q_level_1 = get_child_code_accounting($code);
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      // $code AAAA
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          // $code AAAA.BBB
          $q_level_3 = get_child_code_accounting($row2->code);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              // $code AAAA.BBB.CCC
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total = $total + $saldo;
              }
            }
          }
        }
      }
    }
  }

  return $total;
}

function get_saldo_akhir_2_digit($code, $id_periode)
{
  // $code AA
  $total = 0;
  $q_level_1 = get_child_code_accounting($code);
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      // $code AAAA
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          // $code AAAA.BBB
          $q_level_3 = get_child_code_accounting($row2->code);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              // $code AAAA.BBB.CCC
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total = $total + $saldo;
              }
            }
          }
        }
      }
    }
  }

  return $total;
}

function get_saldo_akhir_4_digit($code, $id_periode)
{
  // $code AAAA
  $total = 0;
  $q_level_2 = get_child_code_accounting($code);
  if ($q_level_2->getNumRows() > 0) {
    foreach ($q_level_2->getResult() as $row2) {
      // $code AAAA.BBB
      $q_level_3 = get_child_code_accounting($row2->code);
      if ($q_level_3->getNumRows() > 0) {
        foreach ($q_level_3->getResult() as $row3) {
          // $code AAAA.BBB.CCC
          $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
          if ($saldo != 0) {
            $total = $total + $saldo;
          }
        }
      }
    }
  }

  return $total;
}

function get_saldo_akhir_8_digit($code, $id_periode)
{
  // $code AAAA.BBB
  $total = 0;
  $q_level_1 = get_child_code_accounting($code);
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row3) {
      // $code AAAA.BBB.CCC
      $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
      if ($saldo != 0) {
        $total = $total + $saldo;
      }
    }
  }

  return $total;
}

function get_saldo_akhir_8_digit_lb($code, $balance, $id_periode)
{
  // $code AAAA.BBB
  $total = 0;
  $q_level_1 = get_child_code_accounting($code);
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row3) {
      // $code AAAA.BBB.CCC
      $saldo = saldo_before_laba_rugi_periode($row3->code, $balance, $id_periode);
      if ($saldo != 0) {
        $total = $total + $saldo;
      }
    }
  }

  return $total;
}

function get_detail_laba_rugi_by_periode($id_periode)
{
  $total41 = $total41debet = $total41credit = 0;
  $q_level_1 = get_child_code_accounting('41');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting($row2->code);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total41 = $total41 + $saldo;
                $total41debet = $row->balance == 'K' ? $total41debet : ($total41debet + $saldo);
                $total41credit = $row->balance == 'D' ? $total41credit : ($total41credit + $saldo);
              }
            }
          }
        }
      }
    }
  }
  $penjualan_bersih = ($total41credit - $total41debet);
  $total51 = $total51debet = $total51credit = 0;
  $q_level_1 = get_child_code_accounting('51');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting($row2->code);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total51 = $total51 + $saldo;
                $total51debet = $row->balance == 'K' ? $total51debet : $total51debet + $saldo;
                $total51credit = $row->balance == 'D' ? $total51credit : $total51credit + $saldo;
              }
            }
          }
        }
      }
    }
  }

  $laba_kotor = ($total51debet - $total51credit);
  $laba_kotor = $penjualan_bersih - $laba_kotor;
  $total_biaya = $total61 = $total61debet = $total61credit = 0;

  $q_level_1 = get_child_code_accounting('61');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting($row2->code);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total61 = $total61 + $saldo;
                $total_biaya = $total_biaya + $saldo;
                $total61debet = $row->balance == 'K' ? $total61debet : $total61debet + $saldo;
                $total61credit = $row->balance == 'D' ? $total61credit : $total61credit + $saldo;
              }
            }
          }
        }
      }
    }
  }
  $laba_bersih = $total61debet - $total61credit;
  $laba_bersih = $laba_kotor - $laba_bersih;
  $total_pendapatan = $total71 = 0;
  $q_level_1 = get_child_code_accounting('71');
  if ($q_level_1->getNumRows() > 0) {
    foreach ($q_level_1->getResult() as $row) {
      $q_level_2 = get_child_code_accounting($row->code);
      if ($q_level_2->getNumRows() > 0) {
        foreach ($q_level_2->getResult() as $row2) {
          $q_level_3 = get_child_code_accounting($row2->code);
          if ($q_level_3->getNumRows() > 0) {
            foreach ($q_level_3->getResult() as $row3) {
              $saldo = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
              if ($saldo != 0) {
                $total71 = $total71 + $saldo;
                $total_pendapatan = $total_pendapatan + $saldo;
              }
            }
          }
        }
      }
    }
  }

  $laba_sebelum_pajak = $laba_bersih + $total_pendapatan;

  return ['penjualan_bersih' => $penjualan_bersih, 'laba_kotor' => $laba_kotor, 'laba_bersih' => $laba_bersih, 'laba_sebelum_pajak' => $laba_sebelum_pajak];
}

function get_saldo_awal_level_1($code, $id_periode)
{

  $ci->load->helper('journal_accounting_helper');
  $total = 0;
  $q_level_2 = get_child_code_accounting($code);
  if ($q_level_2->getNumRows() > 0) {
    foreach ($q_level_2->getResult() as $row2) {
      $q_level_3 = get_child_code_accounting($row2->code);
      if ($q_level_3->getNumRows() > 0) {
        foreach ($q_level_3->getResult() as $row3) {
          $month = get_bulan_by_id_periode($id_periode);
          $year = get_tahun_by_id_periode($id_periode);
          $id_previous_periode = get_id_periode_bulan_lalu($month, $year);
          $check_saldo_previous_periode = $general_model->count_rows('zacc_saldo', 'id', ['id_periode' => $id_previous_periode, 'code_accounting' => $row3->code]);
          if ($check_saldo_previous_periode > 0) {
            $saldo = $general_model->get_data_by_field('zacc_saldo', 'saldo', ['id_periode' => $id_previous_periode, 'code_accounting' => $row3->code]);
          } else {
            $saldo = 0;
          }
          if ($saldo != 0) {
            $total = $total + $saldo;
          }
        }
      }
    }
  }

  return $total;
}

function saldo_before_laba_rugi_tahunan($code, $balance, $year)
{

  $ci->load->helper('journal_accounting_helper');
  $x = 1;
  $saldo = 0;
  for ($x; $x < 13; ++$x) {
    $month = $x;
    $id_periode = get_id_periode($month, $year);
    if ($id_periode != '' or $id_periode != null) {
      $saldo_bulanan = saldo_before_laba_rugi_periode($code, $balance, $id_periode);
      $saldo = $saldo_bulanan + $saldo;
    }
  }

  return $saldo;
}

function get_saldo_akhir_all_type_report_level_1($code, $id_periode)
{

  $ci->load->helper('journal_accounting_helper');
  $total = 0;
  $bulan = get_bulan_by_id_periode($id_periode);
  $tahun = get_tahun_by_id_periode($id_periode);
  $id_previous_periode = get_id_periode_bulan_lalu($bulan, $tahun);

  $level = list_code_child($code);
  foreach ($level->getResult() as $row2) {
    $level = list_code_child($row2->code);
    foreach ($level->getResult() as $row3) {
      $check_saldo_previous_periode = $ci->saldo_model->count_rows(['id_periode' => $id_previous_periode, 'code_accounting' => $row3->code]);
      if ($check_saldo_previous_periode > 0) {
        $saldo_awal = $ci->saldo_model->get_data_by_field('saldo', ['id_periode' => $id_previous_periode, 'code_accounting' => $row3->code]);
      } else {
        $saldo_awal = 0;
      }
      $saldo_before_laba_rugi_periode = saldo_before_laba_rugi_periode($row3->code, $row3->balance, $id_periode);
      $sum_debet = get_sum_journal_by_pos_before_laba_rugi_periode($bulan, $tahun, $row3->code, 'D');
      $sum_credit = get_sum_journal_by_pos_before_laba_rugi_periode($bulan, $tahun, $row3->code, 'K');
      $saldo_akhir = action_hitung_saldo_akhir($id_previous_periode, $row3->code, $row3->balance, $sum_debet, $sum_credit);
      $total = $total + $saldo_akhir;
    }
  }

  return $total;
}

function get_laba_rugi_divisi_tahunan($code_division, $year)
{

  $ci->load->helper('journal_accounting_helper');
  $x = 1;
  $saldo = 0;
  for ($x; $x < 13; ++$x) {
    $month = $x;
    $id_periode = get_id_periode($month, $year);
    if ($id_periode != '' or $id_periode != null) {
      $saldo_bulanan = get_laba_rugi_divisi($code_division, $id_periode);
      $saldo = $saldo_bulanan + $saldo;
    }
  }

  return $saldo;
}

function code_accounting_max_level(){
  return 3;
}

function code_accounting_journal_min_length(){
  // $general_model = new App\Models\General_model;
  // // $general_model = new App\Models\General_model;
  // $hasil = $general_model->get_data_by_field('a_app_setting','value',array("param"=>'code_accounting_journal_min_length'));
  // return $hasil == '' ? 12 : (int)$hasil;
  return 8;
}
