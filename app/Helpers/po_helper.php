<?php
function get_total_tagihan($id_po){


}

function get_sub_total($id_po){
  $general_model = new App\Models\General_model;
  $q = $general_model->select_data('po_detail','*'," status = '1' AND id_po = '".$id_po."' ");
  $totalharga = 0;
  foreach ($q->getResult() as $row) {
    $qty_come = $general_model->select_sum('po_barang_masuk','qty',['id_po_detail'=>$row->id,'status'=>1]);
    if ($qty_come > 0){
      $subtotal = $row->harga_beli*$qty_come;
    } else {
      $subtotal = $row->harga_beli*$row->qty;
    }
    $nominal_diskon = $row->diskon;
    // if ($row->persen_diskon>0){
    //   $nominal_diskon = $subtotal*($row->persen_diskon/100);
    // }
    $totalharga += $subtotal- round($nominal_diskon);
  }
  return $totalharga;
}

function get_harga_net($id_po){
  $general_model = new App\Models\General_model;
  $sub_total = get_sub_total($id_po);
  $diskon = $general_model->get_data_by_field('po','diskon'," id = '".$id_po."' ");
  if ($diskon>0){
    $nominal_diskon = $sub_total*($diskon/100);
    $sub_total = $sub_total - round($nominal_diskon);
  }
  return $sub_total;
}

function get_nominal_ppn($id_po){
  $general_model = new App\Models\General_model;
  $harga_net = get_harga_net($id_po);
  $ppn = $general_model->get_data_by_field('po','ppn'," id = '".$id_po."' ");
  if ($ppn>0){
    $nominal = $harga_net * ( $ppn/100 );
  } else {
    $nominal = 0;
  }
  return $nominal;
}


function get_harga_kesepakatan($id_po){
  $general_model = new App\Models\General_model;
  $harga_net          = get_harga_net($id_po);
  $nominal_ppn        = get_nominal_ppn($id_po);
  $harga_kesepakatan  = $harga_net + $nominal_ppn;
  $general_model->do_update('po',['harga_kesepakatan'=>$harga_kesepakatan],['id'=>$id_po]);
  return $harga_kesepakatan;
}

?>
