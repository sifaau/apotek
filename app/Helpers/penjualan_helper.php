<?php

function get_sub_total_penjualan($id_penjualan){
  $general_model = new App\Models\General_model;
  $q = $general_model->select_data('penjualan_detail','*'," status = '1' AND id_penjualan = '".$id_penjualan."' ");
  $totalharga = 0;
  foreach ($q->getResult() as $row) {
    $subtotal = ($row->qty * $row->harga_jual);
    if($row->persen_diskon == '' || $row->persen_diskon == null){
      $persen_diskon = 0;
      $nilai_diskon = 0;
    } else {
      $persen_diskon = $row->persen_diskon;
      $nilai_diskon = $subtotal * ($persen_diskon/100);
    }
    $total = ( $subtotal - $nilai_diskon ) + $row->tuslah;
    $totalharga += $total;
  }
  return $totalharga;
}

function get_harga_net_penjualan($id_penjualan){
  $general_model = new App\Models\General_model;
  $sub_total = get_sub_total_penjualan($id_penjualan);
  $diskon = $general_model->get_data_by_field('penjualan','diskon'," id = '".$id_penjualan."' ");
  $tuslah = $general_model->get_data_by_field('penjualan','tuslah'," id = '".$id_penjualan."' ");
  $tuslah = $tuslah > 0 ? $tuslah : 0;
  if ($diskon>0){
    $nominal_diskon = $sub_total * ($diskon/100);
    $sub_total = ( $sub_total - $nominal_diskon ) + $tuslah;
  }
  return $sub_total;
}

function get_nominal_ppn_penjualan($id_penjualan){
  $general_model = new App\Models\General_model;
  $harga_net = get_harga_net_penjualan($id_penjualan);
  $ppn = $general_model->get_data_by_field('penjualan','ppn'," id = '".$id_penjualan."' ");
  if ($ppn>0){
    $nominal = $harga_net * ( $ppn/100 );
  } else {
    $nominal = 0;
  }
  return $nominal;
}



function get_harga_kesepakatan_penjualan($id_penjualan){
  $general_model = new App\Models\General_model;
  $harga_net          = get_harga_net_penjualan($id_penjualan);
  $nominal_ppn        = get_nominal_ppn_penjualan($id_penjualan);
  $harga_kesepakatan  = ($harga_net + $nominal_ppn);
  $general_model->do_update('penjualan',['harga_kesepakatan'=>$harga_kesepakatan],['id'=>$id_penjualan]);
  return $harga_kesepakatan;
}

?>
