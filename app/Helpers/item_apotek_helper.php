<?php
function text_stock_item_terkecil($id_reff,$table_reff){
  $general_model = new App\Models\General_model;
  $stok = '';
  $q_satuan = $general_model->select_data($table_reff.' as o,satuan as s',"s.satuan",
  "s.id_reff=o.id
  AND s.table_reff='".$table_reff."'
  AND o.id= '".$id_reff."'
  AND s.id_reff='".$id_reff."'
  AND s.ket = '1'");
  if ($q_satuan->getNumRows()>0){
    $d_satuan = $q_satuan->getRow();
    $txt_satuan = $d_satuan->satuan;
    $total_stock = get_stock_item_terkecil($id_reff,$table_reff);
    $stok .= '<a href="'.base_url().'/report/stock_awal/list_detail/'.$table_reff.'/'.$id_reff.'"><span class="text-primary pl-1"><b>'.number_format($total_stock,0,',','.').'</b></span></a> '.$txt_satuan;
  }
  return $stok;
}

function get_stock_item_terkecil($id_reff,$table_reff){
  $general_model = new App\Models\General_model;
  $qstock = $general_model->select_order($table_reff.' as o,satuan as s',"s.satuan,s.id,s.ket",
  "s.id_reff=o.id
  AND s.table_reff='".$table_reff."'
  AND o.id= '".$id_reff."'
  AND s.id_reff='".$id_reff."'
  ","s.ket DESC");

  $stok = '';
  $total_stock = 0;
  if ($qstock->getNumRows()>0){
    $get_konversi1 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>1]);
    if ($get_konversi1 == '' || $get_konversi1 == null){
      $get_konversi1 = 0;
    }
    $get_konversi2 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>2]);
    if ($get_konversi2 == '' || $get_konversi2 == null){
      $get_konversi2 = 0;
    }
    $get_konversi3 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>3]);
    if ($get_konversi3 == '' || $get_konversi3 == null){
      $get_konversi3 = 0;
    }
    $get_konversi4 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>4]);
    if ($get_konversi4 == '' || $get_konversi4 == null){
      $get_konversi4 = 0;
    }

    foreach ($qstock->getResult() as $rows) {
      $jml_stock = $general_model->select_sum($table_reff.' as o,satuan as s,master_stok as ms','ms.qty',
      "s.id_reff=o.id AND s.table_reff='".$table_reff."'
      AND ms.id_satuan=s.id
      AND o.id= '".$id_reff."'
      AND s.table_reff= '".$table_reff."'
      AND s.id_reff='".$id_reff."'
      AND s.satuan='".$rows->satuan."'
      AND s.ket='".$rows->ket."'
      ");
      if ($rows->ket=='4'){
        $sub_total = ((($jml_stock*$get_konversi4)*$get_konversi3)*$get_konversi2);
      }
      if ($rows->ket=='3'){
        $sub_total = ($jml_stock*$get_konversi3)*$get_konversi2;
      }
      if ($rows->ket=='2'){
        $sub_total = $jml_stock*$get_konversi2;
      }
      if ($rows->ket=='1'){
        $sub_total = $jml_stock;
      }
      $total_stock = $total_stock+$sub_total;
    }
  }
  return $total_stock;
}

function get_stock_item_terkecil_by_no_batch($id_reff,$table_reff,$no_batch,$date_expired){
  $general_model = new App\Models\General_model;
  $qstock = $general_model->select_order($table_reff.' as o,satuan as s',"s.satuan,s.id,s.ket",
  "s.id_reff=o.id
  AND s.table_reff='".$table_reff."'
  AND o.id= '".$id_reff."'
  AND s.id_reff='".$id_reff."'
  ","s.ket DESC");

  $stok = 0;
  if ($qstock->getNumRows()>0){
    $get_konversi1 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>1]);
    if ($get_konversi1 == '' || $get_konversi1 == null){
      $get_konversi1 = 0;
    }
    $get_konversi2 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>2]);
    if ($get_konversi2 == '' || $get_konversi2 == null){
      $get_konversi2 = 0;
    }
    $get_konversi3 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>3]);
    if ($get_konversi3 == '' || $get_konversi3 == null){
      $get_konversi3 = 0;
    }
    $get_konversi4 = $general_model->get_data_by_field('satuan','konversi',['id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>4]);
    if ($get_konversi4 == '' || $get_konversi4 == null){
      $get_konversi4 = 0;
    }

    $txt_satuan = '';
    $total_stock = 0;
    foreach ($qstock->getResult() as $rows) {
      $jml_stock = $general_model->select_sum($table_reff.' as o,satuan as s,master_stok as ms','ms.qty',
      "s.id_reff=o.id AND s.table_reff='".$table_reff."'
      AND ms.id_satuan=s.id
      AND o.id= '".$id_reff."'
      AND s.table_reff= '".$table_reff."'
      AND s.id_reff='".$id_reff."'
      AND s.satuan='".$rows->satuan."'
      AND s.ket='".$rows->ket."'
      AND ms.no_batch = '".$no_batch."'
      AND ms.date_expired = '".$date_expired."'
      ");
      if ($rows->ket=='4'){
        $sub_total = ((($jml_stock*$get_konversi4)*$get_konversi3)*$get_konversi2);
      }
      if ($rows->ket=='3'){
        $sub_total = ($jml_stock*$get_konversi3)*$get_konversi2;
      }
      if ($rows->ket=='2'){
        $sub_total = $jml_stock*$get_konversi2;
      }
      if ($rows->ket=='1'){
        $sub_total = $jml_stock;
        $txt_satuan = $rows->satuan;
      }
      $total_stock = $total_stock+$sub_total;
    }
    $stok = $stok + ($total_stock);
  }
  return $stok;
}

function text_stock_item_terkecil_by_satuan_batch($id_satuan,$id_rak,$no_batch,$expired){
  $general_model = new App\Models\General_model;
  $cek = $general_model->select_sum('master_stok','qty',['id_satuan'=>$id_satuan,'no_batch'=>$no_batch,'id_gudang_rak'=>$id_rak,'date_expired'=>$expired]);
  return $cek;
}

function hitung_konversi($id_item,$table_reff_item,$no_batch,$date_expired,$date_trans,$satuan_ke){
  $session = session();
  $id_user = $session->get('id_user');
  $general_model = new App\Models\General_model;
  $satuan_ke = (int)$satuan_ke;
  if ($satuan_ke < 4){
    $ket_satuan_ke2 = $satuan_ke+1;
    $id_satuan = $general_model->get_data_by_field('satuan','id'," id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket='".$satuan_ke."' ");
    $satuan = $general_model->get_data_by_field('satuan','satuan'," id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket='".$satuan_ke."' ");
    $harga_beli = $general_model->get_data_by_field('satuan','harga_beli'," id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket='".$satuan_ke."' ");
    $cek_satuan_ke2 = $general_model->count_rows('satuan','id'," id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket='".$ket_satuan_ke2."' ");
    if ($cek_satuan_ke2 > 0){
      $id_satuan2 = $general_model->get_data_by_field('satuan','id'," id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket='".$ket_satuan_ke2."' ");
      $cek_stok_lama_satuan_ke_2 = $general_model->select_sum('master_stok','qty'," id_satuan='".$id_satuan2."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."'");

      if ($cek_stok_lama_satuan_ke_2>0){

        $id_satuan2 = $general_model->get_data_by_field('satuan','id'," id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket='".$ket_satuan_ke2."' ");
        $satuan2 = $general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan2]);
        $harga_beli2 = $general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan2]);
        $konversi_satuan_ke_2 = $general_model->get_data_by_field('satuan','konversi'," id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket='".$ket_satuan_ke2."' ");
        if ((int)$konversi_satuan_ke_2 > 0){

          // kurangi stok satuan ke 2
          $get_master_stok_satuan_ke_2 = $general_model->select_order_limit('master_stok','*'," id_satuan='".$id_satuan2."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' AND qty > 0",'id ASC',1);
          foreach ($get_master_stok_satuan_ke_2->getResult() as $rowms2) {
            $stok_lama = $rowms2->qty;
            $stok_baru = $rowms2->qty - 1;
            $update_stock = $general_model->do_update('master_stok',['qty'=>$stok_baru],['id'=>$rowms2->id]);
            $data_stock_opname = [
              'id_item'         =>  $id_item,
              'table_reff_item' =>  $table_reff_item,
              'id_gudang_rak'   =>  $rowms2->id_gudang_rak,
              'masuk'           =>  0,
              'keluar'          =>  1,
              'stock_awal'      =>  $stok_lama,
              'last_stock'      =>  $stok_baru,
              'satuan'          =>  $satuan2,
              'no_batch'        =>  $no_batch,
              'harga_satuan'    =>  $harga_beli2,
              'date_expired'    =>  $date_expired,
              'ket'             =>  'Konversi',
              'date_opname'     =>  $date_trans,
              'date_insert'     =>  date('Y-m-d H:i:s'),
              'id_user'         =>  $id_user,
            ];
            $general_model->do_save('stock_opname',$data_stock_opname);

            // tambah stok satuan ke 1
            $check_di_rak_satuan1 = $general_model->count_rows('master_stok','*'," id_satuan='".$id_satuan."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' AND id_gudang_rak='".$rowms2->id_gudang_rak."'");
            if ($check_di_rak_satuan1 > 0){
              $get_master_stok_satuan_ke_1 = $general_model->select_order_limit('master_stok','*'," id_satuan='".$id_satuan."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' AND id_gudang_rak='".$rowms2->id_gudang_rak."'",'id ASC',1);
              foreach ($get_master_stok_satuan_ke_1->getResult() as $rowms1) {
                $stok_lama1 = $rowms1->qty;
                $stok_baru1 = $rowms1->qty + $konversi_satuan_ke_2;
                $update_stock = $general_model->do_update('master_stok',['qty'=>$stok_baru1],['id'=>$rowms1->id]);
                $data_stock_opname = [
                  'id_item'         =>  $id_item,
                  'table_reff_item' =>  $table_reff_item,
                  'id_gudang_rak'   =>  $rowms2->id_gudang_rak,
                  'masuk'           =>  $konversi_satuan_ke_2,
                  'keluar'          =>  0,
                  'stock_awal'      =>  $stok_lama1,
                  'last_stock'      =>  $stok_baru1,
                  'satuan'          =>  $satuan,
                  'no_batch'        =>  $no_batch,
                  'harga_satuan'    =>  $harga_beli,
                  'date_expired'    =>  $date_expired,
                  'ket'             =>  'Konversi',
                  'date_opname'     =>  $date_trans,
                  'date_insert'     =>  date('Y-m-d H:i:s'),
                  'id_user'         =>  $id_user,
                ];
                $general_model->do_save('stock_opname',$data_stock_opname);
              }
            } else {
              $data_stok1 = [
                'qty'             =>  $konversi_satuan_ke_2,
                'id_satuan'       =>  $id_satuan,
                'no_batch'        =>  $no_batch,
                'date_expired'    =>  $date_expired,
                'id_gudang_rak'   =>  $rowms2->id_gudang_rak,
                'date_update'     =>  date('Y-m-d H:i:s'),
                'id_user_update'  =>  $id_user,
              ];
              $update_stock = $general_model->do_save('master_stok',$data_stok1);
              $data_stock_opname = [
                'id_item'         =>  $id_item,
                'table_reff_item' =>  $table_reff_item,
                'id_gudang_rak'   =>  $rowms2->id_gudang_rak,
                'masuk'           =>  $konversi_satuan_ke_2,
                'keluar'          =>  0,
                'stock_awal'      =>  0,
                'last_stock'      =>  $konversi_satuan_ke_2,
                'satuan'          =>  $satuan,
                'no_batch'        =>  $no_batch,
                'harga_satuan'    =>  $harga_beli,
                'date_expired'    =>  $date_expired,
                'ket'             =>  'Konversi',
                'date_opname'     =>  $date_trans,
                'date_insert'     =>  date('Y-m-d H:i:s'),
                'id_user'         =>  $id_user,
              ];
              $general_model->do_save('stock_opname',$data_stock_opname);
            }
          }

        } else {
          hitung_konversi($id_item,$table_reff_item,$no_batch,$date_expired,$date_trans,$ket_satuan_ke2);
          // return '0';
        }
      } else {
        hitung_konversi($id_item,$table_reff_item,$no_batch,$date_expired,$date_trans,$ket_satuan_ke2);
      }
    } else {
      return '0';
    }
  } else {
    return '0';
  }
}

function get_max_loop($id_item,$table_reff_item,$satuan_ke,$no_batch,$date_expired){
  $general_model = new App\Models\General_model;
  $q_max_looping = $general_model->select_data('satuan','id,ket,konversi',"id_reff='".$id_item."' AND table_reff='".$table_reff_item."' AND ket >= '".$satuan_ke."' AND ket < '".($satuan_ke+2)."'");
  $max_looping = 0;
  foreach ($q_max_looping->getResult() as $rloop) {
    $stoknya = $general_model->select_sum('master_stok','qty',"id_satuan='".$rloop->id."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' ");
    if ($stoknya > 0){
      if ($rloop != '1'){
        if ($rloop->konversi > 0){
          $jml_loop = $stoknya * $rloop->konversi;
          $max_looping = $max_looping + $jml_loop;
        }
      }
    }
  }
  return $max_looping;
}

function cek_stok_for_penjualan($id_item,$table_reff_item,$satuan_ke,$id_satuan,$no_batch,$date_expired,$jumlah_ambil,$date_trans,$loop_ke,$max_looping){
  $general_model = new App\Models\General_model;
  $cek_stok = $general_model->select_sum('master_stok','qty',"id_satuan='".$id_satuan."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' ");
  if ($cek_stok < $jumlah_ambil){
    $konversi = hitung_konversi($id_item,$table_reff_item,$no_batch,$date_expired,$date_trans,$satuan_ke);
    if ($konversi == '0'){
      return '0';
    } else {
      // return true;
      // echo $max_looping;
      $max_looping = (int)$max_looping;
      if ($loop_ke > $max_looping){
        return '0';
      } else {
        $loop_ke = $loop_ke + 1;
        cek_stok_for_penjualan($id_item,$table_reff_item,$satuan_ke,$id_satuan,$no_batch,$date_expired,$jumlah_ambil,$date_trans,$loop_ke,$max_looping);
      }
    }
  } else {
    return true;
  }
}

function cek_stok_for_penjualan_2($dibutuhkan,$id_reff,$reff,$ket,$no_batch,$date_expired,$date_trans)
{

  $session = session();
  $id_user = $session->get('id_user');
  $general_model = new App\Models\General_model;

  // $dibutuhkan = 8;
  // $id_reff = 2;
  // $ket = 1;
  // $reff = 'obat';
  $nama_item = $general_model->get_data_by_field($reff,'nama',array('id'=>$id_reff));
  $q = $general_model->select_order('satuan','*'," id_reff = '".$id_reff."' AND table_reff = '".$reff."' AND ket >= ".$ket."",'ket ASC');
  $konversi1 = 1;
  $konversi2 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>2]);
  $konversi3 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>3]);
  $konversi4 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>4]);
  if ((int)$ket===2){
    $konversi2 = 1;
  } else if ((int)$ket===3) {
    $konversi2 = 1;
    $konversi3 = 1;
  } else if ((int)$ket===4) {
    $konversi2 = 1;
    $konversi3 = 1;
    $konversi4 = 1;
  }

  // echo 'diambil ='.$dibutuhkan.'<br><br>';
  // echo '<table border="1">';
  // echo '<tr>';
  // echo '<td>urutan</td>';
  // echo '<td>konversi</td>';
  // echo '<td>stok</td>';
  // echo '<td>hasil_konversi</td>';
  // echo '<td>diambil</td>';
  // echo '<td>total diambil</td>';
  // echo '<td>sisa kurang</td>';
  // echo '<td>stok diambil</td>';
  // echo '<td>pembulatan stok diambil</td>';
  // echo '<td>sisa_satuan_terkecil</td>';
  // echo '<td>sisa_stok</td>';
  // echo '</tr>';
  $dibutuhkan_awal      = $dibutuhkan;
  $total_diambil        = 0;
  $sisa_satuan_terkecil = 0;
  $total_stock_akhir    = 0;
  foreach ($q->getResult() as $row) {

    $id_satuannya = get_id_satuan_item($id_reff,$reff,$row->ket);
    $satuan = get_satuan_item($id_reff,$reff,$row->ket);
    $stok = $general_model->select_sum('master_stok','qty',['id_satuan'=>$id_satuannya,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);

    $konversi = $row->konversi;
    if ($row->ket=='1'){
      $hasil_konversi = ((($stok*$konversi1)));
    } else if ($row->ket=='2'){
      if ($konversi2 == '' || $konversi2 == null || $konversi2 === 0 || $konversi2 == '0'){
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, konversi '.$satuan.' '.$nama_item.' belum disetting '));
        exit;
      }
      $hasil_konversi = ((($stok*$konversi2)*$konversi1));
    } else if ($row->ket=='3'){
      if ($konversi3 == '' || $konversi3 == null || $konversi3 === 0 || $konversi3 == '0' || $konversi2 == '' || $konversi2 == null || $konversi2 === 0 || $konversi2 == '0'){
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, konversi '.$satuan.' '.$nama_item.' belum disetting '));
        exit;
      }
      $hasil_konversi = ((($stok*$konversi3)*$konversi2)*$konversi1);
    } else if ($row->ket=='4'){
      if ($konversi4 == '' || $konversi4 == null || $konversi4 === 0 || $konversi4 == '0' || $konversi3 == '' || $konversi3 == null || $konversi3 === 0 || $konversi3 == '0' || $konversi2 == '' || $konversi2 == null || $konversi2 === 0 || $konversi2 == '0'){
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, konversi '.$satuan.' '.$nama_item.' belum disetting '));
        exit;
      }
      $hasil_konversi = ((($stok*$konversi4)*$konversi3)*$konversi2)*$konversi1;
    }

    if ($dibutuhkan > $hasil_konversi ){
      $diambil = $hasil_konversi;
      $total_diambil = $total_diambil + $diambil;
      $dibutuhkan = $dibutuhkan - $diambil;
    } else {
      $diambil = $dibutuhkan;
      $total_diambil = $total_diambil + $diambil;
      $dibutuhkan = $dibutuhkan - $diambil;
    }

    $sisa_satuan_terkecil = $hasil_konversi - $diambil;

    if ($row->ket=='1'){
      $stok_diambil = $diambil/$konversi1;
      $pembulatan_stok_diambil = round_up($stok_diambil,0);
      $sisa_stok = $stok - $pembulatan_stok_diambil;
      $sisa_stok_konversi_terkecil = $sisa_stok*$konversi1;
    } else if ($row->ket=='2'){
      $stok_diambil = ((($diambil/$konversi2)/$konversi1));
      $pembulatan_stok_diambil = round_up($stok_diambil,0);
      $sisa_stok = $stok - $pembulatan_stok_diambil;
      $sisa_stok_konversi_terkecil = ((($sisa_stok*$konversi2)*$konversi1));
    } else if ($row->ket=='3'){
      $stok_diambil = ((($diambil/$konversi3)/$konversi2)/$konversi1);
      $pembulatan_stok_diambil = round_up($stok_diambil,0);
      $sisa_stok = $stok - $pembulatan_stok_diambil;
      $sisa_stok_konversi_terkecil = ((($sisa_stok*$konversi3)*$konversi2)*$konversi1);
    } else if ($row->ket=='4'){
      $stok_diambil = ((($diambil/$konversi4)/$konversi3)/$konversi2)/$konversi1;
      $pembulatan_stok_diambil = round_up($stok_diambil,0);
      $sisa_stok = $stok - $pembulatan_stok_diambil;
      $sisa_stok_konversi_terkecil = ((($sisa_stok*$konversi4)*$konversi3)*$konversi2)*$konversi1;
    }

    $sisa_satuan_terkecil         = $sisa_satuan_terkecil - $sisa_stok_konversi_terkecil ;
    $sisa_stok_konversi_terkecil  = konversi_satuan_terkecil($id_reff,$reff,($row->ket-1),$sisa_satuan_terkecil);
    // $konversixxx                  = konversi_satuan_terkecil($id_reff,$reff,$row->ket,$sisa_stok);
    // $total_stock_akhir            = $total_stock_akhir + $konversixxx;

    // echo '<tr>';
    // echo '<td>'.$row->ket.'</td>';
    // echo '<td>'.$konversi.'</td>';
    // echo '<td>'.$id_satuannya.'</td>';
    // echo '<td>'.$hasil_konversi.'</td>';
    // echo '<td>'.$diambil.'</td>';
    // echo '<td>'.$total_diambil.'</td>';
    // echo '<td>'.$dibutuhkan.'</td>';
    // echo '<td>'.$stok_diambil.'</td>';
    // echo '<td>'.$pembulatan_stok_diambil.'</td>';
    // echo '<td>'.$sisa_satuan_terkecil.'</td>';
    // echo '<td>'.$sisa_stok.'</td>';
    // echo '<td>';

    $id_rak = '';
    if ($pembulatan_stok_diambil>0){

      $get_master_stok_satuan_ke_2 = $general_model->select_order_limit('master_stok','*'," id_satuan='".$id_satuannya."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' ",'qty ASC',1);
      foreach ($get_master_stok_satuan_ke_2->getResult() as $rowms2) {

        $harga_beli = $general_model->get_data_by_field('satuan','harga_beli'," id_reff='".$id_reff."' AND table_reff='".$reff."' AND ket='".$row->ket."' ");
        $check_di_rak_satuan1 = $general_model->count_rows('master_stok','*'," id_satuan='".$id_satuannya."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' AND id_gudang_rak='".$rowms2->id_gudang_rak."'");
        if ($check_di_rak_satuan1 > 0){

          $data_stok1 = [
            'qty'=>$sisa_stok,
            'date_update'=>date('Y-m-d H:i:s'),
            'id_user_update'=>$id_user,
          ];
          $update_stock = $general_model->do_update('master_stok',$data_stok1,['id'=>$rowms2->id]);

        } else {

          $data_stok1 = [
            'qty'             =>  $sisa_stok,
            'id_satuan'       =>  $id_satuannya,
            'no_batch'        =>  $no_batch,
            'date_expired'    =>  $date_expired,
            'id_gudang_rak'   =>  $rowms2->id_gudang_rak,
            'date_update'     =>  date('Y-m-d H:i:s'),
            'id_user_update'  =>  $id_user,
          ];
          $update_stock = $general_model->do_save('master_stok',$data_stok1);
        }
        $id_rak = $rowms2->id_gudang_rak;
      }
    }

    if ($sisa_stok_konversi_terkecil > 0){
      if ($sisa_stok_konversi_terkecil < $hasil_konversi){
        $sisa_stok_terkecil_untuk_generate = $sisa_stok_konversi_terkecil;
        // $stock_satuan_dibawah = $this->stock_satuan_dibawah($id_reff,$reff,$ket,$stok1,$stok2,$stok3,$stok4);
        $stock_satuan_dibawah = 0;
        $sisa_stok_terkecil_untuk_generate_fix = $sisa_stok_terkecil_untuk_generate - $stock_satuan_dibawah;
        if ($sisa_stok_konversi_terkecil>0){
          generate_satuan_terkecil($sisa_stok_terkecil_untuk_generate,$row->ket,$id_reff,$reff,$no_batch,$date_expired,$date_trans,$id_rak);
        } else {

        }
      } else {
        $sisa_stok_terkecil_untuk_generate = 0;
      }
    } else {
      $sisa_stok_terkecil_untuk_generate = 0;
    }

    // echo '</td>';
    // echo '</tr>';

  }
  // echo '</table>';

  if ($total_diambil < $dibutuhkan_awal){
    return 'tidak pas';
  } else {
    return 'true';
  }

}

function generate_satuan_terkecil($sisa_stok_terkecil_untuk_generate,$ket,$id_reff,$reff,$no_batch,$date_expired,$date_trans,$id_rak){

  $general_model = new App\Models\General_model;
  $session = session();
  $id_user = $session->get('id_user');

  $konversi1 = 1;
  $konversi2 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>2]);
  $konversi3 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>3]);
  $konversi4 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>4]);
  // if ((int)$ket===2){
  //   $konversi2 = 1;
  // } else if ((int)$ket===3) {
  //   $konversi2 = 1;
  //   $konversi3 = 1;
  // } else if ((int)$ket===4) {
  //   $konversi2 = 1;
  //   $konversi3 = 1;
  //   $konversi4 = 1;
  // }

  $q = $general_model->select_order('satuan','*',"id_reff = '".$id_reff."' AND table_reff = '".$reff."' AND ket < ".$ket."" ,'ket DESC');

  foreach ($q->getResult() as $row) {

    if ($row->ket=='1'){
      $stok_diberi = $sisa_stok_terkecil_untuk_generate/$konversi1;
    } else if ($row->ket=='2'){
      $stok_diberi = ((($sisa_stok_terkecil_untuk_generate/$konversi2)/$konversi1));
    } else if ($row->ket=='3'){
      $stok_diberi = ((($sisa_stok_terkecil_untuk_generate/$konversi3)/$konversi2)/$konversi1);
    } else if ($row->ket=='4'){
      $stok_diberi = ((($sisa_stok_terkecil_untuk_generate/$konversi4)/$konversi3)/$konversi2)/$konversi1;
    }

    $pembulatan_stok_diberi = round_down($stok_diberi,0);
    if ($row->ket=='1'){
      $stok_pembulatan = ((($stok_diberi*$konversi1)));
      $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi1)));
    } else if ($row->ket=='2'){
      $stok_pembulatan = ((($stok_diberi*$konversi2)*$konversi1));
      $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi2)*$konversi1));
    } else if ($row->ket=='3'){
      $stok_pembulatan = ((($stok_diberi*$konversi3)*$konversi2)*$konversi1);
      $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi3)*$konversi2)*$konversi1);
    } else if ($row->ket=='4'){
      $stok_pembulatan = ((($stok_diberi*$konversi4)*$konversi3)*$konversi2)*$konversi1;
      $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi4)*$konversi3)*$konversi2)*$konversi1;
    }

    $sisa_stok_terkecil_untuk_generate = $sisa_stok;

    $id_satuannya = get_id_satuan_item($id_reff,$reff,$row->ket);
    $satuan = get_satuan_item($id_reff,$reff,$row->ket);


    $check_di_rak_satuan1 = $general_model->count_rows('master_stok','*'," id_satuan='".$id_satuannya."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."'");
    if ($check_di_rak_satuan1 === 0) {
      $data_stok1 = [
        'qty'             =>  $pembulatan_stok_diberi,
        'id_satuan'       =>  $id_satuannya,
        'no_batch'        =>  $no_batch,
        'date_expired'    =>  $date_expired,
        'id_gudang_rak'   =>  $id_rak,
        'date_update'     =>  date('Y-m-d H:i:s'),
        'id_user_update'  =>  $id_user,
      ];
      $update_stock = $general_model->do_save('master_stok',$data_stok1);
    } else {
      $get_master_stok_satuan_ke_2 = $general_model->select_order_limit('master_stok','*'," id_satuan='".$id_satuannya."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' ",'qty DESC',1);
      foreach ($get_master_stok_satuan_ke_2->getResult() as $rowms2) {
        // $harga_beli = $general_model->get_data_by_field('satuan','harga_beli'," id_reff='".$id_reff."' AND table_reff='".$reff."' AND ket='".$row->ket."' ");
        // $condition_beri = " id_satuan='".$id_satuannya."' AND no_batch = '".$no_batch."' AND date_expired = '".$date_expired."' AND id_gudang_rak='".$rowms2->id_gudang_rak."'";
        $condition_beri = ['id'=>$rowms2->id];
        $check_di_rak_satuan1 = $general_model->count_rows('master_stok','*',$condition_beri);
        if ($check_di_rak_satuan1 > 0){
          $last_stock = $general_model->get_data_by_field('master_stok','qty',$condition_beri);
          $data_stok1 = [
            'qty'=>$pembulatan_stok_diberi+$last_stock,
            'date_update'=>date('Y-m-d H:i:s'),
            'id_user_update'=>$id_user,
          ];
          $update_stock = $general_model->do_update('master_stok',$data_stok1,['id'=>$rowms2->id]);
        } else {
          $data_stok1 = [
            'qty'             =>  $pembulatan_stok_diberi,
            'id_satuan'       =>  $id_satuannya,
            'no_batch'        =>  $no_batch,
            'date_expired'    =>  $date_expired,
            'id_gudang_rak'   =>  $rowms2->id_gudang_rak,
            'date_update'     =>  date('Y-m-d H:i:s'),
            'id_user_update'  =>  $id_user,
          ];
          $update_stock = $general_model->do_save('master_stok',$data_stok1);
        }
      }
    }
  }
}

function konversi_satuan_terkecil($id_reff,$reff,$ket,$stok){
  $general_model = new App\Models\General_model;
  $konversi1 = 1;
  $konversi2 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>2]);
  $konversi3 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>3]);
  $konversi4 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>4]);
  $hasil_konversi = 0;
  if ($ket=='1'){
    $hasil_konversi = ((($stok*$konversi1)));
  } else if ($ket=='2'){
    $hasil_konversi = ((($stok*$konversi2)*$konversi1));
  } else if ($ket=='3'){
    $hasil_konversi = ((($stok*$konversi3)*$konversi2)*$konversi1);
  } else if ($ket=='4'){
    $hasil_konversi = ((($stok*$konversi4)*$konversi3)*$konversi2)*$konversi1;
  }
  return $hasil_konversi;
}

function konversi_satuan_terbesar($id_reff,$reff,$ket,$stok){
  $general_model = new App\Models\General_model;
  $konversi1 = 1;
  $konversi2 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>2]);
  $konversi3 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>3]);
  $konversi4 = $general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>4]);

  if ($ket=='1'){
    $hasil_konversi = ((($stok/$konversi1)));
  } else if ($ket=='2'){
    $hasil_konversi = ((($stok/$konversi2)/$konversi1));
  } else if ($ket=='3'){
    $hasil_konversi = ((($stok/$konversi3)/$konversi2)/$konversi1);
  } else if ($ket=='4'){
    $hasil_konversi = ((($stok/$konversi4)/$konversi3)/$konversi2)/$konversi1;
  }
  return $hasil_konversi;
}

function stock_satuan_dibawah($id_reff,$reff,$no_batch,$date_expired,$ket){
  $general_model = new App\Models\General_model;
  $id_satuan1 = get_id_satuan_item($id_reff,$reff,1);
  $id_satuan2 = get_id_satuan_item($id_reff,$reff,2);
  $id_satuan3 = get_id_satuan_item($id_reff,$reff,3);
  $id_satuan4 = get_id_satuan_item($id_reff,$reff,4);
  $stok1 = $general_model->select_sum('master_stok','qty',['id_satuan'=>$id_satuan1,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
  $stok2 = $general_model->select_sum('master_stok','qty',['id_satuan'=>$id_satuan2,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
  $stok3 = $general_model->select_sum('master_stok','qty',['id_satuan'=>$id_satuan3,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
  $stok4 = $general_model->select_sum('master_stok','qty',['id_satuan'=>$id_satuan4,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);

  if ($ket == '4'){
    $stok = konversi_satuan_terkecil($id_reff,$reff,1,$stok1)+ konversi_satuan_terkecil($id_reff,$reff,2,$stok2) + konversi_satuan_terkecil($id_reff,$reff,3,$stok3);
  } else if ($ket == '3'){
    $stok = konversi_satuan_terkecil($id_reff,$reff,1,$stok1) + konversi_satuan_terkecil($id_reff,$reff,2,$stok2);
  } else if ($ket == '2'){
    $stok = konversi_satuan_terkecil($id_reff,$reff,1,$stok1);
  } else {
    $stok = 0;
  }
  return $stok;
}

function get_id_satuan_item($id_reff,$reff,$ket){
  $general_model = new App\Models\General_model;
  $hasil = $general_model->get_data_by_field("satuan",'id',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>$ket]);
  return $hasil;
}

function get_satuan_item($id_reff,$reff,$ket){
  $general_model = new App\Models\General_model;
  $hasil = $general_model->get_data_by_field("satuan",'satuan',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>$ket]);
  return $hasil;
}

function round_up($value, $places)
{
  $mult = pow(10, abs($places));
  return $places < 0 ?
  ceil($value / $mult) * $mult :
  ceil($value * $mult) / $mult;
}

function round_down($value, $precision) {
  $value = (float)$value;
  $precision = (int)$precision;
  if ($precision < 0) {
    $precision = 0;
  }
  $decPointPosition = strpos($value, '.');
  if ($decPointPosition === false) {
    return $value;
  }
  return (float)(substr($value, 0, $decPointPosition + $precision + 1));
}

function get_stock_before_date($id_reff,$table_reff,$no_batch,$date_expired,$tgl){
  $general_model = new App\Models\General_model;

  $con_no_batch1  = " AND pb.no_batch = '".$no_batch."'";
  $con_no_batch   = " AND po.no_batch = '".$no_batch."'";

  $con_expired1   = " AND pb.date_expired = '".$date_expired."'";
  $con_expired    = " AND po.date_expired = '".$date_expired."'";


  $con_tgl1 = " AND DATE(pb.date_come) < '".date('Y-m-d',strtotime($tgl))."' ";
  $con_tgl2 = " AND DATE(p.datetime) < '".date('Y-m-d',strtotime($tgl))."'  ";
  $con_tgl3 = " AND DATE(po.date_opname) < '".date('Y-m-d',strtotime($tgl))."'  ";


  $q = $general_model->do_query(" SELECT * FROM
    (

      SELECT
      pb.qty as masuk,0 as keluar,pd.satuan
      FROM po_barang_masuk as pb,po_detail as pd,".$table_reff." as o,po as p
      WHERE
      pb.id_po_detail = pd.id
      AND pb.id_item = o.id
      AND p.id = pd.id_po
      AND o.id = '".$id_reff."'
      AND pb.table_reff_item = '".$table_reff."'
      ".$con_no_batch1.$con_expired1.$con_tgl1."

      UNION

      SELECT
      0 as masuk,po.qty as keluar,po.satuan
      FROM penjualan_detail as po,penjualan as p,".$table_reff." as o,penjualan as pen
      WHERE p.id = po.id_penjualan
      AND po.id_item = o.id
      AND pen.id = po.id_penjualan
      AND o.id = '".$id_reff."'
      AND po.table_reff_item = '".$table_reff."'
      ".$con_no_batch.$con_expired.$con_tgl2."

      UNION

      SELECT
      po.masuk as masuk,po.keluar as keluar,po.satuan
      FROM stock_opname as po,obat as o
      WHERE po.id_item = o.id
      AND o.id = '".$id_reff."'
      AND po.table_reff_item = '".$table_reff."'
      ".$con_no_batch.$con_expired.$con_tgl3."

    ) AS x
    ");
    $stock_before = 0;
    foreach ($q->getResult() as $row) {
      $ket_satuan = $general_model->get_data_by_field('satuan','ket',array('satuan'=>$row->satuan,'id_reff'=>$id_reff,'table_reff'=>$table_reff));
      $konversi_masuk = konversi_satuan_terkecil($id_reff,$table_reff,$ket_satuan,$row->masuk);
      $konversi_keluar = konversi_satuan_terkecil($id_reff,$table_reff,$ket_satuan,$row->keluar);
      $stok_akhir = $stock_before + ( $konversi_masuk - $konversi_keluar );
      $stock_before = $stok_akhir;
    }
    return $stock_before;
}


;?>
