<?php

function generate_journal_hpp_by_id_penjualan_detail($id_penjualan_detail){
  $general_model = new App\Models\General_model;
  $id_penjualan = $general_model->get_data_by_field('penjualan_detail','id_penjualan',['id'=>$id_penjualan_detail]);
  $ket = $general_model->get_data_by_field('penjualan','no',['id'=>$id_penjualan]);
  $id_item = $general_model->get_data_by_field('penjualan_detail','id_item',['id'=>$id_penjualan_detail]);
  $qty = $general_model->get_data_by_field('penjualan_detail','qty',['id'=>$id_penjualan_detail]);
  $table_reff_item = $general_model->get_data_by_field('penjualan_detail','table_reff_item',['id'=>$id_penjualan_detail]);
  $date_expired = $general_model->get_data_by_field('penjualan_detail','date_expired',['id'=>$id_penjualan_detail]);
  $no_batch = $general_model->get_data_by_field('penjualan_detail','no_batch',['id'=>$id_penjualan_detail]);
  $satuan = $general_model->get_data_by_field('penjualan_detail','satuan',['id'=>$id_penjualan_detail]);

  generate_journal_hpp($id_item,$table_reff_item,$satuan,$no_batch,$date_expired);
}

function generate_journal_hpp($id_item,$table_reff_item,$satuan,$no_batch,$date_expired){
  $session = session();
  $id_user = $session->get('id_user');
  $general_model = new App\Models\General_model;
  $journal = new App\Libraries\Journal;
  $id_satuan = $general_model->get_data_by_field('satuan','id',['id_reff'=>$id_item,'table_reff'=>$table_reff_item,'satuan'=>$satuan]);
  $table = $table_reff_item;
  $nama_item = $general_model->get_data_by_field($table,'nama',['id'=>$id_item]);

  $con_item = " AND o.id = '%".$id_item."%'";
  $con_item = '';
  $con_satuan1 = " AND pd.satuan='".$satuan."'";
  $con_satuan2 = " AND po.satuan='".$satuan."'";
  $con_no_batch1 = " AND pb.no_batch='".$no_batch."'";
  $con_no_batch = " AND po.no_batch='".$no_batch."'";
  $con_expired1 = " AND pb.date_expired = '".date('Y-m-d',strtotime($date_expired))."'";
  $con_expired = " AND po.date_expired = '".date('Y-m-d',strtotime($date_expired))."'";
  // echo $date_expired;
  $q = $general_model->do_query(" SELECT * FROM
    (
      SELECT
      pb.id as x,o.nama AS item,p.no AS ket,pd.harga_beli as nominal,'po' as type,
      pb.qty as masuk,0 as keluar,pd.satuan,pb.date_come as date,pb.no_batch,
      pb.date_expired,pb.stock_awal,pb.last_stock,pb.date_insert,pb.id_user,pb.id,1 as for_order
      FROM po_barang_masuk as pb,po_detail as pd,".$table." as o,po as p
      WHERE
      pb.id_po_detail = pd.id
      AND pb.id_item = o.id
      AND p.id = pd.id_po
      AND pb.table_reff_item = '".$table."'
      ".$con_satuan1.$con_no_batch1.$con_expired1.$con_item."

      UNION

      SELECT
      po.id as x,o.nama AS item,pen.no as ket,po.hpp as nominal,'tr' as type,
      0 as masuk,po.qty as keluar,po.satuan,
      DATE(p.datetime) as date,
      po.no_batch,po.date_expired,
      po.stock_awal,
      po.last_stock,
      po.date_insert,po.id_user,po.id,4 as for_order
      FROM penjualan_detail as po,penjualan as p,".$table." as o,penjualan as pen
      WHERE p.id = po.id_penjualan
      AND po.id_item = o.id
      AND pen.id = po.id_penjualan
      AND po.table_reff_item = '".$table."'
      ".$con_satuan2.$con_no_batch.$con_expired.$con_item."

      UNION

      SELECT
      po.id as x,o.nama AS item,po.ket,po.harga_satuan as nominal,'op' as type,
      po.masuk as masuk,po.keluar as keluar,po.satuan,
      DATE(po.date_opname) as date,
      po.no_batch,po.date_expired,
      po.stock_awal,
      po.last_stock,
      po.date_insert,po.id_user,po.id,
      CASE
      WHEN po.masuk > 0 THEN 3
      ELSE 4
      END AS for_order
      FROM stock_opname as po,".$table." as o
      WHERE po.id_item = o.id
      AND po.table_reff_item = '".$table."'
      ".$con_satuan2.$con_no_batch.$con_expired.$con_item."

    ) AS x
    ORDER BY DATE(x.`date`) ASC,DATE(x.`date_insert`) ASC,TIME(x.`date_insert`) ASC,for_order ASC,stock_awal ASC
    ");
    $saldo = 0;
    $stock_before = 0;

    if ($table == 'obat'){
      $kode_rekening_hpp = $general_model->get_data_by_field('zacc_setting','value',['param'=>'hpp_obat']);
      $kode_persediaan = $general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_obat']);
    }
    if ($table == 'barang'){
      $kode_rekening_hpp = $general_model->get_data_by_field('zacc_setting','value',['param'=>'hpp_barang']);
      $kode_persediaan = $general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_barang']);
    }

    if ($kode_rekening_hpp == '' || $kode_persediaan == ''){

    } else {
      foreach($q->getResult() as $row){

        if ($row->type == 'tr'){
          $debet = 0;
          if ($stock_before === 0 || $saldo === 0){
            $credit = $row->nominal * $row->keluar;
          } else {
            $credit = ($saldo/$stock_before) * $row->keluar;
          }

          $check_id_journal = $general_model->get_data_by_field('penjualan_detail','id_journal_hpp',['id'=>$row->x]);
          if ($check_id_journal == '' OR $check_id_journal == null){

            $date_trans = date('Y-m-d',strtotime($row->date));
            $code_ref = 'HPP'.$table;
            $line = 1;
            $desc = 'HPP '.$row->ket.' '.$nama_item.' '.$no_batch.' exp '.$date_expired.' '.$row->keluar.' '.$row->satuan;
            $id_journal = $journal->generate_id_journal_manual($id_user,$date_trans,$code_ref,$kode_bb=null);
            if ($id_journal != '0'){
              $journal->generate_journal_detail($id_user,$id_journal,$kode_rekening_hpp,$kode_persediaan,$credit,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
              $general_model->do_update('penjualan_detail',['hpp'=>$credit,'id_journal_hpp'=>$id_journal],['id'=>$row->x]);
            }

          }
          $saldo = $saldo - $credit;

        } else if ( $row->type == 'op' && $row->ket == 'Opname') {
          $debet = 0;
          $credit = 0;
          if ($row->masuk > 0){
            if ($stock_before === 0 || $saldo === 0){
              $debet = $row->nominal * $row->masuk;
            } else {
              $harga_rata2 = ( $saldo/$stock_before );
              $debet =  $harga_rata2 * $row->masuk;
              $general_model->do_update('stock_opname',['harga_satuan'=>$harga_rata2],['id'=>$row->x]);
            }

            $check_id_journal = $general_model->get_data_by_field('stock_opname','id_journal_hpp',['id'=>$row->x]);
            if ($check_id_journal == '' || $check_id_journal == null){
              $date_trans = date('Y-m-d',strtotime($row->date));
              $code_ref = 'HPP'.$table;
              $line = 1;
              $desc = 'HPP '.$row->ket.' '.$nama_item.' '.$no_batch.' exp '.$date_expired.' bertambah  '.$row->masuk.' '.$row->satuan;
              $id_journal = $journal->generate_id_journal_manual($id_user,$date_trans,$code_ref,$kode_bb=null);
              if ($id_journal != '0'){
                $journal->generate_journal_detail($id_user,$id_journal,$kode_persediaan,$kode_rekening_hpp,$debet,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
                $general_model->do_update('stock_opname',['id_journal_hpp'=>$id_journal],['id'=>$row->x]);
              }
            }

            $credit = 0;
            $saldo = $saldo + $debet;
          }
          if ($row->keluar > 0){
            $debet = 0;
            $credit = ($saldo/$stock_before) * $row->keluar;

            $check_id_journal = $general_model->get_data_by_field('stock_opname','id_journal_hpp',['id'=>$row->x]);
            if ($check_id_journal == '' || $check_id_journal == null){
              $date_trans = date('Y-m-d',strtotime($row->date));
              $code_ref = 'HPP'.$table;
              $line = 1;
              $desc = 'HPP '.$row->ket.' '.$nama_item.' '.$no_batch.' exp '.$date_expired.' berkurang '.$row->keluar.' '.$row->satuan;
              $id_journal = $journal->generate_id_journal_manual($id_user,$date_trans,$code_ref,$kode_bb=null);
              if ($id_journal != '0'){
                $journal->generate_journal_detail($id_user,$id_journal,$kode_rekening_hpp,$kode_persediaan,$credit,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
                $general_model->do_update('stock_opname',['id_journal_hpp'=>$id_journal],['id'=>$row->x]);
              }
            }

            $saldo = $saldo - $credit;
          }

        } else {
          $debet = $credit = 0;
          if ($row->masuk>0){
            $debet = $row->nominal *  $row->masuk;
            $credit = 0;
            $saldo = $saldo + $debet;
          }
          if ($row->keluar>0){
            $debet = 0;
            $credit = $row->nominal * $row->keluar;
            $saldo = $saldo - $credit;
          }
        }

        // echo $row->item.' '.$row->type.' '.$row->masuk.' '.$row->keluar.' SB:'.$stock_before.' : '.$row->last_stock.' '.$debet.' '.$credit.' '.$saldo.' '.$row->ket.'<br>';
        $stock_before = $row->last_stock;
      }
    }


    // echo $q->getNumRows();

  }

// function sum_transaksi_sudah_hpp_from_journal_detail_by_code_accounting_and_periode($id_periode,$code,$project=null){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $tot_transaction = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if($project == ''){
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//       } else {
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//       }
//       $month            = $general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode));
//       $year             = $general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode));
//       $condition        = " MONTH(zacc_journal.`date_trans`) = '".$month."' AND YEAR(zacc_journal.`date_trans`) = '".$year."'  AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL AND zacc_journal_detail.`desc` LIKE 'HPP%'";
//       $sum_debet        = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
//       $sum_credit       = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
//       $tot_transaction  = 0;
//       if ( $row_balance == 'D' ){
//         $tot_transaction =  ( $sum_debet - $sum_credit );
//       }
//       if ( $row_balance == 'K' ){
//         $tot_transaction =  ( $sum_credit - $sum_debet );
//       }
//     }
//   }
//
//   return $tot_transaction;
// }
//
// function sum_transaksi_sudah_hpp_from_journal_detail_by_code_accounting_and_periode_by_condition($id_periode,$code,$project=null,$param_condition=null) {
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $tot_transaction = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if($project == ''){
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//       } else {
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//       }
//       $month = $general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode));
//       $year = $general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode));
//       $condition = " MONTH(zacc_journal.`date_trans`) = '".$month."' AND YEAR(zacc_journal.`date_trans`) = '".$year."'  AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL AND zacc_journal_detail.`desc` LIKE 'HPP%' ".$param_condition;
//       $sum_debet = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
//       $sum_credit = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
//       $tot_transaction = 0;
//       if ( $row_balance == 'D'){
//         $tot_transaction =  ( $sum_debet - $sum_credit );
//       }
//       if ( $row_balance == 'K'){
//         $tot_transaction =  ( $sum_credit - $sum_debet );
//       }
//     }
//   }
//
//   return $tot_transaction;
// }
//
// function sum_transaksi_from_journal_detail_by_code_accounting_and_periode($id_periode,$code,$project=null) {
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $tot_transaction = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if($project == ''){
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//       } else {
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//       }
//       $month = $general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode));
//       $year = $general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode));
//       $condition = " MONTH(zacc_journal.`date_trans`) = '".$month."' AND YEAR(zacc_journal.`date_trans`) = '".$year."'  AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
//       $sum_debet = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
//       $sum_credit = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
//       $tot_transaction = 0;
//       if ( $row_balance == 'D'){
//         $tot_transaction =  ( $sum_debet - $sum_credit );
//       }
//       if ( $row_balance == 'K'){
//         $tot_transaction =  ( $sum_credit - $sum_debet );
//       }
//     }
//   }
//   return $tot_transaction;
// }
//
// function sum_transaksi_from_journal_detail_by_code_accounting_and_periode_by_condition($id_periode,$code,$project=null,$param_condition=null) {
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $tot_transaction = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if($project == ''){
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//       } else {
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//       }
//       $month = $general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode));
//       $year = $general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode));
//       $condition = " MONTH(zacc_journal.`date_trans`) = '".$month."' AND YEAR(zacc_journal.`date_trans`) = '".$year."'  AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL ".$param_condition;
//       $sum_debet = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
//       $sum_credit = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
//       $tot_transaction = 0;
//       if ( $row_balance == 'D'){
//         $tot_transaction =  ( $sum_debet - $sum_credit );
//       }
//       if ( $row_balance == 'K'){
//         $tot_transaction =  ( $sum_credit - $sum_debet );
//       }
//     }
//   }
//
//   return $tot_transaction;
// }
//
// function sum_transaksi_from_journal_detail_by_code_accounting_and_periode_by_limit($id_periode,$code,$limit,$project) {
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $tot_transaction = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if($project == ''){
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//       } else {
//         $check_row_balance = $general_model->count_rows('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//         if ($check_row_balance >0){
//           $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//         } else {
//           $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//         }
//       }
//       $tot_transaction = 0;
//       if ( $row_balance == 'D'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_debet_by_limit($id_periode,$code,$limit,$project);
//       }
//       if ( $row_balance == 'K'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_credit_by_limit($id_periode,$code,$limit,$project);
//       }
//     }
//   }
//
//   return $tot_transaction;
// }
//
// function sum_transaksi_from_journal_detail_by_code_accounting_before_periode($id_periode,$code,$project){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $tot_transaction = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if($project == ''){
//         $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//       } else {
//         $check_row_balance = $general_model->count_rows('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//         if ($check_row_balance >0){
//           $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
//         } else {
//           $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
//         }
//       }
//       $tot_transaction = 0;
//       if ( $row_balance == 'D'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_debet_before_periode($id_periode,$code,$project);
//       }
//       if ( $row_balance == 'K'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_credit_before_periode($id_periode,$code,$project);
//       }
//     }
//   }
//   return $tot_transaction;
// }
//
// function sum_transaksi_from_journal_detail_by_pos_before_periode($id_periode,$code,$pos,$project){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $tot_transaction = 0;
//   $general_model = new App\Models\General_model;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if ( $pos == 'D'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_debet_before_periode($id_periode,$code,$project);
//       }
//       if ( $pos == 'K'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_credit_before_periode($id_periode,$code,$project);
//       }
//     }
//   }
//   return $tot_transaction;
// }
//
// function sum_transaksi_from_journal_detail_by_pos_after_periode($id_periode,$code,$pos,$project){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $tot_transaction      = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       if ( $pos == 'D'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_debet_after_periode($id_periode,$code,$project);
//       }
//       if ( $pos == 'K'){
//         $tot_transaction = $journal_detail_model->sum_balance_journal_credit_after_periode($id_periode,$code,$project);
//       }
//     }
//   }
//   return $tot_transaction;
// }
//
// function sum_balance_journal_debet_after_periode($id_periode,$code,$pos,$project){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $tot_transaction = 0;
//   if ( $pos == 'D'){
//     $tot_transaction = $journal_detail_model->sum_balance_journal_debet_after_periode($id_periode,$code,$project);
//   }
//   return $tot_transaction;
// }
//
// function sum_balance_journal_credit_after_periode($id_periode,$code,$pos,$project){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $tot_transaction = 0;
//   if ( $pos == 'K'){
//     $tot_transaction = $journal_detail_model->sum_balance_journal_credit_after_periode($id_periode,$code,$project);
//   }
//   return $tot_transaction;
// }
//
//
// function get_setting_income_by_division($condition){
//
//   $general_model = new App\Models\General_model;
//   $nominal = $general_model->get_data_by_field('zacc_income_division','value',$condition);
//   if ($nominal == ''){
//     return 0;
//   } else {
//     return $nominal;
//   }
// }
//
function get_bulan_by_id_periode($id_periode){
  $general_model = new App\Models\General_model;
  $month = $general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode));
  return $month;
}
//
function get_tahun_by_id_periode($id_periode){
  $general_model = new App\Models\General_model;
  $year = $general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode));
  return $year;
}

function get_id_periode($month,$year){

  $general_model = new App\Models\General_model;
  $year = $general_model->get_data_by_field('zacc_periode','id',array('month'=>$month,'year'=>$year));
  return $year;
}

function get_id_periode_bulan_lalu($bulan,$tahun){

  $general_model = new App\Models\General_model;
  if ($bulan == 1){
    $tahun_sebelumnya = $tahun-1;
    $bulan_sebelumnya = 12;
  } else {
    $tahun_sebelumnya = $tahun;
    $bulan_sebelumnya = ($bulan-1);
  }
  return $general_model->get_data_by_field('zacc_periode','id',array('month'=>$bulan_sebelumnya,'year'=>$tahun_sebelumnya));
}

function get_sum_journal_by_pos($month,$year,$code,$pos){
  $journal_detail_model = new App\Models\journal_detail_model;
  helper('code_accounting');
  $min_length = code_accounting_journal_min_length();
  if ( strlen($code) >= $min_length ){
    $awal_bulan = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
    $akhir_bulan = date('Y-m-t',strtotime($awal_bulan));
    // $condition    = "MONTH(zacc_journal.date_trans) = '".$month."' AND YEAR(zacc_journal.date_trans) = '".$year."' AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
    $condition    = " ( DATE(zacc_journal.date_trans) BETWEEN '".$awal_bulan."' AND '".$akhir_bulan."' ) AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
    if ($pos == 'D'){
      $sum    = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
    }
    if ($pos == 'K'){
      $sum    = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
    }
  } else {
    $sum = 0;
  }
  return $sum;
}

function sum_transaksi_from_journal_detail_by_code_accounting_and_two_date_by_limit($date_start,$date_end,$code,$limit,$project) {

  $general_model = new App\Models\General_model;
  $general_model = new App\Models\journal_detail_model;
  helper('code_accounting');
  $min_length = code_accounting_journal_min_length();
  if ( strlen($code) >= $min_length ){
    $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
    if ($check_code >0){
      if($project == ''){
        $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
      } else {
        $check_row_balance = $general_model->count_rows('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
        if ($check_row_balance >0){
          $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
        } else {
          $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
        }
      }
      $tot_transaction = 0;
      if ( $row_balance == 'D'){
        $tot_transaction = $journal_detail_model->sum_balance_journal_debet_by_two_date_by_limit($date_start,$date_end,$code,$limit,$project);
      }
      if ( $row_balance == 'K'){
        $tot_transaction = $journal_detail_model->sum_balance_journal_credit_by_two_date_by_limit($date_start,$date_end,$code,$limit,$project);
      }
    } else {
      $tot_transaction = 0;
    }
  } else {
    $tot_transaction = 0;
  }
  return $tot_transaction;
}

function sum_transaksi_from_journal_detail_by_code_accounting_and_two_date($date_start,$date_end,$code,$project=null) {

  $general_model = new App\Models\General_model;

  $tot_transaction = 0;
  helper('code_accounting_helper');
  $min_length = code_accounting_journal_min_length();
  if ( strlen($code) >= $min_length ){
    $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
    if ($check_code >0){
      if ($date_end =='' OR $date_start == ''){

      } else {
        $journal_detail_model = new App\Models\journal_detail_model;
        if($project == ''){
          $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code));
        } else {
          $row_balance = $general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code,'project'=>$project));
        }
        $date_start       = date('Y-m-d',strtotime($date_start));
        $date_end         = date('Y-m-d',strtotime($date_end));
        $condition        = " DATE(zacc_journal.`date_trans`) >= '".$date_start."' AND DATE(zacc_journal.`date_trans`) <= '".$date_end."'  AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
        $sum_debet        = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
        $sum_credit       = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
        if ( $row_balance == 'D'){
          $tot_transaction =  ( $sum_debet - $sum_credit );
        }
        if ( $row_balance == 'K'){
          $tot_transaction =  ( $sum_credit - $sum_debet );
        }
      }
    }
  }

  return $tot_transaction;
}
//
// function get_sum_journal_by_pos_before_laba_rugi_periode($month,$year,$code,$pos){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   $sum      = 0;
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       $type_report  = $general_model->get_data_by_field('zacc_code_accounting','type_report',array('code'=>$code));
//       if ($type_report == 'LR'){
//         $condition_laba_periode = "AND zacc_journal_detail.desc != 'LABA RUGI PERIODE'";
//       } else {
//         $condition_laba_periode = '';
//       }
//       $condition    = "
//       MONTH(zacc_journal.date_trans) = '".$month."'
//       AND YEAR(zacc_journal.date_trans) = '".$year."'
//       AND zacc_journal_detail.code_accounting = '".$code."'
//       AND zacc_journal.status IS NULL
//       AND zacc_journal_detail.status IS NULL
//       ".$condition_laba_periode;
//
//       if ($pos == 'D'){
//         $sum    = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
//       }
//       if ($pos == 'K'){
//         $sum    = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
//       }
//     }
//   }
//
//   return $sum;
// }
//
// function get_sum_journal_by_pos_before_laba_rugi_periode_between_two_date($date1,$date2,$code,$pos){
//
//   $ci->load->model('accounting/journal_detail_model');
//   $general_model = new App\Models\General_model;
//   $sum      = 0;
//   $ci->load->helper('code_accounting_helper');
//   $min_length = code_accounting_journal_min_length();
//   if ( strlen($code) >= $min_length ){
//     $check_code = $general_model->count_rows('zacc_code_accounting','id',"code = '".$code."' AND status IS NULL");
//     if ($check_code >0){
//       $type_report  = $general_model->get_data_by_field('zacc_code_accounting','type_report',array('code'=>$code));
//       if ($type_report == 'LR'){
//         $condition_laba_periode = "AND zacc_journal_detail.desc != 'LABA RUGI PERIODE'";
//       } else {
//         $condition_laba_periode = '';
//       }
//       $condition    = "
//       DATE(zacc_journal.date_trans) >= '".$date1."'
//       AND DATE(zacc_journal.date_trans) <= '".$date2."'
//       AND zacc_journal_detail.code_accounting = '".$code."'
//       AND zacc_journal.status IS NULL
//       AND zacc_journal_detail.status IS NULL
//       ".$condition_laba_periode;
//
//       if ($pos == 'D'){
//         $sum    = $journal_detail_model->sum_balance_journal('zacc_journal_detail.debet',$condition);
//       }
//       if ($pos == 'K'){
//         $sum    = $journal_detail_model->sum_balance_journal('zacc_journal_detail.credit',$condition);
//       }
//     }
//   }
//   return $sum;
// }
//
// function cek_buku_besar_open($month,$year){
//
//   $general_model = new App\Models\General_model;
//   $cek_buku_besar_open = $general_model->count_rows('zacc_periode','id',array('closing'=>1,'month'=>$month,'year'=>$year));
//   if ( $cek_buku_besar_open > 0 ){
//     return true;
//   } else {
//     return false;
//   }
// }
//
// function generate_no_gl_journal(){
//
//     $general_model = new App\Models\General_model;
//     $ci->load->model('accounting/journal_model');
//     $ci->id_user    = $ci->session->userdata('id_user');
//     $ci->username   = $general_model->get_data_by_field('pengguna','nama_pengguna',array('id_pengguna'=>$ci->id_user));
//     $current_month  = date('m');
//     $current_year   = date('Y');
//     $check_periode  = $general_model->count_rows('zacc_periode','id',array('month'=>$current_month,'year'=>$current_year ));
//     if ($check_periode === 0){
//       $data_periode = array(
//         'month' =>  $current_month,
//         'year'  =>  $current_year
//       );
//       $ci->periode_model->save($data_periode);
//     }
//     $count_journal = $general_model->count_rows('zacc_journal','id',array('id_user'=>$ci->id_user));
//     if ($count_journal > 0){
//       $last_no_trans  = $ci->journal_model->last_field('no_trans','id DESC');
//       $explode        = explode('-', $last_no_trans);
//       $no_trans       =  $count_journal + 1;
//       $full_no_trans  = strtoupper($ci->username).str_pad(($no_trans), 4, '0', STR_PAD_LEFT);
//     } else {
//       $full_no_trans  = strtoupper($ci->username).'0001';
//     }
//     return $full_no_trans;
// }
//
// function get_batas_waktu_journal(){
//   $date = date('Y-m-d');
//   $get_day = date('D',strtotime($date));
//   if ( $get_day == 'Fri' ){
//     return date('Y-m-d',strtotime($date.' -5 DAYS'));
//   } else if ( $get_day == 'Sat' ){
//     return date('Y-m-d',strtotime($date.' -6 DAYS'));
//   } else if ( $get_day == 'Sun' ){
//     return date('Y-m-d',strtotime($date.' +0 DAYS'));
//   } else if ( $get_day == 'Mon' ){
//     return date('Y-m-d',strtotime($date.' -1 DAYS'));
//   } else if ( $get_day == 'Tue' ){
//     return date('Y-m-d',strtotime($date.' -2 DAYS'));
//   } else if ( $get_day == 'Wed' ){
//     return date('Y-m-d',strtotime($date.' -3 DAYS'));
//   } else if ( $get_day == 'Thu' ){
//     return date('Y-m-d',strtotime($date.' -4 DAYS'));
//   }
// }
//
//
// function get_batas_waktu_input_journal(){
//   $day = date('d');
//   $date = date('Y-m-d');
//   $get_day = date('D',strtotime($date));
//   if ( $get_day == 'Fri' ){
//     return '-5d';
//   } else if ( $get_day == 'Sat' ){
//     return '-6d';
//   } else if ( $get_day == 'Sun' ){
//     return 'd';
//   } else if ( $get_day == 'Mon' ){
//     return '-1d';
//   } else if ( $get_day == 'Tue' ){
//     return '-2d';
//   } else if ( $get_day == 'Wed' ){
//     return '-3d';
//   } else if ( $get_day == 'Thu' ){
//     return '-4d';
//   }
// }


?>
