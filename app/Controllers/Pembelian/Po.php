<?php
namespace App\Controllers\Pembelian;
use App\Controllers\BaseController;
use App\Models\General_model;

class Po extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    $this->datetime = date('Y-m-d H:i:s');
    helper("general");
    helper("po");
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=[
      'supplier'=>$this->general_model->select_order('supplier','*','id > 0','nama ASC'),
    ];
    return $this->template->pembelian("po", $data);
  }

  public function get_data(){
    $param = $this->request->getPost('param');
    $search = $this->request->getPost('search');
    $date = $this->request->getPost('date');
    $supplier = $this->request->getPost('supplier');

    if ($search == '' || $search == null){
      $con_search = '';
    } else {
      $con_search = " AND ( no LIKE '%".$search."%' OR faktur LIKE '%".$search."%' )";
    }

    if ($supplier == '' || $supplier == null){
      $con_supplier = '';
    } else {
      $con_supplier = " AND id_supplier = '".$supplier."'";
    }

    if ($date != '' OR $date != NULL){
			$explode_tanggal=explode('-', $date);
			$condition_date =" AND DATE(datetime) >= '".date('Y-m-d',strtotime($explode_tanggal[0]))."' AND DATE(datetime) <= '".date('Y-m-d',strtotime($explode_tanggal[1]))."'";
		} else {
			$condition_date = "  ";
		}

    $col_hutang = '';
    if ($param == 'new'){
      $con_status = " AND ( status_barang != 'OK' OR status_barang is null) ".$con_search.$con_supplier.$condition_date;
    } else if ($param == 'prosesbayar'){
      $con_status = " AND ( status_bayar != 'L' OR status_bayar IS NULL ) ".$con_search.$con_supplier.$condition_date;
      $col_hutang = '<th class="text-right"><span class="text-danger pl-3">HUTANG</span></th>';
    } else if ($param == 'done'){
      $con_status = " AND ( status_bayar = 'L' OR status_bayar = 'OK' ) ".$con_search.$con_supplier.$condition_date;
    } else {
      $con_status = "  ";
    }
    // echo $con_status;
    $q = $this->general_model->select_order('po','*',' status IS NULL '.$con_status,"no ASC");
    $html = '<table id="basic-datatables" class="display table table-striped table-hover" >
    <thead>
    <tr>
    <th>Tanggal</th>
    <th>Nomor</th>
    <th>Faktur</th>
    <th>Supplier</th>
    <th>Nilai Tagihan</th>
    <th>Tagihan Terbayar</th>
    '.$col_hutang.'
    <th>Detail</th>
    <th></th>
    </tr>
    </thead>
    ';
    $html .= '<tbody>';
    $total_harga_kesepakatan = 0;
    $total_pembayaran = 0;
    $total_pembayaran = 0;
    $total_hutang = 0;
    foreach ($q->getResult() as $row) {
      $supplier = $this->general_model->get_data_by_field('supplier','nama',['id'=>$row->id_supplier]);
      $total_pembayaran = $this->general_model->select_sum('pembayaran','value',['id_reff'=>$row->id,'table_reff'=>'po','status'=>1]);
      $harga_kesepakatan = get_harga_kesepakatan($row->id);
      if ($row->harga_kesepakatan <> $row->harga_kesepakatan){
        $this->general_model->do_update('po',['harga_kesepakatan'=>$harga_kesepakatan],['id'=>$row->id]);
      }
      $hutang = $harga_kesepakatan - $total_pembayaran;
      if ($hutang>0){
        $this->general_model->do_update('po',['status_bayar'=>NULL],['id'=>$row->id]);
      }
      $col_hutang2 = '';
      if ($param == 'prosesbayar'){
        $col_hutang2 = '<th class="text-right"><span class="text-danger pl-3">'.number_format($hutang,2,',','.').'</span></th>';
        $total_hutang = $total_hutang + $hutang;
      }
      $html .= '
      <tr>
      <th>'.date('d/m/Y',strtotime($row->datetime)).'</th>
      <th>'.$row->no.'</th>
      <th>'.$row->faktur.'</th>
      <td>'.$supplier.'</td>
      <td class="text-right">'.number_format($harga_kesepakatan,2,',','.').'</td>
      <td class="text-right">'.number_format($total_pembayaran,2,',','.').'</td>
      '.$col_hutang2.'
      <td><a class="btn btn-info btn-xs btn-border btn-round" onClick="modal_detail_po('.$row->id.')"><b>DETAIL</b></a></td>
      <td><button class="btn btn-primary btn-xs btn-round" onClick="modal_bayar_po('.$row->id.')"><b>BAYAR</b></button></td>
      </tr>';
      $total_harga_kesepakatan = $total_harga_kesepakatan + $harga_kesepakatan;
      $total_pembayaran = $total_pembayaran + $total_pembayaran;
    }
    $html .= '</tbody>';

    $col_hutang3 = '';
    if ($param == 'prosesbayar'){
      $col_hutang3 = '<th class="text-right"><span class="text-danger pl-3">'.number_format($total_hutang,2,',','.').'</span></th>';
    }

    $html .= '<tfoot>
    <tr>
    <th></th>
    <th></th>
    <th></th>
    <th>Total</th>
    <th class="text-right">'.number_format($total_harga_kesepakatan,2,',','.').'</th>
    <th class="text-right">'.number_format($total_pembayaran,2,',','.').'</th>
    '.$col_hutang3.'
    <th></th>
    <th></th>
    </tr>
    </tfoot>';
    $html .= '</table>';

    echo $html;

  }

  public function count_po($param){
    if ( $param == "new"){
      $con_status = " AND ( status_barang != 'OK' OR status_barang is null) ";
    } else if ($param == 'prosesbayar'){
      $con_status = " AND ( status_bayar != 'L' OR status_bayar IS NULL ) ";
    } else if ($param == 'done'){
      $con_status = " AND status_bayar = 'L' AND status_barang = 'OK' ";
    } else {
      $con_status = "  ";
    }
    $q = $this->general_model->count_rows('po','id'," status IS NULL ".$con_status,"no ASC");
    echo $q;
  }

  public function action_create_po(){
    helper(['form', 'url']);
    $input = $this->validate([
      'supplier' => for_validate('Supplier','required|numeric'),
      'diskon' => for_validate('Diskon','required|numeric'),
      'ppn' => for_validate('PPN','required|numeric'),
      'date_trans' => for_validate('Tanggal Pembelian','required'),
      'faktur' => for_validate('Faktur','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $supplier = $this->request->getPost('supplier');
      $diskon = $this->request->getPost('diskon');
      $date_trans = $this->request->getPost('date_trans');
      $date_trans = date('Y-m-d',strtotime($date_trans));
      $ppn = $this->request->getPost('ppn');
      $faktur = $this->request->getPost('faktur');
      $count = $this->general_model->count_rows('po','id'," id>0");
      $no = ($count+1).'/PO/'.date('m/Y');

      $this->db->transBegin();
      $data = [
        'faktur'=>$faktur,
        'id_supplier'=>$supplier,
        'diskon'=>$diskon,
        'ppn'=>$ppn,
        'no'=>$no,
        'datetime'=>date('Y-m-d H:i:s',strtotime($date_trans)),
        'id_user'=>$this->id_user,
      ];
      $this->general_model->do_save('po',$data);
      $id_po = $this->general_model->insert_id();

      $q = $this->general_model->select_data('po_detail','*'," status = '1' AND harga_beli > 0 AND id_po IS NULL ");
      $totalharga = 0;
      $cek_gudang = 0;
      $cek_satuan = 0;
      foreach ($q->getResult() as $row) {
        $subtotal = $row->harga_beli*$row->qty;
        if ($row->persen_diskon == '' || $row->persen_diskon == null){
          $persen_diskon = 0;
        } else {
          $persen_diskon = $row->persen_diskon;
        }
        $nominal_diskon = $row->diskon;
        // $nominal_diskon = $subtotal * ($persen_diskon/100);
        $total = $subtotal-$nominal_diskon;
        $totalharga += $total;
        $this->general_model->do_update('po_detail',['id_po'=>$id_po],['id'=>$row->id]);

        $persen_harga_jual = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id_reff'=>$row->id_item,'table_reff'=>$row->table_reff_item,'satuan'=>$row->satuan]);
        $persen_harga_jual = $persen_harga_jual == '' ? 0 : $persen_harga_jual;
        $tambahan_harga_jual = $persen_harga_jual/100 * $row->harga_beli;
        $new_harga_jual = $row->harga_beli + $tambahan_harga_jual;
        $update_harga_beli = $this->general_model->do_update('satuan',['harga_beli'=>$row->harga_beli,'harga_jual'=>$new_harga_jual],['id_reff'=>$row->id_item,'table_reff'=>$row->table_reff_item,'satuan'=>$row->satuan]);
        $update_po_datang = $this->general_model->do_update('po_barang_masuk',['date_come'=>$date_trans],['id_po_detail'=>$row->id,'status'=>1]);

        $update_table_reff = $this->general_model->do_update($row->table_reff_item,['date_update'=>date('Y-m-d H:i:s')],['id'=>$row->id_item]);

        $q_po_barang_masuk = $this->general_model->select_order_limit('po_barang_masuk','*',['id_po_detail'=>$row->id,'status'=>1],'id ASC',1);
        foreach ($q_po_barang_masuk->getResult() as $rbm) {
          if ((int)$rbm->id_gudang_rak > 0){
            $this->hitung_stock_akhir($row->id,$rbm->qty,$rbm->date_come,$rbm->id_gudang_rak,$rbm->no_batch,$rbm->date_expired);
          } else {
            $cek_gudang = $cek_gudang + 1;
          }
        }
        if ($row->satuan == '' || $row->satuan == null){
          $cek_satuan = $cek_satuan + 1;
        }

      }

      if ($totalharga <= 0 ){
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, Total harga 0'));
      } else if ($cek_gudang > 0 ){
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'Ada item belum di setting lokasinya'));
      } else if ($cek_satuan > 0 ){
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'Ada item belum di setting satuanya'));
      } else {
        $nominal_diskon2 = $totalharga * ($diskon/100);
        $harga_net = $totalharga - $nominal_diskon2;
        $nominal_ppn = $harga_net * ($ppn/100);
        $harga_kesepakatan = $harga_net + $nominal_ppn;
        $this->general_model->do_update('po',['harga_kesepakatan'=>$harga_kesepakatan,'status_barang'=>'OK'],['id'=>$id_po]);
        if ($this->db->transStatus() === false) {
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
        } else {
          $this->db->transCommit();
          echo json_encode(array('response'=>TRUE,'message'=>'Berhasil, HNA juga diupdate'));
        }
      }
    }
  }

  public function bayar_po(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_po' => for_validate('PO','required|numeric'),
      'nominal' => for_validate('Nominal','required|numeric'),
      'tanggal' => for_validate('tanggal','required'),
      'code_bayar' => for_validate('code bayar','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_po = $this->request->getPost('id_po');
      $nominal = $this->request->getPost('nominal');
      $tanggal = $this->request->getPost('tanggal');
      $code_accounting = $this->request->getPost('code_bayar');
      $harga_kesepakatan = $this->general_model->get_data_by_field('po','harga_kesepakatan',['id'=>$id_po]);
      if ($harga_kesepakatan > 0){
        $this->db->transBegin();

        $data = [
          'id_reff'=>$id_po,
          'table_reff'=>'po',
          'value'=>$nominal,
          'code_accounting'=>$code_accounting,
          'tgl_bayar'=>date('Y-m-d',strtotime($tanggal)),
          'id_user'=>$this->id_user,
          'date_insert'=>date('Y-m-d H:i:s'),
          'status'=>1,
        ];
        $this->general_model->do_save('pembayaran',$data);

        $harga_kesepakatan = $this->general_model->get_data_by_field('po','harga_kesepakatan',['id'=>$id_po]);
        $cek_total_pembayaran = $this->general_model->select_sum('pembayaran','value',['id_reff'=>$id_po,'table_reff'=>'po','status'=>1]);
        if ($cek_total_pembayaran>=$harga_kesepakatan){
          $this->general_model->do_update('po',['status_bayar'=>'L'],['id'=>$id_po]);
        }
        if ($this->db->transStatus() === false) {
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
        } else {
          $this->db->transCommit();
          echo json_encode(array('response'=>TRUE,'message'=>'OK'));
        }
      } else {
        echo json_encode(array('response'=>FALSE,'message'=>'Tagihan PO 0'));
      }

    }
  }

  public function detail_po(){
    $id_po = $this->request->getGet('id_po');
    $q = $this->general_model->select_data('po_detail','*'," status = '1' AND id_po = '".$id_po."' ");

    $diskon = $this->general_model->get_data_by_field('po','diskon'," id = '".$id_po."' ");
    $ppn = $this->general_model->get_data_by_field('po','ppn'," id = '".$id_po."' ");
    $diskon = $diskon == ''  ? 0 : $diskon;
    $ppn = $ppn == ''  ? 0 : $ppn;

    $html = '<table class="table table-bordered">';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .= '<th>#</th>';
    $html .= '<th>Nama Item</th>';
    $html .= '<th>Spesifikasi</th>';
    // $html .= '<th>Jumlah Pesan</th>';
    $html .= '<th>Jumlah Diterima</th>';
    $html .= '<th>Satuan</th>';
    $html .= '<th>Harga Satuan (Rp)</th>';
    $html .= '<th>Subtotal (Rp)</th>';
    $html .= '<th>Diskon (%)</th>';
    $html .= '<th>Total (Rp)</th>';
    // $html .= '<th>#</th>';
    $html .= '</tr>';
    $html .= '</thead>';

    $no = 1;
    $totalharga = 0;
    $not_done = 0;
    $cek_total_qty_come=0;
    foreach ($q->getResult() as $row) {
      if($row->table_reff_item == 'obat'){
        $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
        $spesifikasi = '';
      } else if($row->table_reff_item == 'barang'){
        $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
        $aspesifikasi = $this->general_model->get_data_by_field('barang','spesifikasi',['id'=>$row->id_item]);
        $merek = $this->general_model->get_data_by_field('barang','merek',['id'=>$row->id_item]);
        $spesifikasi = $merek.' - '.$aspesifikasi;
      } else {
        $nama_item = '';
        $spesifikasi = '';
      }

      $qty_come = $this->general_model->select_sum('po_barang_masuk','qty',['id_po_detail'=>$row->id,'status'=>1]);
      $qty_come = $qty_come == '' ? 0 : $qty_come;
      $cek_total_qty_come=$cek_total_qty_come+$qty_come;
      if ($qty_come < $row->qty){
        $not_done = 1;
      }

      if ($qty_come > 0 ){
        $label_come = '<button class="btn btn-xs btn-success btn-round" onClick="modal_terima_barang('.$row->id.')"><b>'.$qty_come.'</b></button>';
      } else {
        $label_come = '<button class="btn btn-xs btn-danger btn-round" onClick="modal_terima_barang('.$row->id.')"><b>0</b></button>';
      }

      $param_get_satuan = "'".$row->id."','".$row->table_reff_item."'";
      if ($qty_come > 0){
        $subtotal = $row->harga_beli*$qty_come;
      } else {
        $subtotal = $row->harga_beli*$row->qty;
      }

      if ($row->persen_diskon>0){
        // $persen_diskon = $row->persen_diskon;
        // $nominal_diskon = $subtotal * ($persen_diskon/100);
        // $total = $subtotal-$nominal_diskon;
      } else {
        $persen_diskon = 0;
        // $total = $subtotal;
      }
      $nominal_diskon = $row->diskon == '' ? 0 : $row->diskon;
      $total = $subtotal-$nominal_diskon;

      $html .= '<tr style="padding:0 5px !important;height:40px;">';
      $html .= '<td style="padding:0 5px !important;height:40px;" class="text-center">'.$no.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$nama_item.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$spesifikasi.'</td>';
      // $html .= '<td style="padding:0 5px !important;height:40px;" class="text-center">'.$row->qty.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;" class="text-center">'.$label_come.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$row->satuan.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$row->harga_beli.'</td>';
      $html .= '<td id="subtotalharga'.$row->id.'" class="text-right">'.number_format($subtotal,2,',','.').'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.number_format($row->diskon,2,',','.').'</td>';
      $html .= '<td id="total'.$row->id.'" class="text-right">'.number_format($total,2,',','.').'</td>';
      // $html .= '<td><button class="btn btn-primary btn-round btn-xs" onClick="modal_terima_barang('.$row->id.')"><b>INPUT TERIMA BARANG</b></button></td>';
      $html .= '</tr>';
      $no++;
      $totalharga += $total;
    }
    if($diskon > 0){
      $nominal_diskon2 = $totalharga * ($diskon/100);
    } else {
      $nominal_diskon2 = 0;
    }
    $harga_net = $totalharga - $nominal_diskon2;
    $nominal_ppn = $harga_net * ($ppn/100);
    $harga_kesepakatan = $harga_net + $nominal_ppn;

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="8" class="text-center">TOTAL : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($totalharga,2,',','.').'</th>';
    // $html .= '<th></th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="8" class="text-center">DISKON '.$diskon.'%: </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($nominal_diskon2,2,',','.').'</th>';
    // $html .= '<th></th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="8" class="text-center">HARGA NET : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($harga_net,2,',','.').'</th>';
    // $html .= '<th></th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="8" class="text-center">PPN '.$ppn.'% : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($nominal_ppn,2,',','.').'</th>';
    // $html .= '<th></th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="8" class="text-center">HARGA KESEPAKATAN : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($harga_kesepakatan,2,',','.').'</th>';
    // $html .= '<th></th>';
    $html .= '</tr>';

    $html .= '</table>';
    $this->general_model->do_update('po',['harga_kesepakatan'=>$harga_kesepakatan],['id'=>$id_po]);
    if ($not_done > 0){
      $this->general_model->do_update('po',['status_barang'=>'P'],['id'=>$id_po]);
    }
    if ($not_done === 0){
      $this->general_model->do_update('po',['status_barang'=>'OK'],['id'=>$id_po]);
    }
    if ($cek_total_qty_come===0){
      $this->general_model->do_update('po',['status_barang'=>NULL],['id'=>$id_po]);
    }
    echo $html;
  }

  public function desc_item(){
    $id_po_detail = $this->request->getGet('id_po_detail');
    $table_reff_item = $this->general_model->get_data_by_field('po_detail','table_reff_item',['id'=>$id_po_detail]);
    $id_item = $this->general_model->get_data_by_field('po_detail','id_item',['id'=>$id_po_detail]);
    if($table_reff_item == 'obat'){
      $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$id_item]);
      $spesifikasi = '';
    } else if($table_reff_item == 'barang'){
      $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$id_item]);
      $aspesifikasi = $this->general_model->get_data_by_field('barang','spesifikasi',['id'=>$id_item]);
      $merek = $this->general_model->get_data_by_field('barang','merek',['id'=>$id_item]);
      $spesifikasi = $merek.' - '.$aspesifikasi;
    } else {
      $nama_item = '';
      $spesifikasi = '';
    }
    $html = '<table class="table table_bordered">';
    $html .= '<tr>';
    $html .= '<td>'.$nama_item.'</td>';
    $html .= '<td>'.$spesifikasi.'</td>';
    $html .= '</tr>';
    $html .= '</table>';
    echo $html;
  }

  public function hitung_stock_akhir($id_po_detail,$jumlah,$tanggal,$rak,$no_batch,$tanggal_expired){

    if ($tanggal_expired == ''){
      $date_expired = '';
    } else {
      $date_expired = date('Y-m-d',strtotime($tanggal_expired));
    }
    $new_stock = null;
    $old_stok = 0;
    if ($rak == ''){
      $rak = null;
    } else {
      $satuan = $this->general_model->get_data_by_field('po_detail','satuan',['id'=>$id_po_detail]);
      $table_reff_item = $this->general_model->get_data_by_field('po_detail','table_reff_item',['id'=>$id_po_detail]);
      $id_item = $this->general_model->get_data_by_field('po_detail','id_item',['id'=>$id_po_detail]);
      $id_satuan = $this->general_model->get_data_by_field("satuan",'id',['id_reff'=>$id_item,'table_reff'=>$table_reff_item,'satuan'=>$satuan]);
      $check_data_stok = $this->general_model->count_rows('master_stok','id',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
      if ($check_data_stok > 0){
        $old_stok = $this->general_model->get_data_by_field('master_stok','qty',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
        $new_stock =$old_stok+$jumlah;
        $data_stok =[
          'qty'=>$new_stock,
          'date_update'=>date('Y-m-d H:i:s'),
          'id_user_update'=>$this->id_user,
        ];
        $this->general_model->do_update('master_stok',$data_stok,['id_satuan'=>$id_satuan,'id_gudang_rak'=>$rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);

      } else {
        $new_stock = $jumlah;
        $data_stok =[
          'id_satuan'=>$id_satuan,
          'id_gudang_rak'=>$rak,
          'qty'=>$jumlah,
          'no_batch'=>strtoupper($no_batch),
          'date_expired'=>$date_expired,
          'date_update'=>date('Y-m-d H:i:s'),
          'id_user_update'=>$this->id_user,
        ];
        $this->general_model->do_save('master_stok',$data_stok);
      }
    }
    $id_item = $this->general_model->get_data_by_field('po_detail','id_item',['id'=>$id_po_detail]);
    $table_reff_item = $this->general_model->get_data_by_field('po_detail','table_reff_item',['id'=>$id_po_detail]);

    $id_po = $this->general_model->get_data_by_field('po_detail','id_po',['id'=>$id_po_detail]);

    $condition_2 = [
      'id_po_detail'=>$id_po_detail,
      'id_item'=>$id_item,
      'table_reff_item'=>$table_reff_item,
      'qty'=>$jumlah,
      'no_batch'=>strtoupper($no_batch),
      'date_expired'=>$date_expired,
      'date_come'=>date('Y-m-d',strtotime($tanggal)),
      'status'=>1,
      'id_gudang_rak'=>$rak,
    ];

    $data_2 = [
      'stock_awal'=>$old_stok,
      'last_stock'=>$new_stock,
    ];
    $this->general_model->do_update('po_barang_masuk',$data_2,$condition_2);

    $q = $this->general_model->select_data('po_detail','*'," status = '1' AND id_po = '".$id_po."' ");
    $not_done = 0;
    foreach ($q->getResult() as $row) {
      $qty_come = $this->general_model->select_sum('po_barang_masuk','qty',['id_po_detail'=>$row->id,'status'=>1]);
      if ($qty_come > 0){
        if ($qty_come < $row->qty){
          $not_done = 1;
        }
      }
    }
    if ($not_done > 0){
      $this->general_model->do_update('po',['status_barang'=>'P'],['id'=>$id_po]);
    }
  }

  public function terima_barang(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_po_detail' => for_validate('PO Detail','required|numeric'),
      'jumlah' => for_validate('jumlah','required|numeric'),
      'tanggal' => for_validate('tanggal','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $this->db->transBegin();
      $id_po_detail = $this->request->getPost('id_po_detail');
      $jumlah = $this->request->getPost('jumlah');
      $tanggal = $this->request->getPost('tanggal');
      $rak = $this->request->getPost('rak');
      $no_batch = $this->request->getPost('no_batch');
      $tanggal_expired = $this->request->getPost('tanggal_expired');
      if ($tanggal_expired == ''){
        $date_expired = '';
      } else {
        $date_expired = date('Y-m-d',strtotime($tanggal_expired));
      }
      $new_stock = null;
      $old_stok = 0;
      if ($rak == ''){
        $rak = null;
      } else {
        $satuan = $this->general_model->get_data_by_field('po_detail','satuan',['id'=>$id_po_detail]);
        $table_reff_item = $this->general_model->get_data_by_field('po_detail','table_reff_item',['id'=>$id_po_detail]);
        $id_item = $this->general_model->get_data_by_field('po_detail','id_item',['id'=>$id_po_detail]);
        $id_satuan = $this->general_model->get_data_by_field("satuan",'id',['id_reff'=>$id_item,'table_reff'=>$table_reff_item,'satuan'=>$satuan]);
        $check_data_stok = $this->general_model->count_rows('master_stok','id',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
        if ($check_data_stok > 0){
          $old_stok = $this->general_model->get_data_by_field('master_stok','qty',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
          $new_stock =$old_stok+$jumlah;
          $data_stok =[
            'qty'=>$new_stock,
            'date_update'=>date('Y-m-d H:i:s'),
            'id_user_update'=>$this->id_user,
          ];
          $this->general_model->do_update('master_stok',$data_stok,['id_satuan'=>$id_satuan,'id_gudang_rak'=>$rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);

        } else {
          $new_stock = $jumlah;
          $data_stok =[
            'id_satuan'=>$id_satuan,
            'id_gudang_rak'=>$rak,
            'qty'=>$jumlah,
            'no_batch'=>strtoupper($no_batch),
            'date_expired'=>$date_expired,
            'date_update'=>date('Y-m-d H:i:s'),
            'id_user_update'=>$this->id_user,
          ];
          $this->general_model->do_save('master_stok',$data_stok);
        }
      }
      $id_item = $this->general_model->get_data_by_field('po_detail','id_item',['id'=>$id_po_detail]);
      $table_reff_item = $this->general_model->get_data_by_field('po_detail','table_reff_item',['id'=>$id_po_detail]);

      $data = [
        'id_po_detail'=>$id_po_detail,
        'id_item'=>$id_item,
        'table_reff_item'=>$table_reff_item,
        'qty'=>$jumlah,
        'no_batch'=>strtoupper($no_batch),
        'date_expired'=>$date_expired,
        'date_come'=>date('Y-m-d',strtotime($tanggal)),
        'status'=>1,
        'id_user'=>$this->id_user,
        'id_gudang_rak'=>$rak,
        'stock_awal'=>$old_stok,
        'last_stock'=>$new_stock,
        'date_insert'=>date('Y-m-d H:i:s'),
      ];
      $this->general_model->do_save('po_barang_masuk',$data);

      $id_po = $this->general_model->get_data_by_field('po_detail','id_po',['id'=>$id_po_detail]);

      $q = $this->general_model->select_data('po_detail','*'," status = '1' AND id_po = '".$id_po."' ");
      $not_done = 0;
      foreach ($q->getResult() as $row) {
        $qty_come = $this->general_model->select_sum('po_barang_masuk','qty',['id_po_detail'=>$row->id,'status'=>1]);
        if ($qty_come > 0){
          if ($qty_come < $row->qty){
            $not_done = 1;
          }
        }
      }
      if ($not_done > 0){
        $this->general_model->do_update('po',['status_barang'=>'P'],['id'=>$id_po]);
      }

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK','id_po'=>$id_po));
      }
    }
  }

  public function history_barang_data(){
    $id_po_detail = $this->request->getGet('id_po_detail');
    $q = $this->general_model->select_order('po_barang_masuk','*',['id_po_detail'=>$id_po_detail,'status'=>1],'DATE(date_come) ASC');
    $satuan = $this->general_model->get_data_by_field('po_detail','satuan',['id'=>$id_po_detail]);
    $html = '<br><br><br>';
    if ($q->getNumRows() > 0 ){

    $html .= '<table class="table table-striped">';
    $html.= '<tr>';
    $html.= '<td>TANGGAL DATANG</td>';
    $html.= '<td>JUMLAH</td>';
    $html.= '<td>NO BATCH</td>';
    $html.= '<td>EXPIRED</td>';
    $html.= '<td>LOKASI</td>';
    $html.= '</tr>';

    foreach ($q->getResult() as $row) {
      if ( $row->id_gudang_rak == '' || $row->id_gudang_rak == null ){
        $lokasi = '<span class="btn btn-danger btn-xs">Belum diseting</span>';
      } else {
        $nama_rak = $this->general_model->get_data_by_field('gudang_rak','nama',['id'=>$row->id_gudang_rak]);
        $id_gudang = $this->general_model->get_data_by_field('gudang_rak','id_gudang',['id'=>$row->id_gudang_rak]);
        $nama_gudang = $this->general_model->get_data_by_field('gudang','nama',['id'=>$id_gudang]);
        $lokasi = 'Gudang '.$nama_gudang.'<br>Rak '.$nama_rak;
      }

      $rak = $this->general_model->select_order('gudang AS g,gudang_rak AS r','g.nama as gudang,r.nama AS rak,r.id AS id',"g.id = r.id_gudang AND g.status IS NULL AND r.status IS NULL","g.nama ASC,r.nama ASC");
      $option_rak = '<option value=""> - pilih - </opion>';
      foreach ($rak->getResult() as $rowr) {
        $selected = $rowr->id == $row->id_gudang_rak ? 'selected' : '';
        $option_rak .= '<option value="'.$rowr->id.'" '.$selected.'> Gudang '.$rowr->gudang.' Rak '.$rowr->rak.' </opion>';
      }

      $html.= '<tr>';
      $html.= '<td>'.date('d/m/Y',strtotime($row->date_come)).'</td>';
      $html.= '<td class="text-center">'.$row->qty.' '.$satuan.'</td>';
      $html.= '<td>'.$row->no_batch.'</td>';
      $html.= '<td>'.$row->date_expired.'</td>';
      $html.= '<td>'.$lokasi.'
      <br>
      <b>Ubah Lokasi</b>:
      <select class="form-control col-md-12" onChange="change_lokasi_rak_po('.$row->id.',this)">'.$option_rak.'</select></td>';
      $html.= '</tr>';
    }
    $html.= '</table>';
  }
    echo $html;
  }

  public function ubah_rak_barang_datang(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_po_barang_masuk' => for_validate('Barang Masuk','required|numeric'),
      'id_rak' => for_validate('Rak','required|numeric'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $this->db->transBegin();
      $id_po_barang_masuk = $this->request->getPost('id_po_barang_masuk');
      $id_rak = $this->request->getPost('id_rak');

      $id_po_detail = $this->general_model->get_data_by_field('po_barang_masuk','id_po_detail',['id'=>$id_po_barang_masuk]);
      $no_batch = $this->general_model->get_data_by_field('po_barang_masuk','no_batch',['id'=>$id_po_barang_masuk]);
      $date_expired = $this->general_model->get_data_by_field('po_barang_masuk','date_expired',['id'=>$id_po_barang_masuk]);

      $id_po = $this->general_model->get_data_by_field('po_detail','id_po',['id'=>$id_po_detail]);
      $id_item = $this->general_model->get_data_by_field('po_detail','id_item',['id'=>$id_po_detail]);
      $table_reff_item = $this->general_model->get_data_by_field('po_detail','table_reff_item',['id'=>$id_po_detail]);
      $satuan = $this->general_model->get_data_by_field('po_detail','satuan',['id'=>$id_po_detail]);
      $id_satuan = $this->general_model->get_data_by_field("satuan",'id',['id_reff'=>$id_item,'table_reff'=>$table_reff_item,'satuan'=>$satuan]);
      $id_rak_lama = $this->general_model->get_data_by_field('po_barang_masuk','id_gudang_rak',['id'=>$id_po_barang_masuk]);
      $jumlah_item = $this->general_model->get_data_by_field('po_barang_masuk','qty',['id'=>$id_po_barang_masuk]);

      $check_data_stok_baru = $this->general_model->count_rows('master_stok','id',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
      if ($check_data_stok_baru > 0){
        $old_stok = $this->general_model->get_data_by_field('master_stok','qty',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
        $data_stok =[
          'qty'=>$old_stok+$jumlah_item,
          'date_update'=>date('Y-m-d H:i:s'),
          'id_user_update'=>$this->id_user,
        ];
        $this->general_model->do_update('master_stok',$data_stok,['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
      } else {
        $data_stok =[
          'id_satuan'=>$id_satuan,
          'id_gudang_rak'=>$id_rak,
          'no_batch'=>$no_batch,
          'date_expired'=>$date_expired,
          'qty'=>$jumlah_item,
          'date_update'=>date('Y-m-d H:i:s'),
          'id_user_update'=>$this->id_user,
        ];
        $this->general_model->do_save('master_stok',$data_stok);
      }


      if ($id_rak_lama != ''){
        $old_stok_rak_lama = $this->general_model->get_data_by_field('master_stok','qty',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_rak_lama,'no_batch'=>$no_batch,'date_expired'=>$date_expired]);
        $data_stok =[
          'qty'=>$old_stok_rak_lama-$jumlah_item,
          'date_update'=>date('Y-m-d H:i:s'),
          'id_user_update'=>$this->id_user,
        ];
        $this->general_model->do_update('master_stok',$data_stok,['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_rak_lama]);
      }

      $data = ['id_gudang_rak'=>$id_rak];
      $this->general_model->do_update('po_barang_masuk',$data,['id'=>$id_po_barang_masuk]);


      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK','id_po_detail'=>$id_po_detail,'id_po'=>$id_po));
      }

    }
  }

  public function riwayat_pembayaran(){
    $id_po = $this->request->getGet('id');
    $q = $this->general_model->select_data('pembayaran','*',['id_reff'=>$id_po,'table_reff'=>'po','status'=>1]);
    $html = '<b>RIWAYAT PEMBAYARAN</b><br>';
    $html.= '<table class="table table-bordered">';
    $html.= '<tr>';
    $html.= '<th>TANGGAL</th>';
    $html.= '<th class="text-right">NOMINAL (Rp)</th>';
    $html.= '<th>KODE REKENING</th>';
    $html.= '</tr>';
    $total = 0;
    foreach ($q->getResult() as $row) {
      $nama_kode = $this->general_model->get_data_by_field('zacc_code_accounting','name',['code'=>$row->code_accounting]);
      $html.= '<tr>';
      $html.= '<td>'.date('d/m/Y',strtotime($row->tgl_bayar)).'</td>';
      $html.= '<td class="text-right">'.number_format($row->value,2,'.',',').'</td>';
      $html.= '<td class="text-right">'.$row->code_accounting.' '.$nama_kode.'</td>';
      $html.= '</tr>';
      $total = $total + $row->value;
    }
    $html.= '<tr style="background:#deffde;">';
    $html.= '<th>TOTAL</th>';
    $html.= '<th class="text-right">'.number_format($total,2,'.',',').'</th>';
    $html.= '<th></th>';
    $html.= '</tr>';
    $html .= '</table>';
    echo $html;
  }

  public function retur()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->pembelian("retur", $data);
  }



}
