<?php
namespace App\Controllers\Pembelian;
use App\Controllers\BaseController;
use App\Models\General_model;

class Opb extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    helper("general");
  }

  public function create()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->pembelian("opb/create", $data);
  }

  public function add_item(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('ID Reff','required'),
      'reff' => for_validate('Reff','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id = $this->request->getPost('id');
      $reff = $this->request->getPost('reff');
      $this->db->transBegin();

      $data = [
        'id_item'=>$id,
        'table_reff_item'=>$reff,
        'status'=>1,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      $save = $this->general_model->do_save('po_detail',$data);
      $error1 = $this->general_model->error();
      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }
    }
  }

  public function get_new_item_opb(){
    $q = $this->general_model->select_data('po_detail','*'," status = '1' AND id_po IS NULL ");

    $rak = $this->general_model->select_order('gudang AS g,gudang_rak AS r','g.nama as gudang,r.nama AS rak,r.id AS id',"g.id = r.id_gudang AND g.status IS NULL AND r.status IS NULL","g.nama ASC,r.nama ASC");


    $html = '<table class="table table-bordered" >';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .= '<th>No.</th>';
    $html .= '<th>Nama Item</th>';
    $html .= '<th>Golongan</th>';
    $html .= '<th>No Batch</th>';
    $html .= '<th>Expired</th>';
    $html .= '<th>Jumlah Beli</th>';
    $html .= '<th>Satuan</th>';
    $html .= '<th>HNA (Rp)</th>';
    $html .= '<th>Subtotal (Rp)</th>';
    $html .= '<th>Diskon (%)</th>';
    $html .= '<th>Diskon (Rp)</th>';
    $html .= '<th>Total (Rp)</th>';
    $html .= '<th>Lokasi</th>';
    $html .= '<th>#</th>';
    $html .= '</tr>';
    $html .= '</thead>';

    $no = 1;
    $totalharga = 0;
    foreach ($q->getResult() as $row) {
      if($row->table_reff_item == 'obat'){
        $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
        $id_golongan = $this->general_model->get_data_by_field('obat','id_golongan',['id'=>$row->id_item]);
        $nama_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_golongan]);
        $spesifikasi = '';
        $txt_golongan = '<i class="fas fa-capsules"></i> '.$nama_golongan;
      } else if($row->table_reff_item == 'barang'){
        $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
        $id_golongan = $this->general_model->get_data_by_field('barang','id_golongan',['id'=>$row->id_item]);
        $nama_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_golongan]);
        $aspesifikasi = $this->general_model->get_data_by_field('barang','spesifikasi',['id'=>$row->id_item]);
        $merek = $this->general_model->get_data_by_field('barang','merek',['id'=>$row->id_item]);
        $spesifikasi = $merek.' - '.$aspesifikasi;
        $txt_golongan = '<i class="fas fa-stethoscope"></i> '.$nama_golongan;
      } else {
        $nama_item = '';
        $spesifikasi = '';
        $txt_golongan = '';
      }
      $satuan = $this->general_model->select_order('satuan','*',['id_reff'=>$row->id_item,'table_reff'=>$row->table_reff_item],'ket ASC');
      $option = '<option value="">pilih satuan</option>';
      foreach ($satuan->getResult() as $rowsatuan) {
        $selected = $row->satuan == $rowsatuan->satuan ? 'selected' : '';
        $option.= '<option value="'.$rowsatuan->id.'" '.$selected.'>'.$rowsatuan->satuan.'</option>';
      }

      $param_get_satuan = "'".$row->id."','".$row->table_reff_item."'";
      $subtotal = $row->harga_beli*$row->qty;
      if ($row->persen_diskon == '' || $row->persen_diskon == null){
        $persen_diskon = 0;
      } else {
        $persen_diskon = $row->persen_diskon;
      }
      $nilai_diskon = $row->diskon;
      // $nilai_diskon = ($persen_diskon/100)*$subtotal;
      $totalhargaitem = $subtotal - $nilai_diskon;



      $condition = [
        'id_po_detail'=>$row->id,
        'status'=>1,
      ];
      $check_po_barang_masuk = $this->general_model->select_order_limit('po_barang_masuk','no_batch,date_expired,id_gudang_rak',$condition,'id ASC',1);
      $no_batch = '';
      $date_expired = '';
      $value_rak = '';
      if ( $check_po_barang_masuk->getNumRows() > 0 ){
        foreach ($check_po_barang_masuk->getResult() as $rowbm) {
          $no_batch = $rowbm->no_batch;
          if ($rowbm->date_expired == '' || $rowbm->date_expired == null || $rowbm->date_expired == '0000-00-00' ||  $rowbm->date_expired == '1970-01-01' ){
            $date_expired = '';
          } else {
            $date_expired = date('d-m-Y',strtotime($rowbm->date_expired));
          }

          $value_rak = $rowbm->id_gudang_rak;
          // code...
        }
      }


      if ($rak->getNumRows() > 0 ){
        if ($rak->getNumRows() === 1 ){
          $option_rak = '';
          foreach ($rak->getResult() as $rowr) {
            $selected = $rowr->id == $value_rak ? 'selected' : '';
            $option_rak .= '<option value="'.$rowr->id.'" '.$selected.'>Rak '.$rowr->rak.' Gudang '.$rowr->gudang.' '.$value_rak.' </opion>';
          }
        } else {
          $option_rak = '<option value=""> - pilih - </opion>';
          foreach ($rak->getResult() as $rowr) {
            $selected = $rowr->id == $value_rak ? 'selected' : '';
            $option_rak .= '<option value="'.$rowr->id.'" '.$selected.'>Rak '.$rowr->rak.' Gudang '.$rowr->gudang.' '.$value_rak.' </opion>';
          }
        }
      } else {
        $option_rak = '<option value=""> Belum ada rak diinput. </opion>';
      }



      $html .= '<tr style="padding:0 5px !important;height:40px;background:#f2f8ff;">';
      $html .= '<td style="padding:0 5px !important;height:40px;" class="text-center">'.$no.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$nama_item.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$txt_golongan.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;width:200px !important;"><input style="height:35px !important;" type="text" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" id="no_batch'.$row->id.'" value="'.$no_batch.'"/></td>';
      if($row->table_reff_item == 'barang'){
        $html .= '<td style="padding:0 5px !important;height:40px;"></td>';
      } else {
        $html .= '<td style="padding:0 5px !important;height:40px;"><input style="height:35px !important;"  type="text" class="form-control form-control-sm expired" onKeyup="hitung_sub_total_harga('.$row->id.')" id="date_expired'.$row->id.'" value="'.$date_expired.'"/></td>';
      }
      $html .= '<td style="padding:0 5px !important;height:40px;"><input style="height:35px !important;"  type="number" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" id="jumlah'.$row->id.'" value="'.$row->qty.'"/></td>';
      $html .= '<td style="padding:0 5px !important;height:40px;"><select style="height:35px !important;"  class="form-control form-control-sm sss" onChange="get_harga_satuan('.$param_get_satuan.')" id="optionsatuan'.$row->id.'" data-backdrop="static" data-keyboard="false" style="padding:0px;">'.$option.'</select></td>';
      $html .= '<td style="padding:0 5px !important;height:40px;width:200px;"><input style="height:35px !important;"  type="number" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" id="harga_beli'.$row->id.'" value="'.$row->harga_beli.'"/></td>';
      $html .= '<td style="padding:0 5px !important;height:40px;" id="subtotalharga'.$row->id.'" class="text-right">'.number_format($subtotal,2,',','.').'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;"><input style="height:35px !important;"  type="number" class="form-control form-control-sm"  onKeyup="hitung_nominal_diskon('.$row->id.')" id="persen_diskon'.$row->id.'" value="'.$persen_diskon.'"/></td>';
      $html .= '<td style="padding:0 5px !important;height:40px;"><input style="height:35px !important;"  type="number" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" onChange="hitung_sub_total_harga('.$row->id.')" id="nilai_diskon'.$row->id.'" value="'.$nilai_diskon.'" /></td>';
      $html .= '<td style="padding:0 5px !important;height:40px;" id="totalharga'.$row->id.'" class="text-right">'.number_format($totalhargaitem,2,',','.').'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;width:200px !important;">
      <select style="height:35px !important;font-size:12px;"  class="form-control" id="rak'.$row->id.'" onChange="hitung_sub_total_harga('.$row->id.')">
      '.$option_rak.'
      </select>
      </td>';
      $html .= '<td style="padding:0 5px !important;height:40px;"><button class="btn btn-icon btn-danger btn-round btn-xs" id="delete_item'.$row->id.'" onClick="delete_item('.$row->id.')"><i class="fa fa-times"></i></button></td>';

      $html .= '</tr>';
      $no++;
      $totalharga += $subtotal;
    }
    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="11" class="text-right">TOTAL : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($totalharga,2,',','.').'</th>';
    $html .= '<td><input type="hidden" id="harga_total" value="'.$totalharga.'" /></td>';
    $html .= '<td></td>';
    $html .= '</tr>';
    $html .= '</table>';
    echo $html;
  }



  public function delete_item(){
    $id = $this->request->getPost('id');
    $this->general_model->do_delete('po_detail',['id'=>$id]);
    $this->general_model->do_delete('po_barang_masuk',['id_po_detail'=>$id]);
  }

  public function get_harga_jual_satuan(){
    $id_satuan = $this->request->getPost('id_satuan');
    $id_po_detail = $this->request->getPost('id_po_detail');
    $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);
    $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
    // $persen_harga_jual = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id'=>$id_satuan]);
    // $harga = $harga_beli + ( $harga_beli * ($persen_harga_jual/100));
    $this->general_model->do_update('po_detail',['harga_beli'=>$harga_beli,'satuan'=>$satuan],['id'=>$id_po_detail]);

    echo json_encode(['response'=>true,'harga_beli'=>$harga_beli]);
  }



  public function insert_jml_beli_item(){
    $jumlah = $this->request->getPost('jumlah');
    $harga_beli = $this->request->getPost('harga_beli');
    $persen_diskon = $this->request->getPost('persen_diskon');
    $nilai_diskon = $this->request->getPost('nilai_diskon');
    $no_batch = $this->request->getPost('no_batch');
    $date_expired = $this->request->getPost('expired');
    $date_expired = date('Y-m-d',strtotime($date_expired));
    $rak = $this->request->getPost('rak');
    $id_po_detail = $this->request->getPost('id_po_detail');

    $nilai_diskon = $nilai_diskon == '' ? 0 : $nilai_diskon;

    $id_item = $this->general_model->get_data_by_field('po_detail','id_item',['id'=>$id_po_detail]);
    $table_reff_item = $this->general_model->get_data_by_field('po_detail','table_reff_item',['id'=>$id_po_detail]);

    $this->general_model->do_update('po_detail',['qty'=>$jumlah,'harga_beli'=>$harga_beli,'persen_diskon'=>$persen_diskon,'diskon'=>$nilai_diskon],['id'=>$id_po_detail]);
    $q = $this->general_model->select_data('po_detail','*'," status = '1' AND id_po IS NULL ");
    $totalharga = 0;
    foreach ($q->getResult() as $row) {
      $subtotal = $row->harga_beli*$row->qty;
      if ($row->persen_diskon == '' || $row->persen_diskon == null){
        $persen_diskon = 0;
      } else {
        $persen_diskon = $row->persen_diskon;
      }
      // $nilai_diskon = ($persen_diskon/100)*$subtotal;
      $nilai_diskon = $row->diskon;
      $totalharga += ($subtotal-$nilai_diskon);
    }

    if ($table_reff_item == 'obat'){

    }

    if ($date_expired == '' || $date_expired == null || $date_expired == '0000-00-00' || $date_expired == '1970-01-01' ){
      $date_expired = '0000-00-00';
    }

    $condition = [
      'id_po_detail'=>$id_po_detail,
      'status'=>1,
    ];
    $check_po_barang_masuk = $this->general_model->select_order_limit('po_barang_masuk','*',$condition,'id ASC',1);
    if ($check_po_barang_masuk->getNumRows() > 0){
      $data = [
        'id_po_detail'=>$id_po_detail,
        'id_item'=>$id_item,
        'table_reff_item'=>$table_reff_item,
        'qty'=>$jumlah,
        'no_batch'=>strtoupper($no_batch),
        'date_expired'=>$date_expired,
        'id_gudang_rak'=>$rak,
        'status'=>1,
        'id_user'=>$this->id_user,
        'date_insert'=>date('Y-m-d H:i:s'),
      ];
      $this->general_model->do_update('po_barang_masuk',$data,$condition);
    } else {
      $data = [
        'id_po_detail'=>$id_po_detail,
        'id_item'=>$id_item,
        'table_reff_item'=>$table_reff_item,
        'qty'=>$jumlah,
        'no_batch'=>strtoupper($no_batch),
        'date_expired'=>$date_expired,
        'id_gudang_rak'=>$rak,
        'status'=>1,
        'id_user'=>$this->id_user,
        'date_insert'=>date('Y-m-d H:i:s'),
      ];
      $this->general_model->do_save('po_barang_masuk',$data);
    }




    echo json_encode(['response'=>true,'jumlah'=>$jumlah,'harga_total'=>$totalharga]);
  }


}
