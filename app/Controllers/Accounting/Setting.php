<?php
namespace App\Controllers\Accounting;
use App\Controllers\BaseController;
use App\Models\General_model;

class Setting extends BaseController {

	public function __construct() {
		date_default_timezone_set("Asia/Jakarta");
		$this->session = session();
		$this->general_model = new General_model();
		$this->id_user = $this->session->get('id_user');
	}

	public function get_(){
		$data = [];
		$this->template->accounting_main_layout('setting_trans',$data);
	}

	public function change_setting(){
		helper(['form', 'url']);
    $input = $this->validate([
      'param' => for_validate('parameter','required'),
			'value' => for_validate('value','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
			$param = $this->request->getPost('param');
			$value = $this->request->getPost('value');
			$this->db->transBegin();
			$check = $this->general_model->count_rows('zacc_setting','value',['param'=>$param]);
			if ($check > 0){
				$data = ['value'=>$value];
				$this->general_model->do_update('zacc_setting',$data,['param'=>$param]);
			} else {
				$data = [
					'value'=>$value,
					'param'=>$param
				];
				$this->general_model->do_save('zacc_setting',$data,['param'=>$param]);
			}

			if ($this->db->transStatus() === false) {
				$this->db->transRollback();
				echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
			} else {
				$this->db->transCommit();
				echo json_encode(array('response'=>TRUE,'message'=>'OK'));
			}

		}
	}

	public function cek_setting(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			echo json_encode(['response'=>false,'message'=>'anda logout.']);
			exit;
		}
		$persediaan_obat = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_obat']);
		$hpp_obat 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hpp_obat']);
		$persediaan_barang 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_barang']);
		$hpp_barang 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hpp_barang']);

		$biaya_ppn_pembelian 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'biaya_ppn_pembelian']);

		$hutang_supplier 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_supplier']);

		$diskon_penjualan 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'diskon_penjualan']);
		$biaya_diskon_penjualan 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'biaya_diskon_penjualan']);
		$tuslah_penjualan 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'tuslah_penjualan']);
		$kas_penjualan 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'kas_penjualan']);
		$penjualan_obat 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'penjualan_obat']);
		$penjualan_barang 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'penjualan_barang']);

		$hutang_pph21 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_pph21']);
		$hutang_pph23 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_pph23']);
		$hutang_ppn 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_ppn']);

		$kode_kas_pembayaran 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'kode_kas_pembayaran']);
		$kode_bank_pembayaran 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'kode_bank_pembayaran']);

		$code_parent_laba_ditahan 			 = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'code_parent_laba_ditahan']);
		$code_parent_laba_periode = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'code_parent_laba_periode']);
		$code_parent_laba_sd_periode_lalu = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'code_parent_laba_sd_periode_lalu']);

		echo json_encode(
			[
				'response'=>true,
				'message'=>'anda logout.',
				'persediaan_obat'=>$persediaan_obat,
				'hpp_obat'=>$hpp_obat,
				'persediaan_barang'=>$persediaan_barang,
				'hpp_barang'=>$hpp_barang,
				'hutang_supplier'=>$hutang_supplier,
				'diskon_penjualan'=>$diskon_penjualan,
				'biaya_diskon_penjualan'=>$biaya_diskon_penjualan,
				'tuslah_penjualan'=>$tuslah_penjualan,
				'hutang_pph21'=>$hutang_pph21,
				'hutang_pph23'=>$hutang_pph23,
				'hutang_ppn'=>$hutang_ppn,
				'biaya_ppn_pembelian'=>$biaya_ppn_pembelian,
				'kas_penjualan'=>$kas_penjualan,
				'biaya_diskon_penjualan'=>$biaya_diskon_penjualan,
				'penjualan_obat'=>$penjualan_obat,
				'penjualan_barang'=>$penjualan_barang,
				'kode_kas_pembayaran'=>$kode_kas_pembayaran,
				'kode_bank_pembayaran'=>$kode_bank_pembayaran,
				'code_parent_laba_ditahan'=>$code_parent_laba_ditahan,
				'code_parent_laba_periode'=>$code_parent_laba_periode,
				'code_parent_laba_sd_periode_lalu'=>$code_parent_laba_sd_periode_lalu,
			]
		);
	}

}
