<?php
namespace App\Controllers\Accounting;
use App\Controllers\BaseController;
use App\Models\General_model;

class Code_accounting extends BaseController {

	public function __construct() {
		date_default_timezone_set("Asia/Jakarta");
		$this->session = session();
		$this->general_model = new General_model();
		$this->id_user = $this->session->get('id_user');
    helper('code_accounting');
	}

  public function list_(){
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $max_level = code_accounting_max_level();
		$data['max_level'] = $max_level;
		$data['code_accounting'] 	= $this->general_model->select_order('zacc_code_accounting',"*"," status IS NULL AND code IS NOT NULL AND code != '' AND level <= '".$max_level."'","code ASC ");
		$this->template->accounting_main_layout('code_accounting/list',$data);
	}

	public function create( $param = null ){
		$auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
		if ( $param == NULL ) {
			$code_parent = 0;
			$level = 1;
			$id_param = '';
		} else {
			$code_parent_param = $this->general_model->get_data_by_field('zacc_code_accounting','code_parent',array('id'=>$param));
			$id_param = $this->general_model->get_data_by_field("zacc_code_accounting",'id',array('code'=>$code_parent_param));

			$cek_status = $this->general_model->get_data_by_field('zacc_code_accounting','status',array('id'=>$param));
			if ($cek_status == '0'){
				$this->session->setFlashdata('message_error', array('message_error' => 'Kode rekening tidak aktif.'));
				return redirect()->to('accounting/code_accounting/create/'.$param);
			}
			$code_parent = $this->general_model->get_data_by_field('zacc_code_accounting','code',array('id'=>$param));
			// $code_parent = $param;
			$check_param = $this->general_model->count_rows('zacc_code_accounting','id',array('code'=>$code_parent));
			if ($check_param === 0 ){
				$this->session->setFlashdata('message_error', array('message_error' => 'Kode rekening parent tidak ditemukan.'));
				return redirect()->to('accounting/code_accounting/create/'.$param);
			}
			$level = ($this->general_model->get_data_by_field('zacc_code_accounting','level',array('code'=>$code_parent)) + 1);
		}

		$max_level  = code_accounting_max_level();
		if ($level > $max_level){
			$this->session->setFlashdata('message_error', array('message_error' => 'Kode rekening maksimal '.$max_level.' level .'));
			return redirect()->to('accounting/code_accounting/create/'.$param);
		}

		$link 			= base_url().'accounting/code_accounting/create/';
		$slash			='<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
		$name3			= $code_parent 	!= 0 ? ' '.$slash.' <a href="#" style="color:#000;">'.$this->general_model->get_data_by_field('zacc_code_accounting','name',array('code'=>$code_parent)).'</a> ': NULL;
		$id_upline3 =  $code_parent != 0 ? $this->general_model->get_data_by_field('zacc_code_accounting','code_parent',array('code'=>$code_parent)) : 0;
		$code3 			= $code_parent 	!= 0 ? $this->general_model->get_data_by_field('zacc_code_accounting','code',array('code'=>$code_parent)) : NULL;

		$name2			= $id_upline3 	!= 0 ? ' '.$slash.' <a href="#" style="color:#000;">'.$this->general_model->get_data_by_field('zacc_code_accounting','name',array('code'=>$id_upline3)).'</a> ': NULL;
		$id_upline2 = $id_upline3 	!= 0 ? $this->general_model->get_data_by_field('zacc_code_accounting','code_parent',array('code'=>$id_upline3)) : 0;
		$code2 			= $id_upline3 	!= 0 ? $this->general_model->get_data_by_field('zacc_code_accounting','code',array('code'=>$id_upline3)) : NULL;

		$name1			= $id_upline2 	!= 0 ? ' '.$slash.'<a href="#" style="color:#000;">'.$this->general_model->get_data_by_field('zacc_code_accounting','name',array('code'=>$id_upline2)).'</a> ': NULL;
		$id_upline1 = $id_upline2 	!= 0 ? $this->general_model->get_data_by_field('zacc_code_accounting','code_parent',array('code'=>$id_upline2)) : 0;
		$code1 			= $id_upline2 	!= 0 ? $id_upline2 : NULL;

		if ($level == 1 ) {
			$title 		= 'Input Kode Rekening Induk';
		} else {
			$title 		= '<a href="'.$link.'" style="color:#000;">&nbsp;&nbsp;&nbsp;<i class="fa fa-reply" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;</a> '.$name1.$name2.$name3;
		}

		$real_code = $code1.$code2.$code3;

		if ( $code1 != NULL and $code2 != NULL and $code3 != NULL ) {
			$check_last_item = TRUE;
		} else {
			$check_last_item = FALSE;
		}

		$code = $this->request->getPost('code_account');
		$name = strtoupper($this->request->getPost('account_name'));

		if ($code_parent == 0 ){
			$code = $code;
		} elseif ( strlen($code_parent) === 2 ) {
			$code = $code_parent.$code;
		} elseif ( strlen($code_parent) === 4 ) {
			$code = $code_parent.'.'.$code;
		} elseif ( strlen($code_parent) === 8 ) {
			$code = $code_parent.'.'.$code;
		}

		if ($level > 1){
			if ($level < 3){
				$input = $this->validate([
					'code_account' => for_validate('Kode','required'),
					'account_name' => for_validate('Nama','required|date'),
					'balance' => for_validate('Balance','required|date'),
				]);
			} else {
				$input = $this->validate([
					'code_account' => for_validate('Kode','required'),
					'account_name' => for_validate('Nama','required|date'),
				]);
			}


		} else {
			$input = $this->validate([
				'code_account' => for_validate('Kode','required'),
				'account_name' => for_validate('Nama','required|date'),
				'balance' => for_validate('Balance','required|date'),
				'pos' => for_validate('Pos','required|date'),
			]);

		}

		if (!$input) {
			$data['code_accounting'] 	= $this->general_model->select_order('zacc_code_accounting','*',"code_parent='".$code_parent."' ","code ASC");
			$data['code'] 						= $this->request->getPost('code_account');
			$data['old_code'] 				= $this->request->getPost('old_code_account');
			$data['name'] 						= $name;
			$data['code_account']			=	$code_parent;
			$data['code_parent'] 			= $code_parent;
			$data['title']						= $title ;
			$data['check_last_item'] 	= $check_last_item;
			$data['level']						=	$level;
			$data['max_level']				=	$max_level;
			$this->template->accounting_main_layout('code_accounting/create',$data);
		} else {

			if ($level === 1){
				$type_report = ($this->request->getPost('pos'));
				$balance = ($this->request->getPost('balance'));
			} else {
				$type_report = $this->general_model->get_data_by_field('zacc_code_accounting','type_report',array('code'=>$code_parent));
				$balance = $this->general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code_parent));
			}
			$check_code = $this->general_model->count_rows('zacc_code_accounting','id',"code='".$code."'");
			if ($check_code>0){
				$this->session->setFlashdata('message_error', array('message_error' => 'Kode Sudah Ada.'));
				$ket = 'Kode Sudah Ada.';
			} else {
				$data=array(
					'code'						=>	$code,
					'code_parent'			=>	$code_parent,
					'name'						=>	$name,
					'type_report'			=>	$type_report,
					'balance'					=>	$balance,
					'level'						=>	$level,
					'id_user_update'	=> 	$this->id_user,
					'last_update'			=> 	date('Y-m-d H:i:s'),
				);
				$save = $this->general_model->do_save('zacc_code_accounting',$data);
				if ($save){
					$this->session->setFlashdata('message', array('message' => 'Kode Rekening Telah Ditambahkan.'));
					$ket = 'Kode Rekening Telah Ditambahkan.';
				} else {
					$this->session->setFlashdata('message_error', array('message_error' => 'Gagal Simpan.'));
					$ket = 'Gagal Simpan.';
				}
			}
			return redirect()->to('accounting/code_accounting/create/'.$param);
		}
	}

	public function action_change_status(){
		$auth = $this->authentication->is_login();
    if ($auth==FALSE){
      $result = array('response'=>FALSE,'message'=> 'Gagal.');
			echo json_encode($result);
			exit;
    }
		$input = $this->validate([
			'id' => for_validate('ID','required'),
		]);
		if (!$input) {
			$result = array('response'=>FALSE,'message'=> $this->validator->listErrors());
			echo json_encode($result);
		} else {
			$id 		= $this->request->getPost('id');
			$status = $this->general_model->get_data_by_field('zacc_code_accounting','status',array('id'=>$id));
			if ( $status == '0' ){
				$new_status = NULL;
				$text_msg   = ' diaktifkan';
			} else {
				$new_status = '0';
				$text_msg   = ' dinonaktifkan';
			}
			$data 	= array(
				'status'=>$new_status,
				'id_user_update'	=> $this->id_user,
				'last_update'			=> date('Y-m-d H:i:s')
			);
			if ($new_status == '0') {
				$code_accounting 	= $this->general_model->get_data_by_field('zacc_code_accounting','code',array('id'=>$id));
				$check_journal 		= $this->general_model->count_rows('zacc_journal as j,zacc_journal_detail as d','j.id'," j.id = d.id_journal AND YEAR(j.date_trans) > 2017 AND j.status IS NULL AND d.status IS NULL AND d.code_accounting LIKE '".$code_accounting."%' ");
			} else {
				$check_journal 		= 0;
			}
			if ($check_journal > 0 ){
				$result = array('response'=>FALSE,'message'=> 'Terdeteksi digunakan dalam '.$check_journal.' jurnal aktif. Tidak dapat dinonaktifkan.');
			} else {
				$update = $this->general_model->do_update('zacc_code_accounting',$data,array('id'=>$id));
				if ($new_status == '0'){
					$codenya = $this->general_model->get_data_by_field('zacc_code_accounting','code',array('id'=>$id));
					$update2 = $this->general_model->do_update('zacc_code_accounting',$data," code LIKE '".$codenya."%' ");
				}
				if ($update){
					$result = array('response'=>TRUE,'message'=> 'Berhasil '.$text_msg.'.','new_status'=>$new_status);
				} else {
					$result = array('response'=>FALSE,'message'=> 'Gagal.');
				}
			}
			echo json_encode($result);
		}
	}


}
