<?php
namespace App\Controllers\Accounting;
use App\Controllers\BaseController;
use App\Models\General_model;
use App\Libraries\Journal;

class Generate extends BaseController {

	public function __construct() {
		date_default_timezone_set("Asia/Jakarta");
		$this->session = session();
		$this->general_model = new General_model();
		$this->id_user = $this->session->get('id_user');
		$this->journal = new Journal();
		helper('po');
		helper('penjualan');
		helper('journal_accounting');
	}

	public function get_(){
		$data = [];
		$this->template->accounting_main_layout('generate',$data);
	}

	public function get_data(){
		$html = '<table class="table table-bordered">';
		$html .= '<tbody>';
		$q_barang_masuk = $this->general_model->select_data('po_barang_masuk','*',"id_journal IS NULL");
		foreach ($q_barang_masuk->getResult() as $row) {
			$hutang_supplier = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_supplier']);
			$nama_hutang = $this->general_model->get_data_by_field('zacc_code_accounting','name',['code'=>$hutang_supplier]);
			if ($row->table_reff_item == 'obat'){
				$nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
				$kode_persediaan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_obat']);
				$nama_persediaan = $this->general_model->get_data_by_field('zacc_code_accounting','name',['code'=>$kode_persediaan]);
			}
			if ($row->table_reff_item == 'barang'){
				$nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
				$kode_persediaan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_barang']);
				$nama_persediaan = $this->general_model->get_data_by_field('zacc_code_accounting','name',['code'=>$kode_persediaan]);
			}
			$id_po = $this->general_model->get_data_by_field('po_detail','id_po',['id'=>$row->id_po_detail]);
			$satuan = $this->general_model->get_data_by_field('po_detail','satuan',['id'=>$row->id_po_detail]);
			$harga_beli = $this->general_model->get_data_by_field('po_detail','harga_beli',['id'=>$row->id_po_detail]);
			$harga = $row->qty * $harga_beli;

			$ket = $this->general_model->get_data_by_field('po','no',['id'=>$id_po]);

			$html .= '<tr id="barang_masuk'.$row->id.'">';
			$html .= '<td>'.date('d/m/Y',strtotime($row->date_come)).'</td>';
			$html .= '<td>Barang Masuk</td>';
			$html .= '<td>'.$nama_item.'</td>';
			$html .= '<td>'.$row->table_reff_item.'</td>';
			$html .= '<td>'.$row->qty.' '.$satuan.'</td>';
			$html .= '<td class="text-right">'.number_format($harga_beli,2,',','.').'</td>';
			$html .= '<td class="text-right">'.number_format($harga,2,',','.').'</td>';
			// $html .= '<td>'.$kode_persediaan.'<br><small>'.$nama_persediaan.'</small></td>';
			// $html .= '<td>'.$hutang_supplier.'<br><small>'.$nama_hutang.'</small></td>';
			$html .= '<td>'.$ket.'</td>';
			$html .= '<td><button class="btn btn-primary btn-sm" id="tbl_barang_masuk'.$row->id.'" onClick="generate_barang_masuk('.$row->id.')"><b>GENERATE</b></button></td>';
			$html .= '</tr>';
		}

		$q_penjualan = $this->general_model->select_data('penjualan','*',"id_journal IS NULL AND status IS NULL");
		foreach ($q_penjualan->getResult() as $row4) {
			$harga = get_harga_kesepakatan_penjualan($row4->id);
			$html .= '<tr id="penjualan'.$row4->id.'">';
			$html .= '<td>'.date('d/m/Y',strtotime($row4->datetime)).'</td>';
			$html .= '<td>PENJUALAN</td>';
			$html .= '<td>'.$row4->no.'</td>';
			$html .= '<td></td>';
			$html .= '<td></td>';
			$html .= '<td></td>';
			$html .= '<td class="text-right">'.number_format($harga,2,',','.').'</td>';
			// $html .= '<td></small></td>';
			// $html .= '<td></small></td>';
			$html .= '<td></td>';
			$html .= '<td><button class="btn btn-primary btn-sm" id="tbl_penjualan'.$row4->id.'" onClick="generate_penjualan('.$row4->id.')"><b>GENERATE</b></button></td>';
			$html .= '</tr>';
		}

		$q_pembayaran_po = $this->general_model->select_data('pembayaran','*',"id_journal IS NULL AND status ='1' AND table_reff = 'po'");
		foreach ($q_pembayaran_po->getResult() as $row5) {
			$no_po = $this->general_model->get_data_by_field('po','no',['id'=>$row5->id_reff]);
			$html .= '<tr id="pembayaran_po'.$row5->id.'">';
			$html .= '<td>'.date('d/m/Y',strtotime($row5->tgl_bayar)).'</td>';
			$html .= '<td>PEMBAYARAN PO</td>';
			$html .= '<td>'.$no_po.'</td>';
			$html .= '<td></td>';
			$html .= '<td></td>';
			$html .= '<td></td>';
			$html .= '<td class="text-right">'.number_format($row5->value,2,',','.').'</td>';
			// $html .= '<td></small></td>';
			// $html .= '<td></small></td>';
			$html .= '<td></td>';
			$html .= '<td><button class="btn btn-primary btn-sm" id="tbl_pembayaran_po'.$row5->id.'" onClick="generate_pembayaran_po('.$row5->id.')"><b>GENERATE</b></button></td>';
			$html .= '</tr>';
		}

		$html .= '</tbody>';
		$html .= '</table>';

		echo $html;

	}

	public function generate_barang_masuk(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			echo json_encode(['response'=>false,'message'=>'sesi anda habis, silakan logout dan login ulang.']);
			exit;
		}
		$input = $this->validate([
			'id' => for_validate('id','required|numeric'),
		]);
		if (!$input) {
			echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
		} else {
			$id = $this->request->getPost('id');
			$q_barang_masuk = $this->general_model->select_data('po_barang_masuk','*',"id_journal IS NULL AND id = '".$id."'");
			if ($q_barang_masuk->getNumRows()>0){
				$this->db->transBegin();

				foreach ($q_barang_masuk->getResult() as $row) {

					$id_po = $this->general_model->get_data_by_field('po_detail','id_po',['id'=>$row->id_po_detail]);
					$diskon = $this->general_model->get_data_by_field('po','diskon',"id='".$id_po."'");
					$subtotal_po = get_sub_total($id_po);
					$persen_diskon = $diskon > 0 ? (($diskon/$subtotal_po)*100) : 0;
					$ppn = $this->general_model->get_data_by_field('po','ppn',"id='".$id_po."'");
					$ppn = $ppn > 0 ? $ppn : 0;

					$hutang_supplier = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_supplier']);
					$nama_hutang = $this->general_model->get_data_by_field('zacc_code_accounting','name',['code'=>$hutang_supplier]);
					if ($row->table_reff_item == 'obat'){
						$nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
						$kode_persediaan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_obat']);
						$nama_persediaan = $this->general_model->get_data_by_field('zacc_code_accounting','name',['code'=>$kode_persediaan]);
					}
					if ($row->table_reff_item == 'barang'){
						$nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
						$kode_persediaan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'persediaan_barang']);
						$nama_persediaan = $this->general_model->get_data_by_field('zacc_code_accounting','name',['code'=>$kode_persediaan]);
					}
					$id_po = $this->general_model->get_data_by_field('po_detail','id_po',['id'=>$row->id_po_detail]);
					$satuan = $this->general_model->get_data_by_field('po_detail','satuan',['id'=>$row->id_po_detail]);
					$harga_beli = $this->general_model->get_data_by_field('po_detail','harga_beli',['id'=>$row->id_po_detail]);
					$harga = $row->qty * $harga_beli;
					$ket = $this->general_model->get_data_by_field('po','no',['id'=>$id_po]);

					$date_trans = date('Y-m-d',strtotime($row->date_come));
					$code_ref = 'PO';
					$line = 1;
					$desc = 'PO '.$ket.' BARANG MASUK '.$nama_item.' '.$row->qty.' '.$satuan;
					$id_journal = $this->journal->generate_id_journal_manual($this->id_user,$date_trans,$code_ref,$kode_bb=null);
					if ($id_journal == '0'){
						echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Journal.'));
						exit;
					}
					$this->journal->generate_journal_detail($this->id_user,$id_journal,$kode_persediaan,$hutang_supplier,$harga,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);

					if($diskon>0){
						$desc_diskon = 'PO '.$ket.' DISKON '.$persen_diskon.'% '.$nama_item.' '.$row->qty.' '.$satuan. ' Rp '.$harga;
						$nominal_diskon = $harga * ($persen_diskon/100);
						$line2 = 2;
						$this->journal->generate_journal_detail($this->id_user,$id_journal,$hutang_supplier,$kode_persediaan,$nominal_diskon,$line2,$desc_diskon,$pos_for_id_jurnal_detail_for_ledger=null);
					}

					if($ppn>0){
						$desc_ppn = 'PO '.$ket.' BIAYA PAJAK '.$ppn.'%  '.$nama_item.' '.$row->qty.' '.$satuan. ' Rp '.$harga;
						$nominal_ppn = $harga * ($ppn/100);
						$line3 = 3;
						$kode_biaya_ppn_pembelian = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'biaya_ppn_pembelian']);
						$this->journal->generate_journal_detail($this->id_user,$id_journal,$kode_biaya_ppn_pembelian,$hutang_supplier,$nominal_ppn,$line3,$desc_ppn,$pos_for_id_jurnal_detail_for_ledger=null);
					}

					$this->general_model->do_update('po_barang_masuk',['id_journal'=>$id_journal],['id'=>$id]);
				}

				if ($this->db->transStatus() === false) {
					$this->db->transRollback();
					echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Simpan.'));
				} else {
					$this->db->transCommit();
					echo json_encode(array('response'=>TRUE,'message'=>'Berhasil.'));
				}
			} else {
				echo json_encode(array('response'=>FALSE,'message'=>'Sudah dijurnal.'));
			}
		}
	}


	public function generate_penjualan(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			echo json_encode(['response'=>false,'message'=>'sesi anda habis, silakan logout dan login ulang.']);
			exit;
		}
		$input = $this->validate([
			'id' => for_validate('id','required|numeric'),
		]);
		if (!$input) {
			echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
		} else {
			$id = $this->request->getPost('id');
			$cek = $this->general_model->count_rows('penjualan','*'," id='".$id."' AND id_journal IS NULL AND status IS NULL");

			if ($cek > 0){
				$not_ok_journal = 0;
				$this->db->transBegin();

				$ket = $this->general_model->get_data_by_field('penjualan','no'," id='".$id."' ");
				$datetime = $this->general_model->get_data_by_field('penjualan','datetime'," id='".$id."' ");
				$persen_diskon = $this->general_model->get_data_by_field('penjualan','diskon'," id='".$id."' ");
				$persen_ppn = $this->general_model->get_data_by_field('penjualan','ppn'," id='".$id."' ");
				$tuslah = $this->general_model->get_data_by_field('penjualan','tuslah'," id='".$id."' ");


				$date_trans = date('Y-m-d',strtotime($datetime));
				$code_ref = 'KSR';
				$line = 1;
				$id_journal = $this->journal->generate_id_journal_manual($this->id_user,$date_trans,$code_ref,$kode_bb=null);
				if ($id_journal == '0'){
					echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Journal.'));
					exit;
				}
				$kode_kas_penjualan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'kas_penjualan']);

				$q_penjualan_detail = $this->general_model->select_data('penjualan_detail','*'," id_penjualan='".$id."' AND status = '1' ");
				foreach ($q_penjualan_detail->getResult() as $row) {

					generate_journal_hpp_by_id_penjualan_detail($row->id);

					if ($row->table_reff_item == 'obat'){
						$nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
						$kode_penjualan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'penjualan_obat']);
					}
					if ($row->table_reff_item == 'barang'){
						$nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
						$kode_penjualan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'penjualan_obat']);
					}

					if ($kode_penjualan == '' || $kode_penjualan == null){
						$not_ok_journal = $not_ok_journal+1;
					}

					$desc = 'PENJUALAN '.$ket.' '.$nama_item.' '.$row->qty.' '.$row->satuan;
					$line = 1;
					$harga = $row->qty * $row->harga_jual;
					$this->journal->generate_journal_detail($this->id_user,$id_journal,$kode_kas_penjualan,$kode_penjualan,$harga,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);

					if ( $persen_diskon > 0 ) {
						$kode_diskon_penjualan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'diskon_penjualan']);
						// $kode_biaya_diskon_penjualan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'biaya_diskon_penjualan']);

						if ($kode_diskon_penjualan == '' || $kode_diskon_penjualan == null){
							$not_ok_journal = $not_ok_journal+1;
						}

						$desc2 = 'DISKON PENJUALAN '.$persen_diskon.'% '.$ket.' '.$nama_item.' '.$row->qty.' '.$row->satuan;
						$line2 = 2;
						$diskon = $harga * ($persen_diskon/100);
						$this->journal->generate_journal_detail($this->id_user,$id_journal,$kode_diskon_penjualan,$kode_kas_penjualan,$diskon,$line2,$desc2,$pos_for_id_jurnal_detail_for_ledger=null);
					}
				}

				if ($tuslah > 0){
					$kode_tuslah_penjualan = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'tuslah_penjualan']);
					if ($kode_tuslah_penjualan == '' || $kode_tuslah_penjualan == null){
						$not_ok_journal = $not_ok_journal+1;
					}

					$desc3 = 'TUSLAH PENJUALAN '.$ket;
					$line3 = 3;
					$this->journal->generate_journal_detail($this->id_user,$id_journal,$kode_kas_penjualan,$kode_tuslah_penjualan,$tuslah,$line3,$desc3,$pos_for_id_jurnal_detail_for_ledger=null);
				}
				if ($persen_ppn > 0){
					$nominal_ppn = get_nominal_ppn_penjualan($id);
					$kode_hutang_ppn = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_ppn']);

					if ($kode_hutang_ppn == '' || $kode_hutang_ppn == null){
						$not_ok_journal = $not_ok_journal+1;
					}

					$desc4 = 'PPN PENJUALAN '.$persen_ppn.'% '.$ket;
					$line4 = 4;
					$this->journal->generate_journal_detail($this->id_user,$id_journal,$kode_kas_penjualan,$kode_hutang_ppn,$nominal_ppn,$line4,$desc4,$pos_for_id_jurnal_detail_for_ledger=null);
				}

				if ($not_ok_journal > 0){
					$this->db->transRollback();
					echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, Periksa setting kode akuntansi transaksi..'));
				} else {
					$this->general_model->do_update('penjualan',['id_journal'=>$id_journal],['id'=>$id]);
					if ($this->db->transStatus() === false) {
						$this->db->transRollback();
						echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Simpan.'));
					} else {
						$this->db->transCommit();
						echo json_encode(array('response'=>TRUE,'message'=>'Berhasil.'));
					}
				}

			} else {
				echo json_encode(array('response'=>FALSE,'message'=>'Sudah dijurnal.'));
			}
		}
	}



		public function generate_pembayaran_po(){
			$auth = $this->authentication->is_login();
			if ($auth==FALSE){
				echo json_encode(['response'=>false,'message'=>'sesi anda habis, silakan logout dan login ulang.']);
				exit;
			}
			$input = $this->validate([
				'id' => for_validate('id','required|numeric'),
			]);
			if (!$input) {
				echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
			} else {
				$id = $this->request->getPost('id');
				$check = $this->general_model->count_rows('pembayaran','id'," id='".$id."' AND id_journal IS NULL");
				if ($check>0){

					$code_acc = $this->general_model->get_data_by_field("pembayaran",'code_accounting',['id'=>$id]);
					$hutang_supplier = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'hutang_supplier']);

					if ($code_acc == '' OR $hutang_supplier == ''){
						echo json_encode(array('response'=>FALSE,'message'=>'Kode akun belum disetting.'));
					} else {
						$this->db->transBegin();

						$tgl = $this->general_model->get_data_by_field('pembayaran','tgl_bayar',['id'=>$id]);
						$value = $this->general_model->get_data_by_field('pembayaran','value',['id'=>$id]);
						$id_po = $this->general_model->get_data_by_field('pembayaran','id_reff',['id'=>$id]);
						$no_po = $this->general_model->get_data_by_field('po','no',['id'=>$id_po]);
						$date_trans = date('Y-m-d',strtotime($tgl));
						$code_ref = 'POTRANS';
						$desc = 'PEMBAYARAN PO '.$no_po;
						$line = 1;
						$id_journal = $this->journal->generate_id_journal_manual($this->id_user,$date_trans,$code_ref,$kode_bb=null);
						if ($id_journal != '0'){
							$this->journal->generate_journal_detail($this->id_user,$id_journal,$hutang_supplier,$code_acc,$value,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
							$this->general_model->do_update('pembayaran',['id_journal'=>$id_journal],['id'=>$id]);
						} else {
							$this->db->transRollback();
							echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Simpan.'));
							exit;
						}

						if ($this->db->transStatus() === false) {
							$this->db->transRollback();
							echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Simpan.'));
						} else {
							$this->db->transCommit();
							echo json_encode(array('response'=>TRUE,'message'=>'Berhasil.'));
						}
					}


				} else {
					echo json_encode(array('response'=>FALSE,'message'=>'Sudah di jurnal'));
				}
			}
		}

		public function journal_hpp(){
			generate_journal_hpp_by_id_penjualan_detail(3);
		}

	}
