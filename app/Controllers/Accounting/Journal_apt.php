<?php
namespace App\Controllers\Accounting;
use App\Controllers\BaseController;
use App\Models\General_model;
use App\Libraries\Journal;

class Journal_apt extends BaseController {

	public function __construct() {
		date_default_timezone_set("Asia/Jakarta");
		$this->session = session();
		$this->general_model = new General_model();
		$this->id_user = $this->session->get('id_user');
		$this->journal = new Journal();
	}

	public function create(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			return redirect()->to('aptauth');
		}
		$data=[
			'id_user'=>$this->id_user,
		];
		$this->template->accounting_main_layout('journal/create',$data);
	}

	public function riwayat(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			return redirect()->to('aptauth');
		}
		$tanggal=$this->request->getGet('t');
		$search=$this->request->getGet('s');
		if ($tanggal != '' OR $tanggal != NULL){
			$explode_tanggal=explode('-', $tanggal);

			if ($search == '' ){
				$condition_search = '';
			} else {
				$condition_search = " AND id IN (select id_journal from zacc_journal_detail where `desc` like '%".$search."%') ";
			}

			$condition =" DATE(date_trans) >= '".date('Y-m-d',strtotime($explode_tanggal[0]))."' AND DATE(date_trans) <= '".date('Y-m-d',strtotime($explode_tanggal[1]))."' AND status IS NULL".$condition_search;
		} else {
			$condition = " id = '0' ";
		}



		$list = $this->general_model->select_order('zacc_journal','*',$condition," DATE(date_insert) ASC");
		$data = [
			'list' => $list,
		];
		$this->template->accounting_main_layout('journal/riwayat',$data);
	}

	public function ajax_action_create_journal(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			$result = array('response'=>FALSE,'message'=> 'Sesi anda habis.');
			echo json_encode($result);
			exit;
		}
		$input = $this->validate([
			'no_bukti' => for_validate('Nomor Bukti','required'),
			'date_trans' => for_validate('Tanggal','required|date'),
		]);
		if (!$input) {
			$result = array('response'=>FALSE,'message'=> $this->validator->listErrors());
			echo json_encode($result);
		} else {
			$date_trans = date('Y-m-d',strtotime($this->request->getPost('date_trans')));
			$month      = date('m',strtotime($date_trans));
			$year     	= date('Y',strtotime($date_trans));
			$now        = date('Y-m-d');
			$tgl        = date('d');

			$this->db->transBegin();

			$bulan 				= date('m',strtotime($this->request->getPost('date_trans')));
			$tahun 				= date('Y',strtotime($this->request->getPost('date_trans')));
			$id_periode		=	$this->general_model->get_data_by_field('zacc_periode','id',"`month` =  '".$bulan."' AND `year` = '".$tahun."'");
			$xpl_code_ref = explode('/',$this->request->getPost('no_bukti'));
			$code_ref 		= $xpl_code_ref[0];
			$kode_bb      = $this->request->getPost('no_bukti');

			$id_journal = $this->journal->generate_id_journal_manual($this->id_user,$date_trans,$code_ref,$kode_bb);
			if ( $id_journal != '0' ){
				$detail_journal =  json_decode($this->request->getPost('detail_journal'),TRUE) ;
				$insertData	= array();
				if(is_array($detail_journal)){
					$line = 1;
					foreach ($detail_journal as $key => $value) {
						$desc 				= $value['desc'];
						$code_debet 	= $value['code_accounting_debet'];
						$code_credit 	= $value['code_accounting_credit'];
						$value 				= $value['value'];
						$id_journal_detail = $this->journal->generate_journal_detail($this->id_user,$id_journal,$code_debet,$code_credit,$value,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
						$line++;
					}
				}
				$error = $error = $this->db->error();
				$cek = $this->general_model->count_rows('zacc_journal_detail','id',array('id_journal'=>$id_journal));

				if ($this->db->transStatus() === FALSE){
					$this->db->transRollback();
					$result = array('response'=>FALSE,'message'=> 'GAGAL SIMPAN','error'=>$error);
				} else {
					if ($cek === 0){
						$this->db->transRollback();
						$result = array('response'=>FALSE,'message'=> 'GAGAL SIMPAN,Kesalahan diinput Journal Detail.','error'=>'Kesalahan diinput Journal Detail.');
					} else {
						$this->db->transCommit();
						$result = array('response'=>TRUE,'message'=> 'BERHASIL SIMPAN','id_journal'=> $id_journal,'cek'=>$cek,'error'=>'');
					}

				}
			} else {
				$result = array('response'=>FALSE,'message'=> 'GAGAL SIMPAN','error'=> 'Jurnal Utama gagal');
			}

			echo json_encode($result);

		}
	}

	public function load_code_accounting_for_journal_by_company(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			echo 'Anda Logout';
		}
		helper('code_accounting');
		$max_level = code_accounting_max_level();
		$code_accounting=$this->general_model->select_order('zacc_code_accounting as b','b.*',"level = '".$max_level."' AND b.status IS NULL ","b.code ASC");
		echo '<option value="">- PILIH KODE REK -</option>';
		foreach($code_accounting->getResult() as $row){
			child_code_accounting_for_option($row->code,$row->name,$proyek=null);
		}
	}

	public function list_periode(){
		$num=array('-1-','-2-','-3-','-4-','-5-','-6-','-7-','-8-','-9-','-10-','-11-','-12-');
		$day=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$periode = $this->general_model->select_order('zacc_periode','id,month,year','year > 2017',"id DESC");
		echo '<option value="">Pilih Periode</option>';
		foreach ($periode->getResult() as $row) {
			echo '<option value="'.$row->id.'">'.str_replace($num,$day,'-'.$row->month.'-').' '.$row->year.'</option>';
		}
	}

	// public function get_detail_journal(){
	// 	$id_journal = $this->request->getPost('id');
	// 	$journal	=	$this->journal_model->select_data('*',array('id'=>$id_journal))->result();
	// 	$detail = array();
	// 	$line = $this->general_model->select_distinct('zacc_journal_detail','line'," id_journal = '".$id_journal."' AND status IS NULL");
	// 	foreach ($line->result() as $row) {
	// 		$x['line'] = $row->line;
	// 		$x['id_journal']=$id_journal;
	// 		$x['code_debet'] = $this->general_model->get_data_by_field('zacc_journal_detail','code_accounting'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND debet > 0 AND status IS NULL");
	// 		$x['code_credit'] = $this->general_model->get_data_by_field('zacc_journal_detail','code_accounting'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND credit > 0 AND status IS NULL");
	// 		$x['name_code_debet'] = $this->general_model->get_data_by_field('zacc_code_accounting','name'," code = '".$x['code_debet']."' ");
	// 		$x['name_code_credit'] = $this->general_model->get_data_by_field('zacc_code_accounting','name'," code = '".$x['code_credit']."' ");
	// 		$desc = $this->general_model->get_data_by_field('zacc_journal_detail','desc'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND debet > 0 AND status IS NULL");
	// 		$value = $this->general_model->get_data_by_field('zacc_journal_detail','debet'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND debet > 0 AND status IS NULL");
	// 		if ( $desc == '' || $desc == null ){
	// 			$x['desc'] = $this->general_model->get_data_by_field('zacc_journal_detail','desc'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND credit > 0 AND status IS NULL");
	// 		} else {
	// 			$x['desc'] = $desc;
	// 		}
	// 		if ( (float) $value > 0 ){
	// 			$x['value'] = $value;
	// 		} else {
	// 			$x['value'] = $this->general_model->get_data_by_field('zacc_journal_detail','credit'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND credit > 0 AND status IS NULL");
	// 		}
	// 		if ( $x['code_debet'] == ''){
	// 			$x['first_code'] = substr($x['code_credit'], 0,1);
	// 		} else {
	// 			$x['first_code'] = substr($x['code_debet'], 0,1);
	// 		}
	// 		if ( $x['first_code'] == '0' ){
	// 			$x['id_company'] = 2;
	// 		} else {
	// 			$x['id_company'] = 1;
	// 		}
	// 		array_push($detail,$x);
	// 	}
	//
	// 	$arr = array(
	// 		'id_journal'=>$id_journal,
	// 		'journal'=>$journal,
	// 		'journal_detail'=>$detail,
	// 	);
	// 	echo json_encode($arr);
	// }
	//
	// public function get_detail_journal_html(){
	// 	$id_journal = $this->request->getPost('id_journal');
	// 	$journal	=	$this->journal_model->select_data('*',array('id'=>$id_journal));
	// 	$data_journal = $journal->row();
	// 	$detail = array();
	// 	$line = $this->general_model->select_distinct('zacc_journal_detail','line'," id_journal = '".$id_journal."' AND status IS NULL");
	// 	echo '<div class="col-md-12 text-center">';
	// 	echo '<h3><b>'.date('d M Y',strtotime($data_journal->date_trans)).'</b></h3>';
	// 	echo '<hr>';
	// 	echo '</div>';
	// 	echo '<div class="col-md-12 table-responsive">';
	// 	echo '<table class="table table-bordered">';
	// 	echo '<thead>';
	// 	echo '<tr>';
	// 	echo '<th>DEBET</th>';
	// 	echo '<th>KREDIT</th>';
	// 	echo '<th>KETERANGAN</td>';
	// 	echo '<th>NOMINAL</th>';
	// 	echo '</tr>';
	// 	echo '</thead>';
	// 	echo '<tbody>';
	// 	$total = 0;
	// 	foreach ($line->result() as $row) {
	// 		$x['line'] = $row->line;
	// 		$x['id_journal']=$id_journal;
	// 		$x['code_debet'] = $this->general_model->get_data_by_field('zacc_journal_detail','code_accounting'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND debet > 0 AND status IS NULL");
	// 		$x['code_credit'] = $this->general_model->get_data_by_field('zacc_journal_detail','code_accounting'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND credit > 0 AND status IS NULL");
	// 		$x['name_code_debet'] = $this->general_model->get_data_by_field('zacc_code_accounting','name'," code = '".$x['code_debet']."' ");
	// 		$x['name_code_credit'] = $this->general_model->get_data_by_field('zacc_code_accounting','name'," code = '".$x['code_credit']."' ");
	// 		$desc = $this->general_model->get_data_by_field('zacc_journal_detail','desc'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND debet > 0 AND status IS NULL");
	// 		$value = $this->general_model->get_data_by_field('zacc_journal_detail','debet'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND debet > 0 AND status IS NULL");
	// 		if ( $desc == '' || $desc == null ){
	// 			$x['desc'] = $this->general_model->get_data_by_field('zacc_journal_detail','desc'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND credit > 0 AND status IS NULL");
	// 		} else {
	// 			$x['desc'] = $desc;
	// 		}
	// 		if ( (float) $value > 0 ){
	// 			$x['value'] = $value;
	// 		} else {
	// 			$x['value'] = $this->general_model->get_data_by_field('zacc_journal_detail','credit'," id_journal = '".$id_journal."' AND line = '".$row->line."' AND credit > 0 AND status IS NULL");
	// 		}
	// 		if ( $x['code_debet'] == ''){
	// 			$x['first_code'] = substr($x['code_credit'], 0,1);
	// 		} else {
	// 			$x['first_code'] = substr($x['code_debet'], 0,1);
	// 		}
	// 		if ( $x['first_code'] == '0' ){
	// 			$x['id_company'] = 2;
	// 		} else {
	// 			$x['id_company'] = 1;
	// 		}
	// 		echo '<tr>';
	// 		echo '<td class="text-left">'.$x['code_debet'].' '.$x['name_code_debet'].'</td>';
	// 		echo '<td class="text-left">'.$x['code_credit'].' '.$x['name_code_credit'].'</td>';
	// 		echo '<td class="text-left">'.$x['desc'].'</td>';
	// 		echo '<td class="text-right">'.number_format($x['value'],2,'.',',').'</td>';
	// 		echo '</tr>';
	// 		$total += $x['value'];
	// 	}
	// 	echo '</tbody>';
	// 	echo '<tfoot>';
	// 	echo '<tr>';
	// 	echo '<th></th>';
	// 	echo '<th></th>';
	// 	echo '<th>TOTAL</td>';
	// 	echo '<th class="text-right">'.number_format($total,2,'.',',').'</th>';
	// 	echo '</tr>';
	// 	echo '</tfoot>';
	// 	echo '</table>';
	// 	echo '</div>';
	//
	// }
	//
	// public function edit_item_detail_journal(){
	// 	$this->authentication->is_login_ajax();
	// 	$this->form_validation->set_rules('id_journal', 'Journal', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('line', 'Line', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('code_debet', 'Kode Rek Debet', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('code_credit', 'Kode Rek Kredit', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('desc', 'Keterangan', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('value', 'Nominal', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	if ( $this->form_validation->run() == FALSE ) {
	// 		$this->form_validation->set_error_delimiters('', '');
	// 		$result = array('response'=>FALSE,'message'=> validation_errors('',''));
	// 		echo json_encode($result);
	// 	} else {
	// 		$id_journal 			= $this->request->getPost('id_journal');
	// 		$line 						= $this->request->getPost('line');
	// 		$code_debet 			= $this->request->getPost('code_debet');
	// 		$code_credit 			= $this->request->getPost('code_credit');
	// 		$desc 						=	$this->request->getPost('desc');
	// 		$value						=	$this->request->getPost('value');
	// 		if ($code_debet == $code_credit){
	// 			$result = array('response'=>FALSE,'message'=> 'Kode Rekening debet kredit tak boleh sama');
	// 			echo json_encode($result);
	// 			exit;
	// 		}
	//
	// 		$check_line = $this->general_model->count_rows('zacc_journal_detail','id'," id_journal = '".$id_journal."' AND line = '".$line."' AND status IS NULL ");
	// 		if ($check_line > 2 ){
	// 			$result = array('response'=>FALSE,'message'=> 'Gagal Jurnal.');
	// 			echo json_encode($result);
	// 			exit;
	// 		}
	// 		$this->db->trans_begin();
	//
	// 		$condition_debet = " id_journal = '".$id_journal."' AND line = '".$line."' AND debet > 0 AND status IS NULL ";
	// 		$check_debet = $this->general_model->count_rows('zacc_journal_detail','id',$condition_debet);
	// 		if ($check_debet > 0 ){
	// 			$data_debet = array(
	// 				'code_accounting'=>$code_debet,
	// 				'desc'=>$desc,
	// 				'credit'=>0,
	// 				'debet'=>$value,
	// 				'id_user_update'=>$this->id_user,
	// 				'date_update'=>date('Y-m-d H:i:s'),
	// 			);
	// 			$this->general_model->update('zacc_journal_detail',$data_debet,$condition_debet);
	// 		} else {
	// 			$data_debet = array(
	// 				'code_accounting'=>$code_debet,
	// 				'desc'=>$desc,
	// 				'credit'=>0,
	// 				'debet'=>$value,
	// 				'id_journal'=>$id_journal,
	// 				'line'=>$line,
	// 				'id_user'=>$this->id_user,
	// 				'date_insert'=>date('Y-m-d H:i:s'),
	// 				'id_user_update'=>$this->id_user,
	// 				'date_update'=>date('Y-m-d H:i:s'),
	// 			);
	// 			$this->general_model->save('zacc_journal_detail',$data_debet);
	// 		}
	//
	// 		$condition_credit = " id_journal = '".$id_journal."' AND line = '".$line."' AND credit > 0 AND status IS NULL ";
	// 		$check_credit = $this->general_model->count_rows('zacc_journal_detail','id',$condition_credit);
	// 		if ($check_credit > 0 ){
	// 			$data_credit = array(
	// 				'code_accounting'=>$code_credit,
	// 				'desc'=>$desc,
	// 				'credit'=>$value,
	// 				'debet'=>0,
	// 				'id_user_update'=>$this->id_user,
	// 				'date_update'=>date('Y-m-d H:i:s'),
	// 			);
	// 			$this->general_model->update('zacc_journal_detail',$data_credit,$condition_credit);
	// 		} else {
	// 			$data_credit = array(
	// 				'code_accounting'=>$code_credit,
	// 				'desc'=>$desc,
	// 				'credit'=>$value,
	// 				'debet'=>0,
	// 				'id_journal'=>$id_journal,
	// 				'line'=>$line,
	// 				'id_user'=>$this->id_user,
	// 				'date_insert'=>date('Y-m-d H:i:s'),
	// 				'id_user_update'=>$this->id_user,
	// 				'date_update'=>date('Y-m-d H:i:s'),
	// 			);
	// 			$this->general_model->save('zacc_journal_detail',$data_credit);
	// 		}
	//
	// 		if ($this->db->trans_status() === FALSE){
	// 			$this->db->trans_rollback();
	// 			$result = array('response'=>FALSE,'message'=> 'GAGAL SIMPAN','error'=>$error);
	// 		} else {
	// 			$this->db->trans_commit();
	// 			$result = array('response'=>TRUE,'message'=>'Berhasil','error'=>'');
	// 		}
	// 		echo json_encode($result);
	// 	}
	// }
	//
	// public function add_item_detail(){
	// 	$this->authentication->is_login_ajax();
	// 	$this->form_validation->set_rules('id_journal', 'Journal', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('code_debet', 'Kode Rek Debet', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('code_credit', 'Kode Rek Kredit', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('desc', 'Keterangan', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('value', 'Nominal', 'xss_clean|required|numeric',array('required'=>'%s harus diisi.'));
	// 	if ( $this->form_validation->run() == FALSE ) {
	// 		$this->form_validation->set_error_delimiters('', '');
	// 		$result = array('response'=>FALSE,'message'=> validation_errors('',''));
	// 		echo json_encode($result);
	// 	} else {
	// 		$id_journal 			= $this->request->getPost('id_journal');
	// 		$code_debet 			= $this->request->getPost('code_debet');
	// 		$code_credit 			= $this->request->getPost('code_credit');
	// 		$desc 						=	$this->request->getPost('desc');
	// 		$value						=	$this->request->getPost('value');
	//
	// 		if ($code_debet == $code_credit){
	// 			$result = array('response'=>FALSE,'message'=> 'Kode Rekening debet kredit tak boleh sama');
	// 			echo json_encode($result);
	// 			exit;
	// 		}
	//
	// 		$this->db->trans_begin();
	// 		$row = $this->general_model->count_rows('zacc_journal_detail','id'," id_journal = '".$id_journal."' AND status IS NULL");
	// 		$line = ( $row + 1 ) * 10;
	// 		$id_journal_detail = $this->journal->generate_journal_detail($this->id_user,$id_journal,$code_debet,$code_credit,$value,$line,$desc,null);
	//
	// 		if ($this->db->trans_status() === FALSE){
	// 			$this->db->trans_rollback();
	// 			$result = array('response'=>FALSE,'message'=> 'GAGAL SIMPAN','error'=>$error);
	// 		} else {
	// 			$this->db->trans_commit();
	// 			$result = array('response'=>TRUE,'message'=>'Berhasil','error'=>'');
	// 		}
	// 		echo json_encode($result);
	// 	}
	// }
	//
	// public function ajax_delete_item_detail(){
	// 	$this->authentication->is_login_ajax();
	// 	$this->form_validation->set_rules('id_journal', 'Journal', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('line', 'Baris', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	if ( $this->form_validation->run() == FALSE ) {
	// 		$this->form_validation->set_error_delimiters('', '');
	// 		$result = array('response'=>FALSE,'message'=> validation_errors('',''));
	// 		echo json_encode($result);
	// 	} else {
	// 		$id_journal = $this->request->getPost('id_journal');
	// 		$line 			= $this->request->getPost('line');
	// 		$data = array(
	// 			'status'=>'0',
	// 			'id_user_update'=>$this->id_user,
	// 			'date_update'=>date('Y-m-d H:i:s')
	// 		);
	// 		$this->general_model->update('zacc_journal_detail',$data," id_journal = '".$id_journal."' AND line = '".$line."' AND status IS NULL ");
	// 		$check_detail = $this->general_model->count_rows('zacc_journal_detail','id'," id_journal = '".$id_journal."' AND status IS NULL ");
	// 		if ( $check_detail === 0 ){
	// 			$this->general_model->update('zacc_journal',$data," id = '".$id_journal."'  ");
	// 		}
	// 		$result = array('response'=>TRUE,'message'=> 'Berhasil dihapus.');
	// 		echo json_encode($result);
	// 	}
	// }
	//
	// public function edit_code_item_detail_journal(){
	// 	$this->authentication->is_login_ajax();
	// 	$this->form_validation->set_rules('id_journal', 'Journal', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('line', 'Line', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('code_debet', 'Kode Rek Debet', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	$this->form_validation->set_rules('code_credit', 'Kode Rek Kredit', 'xss_clean|required',array('required'=>'%s harus diisi.'));
	// 	if ( $this->form_validation->run() == FALSE ) {
	// 		$this->form_validation->set_error_delimiters('', '');
	// 		$result = array('response'=>FALSE,'message'=> validation_errors('',''));
	// 		echo json_encode($result);
	// 	} else {
	// 		$id_journal 			= $this->request->getPost('id_journal');
	// 		$line 						= $this->request->getPost('line');
	// 		$code_debet 			= $this->request->getPost('code_debet');
	// 		$code_credit 			= $this->request->getPost('code_credit');
	// 		if ($code_debet == $code_credit){
	// 			$result = array('response'=>FALSE,'message'=> 'Kode Rekening debet kredit tak boleh sama');
	// 			echo json_encode($result);
	// 			exit;
	// 		}
	//
	// 		$check_line = $this->general_model->count_rows('zacc_journal_detail','id'," id_journal = '".$id_journal."' AND line = '".$line."' AND status IS NULL ");
	// 		if ($check_line > 2 ){
	// 			$result = array('response'=>FALSE,'message'=> 'Gagal Jurnal.');
	// 			echo json_encode($result);
	// 			exit;
	// 		}
	// 		$this->db->trans_begin();
	//
	// 		$condition_debet = " id_journal = '".$id_journal."' AND line = '".$line."' AND debet > 0 AND status IS NULL ";
	// 		$check_debet = $this->general_model->count_rows('zacc_journal_detail','id',$condition_debet);
	// 		if ($check_debet > 0 ){
	// 			$data_debet = array(
	// 				'code_accounting'=>$code_debet,
	// 				'id_user_update'=>$this->id_user,
	// 				'date_update'=>date('Y-m-d H:i:s'),
	// 			);
	// 			$this->general_model->update('zacc_journal_detail',$data_debet,$condition_debet);
	// 		}
	//
	// 		$condition_credit = " id_journal = '".$id_journal."' AND line = '".$line."' AND credit > 0 AND status IS NULL ";
	// 		$check_credit = $this->general_model->count_rows('zacc_journal_detail','id',$condition_credit);
	// 		if ($check_credit > 0 ){
	// 			$data_credit = array(
	// 				'code_accounting'=>$code_credit,
	// 				'id_user_update'=>$this->id_user,
	// 				'date_update'=>date('Y-m-d H:i:s'),
	// 			);
	// 			$this->general_model->update('zacc_journal_detail',$data_credit,$condition_credit);
	// 		}
	//
	// 		if ($this->db->trans_status() === FALSE){
	// 			$this->db->trans_rollback();
	// 			$result = array('response'=>FALSE,'message'=> 'GAGAL SIMPAN','error'=>$error);
	// 		} else {
	// 			$this->db->trans_commit();
	// 			$result = array('response'=>TRUE,'message'=>'Berhasil','error'=>'');
	// 		}
	// 		echo json_encode($result);
	// 	}
	// }

}
