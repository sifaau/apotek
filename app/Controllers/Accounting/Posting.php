<?php
namespace App\Controllers\Accounting;
use App\Controllers\BaseController;
use App\Models\General_model;
use App\Models\Saldo_model;
use App\Libraries\Journal;

class Posting extends BaseController {

  public function __construct() {
    date_default_timezone_set("Asia/Jakarta");
    $this->session = session();
    $this->general_model = new General_model();
    $this->saldo_model = new Saldo_model();
    $this->id_user = $this->session->get('id_user');
    $this->journal = new Journal();
    helper('code_accounting');
    helper('journal_accounting');
  }

  public function index(){
    $data = [
      'periode' => $this->general_model->select_order('zacc_periode','*','id > 0','id ASC'),
    ];
    $this->template->accounting_main_layout('posting',$data);
  }

  public function action_posting(){

    $auth = $this->authentication->is_login();
		if ($auth==FALSE){
			$result = array('response'=>FALSE,'message'=> 'Sesi anda habis.');
			echo json_encode($result);
			exit;
		}
		$input = $this->validate([
			'periode' => for_validate('Periode','required'),
		]);
    if (!$input) {
			$result = array('response'=>FALSE,'message'=> $this->validator->listErrors());
			echo json_encode($result);
		} else {
			$max_level = code_accounting_max_level();
			$id_company 							= ($this->request->getPost('perusahaan'));
      if ($id_company == '' || $id_company == 'null' || $id_company == NULL ){
        $id_company = NULL;
      }
			// $id_company 							= $this->general_model->get_data_by_field('company','id',array('kode_surat'=>$perusahaan));
			$id_periode 							= ($this->request->getPost('periode'));
			$cek_buku_besar_close 		= $this->general_model->count_rows('zacc_periode','id',array('closing'=>1,'id'=>$id_periode));
			if ( $cek_buku_besar_close > 0 ){
				$result = array('response'=>false,'message'=> 'BUKU BESAR SUDAH DITUTUP PADA BULAN INI..');
				echo json_encode($result);
			}  else {
				// $this->benchmark->mark('code_start');
				$month 										=	get_bulan_by_id_periode($id_periode);
				$year 										=	get_tahun_by_id_periode($id_periode);
				$awal_bulan = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
				$akhir_bulan = date('Y-m-t',strtotime($awal_bulan));
				$get_date_last_posting = $this->general_model->get_data_by_field('zacc_periode','date_posting',array('month'=>$month,'year'=>$year));
				$cek_last_posting = 0;

				if ($get_date_last_posting == '' || $get_date_last_posting == null){
					$cek_last_posting = 1;
				} else {
					$cek_tgl_posting_sebelumnya = $this->general_model->count_rows('zacc_periode','id'," year > 2020 AND id < '".$id_periode."' AND date_posting > '".$get_date_last_posting."'");

					$qdate_last_posting = $this->general_model->select_order_limit('zacc_code_accounting AS c,zacc_saldo AS s','s.date_insert'," c.code = s.code_accounting AND s.id_periode = '".$id_periode."' ","s.date_insert DESC",1);
					if ($qdate_last_posting->getNumRows() > 0){
						$ddate_last_posting = $qdate_last_posting->getRow();
						$date_last_posting = $ddate_last_posting->date_insert;
					} else {
						$date_last_posting = $get_date_last_posting;
					}
					$cek_last_data = $this->general_model->count_rows('zacc_journal as j, zacc_journal_detail as d,zacc_code_accounting as c','d.id',"j.id = d.id_journal
					AND c.code = d.code_accounting
					AND j.status IS NULL
					AND d.status IS NULL
					AND ( DATE(j.date_trans) BETWEEN '".$awal_bulan."' AND '".$akhir_bulan."' )

					AND d.date_insert > '".$date_last_posting."'");

					$cek_last_posting = $cek_last_posting + $cek_tgl_posting_sebelumnya + $cek_last_data;
				}
				$cek_last_posting = 1;
				if ($cek_last_posting > 0){

					$id_previous_periode 			= get_id_periode_bulan_lalu($month,$year);

          // $con_where_parent 	= " id_company = '".$id_company."' ";
					// $con_where_parent2 	= " AND zacc_code_accounting.id_company = '".$id_company."' ";
					// $con_where_parent3 	= "  ";

          $con_where_parent 	= "";
					$con_where_parent2 	= "";
					$con_where_parent3 	= "";

					$list_code_accounting 		= $this->general_model->select_data('zacc_code_accounting','code,balance,level,type_report'," level = '".$max_level."' AND status IS NULL ".$con_where_parent);
					$save_saldo 							= TRUE;
					$this->db->transBegin();

					// hapus laba rugi periode sampah
					// $condition_jurnal_lama_laba_rugi_periode 	= "zacc_journal.id = zacc_journal_detail.id_journal AND zacc_journal_detail.desc = 'LABA RUGI PERIODE' AND MONTH(zacc_journal.date_trans) = '".$month."' AND YEAR(zacc_journal.date_trans) = '".$year."'".$con_where_parent2;
					$condition_jurnal_lama_laba_rugi_periode 	= "zacc_journal.id = zacc_journal_detail.id_journal AND zacc_code_accounting.code = zacc_journal_detail.code_accounting AND zacc_journal_detail.desc = 'LABA RUGI PERIODE' AND ( DATE(zacc_journal.date_trans) BETWEEN '".$awal_bulan."'  AND '".$akhir_bulan."' ) ".$con_where_parent2;
					$journal_lama	= $this->general_model->select_data('zacc_journal,zacc_journal_detail,zacc_code_accounting',"zacc_journal.id",$condition_jurnal_lama_laba_rugi_periode);
					foreach ($journal_lama->getResult() as $row) {
						$this->general_model->do_delete('zacc_journal_detail',array('id_journal'=>$row->id));
						$this->general_model->do_delete('zacc_journal',array('id'=>$row->id));
					}

					// hapus jurnal2 sampah
					// $condition_jurnal_lama_laba_rugi_periodex 	= "zacc_journal.id = zacc_journal_detail.id_journal AND zacc_journal_detail.desc = 'LABA RUGI PERIODE' AND MONTH(zacc_journal.date_trans) = '01' AND YEAR(zacc_journal.date_trans) = '1970' ".$con_where_parent2;
					$condition_jurnal_lama_laba_rugi_periodex 	= "zacc_journal.id = zacc_journal_detail.id_journal AND zacc_code_accounting.code = zacc_journal_detail.code_accounting AND YEAR(zacc_journal.date_trans) < 1990 AND zacc_journal_detail.desc = 'LABA RUGI PERIODE' ".$con_where_parent2;
					$journal_lamax	= $this->general_model->select_data('zacc_journal,zacc_journal_detail,zacc_code_accounting',"zacc_journal.id",$condition_jurnal_lama_laba_rugi_periodex);
					foreach ($journal_lamax->getResult() as $rowx) {
						$this->general_model->do_delete('zacc_journal_detail',array('id_journal'=>$rowx->id));
						$this->general_model->do_delete('zacc_journal',array('id'=>$rowx->id));
					}
					// $condition_jurnal_lama_laba_rugi_periodexx 	= "zacc_journal.id = zacc_journal_detail.id_journal AND zacc_journal_detail.desc = 'LABA RUGI DITAHAN' AND MONTH(zacc_journal.date_trans) = '01' AND YEAR(zacc_journal.date_trans) = '1970' ".$con_where_parent2;
					$condition_jurnal_lama_laba_rugi_periodexx 	= "zacc_journal.id = zacc_journal_detail.id_journal AND zacc_code_accounting.code = zacc_journal_detail.code_accounting AND YEAR(zacc_journal.date_trans) < 1990 AND zacc_journal_detail.desc = 'LABA RUGI DITAHAN'  ".$con_where_parent2;
					$journal_lamaxx	= $this->general_model->select_data('zacc_journal,zacc_journal_detail,zacc_code_accounting',"zacc_journal.id",$condition_jurnal_lama_laba_rugi_periodexx);
					foreach ($journal_lamaxx->getResult() as $rowxx) {
						$this->general_model->do_delete('zacc_journal_detail',array('id_journal'=>$rowxx->id));
						$this->general_model->do_delete('zacc_journal',array('id'=>$rowxx->id));
					}
					// end hapus jurnal sampah

					$this->general_model->do_delete('zacc_journal_detail'," desc = 'LABA RUGI PERIODE' AND status = '0' ");
					$this->general_model->do_delete('zacc_journal_detail'," desc = 'LABA RUGI DITAHAN' AND status = '0' ");

					$q_l_p = $this->general_model->do_query("SELECT
						d.id,d.id_journal
						FROM
						zacc_journal_detail AS d,
						zacc_journal AS j,
						zacc_code_accounting AS c
						WHERE
						DATE(j.date_trans) BETWEEN '".$awal_bulan."' AND '".$akhir_bulan."'
						AND d.id_journal = j.id
						AND c.code = d.code_accounting
						and d.desc = 'LABA RUGI PERIODE'
						and d.status is null
						and j.status is null
						".$con_where_parent3
					);

					foreach ($q_l_p->getResult() as $value) {
						$this->general_model->do_delete('zacc_journal_detail',array('id'=>$value->id));
						$this->general_model->do_delete('zacc_journal',array('id'=>$value->id_journal));
					}

					// menghapus jurnal laba ditahan rumus lama
					$desc_laba_ditahan 									= 'LABA RUGI DITAHAN';
					$condition_jurnal_lama_laba_ditahan 	= "  j.id = d.id_journal AND c.code = d.code_accounting  AND d.desc = '".$desc_laba_ditahan."' AND ( DATE(j.date_trans) BETWEEN '".$awal_bulan."' AND '".$akhir_bulan."' ) ";
					$journal_lama_laba_ditahan 					= $this->general_model->select_data('zacc_journal AS j,zacc_journal_detail AS d,zacc_code_accounting AS c',"j.id",$condition_jurnal_lama_laba_ditahan);
					foreach ($journal_lama_laba_ditahan->getResult() as $row_laba_ditahan) {
						$this->general_model->do_delete('zacc_journal_detail',array('id_journal'=>$row_laba_ditahan->id));
						$this->general_model->do_delete('zacc_journal',array('id'=>$row_laba_ditahan->id));
					}
					// end menghapus jurnal laba ditahan.

					if ( $month == '01' ){
						$this->generate_laba_rugi_ditahan_with_id_company($id_periode,$id_previous_periode,$month,$year,$id_company);
					}


					$this->general_laba_rugi_sd_bulan_lalu_with_id_company($id_periode,$id_previous_periode,$month,$year,$id_company,null);

					foreach ($list_code_accounting->getResult() as $row) {
						$saldo = 0;
						// menghitung laba rugi periode pendapatan dan biaya
						$get_code_parent_level_1 = substr($row->code,0,2);
						$check_type_report_parent_level_1 = $this->general_model->get_data_by_field('zacc_code_accounting','type_report',array('code'=>$get_code_parent_level_1));
						if ($check_type_report_parent_level_1 == 'LR') {
							$this->generate_laba_periode_by_company($id_company,$month,$year,$id_previous_periode,$row->code,$row->balance);
						}
						$sum_debet		= get_sum_journal_by_pos($month,$year,$row->code,'D');
						$sum_credit		= get_sum_journal_by_pos($month,$year,$row->code,'K');
						$saldo = $this->hitung_saldo_akhir($id_previous_periode,$row->code,$row->balance,$sum_debet,$sum_credit);
						$this->journal->insert_saldo_code_accounting($id_periode,$row->code,$sum_debet,$sum_credit,$saldo);

					}

					$this->posting_laba_rugi_periode_with_id_company($month,$year,$id_periode,$id_previous_periode,$id_company);
					// generate laba rugi ditahan

					// $this->benchmark->mark('code_end');
					// $benchmark = $this->benchmark->elapsed_time('code_start', 'code_end');
          $benchmark = 0;

					if ($this->db->transStatus() === FALSE) {
						$result = array('response'=>false,'message'=> 'Posting Gagal'.$benchmark);
						$this->db->transRollback();
					} else {
						$result = array('response'=>true,'message'=> 'Posting '.$month.'-'.$year.' selesai, ('.$benchmark.' detik.)');
						$this->db->transCommit();
					}


					$this->general_model->do_update('zacc_periode',array('id_user'=>$this->id_user,'status_posting'=>1,'date_posting'=>date('Y-m-d H:i:s')),array('id'=>$id_periode));
				} else {
					$result = array('response'=>false,'message'=> 'Sudah di Posting, belum ada perubahan data untuk bulan '.$month.'-'.$year.'. Coba posting perusahan lain.');
				}
				echo json_encode($result);
			}

		}
	}

  public function generate_laba_rugi_ditahan_with_id_company($id_periode,$id_previous_periode,$month,$year,$id_company){

		// hapus jurnal lama
		$desc 									= 'LABA RUGI DITAHAN';
		$line 									= 1;
		$code_laba_ditahan 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_ditahan'));
		$check_turunan_code = $this->general_model->count_rows('zacc_code_accounting','id'," code_parent = '".$code_laba_ditahan."' AND status IS NULL");

		$awal_bulan = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
		if ($check_turunan_code > 0){

			$code_acc = $this->general_model->select_data('zacc_code_accounting','code,balance,project'," code_parent = '".$code_laba_ditahan."' AND status IS NULL ");
			foreach ($code_acc->getResult() as $row) {
				if ( $row->project == '' OR $row->project == null OR $row->project == '10' OR $row->project == '12' OR (int)$row->project < 7 ){
					$con_proyek = " AND ( project IS NULL OR project = '' OR project = 'UMUM' OR project = 'ALL' ) ";
				} else {
					$con_proyek = " AND project = '".$row->project."' ";
				}
				$code_parent_laba_periode 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_periode'));
				$q_code_laba_periode = $this->general_model->select_data('zacc_code_accounting','code,balance,project'," code_parent = '".$code_parent_laba_periode."' AND status IS NULL ".$con_proyek);
				if ($q_code_laba_periode->num_rows() === 1){
					$d_code_laba_periode = $q_code_laba_periode->getRow();
					$code_laba_periode = $d_code_laba_periode->code;
					$code_debet							= $code_laba_periode;
					$code_credit 						= $row->code;
					$check_saldo_previous_periode_laba_rugi_periode = $this->general_model->count_rows('zacc_saldo','id',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode));
					if ( $check_saldo_previous_periode_laba_rugi_periode >  0) {
						$previous_saldo_laba_rugi_periode = $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode));
					} else {
						$previous_saldo_laba_rugi_periode = 0 ;
					}

					if ($previous_saldo_laba_rugi_periode < 0){
						$saldo 			= -$previous_saldo_laba_rugi_periode;
						$debet 			= $code_credit;
						$credit 		= $code_debet;
					} else {
						$saldo			= $previous_saldo_laba_rugi_periode;
						$debet 			= $code_debet;
						$credit 		= $code_credit;
					}

					$id_journal = $this->journal->generate_id_journal(0,$awal_bulan,'LRT',$kode_bb=null);
					$this->journal->generate_journal_detail(0,$id_journal,$debet,$credit,$saldo,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
				}

				$code_parent_laba_periode_bulan_lalu 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_sd_periode_lalu'));
				$q_code_laba_periode_bulan_lalu = $this->general_model->select_data('zacc_code_accounting','code,balance,project'," code_parent = '".$code_parent_laba_periode_bulan_lalu."' AND status IS NULL ".$con_proyek);
				if ($q_code_laba_periode_bulan_lalu->num_rows() === 1){
					$d_code_laba_periode_bulan_lalu = $q_code_laba_periode_bulan_lalu->getRow();
					$code_laba_periode_bulan_lalu = $d_code_laba_periode_bulan_lalu->code;
					$code_debet							= $code_laba_periode_bulan_lalu;
					$code_credit 						= $row->code;
					$check_saldo_previous_periode_laba_rugi_periode_bulan_lalu = $this->general_model->count_rows('zacc_saldo','id',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode_bulan_lalu));
					if ( $check_saldo_previous_periode_laba_rugi_periode_bulan_lalu >  0) {
						$previous_saldo_laba_rugi_periode_bulan_lalu = $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode_bulan_lalu));
					} else {
						$previous_saldo_laba_rugi_periode_bulan_lalu = 0 ;
					}

					if ($previous_saldo_laba_rugi_periode_bulan_lalu < 0){
						$saldo 			= -$previous_saldo_laba_rugi_periode_bulan_lalu;
						$debet 			= $code_credit;
						$credit 		= $code_debet;
					} else {
						$saldo 			= $previous_saldo_laba_rugi_periode_bulan_lalu;
						$debet 			= $code_debet;
						$credit 		= $code_credit;
					}
					$id_journal = $this->journal->generate_id_journal(0,$awal_bulan,'LRT',$kode_bb=null);
					$this->journal->generate_journal_detail(0,$id_journal,$debet,$credit,$saldo,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
				}

			}

		} else {


			$code_laba_periode 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_periode'));
			if ( $code_laba_periode == '' OR $code_laba_periode == null){

			} else {
				$code_debet							= $code_laba_periode;
				$code_credit 						= $code_laba_ditahan;
				$check_saldo_previous_periode_laba_rugi_periode = $this->general_model->count_rows('zacc_saldo','id',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode));
				if ( $check_saldo_previous_periode_laba_rugi_periode >  0) {
					$previous_saldo_laba_rugi_periode = $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode));
				} else {
					$previous_saldo_laba_rugi_periode = 0 ;
				}

				if ($previous_saldo_laba_rugi_periode < 0){
					$saldo 			= -$previous_saldo_laba_rugi_periode;
					$debet 			= $code_credit;
					$credit 		= $code_debet;
				} else {
					$saldo			= $previous_saldo_laba_rugi_periode;
					$debet 			= $code_debet;
					$credit 		= $code_credit;
				}

				$id_journal = $this->journal->generate_id_journal(0,$awal_bulan,'LRT',$kode_bb=null);
				$this->journal->generate_journal_detail(0,$id_journal,$debet,$credit,$saldo,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
			}

			$code_laba_periode_bulan_lalu 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_sd_periode_lalu'));
			if ($code_laba_periode_bulan_lalu == '' OR $code_laba_periode_bulan_lalu == NULL){

			} else {
				$code_debet							= $code_laba_periode_bulan_lalu;
				$code_credit 						= $code_laba_ditahan;
				$check_saldo_previous_periode_laba_rugi_periode_bulan_lalu = $this->general_model->count_rows('zacc_saldo','id',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode_bulan_lalu));
				if ( $check_saldo_previous_periode_laba_rugi_periode_bulan_lalu >  0) {
					$previous_saldo_laba_rugi_periode_bulan_lalu = $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode_bulan_lalu));
				} else {
					$previous_saldo_laba_rugi_periode_bulan_lalu = 0 ;
				}

				if ($previous_saldo_laba_rugi_periode_bulan_lalu < 0){
					$saldo 			= -$previous_saldo_laba_rugi_periode_bulan_lalu;
					$debet 			= $code_credit;
					$credit 		= $code_debet;
				} else {
					$saldo 			= $previous_saldo_laba_rugi_periode_bulan_lalu;
					$debet 			= $code_debet;
					$credit 		= $code_credit;
				}
				$id_journal = $this->journal->generate_id_journal(0,$awal_bulan,'LRT',$kode_bb=null);
				$this->journal->generate_journal_detail(0,$id_journal,$debet,$credit,$saldo,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
			}

		}

	}

  public function general_laba_rugi_sd_bulan_lalu_with_id_company($id_periode,$id_previous_periode,$month,$year,$id_company){
		$first_date = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
		$line = 1;
		$desc 				= 'LABA RUGI PERIODE SD BULAN LALU';
		$desc2 				= 'LABA RUGI PERIODE';

		$awal_bulan = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
		$akhir_bulan = date('Y-m-t',strtotime($awal_bulan));
		// hapus jurnal lama
		$condition_jurnal_lama 	= "j.id = d.id_journal AND d.code_accounting = c.code
		AND ( DATE(j.date_trans) BETWEEN '".$awal_bulan."' AND '".$akhir_bulan."' ) AND ( d.desc = '".$desc."' OR d.desc = '".$desc2."' ) ";
		$journal_lama 					= $this->general_model->select_data('zacc_journal AS j,zacc_journal_detail AS d,zacc_code_accounting AS c',"j.id",$condition_jurnal_lama);
		foreach ($journal_lama->getResult() as $rowjl) {
			$this->general_model->do_delete('zacc_journal_detail',array('id_journal'=>$rowjl->id));
			$this->general_model->do_delete('zacc_journal',array('id'=>$rowjl->id));
		}

		$code_parent_laba_periode 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_periode'));
		$code_parent_laba_sd_periode_lalu 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_sd_periode_lalu'));
		$check_turunan_code = $this->general_model->count_rows('zacc_code_accounting','id'," code_parent = '".$code_parent_laba_sd_periode_lalu."' AND status IS NULL");
		if ($check_turunan_code > 0){
			$code_acc = $this->general_model->select_data('zacc_code_accounting','code,balance,project',"code_parent = '".$code_parent_laba_sd_periode_lalu."' AND status IS NULL ");
			foreach ($code_acc->getResult() as $row){
				if ( $row->project == '' OR $row->project == null OR $row->project == '10' OR $row->project == '12' OR (int)$row->project < 7 ){
					$con_proyek = " AND ( project IS NULL OR project = '' OR project = 'UMUM' OR project = 'ALL' ) ";
				} else {
					$con_proyek = " AND project = '".$row->project."' ";
				}
				$code_laba_periode 	= $this->general_model->get_data_by_field('zacc_code_accounting','code',"code_parent = '".$code_parent_laba_periode."' AND status IS NULL ".$con_proyek);
				$previous_saldo 		= $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_laba_periode));
				$code_debet 							= $code_laba_periode;
				$code_credit  						=	$row->code; // code laba bulan lalu
				if ($previous_saldo < 0){
					$saldo 			= -$previous_saldo;
					$debet 			= $code_credit;
					$credit 		= $code_debet;
				} else {
					$saldo			= $previous_saldo;
					$debet 			= $code_debet;
					$credit 		= $code_credit;
				}
				$id_journal 				= $this->journal->generate_id_journal(0,$first_date,'LRP',$kode_bb=null);
				$this->journal->generate_journal_detail(0,$id_journal,$debet,$credit,$saldo,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);

				$sum_debet	= get_sum_journal_by_pos($month,$year,$code_laba_periode,'D');
				$sum_credit	= get_sum_journal_by_pos($month,$year,$code_laba_periode,'K');
				$balancee = $this->general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code_laba_periode));
				$saldo 			= $this->hitung_saldo_akhir($id_previous_periode,$code_laba_periode,$balancee,$sum_debet,$sum_credit);
				$this->journal->insert_saldo_code_accounting($id_periode,$code_laba_periode,$sum_debet,$sum_credit,$saldo);

			}
		} else {
			$previous_saldo = $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_parent_laba_periode));
			$code_debet 		= $code_parent_laba_periode;
			$code_credit  	=	$code_parent_laba_sd_periode_lalu;

			if ($previous_saldo < 0){
				$saldo 			= -$previous_saldo;
				$debet 			= $code_credit;
				$credit 		= $code_debet;
			} else {
				$saldo			= $previous_saldo;
				$debet 			= $code_debet;
				$credit 		= $code_credit;
			}

			$id_journal 		= $this->journal->generate_id_journal(0,$first_date,'LRP',$kode_bb=null);
			$this->journal->generate_journal_detail(0,$id_journal,$debet,$credit,$saldo,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);

			$balance = $this->general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code_parent_laba_periode));
			$sum_debet	= get_sum_journal_by_pos($month,$year,$code_parent_laba_periode,'D');
			$sum_credit	= get_sum_journal_by_pos($month,$year,$code_parent_laba_periode,'K');
			$saldo 			= $this->hitung_saldo_akhir($id_previous_periode,$code_parent_laba_periode,$balance,$sum_debet,$sum_credit);
			$this->journal->insert_saldo_code_accounting($id_periode,$code_parent_laba_periode,$sum_debet,$sum_credit,$saldo);

		}
	}

  public function hitung_saldo_akhir($id_previous_periode,$code,$balance,$sum_debet,$sum_credit){
		$saldo = 0;
		$check_saldo_previous_periode = $this->general_model->count_rows('zacc_saldo','id',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code));
		if ( $check_saldo_previous_periode >  0) {
			$previous_saldo = $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code));
		} else {
			$previous_saldo = 0 ;
		}

		if ( $balance == 'D'){
			$saldo = $previous_saldo + ( $sum_debet - $sum_credit );
		}
		if ( $balance == 'K'){
			$saldo = $previous_saldo + ( $sum_credit - $sum_debet );
		}
		return $saldo;
	}

  public function generate_laba_periode_by_company($id_company,$month,$year,$id_previous_periode,$code,$balance){
		$get_code_parent_level_1 = substr($code,0,2);
		$desc 				= 'LABA RUGI PERIODE';
		// go
		$sum_debet		= get_sum_journal_by_pos($month,$year,$code,'D');
		$sum_credit		= get_sum_journal_by_pos($month,$year,$code,'K');
		$saldo 				= $this->hitung_saldo_akhir($id_previous_periode,$code,$balance,$sum_debet,$sum_credit);
		if ($saldo <> 0){
			$last_date 					= date('Y-m-t',strtotime($year.'-'.$month.'-01'));
			$proyek     				= $this->general_model->get_data_by_field('zacc_code_accounting','project',array('code'=>$code));
			if ( $proyek == '' OR $proyek == null OR $proyek == '10' OR $proyek == '12' OR (int)$proyek < 7 ){
				$proyek = 'ALL';
			}

			$code_laba_periode 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_periode'));
			$check_turunan_code = $this->general_model->count_rows('zacc_code_accounting','id'," code_parent = '".$code_laba_periode."' AND status IS NULL");
			if ($check_turunan_code > 0){
				if ($proyek === 'ALL'){
					$con_proyek = " AND ( project IS NULL OR project = '' OR project = 'UMUM' OR project = 'ALL' ) ";
					$code_laba_periode  = $this->general_model->get_data_by_field('zacc_code_accounting','code'," code_parent = '".$code_laba_periode."' AND status IS NULL ".$con_proyek );
				} else {
					$code_laba_periode  = $this->general_model->get_data_by_field('zacc_code_accounting','code'," code_parent = '".$code_laba_periode."' AND status IS NULL AND project = '".$proyek."'");
				}
			}

			if ($balance == 'K'){
				$code_debet 	= $code;
				$code_credit 	= $code_laba_periode;
			}
			if ($balance == 'D'){
				$code_debet 	= $code_laba_periode;
				$code_credit 	= $code;
			}
			if ($code_debet == '' OR $code_credit == '' OR $code_debet == NULL OR $code_credit == NULL){
				// alert jika kode rekening kosong
			} else {
				$line 				= 1;
				// insert journal baru;
				if ($saldo < 0){
					$saldo 			= -$saldo;
					$debet 			= $code_credit;
					$credit 		= $code_debet;
				} else {
					$debet 			= $code_debet;
					$credit 		= $code_credit;
				}
				$id_journal = $this->journal->generate_id_journal(0,$last_date,'LP',$kode_bb=null);
				$this->journal->generate_journal_detail(0,$id_journal,$debet,$credit,$saldo,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null);
			}
		}
	}

  public function posting_laba_rugi_periode_with_id_company($month,$year,$id_periode,$id_previous_periode,$id_company){
		$code_laba_periode 	= $this->general_model->get_data_by_field('zacc_setting','value',array('param'=>'code_parent_laba_periode'));
		$check_turunan_code = $this->general_model->count_rows('zacc_code_accounting','id'," code_parent = '".$code_laba_periode."' AND status IS NULL");
		if ($check_turunan_code > 0){
			$code_acc = $this->general_model->select_data('zacc_code_accounting','code,balance,project',"code_parent = '".$code_laba_periode."' AND status IS NULL");
			foreach ($code_acc->result() as $row){
				// posting akun laba rugi periode
				if ( $row->project == '' OR $row->project == null OR $row->project == '10' OR $row->project == '12' OR (int)$row->project < 7 ){
					$proyek = 'ALL';
				} else {
					$proyek = $row->project;
				}

				$sum_debet	= get_sum_journal_by_pos($month,$year,$row->code,'D');
				$sum_credit	= get_sum_journal_by_pos($month,$year,$row->code,'K');
				$saldo 			= $this->hitung_saldo_akhir($id_previous_periode,$row->code,$row->balance,$sum_debet,$sum_credit);
				$this->journal->insert_saldo_code_accounting($id_periode,$row->code,$sum_debet,$sum_credit,$saldo);

			}
		} else {
			$balance = $this->general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code_laba_periode));

			$sum_debet	= get_sum_journal_by_pos($month,$year,$code_laba_periode,'D');
			$sum_credit	= get_sum_journal_by_pos($month,$year,$code_laba_periode,'K');
			$saldo 			= $this->hitung_saldo_akhir($id_previous_periode,$code_laba_periode,$balance,$sum_debet,$sum_credit);
			$this->journal->insert_saldo_code_accounting($id_periode,$code_laba_periode,$sum_debet,$sum_credit,$saldo);

		}

	}


}
