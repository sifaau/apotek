<?php
namespace App\Controllers\Accounting;
use App\Controllers\BaseController;
use App\Models\General_model;
use App\Models\Journal_model;

class Ledger extends BaseController {

	public function __construct() {
		date_default_timezone_set("Asia/Jakarta");
		$this->session = session();
		$this->general_model = new General_model();
    $this->journal_model = new Journal_model();
		$this->id_user = $this->session->get('id_user');
	}

  public function search($param=null){
    $auth = $this->authentication->is_login();
		if ($auth==FALSE){
			return redirect()->to('aptauth');
		}
		$num							=	array('-1-','-2-','-3-','-4-','-5-','-6-','-7-','-8-','-9-','-10-','-11-','-12-');
		$day							=	array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$type_filter 			=  ($this->request->getGet('type_filter'));
		$id_periode 			=  ($this->request->getGet('periode'));
		$id_periode2 			=  ($this->request->getGet('periode2'));
		$start_year 			=  ($this->request->getGet('start_year'));
		$end_year 				=  ($this->request->getGet('end_year'));
		$proyek 					=  ($this->request->getGet('proyek'));
		$code_accounting	=	 ($this->request->getGet('code_accounting'));
		$desc							=	 ($this->request->getGet('desc'));
		$date_start  			= '';
		$date_end  				= '';
		$type_filter 			= '0';

		if ($type_filter == '0') {
			$month 			= $this->general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode));
			$year 			= $this->general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode));
			$month2 		= $this->general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode2));
			$year2 			= $this->general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode2));
			$date_start = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
			$date_end 	= date('Y-m-t',strtotime($year2.'-'.$month2.'-27'));
			// $condition 	= " MONTH(zacc_journal.`date_trans`) = '".$month."' AND YEAR(zacc_journal.`date_trans`) = '".$year."' AND zacc_journal_detail.code_accounting = '".$code_accounting."' AND zacc_journal_detail.desc LIKE '%".$desc."%' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
			$condition  = " DATE(zacc_journal.`date_trans`) >= '".$date_start."' AND DATE(zacc_journal.`date_trans`) <= '".$date_end."' AND zacc_journal_detail.code_accounting = '".$code_accounting."' AND zacc_journal_detail.desc LIKE '%".$desc."%' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL";
			if ( ( $id_periode !=NULL OR $id_periode !='' ) AND ( $code_accounting !=NULL OR $code_accounting !='')) {
				$check_periode = $this->general_model->select_data('zacc_periode','month,year',array('id'=>$id_periode));
				if ($check_periode->getNumRows() === 0 ){
					return redirect()->to('aptauth/logout');
				}

				$year_periode 									= $this->general_model->get_data_by_field('zacc_periode','year',array('id'=>$id_periode));
				$month_periode  								= $this->general_model->get_data_by_field('zacc_periode','month',array('id'=>$id_periode));

				$month_periode_before 					= date('m-Y',strtotime($year_periode.'-'.$month_periode." -1 month"));
				$explode_month_periode_before 	= explode('-', $month_periode_before);
				$month_before 									= $explode_month_periode_before[0];
				$year_before 										=	$explode_month_periode_before[1];
				$id_previous_periode 						= $this->general_model->get_data_by_field('zacc_periode','id',array('month'=>$month_before,'year'=>$year_before));
				$saldo_awal 										= $this->general_model->get_data_by_field('zacc_saldo','saldo',array('id_periode'=>$id_previous_periode,'code_accounting'=>$code_accounting));
				$text_periode										=	$id_periode == $id_periode2 ? ' '.str_replace($num,$day,'-'.$month.'-').' '.$year : ' '.str_replace($num,$day,'-'.$month.'-').' '.$year.' - '.str_replace($num,$day,'-'.$month2.'-').' '.$year2;
				$text_saldo_awal 								=	'Rp '.number_format($saldo_awal, 0, ',','.');
				$name_code 	= $this->general_model->get_data_by_field('zacc_code_accounting','name',array('code'=>$code_accounting));
				if ($name_code == ''){
          $text_code_accounting	= '';
				} else {
					$text_code_accounting	= $code_accounting.' '.$name_code;
				}
			} else {
				$text_periode						=	'';
				$text_code_accounting		=	'-';
				$text_saldo_awal 				=	'-';
				$saldo_awal							=	0;
			}
		} else {
			$condition 							= "zacc_journal.`id` IS NULL AND zacc_journal_detail.status IS NULL";
			$text_periode						=	'';
			$text_code_accounting		=	'-';
			$text_saldo_awal 				= '-';
			$saldo_awal							=	0;
		}
    $saldo_awal = $saldo_awal == '' ? 0 : $saldo_awal;

		//229.707.794,23
    $order_query = "DATE(zacc_journal.date_trans) ASC,DATE(zacc_journal.date_insert) ASC";
		$jml 											= $this->general_model->count_rows('zacc_journal,zacc_journal_detail','zacc_journal_detail.id'," zacc_journal.id=zacc_journal_detail.id_journal AND ".$condition);
    $list 										= $this->journal_model->list_ledger_no_page($condition,$order_query);


		helper('journal_accounting');
		// $saldo_awal_sementara = sum_transaksi_from_journal_detail_by_code_accounting_and_periode_by_limit($id_periode,$code_accounting,$limit,$proyek);
		// $saldo_awal_sementara = sum_transaksi_from_journal_detail_by_code_accounting_and_two_date_by_limit($date_start,$date_end,$code_accounting,$limit);
		$data=array(
			'jumlah_baris'		=>	$jml,
			'list'						=>	$list,
			'periode'					=>	$this->general_model->select_data('zacc_periode','id,month,year','id IS NOT NULL'),
			'text_info'				=>	'',
			'code_accounting'	=>	'',
			'text_code_accounting'	=>	$text_code_accounting,
			'text_periode'		=>	$text_periode,
			'text_saldo_awal'	=>	$text_saldo_awal,
			'saldo_awal'			=>	$saldo_awal,
			'num'							=>	$num,
			'day'							=>	$day,
			'id_periode'			=>	$id_periode,
			'id_periode2'			=>	$id_periode2,
			'date_start'			=>	$date_start,
			'date_end'				=>	$date_end,
			'id_code_accounting'	=>	$code_accounting,
			'type_filter'			=>	$type_filter,
			'condition'				=>	$condition,
		);
		$this->template->accounting_main_layout('journal/ledger',$data);
	}


	public function list_ref_ledger(){
		$auth = $this->authentication->is_login();
		if ($auth==FALSE){
			$arr=array(
				'code_ref'=>'FALSE',
			);
			echo json_encode($arr);
			exit;
		}
		helper('code_accounting');
		$id_journal_detail 	= ($this->request->getPost('id_journal_detail'));
		$id_journal 				= ($this->request->getPost('id_journal'));
		$line 							= ($this->request->getPost('line'));
		$condition					=	"AND zacc_journal.id='".$id_journal."' AND zacc_journal_detail.id_journal = '".$id_journal."' AND zacc_journal_detail.line = '".$line."' ";
		$query 							= $this->journal_model->list_ref_for_ledger($condition);
		$debet 							= $this->general_model->get_data_by_field('zacc_journal_detail','debet',array('id'=>$id_journal_detail));
		$credit 						= $this->general_model->get_data_by_field('zacc_journal_detail','credit',array('id'=>$id_journal_detail));
		$code_ref 					= '';
		foreach ( $query->getResult() as $ref ){
			if ($debet > 0 AND $ref->credit > 0){
				$code_ref = $code_ref.$ref->code_accounting.'<br>';
			}
			if ($credit > 0 AND $ref->debet > 0){
				$code_ref = $code_ref.$ref->code_accounting.'<br>';
			}
		}
		$arr=array(
			'code_ref'=>$code_ref,
		);
		echo json_encode($arr);
	}

}
