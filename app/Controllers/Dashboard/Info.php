<?php
namespace App\Controllers\Dashboard;
use App\Controllers\BaseController;
use App\Models\General_model;
use App\Models\Obat_model;

class Info extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->obat_model = new Obat_model();
    $this->id_user = $this->session->get('id_user');
    helper("item_apotek");
  }

  public function get_summary_jumlah_item($table=null){
    $auth = $this->authentication->is_login_ajax();
    if ($table == '' || $table == null){
      $all    = 0;
      $aktif    = 0;
      $nonaktif = 0;
    } else {
      $all    = $this->general_model->count_rows($table,'id',"id > 0");
      $aktif    = $this->general_model->count_rows($table,'id',"status IS NULL");
      $nonaktif = $this->general_model->count_rows($table,'id',"status = '0'");
    }
    echo json_encode(['all'=>$all,'aktif'=>$aktif,'nonaktif'=>$nonaktif]);
  }

  public function get_stok_obat_terbanyak(){
    $auth = $this->authentication->is_login_div();
    $query = $this->obat_model->get_stok_terbanyak(5);
    $html = '';
    $no = 1;
    foreach ($query->getResult() as $row) {
      $html .= '<div class="d-flex">
      <span class="stamp stamp-md bg-default mr-3">
        <b>'.$no.'</b>
      </span>
        <div class="flex-1 pt-1 ml-2">
          <h6 class="fw-bold mb-1">'.$row->nama.'</h6>
          <small class="text-muted"></small>
        </div>
        <div class="d-flex ml-auto align-items-center">
          <h3 class="text-info fw-bold">'.$row->stok_asli.' <small class="text-muted">'.$row->s1.'</small></h3>
        </div>
      </div>
      <div class="separator-dashed"></div>';
      $no++;
    }
    echo $html;
  }

  public function sering_terjual_dua_bulan_terakhir(){
    $auth = $this->authentication->is_login_div();
    $query = $this->obat_model->sering_terjual_dua_bulan_terakhir(10);
    $html = '';
    $no = 1;
    foreach ($query->getResult() as $row) {
      $stok=text_stock_item_terkecil($row->id_reff,'obat');
      $html .= '<div class="d-flex">
      <span class="stamp stamp-md bg-success mr-3">
        <b>'.$no.'</b>
      </span>
        <div class="flex-1 pt-1 ml-2">
          <h6 class="fw-bold mb-1">'.$row->nama.'</h6>
          <small class="text-muted">Stok : '.$stok.'</small>
        </div>
        <div class="d-flex ml-auto align-items-center">
          <h3 class="text-info fw-bold">'.$row->kali.' kali</h3>
        </div>
      </div>
      <div class="separator-dashed"></div>';
      $no++;
    }
    echo $html;
  }

  public function grafik_transaksi(){
    helper("penjualan");
    // $auth = $this->authentication->is_login_ajax();
    $start = date('Y-m-d');
    $awal_jadwal = strtotime('-14 days',strtotime($start));
    $end = strtotime(date('Y-m-d'));
    $x=array();
    $y=array();
    $z=array();
    $total = 0;
    while($awal_jadwal <= $end){
      $text_tgl =  date('Y-m-d',($awal_jadwal));
      $awal_jadwal      = strtotime("+1 days", $awal_jadwal);

      $jml_trans = $this->general_model->count_rows('penjualan','*',"status IS NULL AND DATE(`datetime`) = '".$text_tgl."'");
      $data_pen = $this->general_model->select_data('penjualan','id',"status IS NULL AND DATE(`datetime`) = '".$text_tgl."'");
      $total_harga = 0;
      foreach ($data_pen->getResult() as $rowharga) {
        $harga = get_harga_kesepakatan_penjualan($rowharga->id);
        $total_harga = $total_harga + $harga;
      }

      $x[]= date('d',strtotime($text_tgl));
      $y[]= $jml_trans;
      $z[]= $total_harga;
      $total = $total + $jml_trans;
    }
    echo json_encode(array('tgl'=>$x,'tr'=>$y,'nm'=>$z,'total'=>$total));

  }


}
