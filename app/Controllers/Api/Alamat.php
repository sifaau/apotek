<?php
namespace App\Controllers\Api;
use App\Controllers\BaseController;
use App\Models\General_model;

class Alamat extends BaseController
{

  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
  }


  public function get_city()
  {
    $auth = $this->authentication->is_login_option();
    $id_province = $this->request->getPost('id_province');
    $q = $this->general_model->select_data('alamat_city','*',array('id_province'=>$id_province));
    echo '<option value=""> - pilih - </option>';
    foreach ($q->getResult() as $row) {
      echo '<option value="'.$row->id.'"> '.$row->city.' </option>';
    }
  }

  public function get_subdistrict(){
    $auth = $this->authentication->is_login_option();
    $id_city = $this->request->getPost('id_city');
    $q = $this->general_model->select_data('alamat_subdistrict','*',array('id_city'=>$id_city));
    echo '<option value=""> - pilih - </option>';
    foreach ($q->getResult() as $row) {
      echo '<option value="'.$row->id.'"> '.$row->subdistrict.' </option>';
    }
  }

}
