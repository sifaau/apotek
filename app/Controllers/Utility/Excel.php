<?php
namespace App\Controllers\Utility;
use App\Controllers\BaseController;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Models\General_model;

class Excel extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    helper("item_apotek");
  }

  public function download_item($table_reff = null){
    if ($table_reff == '' || $table_reff == null ){
      exit;
    }

    $spreadsheet = new Spreadsheet();
    $set = $spreadsheet->setActiveSheetIndex(0);

    $a = 1;

    if ($table_reff == 'obat'){
      $set->setCellValue('A'.$a, 'ID');
      $set->setCellValue('B'.$a, 'BARCODE');
      $set->setCellValue('C'.$a, 'NAMA');
      $set->setCellValue('D'.$a, 'GOLONGAN');
      $set->setCellValue('E'.$a, 'DIPRODUKSI OLEH');
      $set->setCellValue('F'.$a, 'DESKRIPSI');
      $set->setCellValue('G'.$a, 'INDIKASI');
      $set->setCellValue('H'.$a, 'KANDUNGAN');
      $set->setCellValue('I'.$a, 'ZAT AKTIF');
      $set->setCellValue('J'.$a, 'EFEK SAMPING');
      $set->setCellValue('K'.$a, 'DOSIS');
      $set->setCellValue('L'.$a, 'ATURAN PAKAI');
      $set->setCellValue('M'.$a, 'WAKTU 1');
      $set->setCellValue('N'.$a, 'WAKTU 2');
      $set->setCellValue('O'.$a, 'WAKTU 3');
      $set->setCellValue('P'.$a, 'WAKTU 4');
      $set->setCellValue('Q'.$a, 'WAKTU 5');
      $set->setCellValue('R'.$a, 'SYARAT PAKAI');
      $set->setCellValue('S'.$a, 'STOK TERKECIL');
      $set->setCellValue('T'.$a, 'SATUAN TERKECIL');
      $set->setCellValue('U'.$a, 'STOK 2');
      $set->setCellValue('V'.$a, 'SATUAN 2');
      $set->setCellValue('W'.$a, 'STOK 3');
      $set->setCellValue('X'.$a, 'SATUAN 3');
      $set->setCellValue('Y'.$a, 'STOK TERBESAR');
      $set->setCellValue('Z'.$a, 'SATUAN TERBESAR');
      $set->setCellValue('AA'.$a, 'TOTAL KONVERSI KE STOK TERKECIL');
      $set->setCellValue('AB'.$a, 'SATUAN');
    }

    if ($table_reff == 'barang'){
      $set->setCellValue('A'.$a, 'ID');
      $set->setCellValue('B'.$a, 'BARCODE');
      $set->setCellValue('C'.$a, 'NAMA');
      $set->setCellValue('D'.$a, 'GOLONGAN');
      $set->setCellValue('E'.$a, 'DIPRODUKSI OLEH');
      $set->setCellValue('F'.$a, 'MEREK');
      $set->setCellValue('G'.$a, 'SPESIFIKASI');
      $set->setCellValue('H'.$a, 'STOK TERKECIL');
      $set->setCellValue('I'.$a, 'SATUAN TERKECIL');
      $set->setCellValue('J'.$a, 'STOK 2');
      $set->setCellValue('K'.$a, 'SATUAN 2');
      $set->setCellValue('L'.$a, 'STOK 3');
      $set->setCellValue('M'.$a, 'SATUAN 3');
      $set->setCellValue('N'.$a, 'STOK TERBESAR');
      $set->setCellValue('O'.$a, 'SATUAN TERBESAR');
      $set->setCellValue('P'.$a, 'TOTAL KONVERSI KE STOK TERKECIL');
      $set->setCellValue('Q'.$a, 'SATUAN');
    }

    $item = $this->general_model->select_order($table_reff,'*','status IS NULL',"nama ASC");
    $a = 2;
    foreach ($item->getResult() as $row) {
      $id_reff = $row->id;
      $id_satuan_1 =get_satuan_item($row->id,$table_reff,1);
      $id_satuan_2 =get_satuan_item($row->id,$table_reff,2);
      $id_satuan_3 =get_satuan_item($row->id,$table_reff,3);
      $id_satuan_4 =get_satuan_item($row->id,$table_reff,4);

      $satuan_1 =get_satuan_item($row->id,$table_reff,1);
      $satuan_2 =get_satuan_item($row->id,$table_reff,2);
      $satuan_3 =get_satuan_item($row->id,$table_reff,3);
      $satuan_4 =get_satuan_item($row->id,$table_reff,4);

      $total_stok_terkecil = get_stock_item_terkecil($id_reff,$table_reff);

      $jml_stock_1 = $this->general_model->select_sum($table_reff.' as o,satuan as s,master_stok as ms','ms.qty',
      "s.id_reff=o.id AND s.table_reff='".$table_reff."'
      AND ms.id_satuan=s.id
      AND o.id= '".$id_reff."'
      AND s.table_reff= '".$table_reff."'
      AND s.id_reff='".$id_reff."'
      AND s.satuan='".$satuan_1."'
      AND s.ket='1'
      ");

      $jml_stock_2 = $this->general_model->select_sum($table_reff.' as o,satuan as s,master_stok as ms','ms.qty',
      "s.id_reff=o.id AND s.table_reff='".$table_reff."'
      AND ms.id_satuan=s.id
      AND o.id= '".$id_reff."'
      AND s.table_reff= '".$table_reff."'
      AND s.id_reff='".$id_reff."'
      AND s.satuan='".$satuan_2."'
      AND s.ket='2'
      ");

      $jml_stock_3 = $this->general_model->select_sum($table_reff.' as o,satuan as s,master_stok as ms','ms.qty',
      "s.id_reff=o.id AND s.table_reff='".$table_reff."'
      AND ms.id_satuan=s.id
      AND o.id= '".$id_reff."'
      AND s.table_reff= '".$table_reff."'
      AND s.id_reff='".$id_reff."'
      AND s.satuan='".$satuan_3."'
      AND s.ket='3'
      ");

      $jml_stock_4 = $this->general_model->select_sum($table_reff.' as o,satuan as s,master_stok as ms','ms.qty',
      "s.id_reff=o.id AND s.table_reff='".$table_reff."'
      AND ms.id_satuan=s.id
      AND o.id= '".$id_reff."'
      AND s.table_reff= '".$table_reff."'
      AND s.id_reff='".$id_reff."'
      AND s.satuan='".$satuan_4."'
      AND s.ket='4'
      ");

      if ($table_reff == 'obat'){
        $golongan = $this->general_model->get_data_by_field('obat_gol','nama',array('id'=>$row->id_golongan));
        $perusahaan = $this->general_model->get_data_by_field('obat_perusahaan','nama',array('id'=>$row->id_perusahaan));
        $set->setCellValue('A'.$a, $row->id);
        $set->setCellValue('B'.$a, $row->barcode);
        $set->setCellValue('C'.$a, $row->nama);
        $set->setCellValue('D'.$a, $golongan);
        $set->setCellValue('E'.$a, $perusahaan);
        $set->setCellValue('F'.$a, $row->deskripsi);
        $set->setCellValue('G'.$a, $row->indikasi);
        $set->setCellValue('H'.$a, $row->kandungan);
        $set->setCellValue('I'.$a, $row->zat_aktif);
        $set->setCellValue('J'.$a, $row->efek_samping);
        $set->setCellValue('K'.$a, $row->dosis);
        $set->setCellValue('L'.$a, $row->aturan_pakai);
        $set->setCellValue('M'.$a, $row->waktu_1);
        $set->setCellValue('N'.$a, $row->waktu_2);
        $set->setCellValue('O'.$a, $row->waktu_3);
        $set->setCellValue('P'.$a, $row->waktu_4);
        $set->setCellValue('Q'.$a, $row->waktu_5);
        $set->setCellValue('R'.$a, $row->syarat_pakai);
        $set->setCellValue('S'.$a, $jml_stock_1);
        $set->setCellValue('T'.$a, $satuan_1);
        $set->setCellValue('U'.$a, $jml_stock_2);
        $set->setCellValue('V'.$a, $satuan_2);
        $set->setCellValue('W'.$a, $jml_stock_3);
        $set->setCellValue('X'.$a, $satuan_3);
        $set->setCellValue('Y'.$a, $jml_stock_4);
        $set->setCellValue('Z'.$a, $satuan_4);
        $set->setCellValue('AA'.$a, $total_stok_terkecil);
        $set->setCellValue('AB'.$a, $satuan_1);
      }

      if ($table_reff == 'barang'){
        $golongan = $this->general_model->get_data_by_field('obat_gol','nama',array('id'=>$row->id_golongan));
        $perusahaan = $this->general_model->get_data_by_field('obat_perusahaan','nama',array('id'=>$row->id_perusahaan));
        $set->setCellValue('A'.$a, $row->id);
        $set->setCellValue('B'.$a, '');
        $set->setCellValue('C'.$a, $row->nama);
        $set->setCellValue('D'.$a, $golongan);
        $set->setCellValue('E'.$a, $perusahaan);
        $set->setCellValue('F'.$a, $row->merek);
        $set->setCellValue('G'.$a, $row->spesifikasi);
        $set->setCellValue('H'.$a, $jml_stock_1);
        $set->setCellValue('I'.$a, $satuan_1);
        $set->setCellValue('J'.$a, $jml_stock_2);
        $set->setCellValue('K'.$a, $satuan_2);
        $set->setCellValue('L'.$a, $jml_stock_3);
        $set->setCellValue('M'.$a, $satuan_3);
        $set->setCellValue('N'.$a, $jml_stock_4);
        $set->setCellValue('O'.$a, $satuan_4);
        $set->setCellValue('P'.$a, $total_stok_terkecil);
        $set->setCellValue('Q'.$a, $satuan_1);
      }



      $a++;
    }


    $spreadsheet->getProperties()->setTitle('Data '.$table_reff);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Data_'.$table_reff.'_'.date('dMYHis').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    header('Pragma: no-cache');
    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
  }

  public function upload_obat()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->utility("excel/upload_obat", $data);
  }

  public function action_upload_excel(){
    $this->authentication->is_login();
    helper("file");
    // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
    $file = $this->request->getFile('file');
    $path = ROOTPATH.'public/assets/excel/';
    if (! $file->isValid()) {
      echo 'tak valid';
    } else {

      if (! $file->hasMoved()) {
        // $newName = 'tes';
        // $img->move(WRITEPATH . 'uploads', $newName);
        $newName = $file->getRandomName();
        $file->move($path,$newName);
        $inputFileName 			= $path.$newName;

        try {
          $inputFileType		= IOFactory::identify($inputFileName);
          // $objReader 				= IOFactory::createReader($inputFileType);
          $objPHPExcel 			= IOFactory::load($inputFileName);
        } catch(Exception $e) {
          $next  = 0;
          echo '<div class="alert alert-danger">
          <strong>Gagal Upload!</strong> '.pathinfo($inputFileName,PATHINFO_BASENAME).'".
          </div>';
          exit;
        }

        $sheet 							= $objPHPExcel->getSheet(3);
        $highestRow 				= $sheet->getHighestRow();
        $highestColumn 			= $sheet->getHighestColumn();

        $data_ok = 0;
        echo '<table border="1">';
        echo '<tbody>';
        $row = 1;
        for ($row ; $row <= $highestRow; $row++){                  //  Read a row of data into an array
          $rowData 	= $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
          NULL,
          TRUE,
          FALSE);
          $nama_obat		= $rowData[0][0];
          $golongan			= $rowData[0][1];
          $kandungan		= $rowData[0][2];
          $zat_aktif		= $rowData[0][3];
          $satuan1		  = $rowData[0][4];
          $hna		      = $rowData[0][5];
          $margin		     = $rowData[0][6];
          $satuan2		  = $rowData[0][7];
          $isi2		     = $rowData[0][8];
          $margin2		  = $rowData[0][9];
          $satuan3		  = $rowData[0][10];
          $isi3		     = $rowData[0][11];
          $margin3		  = $rowData[0][12];
          $stok		     = $rowData[0][13];
          $hargajual		= $rowData[0][14];

          if ($golongan == 'obat keras'){
            $id_golongan = 4;
          } elseif ($golongan == 'obat bebas terbatas'){
            $id_golongan = 5;
          } elseif ($golongan == 'obat bebas'){
            $id_golongan = 6;
          } elseif ($golongan == 'generik'){
            $id_golongan = 7;
          } elseif ($golongan == 'salep OK'){
            $id_golongan = 8;
          } elseif ($golongan == 'powder OK'){
            $id_golongan = 9;
          } elseif ($golongan == 'tetes mata OB'){
            $id_golongan = 10;
          } elseif ($golongan == 'sirup OK'){
            $id_golongan = 11;
          } elseif ($golongan == 'tetes telingan OK'){
            $id_golongan = 12;
          } elseif ($golongan == 'sirup OBT'){
            $id_golongan = 13;
          } elseif ($golongan == 'sirup OB'){
            $id_golongan = 14;
          } elseif ($golongan == 'sirup OBL'){
            $id_golongan = 15;
          } elseif ($golongan == 'tab OBT'){
            $id_golongan = 16;
          } elseif ($golongan == 'syr OBT'){
            $id_golongan = 17;
          } elseif ($golongan == 'tetes mata OK'){
            $id_golongan = 18;
          } elseif ($golongan == 'salep mata OK'){
            $id_golongan = 19;
          } elseif ($golongan == 'tetes telinga OK'){
            $id_golongan = 20;
          } elseif ($golongan == 'tab OB'){
            $id_golongan = 21;
          } else {
            $id_golongan = 'x';
          }

          $check_satuan1 = $this->general_model->count_rows('satuan_master','id',['nama'=>$satuan1]);
          if ( $check_satuan1 > 0 ){
            $id_satuan1 = $this->general_model->get_data_by_field("satuan_master",'id',['nama'=>$satuan1]);
          } else {
            $this->general_model->do_save("satuan_master",['nama'=>$satuan1]);
            $id_satuan1 = $this->general_model->get_data_by_field("satuan_master",'id',['nama'=>$satuan1]);
          }

          if ($satuan2 == '' || $satuan2 == '' || $satuan2 == null){
            $id_satuan2 = '';
          } else {
            $check_satuan2 = $this->general_model->count_rows('satuan_master','id',['nama'=>$satuan2]);
            if ( $check_satuan2 > 0 ){
              $id_satuan2 = $this->general_model->get_data_by_field("satuan_master",'id',['nama'=>$satuan2]);
            } else {
              $this->general_model->do_save("satuan_master",['nama'=>$satuan2]);
              $id_satuan2 = $this->general_model->get_data_by_field("satuan_master",'id',['nama'=>$satuan2]);
            }
          }

          if ($satuan3 == '' || $satuan3 == '' || $satuan3 == null){
            $id_satuan3 = '';
          } else {
            $check_satuan3 = $this->general_model->count_rows('satuan_master','id',['nama'=>$satuan3]);
            if ( $check_satuan3 > 0 ){
              $id_satuan3 = $this->general_model->get_data_by_field("satuan_master",'id',['nama'=>$satuan3]);
            } else {
              $this->general_model->do_save("satuan_master",['nama'=>$satuan3]);
              $id_satuan3 = $this->general_model->get_data_by_field("satuan_master",'id',['nama'=>$satuan3]);
            }
          }


          echo '<tr>';
          echo '<td>'.$nama_obat.'</td>';
          echo '<td>'.$golongan.' '.$id_golongan.'</td>';
          echo '<td>'.$kandungan.'</td>';
          echo '<td>'.$zat_aktif.'</td>';
          echo '<td>'.$satuan1.' '.$id_satuan1.'</td>';
          echo '<td>'.$hna.'</td>';
          echo '<td>'.$margin.'</td>';
          echo '<td>'.$satuan2.' '.$id_satuan2.'</td>';
          echo '<td>'.$isi2.'</td>';
          echo '<td>'.$margin2.'</td>';
          echo '<td>'.$satuan3.' '.$id_satuan3.'</td>';
          echo '<td>'.$isi3.'</td>';
          echo '<td>'.$margin3.'</td>';
          echo '<td>'.$stok.'</td>';
          echo '<td>'.$hargajual.'</td>';

          echo '<td>';
          $condition = [
            'nama'=> $nama_obat,
            'id_golongan'=> $id_golongan,
            'kandungan'=> $kandungan,
            'zat_aktif'=> $zat_aktif,
          ];
          $check = $this->general_model->count_rows('obat','id',$condition);
          echo 'cek obat '.$check;
          if ($check === 0){
            $this->general_model->do_save('obat',$condition);
          }
          $id_item = $this->general_model->get_data_by_field('obat','id',$condition);
          echo ' '.$id_item;


          $condition_satuan_item1 = [
            'id_reff'=>$id_item,
            'table_reff'=>'obat',
            'ket'=>1,
          ];
          $check_satuan_item1 = $this->general_model->count_rows('satuan','id',$condition_satuan_item1);
          $data_satuan1 = [
            'id_reff'=>$id_item,
            'table_reff'=>'obat',
            'satuan'=>$satuan1,
            'harga_beli'=>$hna,
            'persen_harga_jual'=>$margin,
            'ket'=>1,
            'konversi'=>1,
            'harga_jual'=>$hargajual,
            'date_insert'=>date('Y-m-d H:i:s'),
          ];
          if ($check_satuan_item1 > 0){
            $this->general_model->do_update('satuan',$data_satuan1,$condition_satuan_item1);
          } else {
            $this->general_model->do_save('satuan',$data_satuan1);
          }

          $id_satuan1 = $this->general_model->get_data_by_field('satuan','id',$condition_satuan_item1);

          $condition_master_stok = [
            'id_satuan'=>$id_satuan1,
            'id_gudang_rak'=>1,
            'no_batch'=>'',
            'date_expired'=>'0000-00-00',
          ];
          $this->general_model->do_delete('master_stok',$condition_master_stok);

          $data_stok = [
            'id_satuan'=>$id_satuan1,
            'id_gudang_rak'=>1,
            'no_batch'=>'',
            'date_expired'=>'',
            'qty'=>$stok,
          ];
          $this->general_model->do_save('master_stok',$data_stok);


          $con_delete_stok_opname = [
            'id_item'=>$id_item,
            'table_reff_item'=>'obat',
            'id_gudang_rak'=>1,
            'satuan'=>$satuan1,
            'no_batch'=>'',
            'date_expired'=>'0000-00-00',
            'ket'=>'Stok Awal',
          ];
          $this->general_model->do_delete('stock_opname',$con_delete_stok_opname);
          $data_stock_opname = [
            'id_item'=>$id_item,
            'table_reff_item'=>'obat',
            'id_gudang_rak'=>1,
            'masuk'=>$stok,
            'keluar'=>0,
            'stock_awal'=>0,
            'last_stock'=>$stok,
            'harga_satuan'=>$hna,
            'satuan'=>$satuan1,
            'no_batch'=>'',
            'date_expired'=>'',
            'ket'=>'Stok Awal',
            'date_opname'=>date('Y-m-d'),
            'date_insert'=>date('Y-m-d H:i:s'),
          ];
          $this->general_model->do_save('stock_opname',$data_stock_opname);

          echo '<br>ceksatuan1='.$check_satuan_item1.'-'.$id_satuan1;

          $konversi2 = 1;
          if ($satuan2 == '' || $satuan2 == 'null' || $satuan2 == null){

          } else {
            $konversi2 = intval($isi2);
            $harga_beli2 = $hna*$konversi2;
            $persen_harga_jual2 = $harga_beli2 * ($margin2/100);
            $harga_jual2 = $harga_beli2 + $persen_harga_jual2;
            $condition_satuan_item2 = [
              'id_reff'=>$id_item,
              'table_reff'=>'obat',
              'ket'=>2,
            ];
            $check_satuan_item2 = $this->general_model->count_rows('satuan','id',$condition_satuan_item2);
            $data_satuan2 = [
              'id_reff'=>$id_item,
              'table_reff'=>'obat',
              'satuan'=>$satuan2,
              'harga_beli'=>$harga_beli2,
              'persen_harga_jual'=>$margin2,
              'ket'=>2,
              'konversi'=>$konversi2,
              'harga_jual'=>$harga_jual2,
              'date_insert'=>date('Y-m-d H:i:s'),
            ];
            if ($check_satuan_item2 > 0){
              $this->general_model->do_update('satuan',$data_satuan2,$condition_satuan_item2);
            } else {
              $this->general_model->do_save('satuan',$data_satuan2);
            }
            echo '<br>ceksatuan2='.$check_satuan_item2;
          }



          if ($satuan3 == '' || $satuan3 == 'null' || $satuan3 == null){

          } else {
            $konversi3 = intval($isi3);
            $harga_beli3 = ($hna*$konversi2)*$konversi3;
            $persen_harga_jual3 = $harga_beli3 * ($margin3/100);
            $harga_jual3 = $harga_beli3 + $persen_harga_jual3;
            $condition_satuan_item3 = [
              'id_reff'=>$id_item,
              'table_reff'=>'obat',
              'ket'=>3,
            ];
            $check_satuan_item3 = $this->general_model->count_rows('satuan','id',$condition_satuan_item3);
            $data_satuan3 = [
              'id_reff'=>$id_item,
              'table_reff'=>'obat',
              'satuan'=>$satuan3,
              'harga_beli'=>$harga_beli3,
              'persen_harga_jual'=>$margin3,
              'ket'=>3,
              'konversi'=>$konversi3,
              'harga_jual'=>$harga_jual3,
              'date_insert'=>date('Y-m-d H:i:s'),
            ];
            if ($check_satuan_item3 > 0){
              $this->general_model->do_update('satuan',$data_satuan3,$condition_satuan_item3);
            } else {
              $this->general_model->do_save('satuan',$data_satuan3);
            }
            echo '<br>ceksatuan3='.$check_satuan_item3;
          }


          echo '</td>';

          echo '</tr>';
        }

        echo '</tbody>';
        echo '</table>';

      } else {
        echo 'gagal';
      }

    }
  }



}
