<?php

namespace App\Controllers;
use App\Models\General_model;

class Testing extends BaseController
{
  public function __construct()
  {
    $this->general_model = new General_model();
    // helper('item_apotek');
  }

  public function tes2()
  {
    helper('item_apotek2');
    $dibutuhkan = 6;
    cek_stok_for_penjualan_2($dibutuhkan,$id_reff,$reff,$ket,$no_batch,$date_expired,$date_trans);
  }


  function stock_satuan_dibawah($id_reff,$reff,$ket,$stok1,$stok2,$stok3,$stok4){
    if ($ket == '4'){
      $stok = konversi_satuan_terkecil($id_reff,$reff,1,$stok1)+ konversi_satuan_terkecil($id_reff,$reff,2,$stok2) + konversi_satuan_terkecil($id_reff,$reff,3,$stok3);
    } else if ($ket == '3'){
      $stok = konversi_satuan_terkecil($id_reff,$reff,1,$stok1) + konversi_satuan_terkecil($id_reff,$reff,2,$stok2);
    } else if ($ket == '2'){
      $stok = konversi_satuan_terkecil($id_reff,$reff,1,$stok1);
    } else {
      $stok = 0;
    }
    return $stok;
  }

  public function tes()
  {
    helper('item_apotek');
    $dibutuhkan = 11;
    $id_reff = 2;
    $ket = 3;
    $reff = 'obat';
    $q = $this->general_model->select_order('satuan','*'," id_reff = '".$id_reff."' AND table_reff = '".$reff."' AND ket >= ".$ket."",'ket ASC');

    $stok1 = 10;
    $stok2 = 10;
    $stok3 = 10;
    $stok4 = 10;

    $satuan1 = 'tablet';
    $satuan2 = 'bungkus';
    $satuan3 = 'kardus';
    $satuan4 = 'truk';

    $total_1 = konversi_satuan_terkecil($id_reff,$reff,1,$stok1);
    $total_2 = konversi_satuan_terkecil($id_reff,$reff,2,$stok2);
    $total_3 = konversi_satuan_terkecil($id_reff,$reff,3,$stok3);
    $total_4 = konversi_satuan_terkecil($id_reff,$reff,4,$stok4);
    echo $total_1+$total_2+$total_3+$total_4.'<br>';
    echo $stok1.' '.$stok2.' '.$stok3.' '.$stok4.'<br>';
    echo 'total diambil '.konversi_satuan_terkecil($id_reff,$reff,$ket,$dibutuhkan).'<br>';

    $konversi1 = 1;
    $konversi2 = $this->general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>2]);
    $konversi3 = $this->general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>3]);
    $konversi4 = $this->general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>4]);
    if ((int)$ket===2){
      $konversi2 = 1;
    } else if ((int)$ket===3) {
      $konversi2 = 1;
      $konversi3 = 1;
    } else if ((int)$ket===4) {
      $konversi2 = 1;
      $konversi3 = 1;
      $konversi4 = 1;
    }

    echo 'diambil ='.$dibutuhkan.'<br><br>';
    echo '<table border="1">';
    echo '<tr>';
    echo '<td>satuan</td>';
    echo '<td>urutan</td>';
    echo '<td>konversi</td>';
    echo '<td>stok</td>';
    echo '<td>hasil_konversi</td>';
    echo '<td>diambil</td>';
    echo '<td>total diambil</td>';
    echo '<td>sisa kurang</td>';
    echo '<td>stok diambil</td>';
    echo '<td>pembulatan stok diambil</td>';
    echo '<td>sisa_stok</td>';
    echo '<td>sisa_stok konversi terkecil</td>';
    echo '<td>sisa_satuan_terkecil</td>';
    echo '</tr>';
    $dibutuhkan_awal = $dibutuhkan;
    $total_diambil = 0;
    $sisa_satuan_terkecil = 0;
    $total_stock_akhir = 0;
    foreach ($q->getResult() as $row) {
      $konversi = $row->konversi;
      if ($row->ket=='1'){
        $satuan = $satuan1;
        $stok = $stok1;
        $hasil_konversi = ((($stok*$konversi1)));
      } else if ($row->ket=='2'){
        $satuan = $satuan2;
        $stok = $stok2;
        $hasil_konversi = ((($stok*$konversi2)*$konversi1));
      } else if ($row->ket=='3'){
        $satuan = $satuan3;
        $stok = $stok3;
        $hasil_konversi = ((($stok*$konversi3)*$konversi2)*$konversi1);
      } else if ($row->ket=='4'){
        $satuan = $satuan4;
        $stok = $stok4;
        $hasil_konversi = ((($stok*$konversi4)*$konversi3)*$konversi2)*$konversi1;
      }


      if ($dibutuhkan > $hasil_konversi ){
        $diambil = $hasil_konversi;
        $total_diambil = $total_diambil + $diambil;
        $dibutuhkan = $dibutuhkan - $diambil;
      } else {
        $diambil = $dibutuhkan;
        $total_diambil = $total_diambil + $diambil;
        $dibutuhkan = $dibutuhkan - $diambil;
      }

      $sisa_satuan_terkecil = $hasil_konversi - $diambil;

      if ($row->ket=='1'){
        $stok_diambil = $diambil/$konversi1;
        $pembulatan_stok_diambil = $this->round_up($stok_diambil,0);
        $sisa_stok = $stok - $pembulatan_stok_diambil;
        $sisa_stok_konversi_terkecil = $sisa_stok*$konversi1;
      } else if ($row->ket=='2'){
        $stok_diambil = ((($diambil/$konversi2)/$konversi1));
        $pembulatan_stok_diambil = $this->round_up($stok_diambil,0);
        $sisa_stok = $stok - $pembulatan_stok_diambil;
        $sisa_stok_konversi_terkecil = ((($sisa_stok*$konversi2)*$konversi1));
      } else if ($row->ket=='3'){
        $stok_diambil = ((($diambil/$konversi3)/$konversi2)/$konversi1);
        $pembulatan_stok_diambil = $this->round_up($stok_diambil,0);
        $sisa_stok = $stok - $pembulatan_stok_diambil;
        $sisa_stok_konversi_terkecil = ((($sisa_stok*$konversi3)*$konversi2)*$konversi1);
      } else if ($row->ket=='4'){
        $stok_diambil = ((($diambil/$konversi4)/$konversi3)/$konversi2)/$konversi1;
        $pembulatan_stok_diambil = $this->round_up($stok_diambil,0);
        $sisa_stok = $stok - $pembulatan_stok_diambil;
        $sisa_stok_konversi_terkecil = ((($sisa_stok*$konversi4)*$konversi3)*$konversi2)*$konversi1;
      }

      $sisa_satuan_terkecil         = $sisa_satuan_terkecil - $sisa_stok_konversi_terkecil ;
      $sisa_stok_konversi_terkecil  = konversi_satuan_terkecil($id_reff,$reff,($row->ket-1),$sisa_satuan_terkecil);
      $konversixxx                  = konversi_satuan_terkecil($id_reff,$reff,$row->ket,$sisa_stok);
      $total_stock_akhir            = $total_stock_akhir + $konversixxx;



      echo '<tr>';
      echo '<td>'.$satuan.'</td>';
      echo '<td>'.$row->ket.'</td>';
      echo '<td>'.$konversi.'</td>';
      echo '<td>'.$stok.'</td>';
      echo '<td>'.$hasil_konversi.'</td>';
      echo '<td>'.$diambil.'</td>';
      echo '<td>'.$total_diambil.'</td>';
      echo '<td>'.$dibutuhkan.'</td>';
      echo '<td>'.$stok_diambil.'</td>';
      echo '<td>'.$pembulatan_stok_diambil.'</td>';
      echo '<td>'.$sisa_stok.'</td>';
      echo '<td>'.$sisa_stok_konversi_terkecil.'</td>';
      echo '<td>'.$sisa_satuan_terkecil.'</td>';

      echo '<td>';

      if ($sisa_stok_konversi_terkecil > 0){
        if ($sisa_stok_konversi_terkecil < $hasil_konversi){
          $sisa_stok_terkecil_untuk_generate = $sisa_stok_konversi_terkecil;
          // $stock_satuan_dibawah = $this->stock_satuan_dibawah($id_reff,$reff,$ket,$stok1,$stok2,$stok3,$stok4);
          $stock_satuan_dibawah = 0;
          $sisa_stok_terkecil_untuk_generate_fix = $sisa_stok_terkecil_untuk_generate - $stock_satuan_dibawah;
          if ($sisa_stok_konversi_terkecil>0){
            $this->generate_satuan_terkecil($sisa_stok_terkecil_untuk_generate_fix,$row->ket,$id_reff,$reff,$stok1,$stok2,$stok3,$stok4);
            // echo $sisa_stok_konversi_terkecil.' '.$stock_satuan_dibawah.' '.$sisa_stok_terkecil_untuk_generate_fix.' ok';
          } else {
            echo $sisa_stok_konversi_terkecil.' '.$stock_satuan_dibawah.' no';
          }
        } else {
          $sisa_stok_terkecil_untuk_generate = 0;
        }
      } else {
        $sisa_stok_terkecil_untuk_generate = 0;
      }

      echo '</td>';
      echo '</tr>';

    }
    echo '</table>';

    if ($total_diambil < $dibutuhkan_awal){
      echo 'tidak pas '.$total_stock_akhir;
    } else {
      echo 'ok '.$total_stock_akhir;
    }

  }

  public function generate_satuan_terkecil($sisa_stok_terkecil_untuk_generate,$ket,$id_reff,$reff,$stok1,$stok2,$stok3,$stok4){
    echo $sisa_stok_terkecil_untuk_generate.' '.$ket.'<br>';
    $konversi1 = 1;
    $konversi2 = $this->general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>2]);
    $konversi3 = $this->general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>3]);
    $konversi4 = $this->general_model->get_data_by_field("satuan",'konversi',['id_reff'=>$id_reff,'table_reff'=>$reff,'ket'=>4]);

    // if ((int)$ket===2){
    //   $konversi2 = 1;
    // } else if ((int)$ket===3) {
    //   $konversi2 = 1;
    //   $konversi3 = 1;
    // } else if ((int)$ket===4) {
    //   $konversi2 = 1;
    //   $konversi3 = 1;
    //   $konversi4 = 1;
    // }

    $q = $this->general_model->select_order('satuan','*',"id_reff = '".$id_reff."' AND table_reff = '".$reff."' AND ket < ".$ket."" ,'ket DESC');

    echo '<br>';
    echo '<table border="1">';
    echo '<tr>';
    echo '<td>ket</td>';
    echo '<td>ket</td>';
    echo '<td>konversi</td>';
    echo '<td>stok diberi</td>';
    echo '<td>selisih</td>';
    echo '<td>stok pembulatan</td>';
    echo '<td>stok selanjutnya</td>';
    echo '<td>sisa stok</td>';
    echo '<td>pembulatan stok diberi</td>';
    echo '</tr>';
    $total_stock_akhir = 0;
    foreach ($q->getResult() as $row) {

      if ($row->ket=='1'){
        $stok_diberi = $sisa_stok_terkecil_untuk_generate/$konversi1;
      } else if ($row->ket=='2'){
        $stok_diberi = ((($sisa_stok_terkecil_untuk_generate/$konversi2)/$konversi1));
      } else if ($row->ket=='3'){
        $stok_diberi = ((($sisa_stok_terkecil_untuk_generate/$konversi3)/$konversi2)/$konversi1);
      } else if ($row->ket=='4'){
        $stok_diberi = ((($sisa_stok_terkecil_untuk_generate/$konversi4)/$konversi3)/$konversi2)/$konversi1;
      }

      $pembulatan_stok_diberi = $this->round_down($stok_diberi,0);
      if ($row->ket=='1'){
        $stok_pembulatan = ((($stok_diberi*$konversi1)));
        $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi1)));
      } else if ($row->ket=='2'){
        $stok_pembulatan = ((($stok_diberi*$konversi2)*$konversi1));
        $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi2)*$konversi1));
      } else if ($row->ket=='3'){
        $stok_pembulatan = ((($stok_diberi*$konversi3)*$konversi2)*$konversi1);
        $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi3)*$konversi2)*$konversi1);
      } else if ($row->ket=='4'){
        $stok_pembulatan = ((($stok_diberi*$konversi4)*$konversi3)*$konversi2)*$konversi1;
        $sisa_stok = $stok_pembulatan - ((($pembulatan_stok_diberi*$konversi4)*$konversi3)*$konversi2)*$konversi1;
      }

      if ($row->ket < $ket){
        if ($row->ket=='1'){
          $konversixxx = konversi_satuan_terkecil($id_reff,$reff,$row->ket,($pembulatan_stok_diberi));
        } else if ($row->ket=='2'){
          $konversixxx = konversi_satuan_terkecil($id_reff,$reff,$row->ket,($pembulatan_stok_diberi)) + $stok1;
        } else if ($row->ket=='3'){
          $konversixxx = konversi_satuan_terkecil($id_reff,$reff,$row->ket,($pembulatan_stok_diberi)) + $stok2;
        } else if ($row->ket=='4'){
          $konversixxx = konversi_satuan_terkecil($id_reff,$reff,$row->ket,($pembulatan_stok_diberi)) + $stok3;
        }
      } else {
        $konversixxx = konversi_satuan_terkecil($id_reff,$reff,$row->ket,$pembulatan_stok_diberi) + $stok3;
      }


      $total_stock_akhir = $total_stock_akhir + $konversixxx;

      echo '<tr>';
      echo '<td>'.$row->ket.'</td>';
      echo '<td>'.$ket.'</td>';
      echo '<td>'.$row->konversi.'</td>';
      echo '<td>'.$stok_diberi.'</td>';
      echo '<td></td>';
      echo '<td>'.$stok_pembulatan.'</td>';
      echo '<td></td>';
      echo '<td>'.$sisa_stok.'</td>';
      echo '<td>'.$pembulatan_stok_diberi.'</td>';
      echo '<td>'.$konversixxx.'</td>';
      echo '<td>'.$total_stock_akhir.'</td>';
      echo '</tr>';

      $sisa_stok_terkecil_untuk_generate = $sisa_stok;
    }

    echo '</table>';
    echo $total_stock_akhir;
  }



  public static function round_up($value, $places)
  {
    $mult = pow(10, abs($places));
     return $places < 0 ?
    ceil($value / $mult) * $mult :
        ceil($value * $mult) / $mult;
  }

  public function round_down($value, $precision) {
    $value = (float)$value;
    $precision = (int)$precision;
    if ($precision < 0) {
        $precision = 0;
    }
    $decPointPosition = strpos($value, '.');
    if ($decPointPosition === false) {
        return $value;
    }
    return (float)(substr($value, 0, $decPointPosition + $precision + 1));
}



}
