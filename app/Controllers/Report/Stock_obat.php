<?php
namespace App\Controllers\Report;
use App\Controllers\BaseController;
use App\Models\General_model;
use App\Models\Obat_model;

class Stock_obat extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->obat_model = new Obat_model();
    $this->id_user = $this->session->get('id_user');
  }

  public function terbanyak()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $query = $this->obat_model->get_stok_terbanyak(null);
    $data['data']=$query;
    return $this->template->report("obat/terbanyak", $data);
  }


  }
