<?php
namespace App\Controllers\Report;
use App\Controllers\BaseController;
use App\Models\General_model;

class Stock_awal extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    helper("general");
    helper("item_apotek");
  }

  public function list_detail($reff=null,$id=null){
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $nama_item = $this->general_model->get_data_by_field($reff,'nama',['id'=>$id]);
    $id_golongan = $this->general_model->get_data_by_field($reff,'id_golongan',['id'=>$id]);
    $nama_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_golongan]);
    $id_parent_golongan = $this->general_model->get_data_by_field('obat_gol','id_parent',['id'=>$id_golongan]);
    $nama_parent_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_parent_golongan]);

    $data=[
      'id'=>$id,
      'reff'=>$reff,
      'nama_item'=>$nama_item,
      'nama_golongan'=>$nama_golongan,
      'nama_parent_golongan'=>$nama_parent_golongan,
    ];
    return $this->template->report("stock_awal/list_detail", $data);
  }

  public function get_data_stock_detail(){
    $id = $this->request->getGet('id');
    $reff = $this->request->getGet('reff');
    $con_item = " AND o.id='".$id."' ";
    $q = $this->general_model->do_query("SELECT DISTINCT '".$reff."' as reff,ms.id_gudang_rak,o.id,o.nama,gr.nama as nama_rak,g.nama as nama_gudang,ms.no_batch,ms.date_expired,gol.nama as nama_golongan,(SELECT gol2.nama FROM obat_gol AS gol2 WHERE gol2.id=gol.id_parent) as  parent_golongan
    FROM master_stok as ms, satuan as s,".$reff." as o,gudang_rak as gr,gudang as g,obat_gol as gol
    WHERE ms.id_satuan = s.id
    AND s.id_reff = o.id
    AND ms.id_gudang_rak=gr.id
    AND gr.id_gudang=g.id
    AND gol.id = o.id_golongan
    AND s.table_reff = '".$reff."'
    ".$con_item."
    "
  );

  $satuan1 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>1]);
  $satuan2 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>2]);
  $satuan3 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>3]);
  $satuan4 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>4]);

  $id_satuan1 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>1]);
  $id_satuan2 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>2]);
  $id_satuan3 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>3]);
  $id_satuan4 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>4]);

  if ($satuan1 == '' || $satuan1 == null){
    $form1 = '';
  } else {
    $form1 = '<input type="text" class="form-control" id="jml1" value="0" /><input type="hidden" class="form-control" id="sat1" value="'.$id_satuan1.'" />';
  }

  if ($satuan2 == '' || $satuan2 == null){
    $form2 = '';
  } else {
    $form2 = '<input type="text" class="form-control" id="jml2" value="0" /><input type="hidden" class="form-control" id="sat2" value="'.$id_satuan2.'" />';
  }

  if ($satuan3 == '' || $satuan3 == null){
    $form3 = '';
  } else {
    $form3 = '<input type="text" class="form-control" id="jml3" value="0" /><input type="hidden" class="form-control" id="sat3" value="'.$id_satuan3.'" />';
  }

  if ($satuan4 == '' || $satuan4 == null){
    $form4 = '';
  } else {
    $form4 = '<input type="text" class="form-control" id="jml4" value="0" /><input type="hidden" class="form-control" id="sat4" value="'.$id_satuan4.'" />';
  }

  $q_rak = $this->general_model->select_order('gudang AS g,gudang_rak AS r','g.nama as gudang,r.nama AS rak,r.id AS id',"g.id = r.id_gudang AND g.status IS NULL AND r.status IS NULL","g.nama ASC,r.nama ASC");
  $option_golongan = '';
  if ($q_rak->getNumrows() > 1){
    $option_golongan .= '<option value="">- Pilih Lokasi -</option>';
  }
  foreach ($q_rak->getResult() as $rowr) {
    $option_golongan .= '<option value="'.$rowr->id.'"> Gudang '.$rowr->gudang.' Rak '.$rowr->rak.' </opion>';
  }

  $param = "'".$reff."','".$id."'";

  $html = '<table class="table" id="table_stock_opname">';
  $html .= '<thead>';
  $html .= '<tr>';
  $html .= '<th>#</th>';
  if ($reff=='barang'){
    $html .= '<th>NO BARANG</th>';
  } else {
    $html .= '<th>NO BATCH</th>';
    $html .= '<th>TGL EXPIRED</th>';
  }

  $html .= '<th>LOKASI</th>';
  $html .= '<th class="text-center">'.strtoupper($satuan4).'</th>';
  $html .= '<th class="text-center">'.strtoupper($satuan3).'</th>';
  $html .= '<th class="text-center">'.strtoupper($satuan2).'</th>';
  $html .= '<th class="text-center">'.strtoupper($satuan1).'</th>';
  $html .= '<th>TOTAL SATUAN TERKECIL</th>';
  $html .= '</tr>';
  $html .= '</thead>';

  $html .= '<tbody>';
  if ($satuan1 == '' && $satuan2 == '' && $satuan3 == '' && $satuan4 == ''){
    $html .= '<tr style="padding:0 3px !important;height:40px">';
    $html .= '<th style="padding:0 3px !important;height:40px" colspan="9"><center style="color:red;"> <i class="fa fa-warning"></i> SATUAN BELUM DISETTING. </center></th>';
    $html .= '</tr>';
  } else {
    $html .= '<tr style="padding:0 3px !important;height:40px;background:#cfeaff;">';
    $html .= '<th style="color:white;">0</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" id="0"><input type="text" class="form-control" id="no_batch" value="" /></th>';
    $html .= '<th style="padding:0 3px !important;height:40px"><input type="text" class="form-control" id="expired" value="" /></th>';
    $html .= '<th style="padding:0 3px !important;height:40px"><select class="form-control" id="id_gudang_rak">'.$option_golongan.'</select></th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form4.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form3.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form2.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form1.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px"><a class="btn btn-sx btn-round btn-danger" style="color:#fff;" onClick="save_stock_awal('.$param.')" id="btnsave'.$id.'"><i class="fa fa-save"></i>  SIMPAN STOK AWAL</a></th>';
    $html .= '</tr>';
  }
  $no = 1;
  $totak_stok = 0;
  foreach ( $q->getResult() as $row ) {

    if ($satuan1 == '' || $satuan1 == null){
      $jumlah1 = '';
    } else {
      $jumlah1 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan1."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
    }

    if ($satuan2 == '' || $satuan2 == null){
      $jumlah2 = '';
    } else {
      $jumlah2 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan2."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
    }

    if ($satuan3 == '' || $satuan3 == null){
      $jumlah3 = '';
    } else {
      $jumlah3 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan3."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
    }

    if ($satuan4 == '' || $satuan4 == null){
      $jumlah4 = '';
    } else {
      $jumlah4 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan4."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
    }

    $param = "'".$row->id_gudang_rak."','".$row->no_batch."','".$row->date_expired."'";
    if ($row->date_expired == '' || $row->date_expired == null || $row->date_expired == '0000-00-00' || $row->date_expired == '1970-01-01'){
      $date_expired = '';
    } else {
      $date_expired = date('d/m/Y',strtotime($row->date_expired));
    }
    $html .= '<tr style="padding:0 3px !important;height:35px";>';
    $html .= '<td>'.$no.'</td>';
    if ($reff=='barang'){
      $html .= '<td style="padding:0 3px !important;height:35px" class="text-center">'.$row->no_batch.'</td>';

    } else {
      $html .= '<td style="padding:0 3px !important;height:35px" class="text-center">'.$row->no_batch.'</td>';
      $html .= '<td style="padding:0 3px !important;height:35px" class="text-center">'.$date_expired.'</td>';
    }

    $text_stock_by_no_batch = get_stock_item_terkecil_by_no_batch($id,$reff,$row->no_batch,$row->date_expired);
    $param_id_item = $id.'~'.$reff;
    $link_kartu_stock = '<a href="'.base_url().'/report/stock/card?id_item='.$param_id_item.'&no_batch='.$row->no_batch.'&date_expired='.$row->date_expired.'">'.number_format($text_stock_by_no_batch,0,',','.').' '.$satuan1.'</a>';

    $html .= '<td style="padding:0 3px !important;height:35px" class="text-center">'.$row->nama_rak.' / '.$row->nama_gudang.'</td>';
    $html .= '<td style="padding:0 3px !important;height:35px" class="text-center" style="border-right:none;">'.$jumlah4.' </span></td>';
    $html .= '<td style="padding:0 3px !important;height:35px" class="text-center" style="border-right:none;">'.$jumlah3.' </span></td>';
    $html .= '<td style="padding:0 3px !important;height:35px" class="text-center" style="border-right:none;">'.$jumlah2.' </span></td>';
    $html .= '<td style="padding:0 3px !important;height:35px" class="text-center" style="border-right:none;">'.$jumlah1.' </span></td>';
    $html .= '<td style="padding:0 3px !important;height:35px" class="text-right">'.$link_kartu_stock.'</td>';
    $html .= '</tr>';
    $totak_stok = $totak_stok + $text_stock_by_no_batch;
    $no++;
  }
  $html .= '</tbody>';

  $html .= '<tfoot>';
  $html .= '<tr>';
  if ($reff=='barang'){
    $html .= '<th></th>';
  } else {
    $html .= '<th></th>';
    $html .= '<th></th>';
  }

  $html .= '<th></th>';
  $html .= '<th class="text-center"></th>';
  $html .= '<th class="text-center"></th>';
  $html .= '<th class="text-center"></th>';
  $html .= '<th class="text-right">TOTAL : </th>';
  $html .= '<th style="padding:0 3px !important;height:35px" class="text-right">'.number_format($totak_stok,0,',','.').' '.$satuan1.'</th>';
  $html .= '</tr>';
  $html .= '</tfoot>';

  $html .= '</table>';

  echo $html;
}

public function save_stock_awal(){
  helper(['form', 'url']);
  $input = $this->validate([
    'id' => for_validate('id','string|required'),
    'reff' => for_validate('reff','string|required'),
    'no_batch' => for_validate('no_batch','string'),
    'date_expired' => for_validate('date_expired','string'),
    'id_gudang_rak' => for_validate('Rak Gudang','string'),
  ]);
  if (!$input) {
    echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
  } else {
    $id_item = $this->request->getPost('id');
    $table_reff = $this->request->getPost('reff');
    $no_batch = $this->request->getPost('no_batch');
    $id_gudang_rak = $this->request->getPost('id_gudang_rak');
    $date_expired = $this->request->getPost('date_expired');
    $cek_error = '';
    $this->db->transBegin();

    for ($i=1; $i < 5; $i++) {

      $qty = $this->request->getPost('jml'.$i);
      $id_satuan = $this->request->getPost('sat'.$i);
      if ( $qty == '' || $qty == null || $qty == 'undefined' || $id_satuan == '' || $id_satuan == null || $id_satuan == 'undefined' ){
        $cek_error = $cek_error.' '.$i.'='.$qty.'~'.$id_satuan;
      } else {
        $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);

        $get_last_stock = $this->general_model->get_data_by_field('master_stok','qty',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_gudang_rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
        $get_last_stock = $get_last_stock == '' ? 0 : (float)$get_last_stock;
        if ($qty < $get_last_stock){
          $keluar = $get_last_stock-$qty;
          $masuk = 0;
          $new_stock = $qty;
        } else if ($qty > $get_last_stock){
          $keluar = 0;
          $masuk = $qty-$get_last_stock;
          $new_stock = $qty;
        } else {
          $masuk = 0;
          $keluar = 0;
          $new_stock = $get_last_stock;
        }
        $harga_satuan = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);

        if ($harga_satuan === 0 || $harga_satuan == '' || $harga_satuan == null || $harga_satuan == '0'){
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL. HNA satuan '.$satuan.' belum disetting.','error'=>$cek_error));
          exit;
        }

        if ($date_expired == '' || $date_expired == null || $date_expired == '0000-00-00' || $date_expired == '1970-01-01'){
          $date_expired = '0000-00-00';
        }

        if ($qty <> $get_last_stock){
          $data_stock_opname = [
            'id_item'=>$id_item,
            'table_reff_item'=>$table_reff,
            'id_gudang_rak'=>$id_gudang_rak,
            'masuk'=>$masuk,
            'keluar'=>$keluar,
            'stock_awal'=>$get_last_stock,
            'last_stock'=>$new_stock,
            'harga_satuan'=>$harga_satuan,
            'satuan'=>$satuan,
            'no_batch'=>$no_batch,
            'date_expired'=>$date_expired,
            'ket'=>'Stok Awal',
            'date_opname'=>date('Y-m-d'),
            'date_insert'=>date('Y-m-d H:i:s'),
            'id_user'=>$this->id_user,
          ];
          $this->general_model->do_save('stock_opname',$data_stock_opname);
          $cek_error = $cek_error.implode(' ',$this->general_model->error());
          $check = $this->general_model->count_rows('master_stok',['id'],['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_gudang_rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
          if ($check>0){
            // $this->general_model->do_update('master_stok',
            // [
            //   'qty'=>$new_stock,
            //   'date_update'=>date('Y-m-d H:i:s'),
            //   'id_user_update'=>$this->id_user,
            // ]
            // ,['id_satuan'=>$id_satuan,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
            $this->db->transRollback();
            echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Data obat untuk batch dan expired ini sudah ada, gunakan ubah stok opname didaftar yang ada'));
            exit;
          } else {
            $data_master_stok =[
              'id_satuan'=>$id_satuan,
              'id_gudang_rak'=>$id_gudang_rak,
              'no_batch'=>$no_batch,
              'date_expired'=>$date_expired,
              'qty'=>$new_stock,
              'date_update'=>date('Y-m-d H:i:s'),
              'id_user_update'=>$this->id_user,
            ];
            $this->general_model->do_save('master_stok',$data_master_stok);
          }
          $cek_error = $cek_error.implode(' ',$this->general_model->error());
        } else {
          $cek_error = $cek_error.' jumlah'.$i;
        }
      }
      $qty = '';
      $id_satuan = '';
    }

    if ($this->db->transStatus() === false) {
      $this->db->transRollback();
      echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error'=>$cek_error));
    } else {
      $this->db->transCommit();
      echo json_encode(array('response'=>TRUE,'message'=>'OK','error'=>$cek_error));
    }


  }
}

public function save_stock_awal_by_one(){
  helper(['form', 'url']);
  $input = $this->validate([
    'id' => for_validate('id','string|required'),
    'reff' => for_validate('reff','string|required'),
    'jml' => for_validate('Jumlah Stok','numeric|required'),
    'satuan' => for_validate('Satuan','required'),
    'id_gudang_rak' => for_validate('Rak Gudang','string|required'),
  ]);
  if (!$input) {
    echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
  } else {
    $id_item = $this->request->getPost('id');
    $table_reff = $this->request->getPost('reff');
    $no_batch = $this->request->getPost('no_batch');
    $id_gudang_rak = $this->request->getPost('id_gudang_rak');
    $date_expired = $this->request->getPost('date_expired');
    $qty = $this->request->getPost('jml');
    $id_satuan = $this->request->getPost('satuan');
    $cek_error = '';
    $this->db->transBegin();

    if ( $qty == '' || $qty == null || $qty == 'undefined' || $id_satuan == '' || $id_satuan == null || $id_satuan == 'undefined' ){
      $cek_error = $cek_error.' '.$i.'='.$qty.'~'.$id_satuan;
    } else {
      $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);

      $get_last_stock = $this->general_model->get_data_by_field('master_stok','qty',['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_gudang_rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
      $get_last_stock = $get_last_stock == '' ? 0 : (float)$get_last_stock;
      if ($qty < $get_last_stock){
        $keluar = $get_last_stock-$qty;
        $masuk = 0;
        $new_stock = $qty;
      } else if ($qty > $get_last_stock){
        $keluar = 0;
        $masuk = $qty-$get_last_stock;
        $new_stock = $qty;
      } else {
        $masuk = 0;
        $keluar = 0;
        $new_stock = $get_last_stock;
      }
      $harga_satuan = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);

      if ($harga_satuan === 0 || $harga_satuan == '' || $harga_satuan == null || $harga_satuan == '0'){
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL. HNA satuan '.$satuan.' belum disetting.','error'=>$cek_error));
        exit;
      }

      if ($date_expired == '' || $date_expired == null || $date_expired == '0000-00-00' || $date_expired == '1970-01-01'){
        $date_expired = '0000-00-00';
      }

      if ($qty <> $get_last_stock){
        $data_stock_opname = [
          'id_item'=>$id_item,
          'table_reff_item'=>$table_reff,
          'id_gudang_rak'=>$id_gudang_rak,
          'masuk'=>$masuk,
          'keluar'=>$keluar,
          'stock_awal'=>$get_last_stock,
          'last_stock'=>$new_stock,
          'harga_satuan'=>$harga_satuan,
          'satuan'=>$satuan,
          'no_batch'=>$no_batch,
          'date_expired'=>$date_expired,
          'ket'=>'Stok Awal',
          'date_opname'=>date('Y-m-d'),
          'date_insert'=>date('Y-m-d H:i:s'),
          'id_user'=>$this->id_user,
        ];
        $this->general_model->do_save('stock_opname',$data_stock_opname);
        $cek_error = $cek_error.implode(' ',$this->general_model->error());
        $check = $this->general_model->count_rows('master_stok',['id'],['id_satuan'=>$id_satuan,'id_gudang_rak'=>$id_gudang_rak,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
        if ($check>0){
          // $this->general_model->do_update('master_stok',
          // [
          //   'qty'=>$new_stock,
          //   'date_update'=>date('Y-m-d H:i:s'),
          //   'id_user_update'=>$this->id_user,
          // ]
          // ,['id_satuan'=>$id_satuan,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
          $this->db->transRollback();
          if ($table_reff == 'obat'){
            echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Data obat untuk batch dan expired ini sudah ada, gunakan ubah stok opname didaftar yang ada'));
            exit;
          } else if ($table_reff == 'barang'){
            echo json_encode(array('response'=>FALSE,'message'=>'GAGAL Data barang untuk satuan ini sudah ada, gunakan ubah stok opname didaftar yang ada'));
            exit;
          } else {
            echo json_encode(array('response'=>FALSE,'message'=>'GAGAL '));
            exit;
          }

        } else {
          $data_master_stok =[
            'id_satuan'=>$id_satuan,
            'id_gudang_rak'=>$id_gudang_rak,
            'no_batch'=>$no_batch,
            'date_expired'=>$date_expired,
            'qty'=>$new_stock,
            'date_update'=>date('Y-m-d H:i:s'),
            'id_user_update'=>$this->id_user,
          ];
          $this->general_model->do_save('master_stok',$data_master_stok);
        }
        $cek_error = $cek_error.implode(' ',$this->general_model->error());
      } else {
        $cek_error = $cek_error;
      }
    }


    if ($this->db->transStatus() === false) {
      $this->db->transRollback();
      echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error'=>$cek_error));
    } else {
      $this->db->transCommit();
      echo json_encode(array('response'=>TRUE,'message'=>'OK','error'=>$cek_error));
    }


  }
}

public function get_data_stok_by_no_batch(){
  $id = $this->request->getGet('id');
  $reff = $this->request->getGet('reff');
  $con_item = " AND o.id='".$id."' ";
  $q = $this->general_model->do_query("SELECT '".$reff."' as reff,ms.*,o.id,o.nama,gr.nama as nama_rak,g.nama as nama_gudang,s.satuan
  FROM master_stok as ms, satuan as s,".$reff." as o,gudang_rak as gr,gudang as g,obat_gol as gol
  WHERE ms.id_satuan = s.id
  AND s.id_reff = o.id
  AND ms.id_gudang_rak=gr.id
  AND gr.id_gudang=g.id
  AND gol.id = o.id_golongan
  AND s.table_reff = '".$reff."'
  ".$con_item."
  "
);

$html = '<table class="table" id="table_stock_opname">';
$html .= '<thead>';
$html .= '<tr>';
$html .= '<th>#</th>';
if ($reff == 'obat'){
  $html .= '<th>NO BATCH</th>';
  $html .= '<th class="text-center">DATE EXPIRED</th>';
}

$html .= '<th class="text-center">JML</th>';
$html .= '<th>SATUAN</th>';
$html .= '</tr>';
$html .= '</thead>';

$html .= '<tbody>';

$no = 1;

foreach ( $q->getResult() as $row ) {
 if ($row->date_expired == '' || $row->date_expired == null || $row->date_expired == '0000-00-00' || $row->date_expired == '1970-01-01'){
   $date_expired = '';
 } else {
   $date_expired = date('d/m/Y',strtotime($row->date_expired));
 }
  $html .= '<tr>';
  $html .= '<td>#</td>';
  if ($reff == 'obat'){
  $html .= '<td>'.$row->no_batch.'</td>';
  $html .= '<td>'.$date_expired.'</td>';
}
  $html .= '<td class="text-center">'.$row->qty.'</td>';
  $html .= '<td>'.$row->satuan.'</td>';
  $html .= '</tr>';
}
$html .= '</tbody>';
$html .= '</table>';

echo $html;
}


public function text_stock_item_terkecil(){
  $id = $this->request->getPost('id');
  $reff = $this->request->getPost('reff');
  echo text_stock_item_terkecil($id,$reff);
}

}
