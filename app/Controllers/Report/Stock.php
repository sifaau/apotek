<?php
namespace App\Controllers\Report;
use App\Controllers\BaseController;
use App\Models\General_model;

class Stock extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    helper("general");
    helper('journal_accounting');
  }

  public function card()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=['list_item'=>$this->general_model->do_query("SELECT * FROM (
      SELECT id,nama,'obat' as reff FROM obat WHERE status IS NULL
      UNION
      SELECT id,nama,'barang' as reff FROM barang WHERE status IS NULL
    ) AS xxx
    ORDER BY nama ASC "),
  ];
  return $this->template->report("stock/kartu", $data);
}

public function get_kartu(){
  helper('item_apotek');
  $item     = $this->request->getGet('item');
  $satuan   = $this->request->getGet('satuan');
  $no_batch = $this->request->getGet('no_batch');
  $expired  = $this->request->getGet('expired');
  $start    = $this->request->getGet('start');
  $end      = $this->request->getGet('end');
  $html     = '';

  $pecah_item = explode('~',$item);
  $id_reff    = $pecah_item[0];
  $table_reff = $pecah_item[1];

  if ($no_batch == '' || $no_batch == null){
    $con_nb = " ";
  } else {
    $con_nb = " AND ms.no_batch='".$no_batch."'";
  }

  if ($expired == '' || $expired == null){
    $con_ex = " ";
  } else {
    $con_ex = " AND ms.date_expired = '".date('Y-m-d',strtotime($expired))."'";
  }

  $con_item = " AND o.id='".$id_reff."' ".$con_nb.$con_ex;
  $q1 = $this->general_model->do_query("SELECT DISTINCT '".$table_reff."' as reff,o.id,o.nama,ms.no_batch,ms.date_expired
  FROM master_stok as ms, satuan as s,".$table_reff." as o
  WHERE ms.id_satuan = s.id
  AND s.id_reff = o.id
  AND s.table_reff = '".$table_reff."'
  ".$con_item);

  $html .= '<table class="table table-bordered">';
  foreach ($q1->getResult() as $row1) {


    $con_no_batch1  = " AND pb.no_batch = '".$row1->no_batch."'";
    $con_no_batch   = " AND po.no_batch = '".$row1->no_batch."'";

    $con_expired1   = " AND pb.date_expired = '".$row1->date_expired."'";
    $con_expired    = " AND po.date_expired = '".$row1->date_expired."'";

    if ( $start == '' && $end == '' ) {
      $con_tgl1 = "";
      $con_tgl2 = "";
      $con_tgl3 = "";
    } else if ( $start == '' && $end != '' ) {
      $con_tgl1 = " AND DATE(pb.date_come) <= '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl2 = " AND DATE(p.datetime) <= '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl3 = " AND DATE(po.date_opname) <= '".date('Y-m-d',strtotime($end))."' ";
    } elseif ( $start != '' && $end == '' ) {
      $con_tgl1 = " AND DATE(pb.date_come) >= '".date('Y-m-d',strtotime($start))."' ";
      $con_tgl2 = " AND DATE(p.datetime) >= '".date('Y-m-d',strtotime($start))."' ";
      $con_tgl3 = " AND DATE(po.date_opname) >= '".date('Y-m-d',strtotime($end))."' ";
    } else {
      $con_tgl1 = " AND DATE(pb.date_come) BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl2 = " AND DATE(p.datetime) BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl3 = " AND DATE(po.date_opname) BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."' ";
    }



    $q = $this->general_model->do_query(" SELECT * FROM
      (

        SELECT
        o.id as id_reff,'".$table_reff."' as reff,o.nama AS item,p.no AS ket,pd.harga_beli as nominal,'po' as type,
        pb.qty as masuk,0 as keluar,pd.satuan,pb.date_come as date,
        pb.no_batch,pb.date_expired,pb.stock_awal,pb.last_stock,
        pb.date_insert,pb.id_user,pb.id,2 AS for_order
        FROM po_barang_masuk as pb,po_detail as pd,".$table_reff." as o,po as p
        WHERE
        pb.id_po_detail = pd.id
        AND pb.id_item = o.id
        AND p.id = pd.id_po
        AND o.id = '".$id_reff."'
        AND pb.table_reff_item = '".$table_reff."'
        ".$con_no_batch1.$con_expired1.$con_tgl1."

        UNION

        SELECT
        o.id as id_reff,'".$table_reff."' as reff,o.nama AS item,pen.no AS ket,po.hpp as nominal,'tr' as type,
        0 as masuk,po.qty as keluar,po.satuan,
        DATE(p.datetime) as date,
        po.no_batch,po.date_expired,
        po.stock_awal,
        po.last_stock,
        po.date_insert,po.id_user,po.id,6 AS for_order
        FROM penjualan_detail as po,penjualan as p,".$table_reff." as o,penjualan as pen
        WHERE p.id = po.id_penjualan
        AND po.id_item = o.id
        AND pen.id = po.id_penjualan
        AND o.id = '".$id_reff."'
        AND po.table_reff_item = '".$table_reff."'
        ".$con_no_batch.$con_expired.$con_tgl2."

        UNION

        SELECT
        o.id as id_reff,'".$table_reff."' as reff,o.nama AS item,po.ket,po.harga_satuan as nominal,'op' as type,
        po.masuk as masuk,po.keluar as keluar,po.satuan,
        DATE(po.date_opname) as date,
        po.no_batch,po.date_expired,
        po.stock_awal,
        po.last_stock,
        po.date_insert,po.id_user,po.id,
        CASE
        WHEN po.keluar > 0 THEN 4
        ELSE 3
        END AS for_order
        FROM stock_opname as po,".$table_reff." as o
        WHERE po.id_item = o.id
        AND o.id = '".$id_reff."'
        AND po.table_reff_item = '".$table_reff."'
        ".$con_no_batch.$con_expired.$con_tgl3."

      ) AS x

      ORDER BY DATE(x.`date`) ASC,DATE(x.`date_insert`) ASC,TIME(x.`date_insert`) ASC,for_order ASC

      ");

      if ( $row1->date_expired == '' OR $row1->date_expired == 'null' OR $row1->date_expired == null OR $row1->date_expired == '1970-01-01' ){
        $text_date_expired = '';
      } else {
        $text_date_expired = date('d/m/Y',strtotime($row1->date_expired));
      }

      $html .= '<tr>';
      $html .= '<th colSpan="10">
      NO BATCH : '.$row1->no_batch.'   ; TGL EXPIRED : '.$text_date_expired.'
      </th>';
      $html .= '</tr>';


      $html .= '<tr>';
      $html .= '<th></th>';
      $html .= '<th>Nama Item</th>';
      $html .= '<th>No Batch</th>';
      $html .= '<th>Tanggal</th>';
      $html .= '<th>Stok Awal</th>';
      $html .= '<th>Masuk</th>';
      $html .= '<th>Keluar</th>';
      $html .= '<th>Sisa Stok</th>';
      $html .= '<th>Satuan</th>';
      $html .= '<th>Ket</th>';
      $html .= '</tr>';


      $saldo = 0;
      if ($start == ''){
        $stock_before = 0;
      } else {
        $stock_before = get_stock_before_date($id_reff,$table_reff,$row1->no_batch,$row1->date_expired,$start);
      }
      $satuan_terkecil = $this->general_model->get_data_by_field('satuan','satuan',array('id_reff'=>$id_reff,'table_reff'=>$table_reff,'ket'=>1));

      if ($stock_before <> 0){
        if ( $row1->date_expired == '' || $row1->date_expired == null || $row1->date_expired == '0000-00-00' || $row1->date_expired == '1970-01-01' ){
          $date_expired = '';
        } else {
          $date_expired = ' EXP : '.date('d/m/Y',strtotime($row1->date_expired));
        }

        $html .= '<tr>';
        $html .= '<td>1</td>';
        $html .= '<td>'.$row1->nama.'</td>';
        $html .= '<td>'.$row1->no_batch.' - '.$date_expired.'</td>';
        $html .= '<td>'.date('d/m/Y',strtotime($start)).'</td>';
        $html .= '<td class="text-center">'.$stock_before.'</td>';
        $html .= '<td class="text-center">0</td>';
        $html .= '<td class="text-center">0</td>';
        $html .= '<td class="text-center">'.$stock_before.'</td>';
        $html .= '<td>'.$satuan_terkecil.'</td>';
        $html .= '<td>Stok Sebelumnya</td>';
        $html .= '</tr>';
      }

      $no = 2;
      foreach ($q->getResult() as $row) {
        if ($row->date_expired == '' || $row->date_expired == null || $row->date_expired == '0000-00-00' || $row->date_expired == '1970-01-01'){
          $date_expired = '';
        } else {
          $date_expired = ' EXP : '.date('d/m/Y',strtotime($row->date_expired));
        }

        $ket_satuan = $this->general_model->get_data_by_field('satuan','ket',array('satuan'=>$row->satuan,'id_reff'=>$row->id_reff,'table_reff'=>$row->reff));
        $konversi_masuk = konversi_satuan_terkecil($row->id_reff,$row->reff,$ket_satuan,$row->masuk);
        $konversi_keluar = konversi_satuan_terkecil($row->id_reff,$row->reff,$ket_satuan,$row->keluar);
        $stok_akhir = $stock_before + ( $konversi_masuk - $konversi_keluar );
        $text_ket= '';
        if ($row->masuk>0){
          $text_ket = $row->masuk.' '.$row->satuan;
        }
        if ($row->keluar>0){
          $text_ket = $row->keluar.' '.$row->satuan;
        }
        $html .= '<tr>';
        $html .= '<td>'.$no.'</td>';
        $html .= '<td>'.$row->item.'</td>';
        $html .= '<td>'.$row->no_batch.' - '.$date_expired.'</td>';
        $html .= '<td>'.date('d/m/Y',strtotime($row->date)).'</td>';
        $html .= '<td class="text-center">'.$stock_before.'</td>';
        $html .= '<td class="text-center">'.$konversi_masuk.'</td>';
        $html .= '<td class="text-center">'.$konversi_keluar.'</td>';
        $html .= '<td class="text-center">'.$stok_akhir.'</td>';
        $html .= '<td>'.$satuan_terkecil.'</td>';
        // if ($satuan =='' || $no_batch == '' || $expired == ''){
        //
        // } else {
        //   $html .= '<td>'.$debet.'</td>';
        //   $html .= '<td>'.$credit.'</td>';
        //   $html .= '<td>'.$saldo.'</td>';
        // }
        $html .= '<td>'.$text_ket.' '.$row->ket.'<br><small style="font-size:10px;">'.date('d/m/Y H:i:s',strtotime($row->date_insert)).'</small></td>';
        $html .= '</tr>';
        $no++;
        $stock_before = $stok_akhir;
      }

      $html .= '<tr>';
      $html .= '<th colSpan="10"></th>';
      $html .= '</tr>';

    }
    $html .= '</table>';

    echo $html;
  }

  public function get_kartu_lama(){
    helper('item_apotek');
    $item = $this->request->getGet('item');
    $satuan = $this->request->getGet('satuan');
    $no_batch = $this->request->getGet('no_batch');
    $expired = $this->request->getGet('expired');
    $start = $this->request->getGet('start');
    $end = $this->request->getGet('end');

    if ($item == '' || $item == null){
      $con_item = " AND o.nama LIKE 'invalid'";
    } else {
      $con_item = " AND o.nama LIKE '%".$item."%'";
    }



    if ($satuan == '' || $satuan == null || $satuan == 'all'){
      $con_satuan1 = " ";
      $con_satuan2 = " ";
    } else {
      $con_satuan1 = " AND pd.satuan='".$satuan."'";
      $con_satuan2 = " AND po.satuan='".$satuan."'";
    }

    if ($no_batch == '' || $no_batch == null){
      $con_no_batch1 = " ";
      $con_no_batch = " ";
    } else {
      $con_no_batch1 = " AND pb.no_batch='".$no_batch."'";
      $con_no_batch = " AND po.no_batch='".$no_batch."'";
    }

    if ($expired == '' || $expired == null){
      $con_expired1 = " ";
      $con_expired = " ";
    } else {
      $con_expired1 = " AND pb.date_expired = '".date('Y-m-d',strtotime($expired))."'";
      $con_expired = " AND po.date_expired = '".date('Y-m-d',strtotime($expired))."'";
    }

    if ( $start == '' && $end == '' ) {
      $con_tgl1 = "  ";
      $con_tgl2 = "  ";
      $con_tgl3 = "  ";
    } else if ( $start == '' && $end != '' ) {
      $con_tgl1 = " AND DATE(pb.date_come) <= '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl2 = " AND DATE(p.datetime) <= '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl3 = " AND DATE(po.date_opname) <= '".date('Y-m-d',strtotime($end))."' ";
    } elseif ( $start != '' && $end == '' ) {
      $con_tgl1 = " AND DATE(pb.date_come) >= '".date('Y-m-d',strtotime($start))."' ";
      $con_tgl2 = " AND DATE(p.datetime) >= '".date('Y-m-d',strtotime($start))."' ";
      $con_tgl3 = " AND DATE(po.date_opname) >= '".date('Y-m-d',strtotime($end))."' ";
    } else {
      $con_tgl1 = " AND DATE(pb.date_come) BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl2 = " AND DATE(p.datetime) BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."' ";
      $con_tgl3 = " AND DATE(po.date_opname) BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."' ";
    }

    $q = $this->general_model->do_query(" SELECT * FROM
      (
        SELECT
        o.id as id_reff,'obat' as reff,o.nama AS item,p.no AS ket,pd.harga_beli as nominal,'po' as type,
        pb.qty as masuk,0 as keluar,pd.satuan,pb.date_come as date,
        pb.no_batch,pb.date_expired,pb.stock_awal,pb.last_stock,
        pb.date_insert,pb.id_user,pb.id,1 AS for_order
        FROM po_barang_masuk as pb,po_detail as pd,obat as o,po as p
        WHERE
        pb.id_po_detail = pd.id
        AND pb.id_item = o.id
        AND p.id = pd.id_po
        AND pb.table_reff_item = 'obat'
        ".$con_satuan1.$con_no_batch1.$con_expired1.$con_item.$con_tgl1."

        UNION

        SELECT
        o.id as id_reff,'barang' as reff,o.nama AS item,p.no AS ket,pd.harga_beli as nominal,'po' as type,
        pb.qty as masuk,0 as keluar,pd.satuan,pb.date_come as date,
        pb.no_batch,pb.date_expired,pb.stock_awal,pb.last_stock,
        pb.date_insert,pb.id_user,pb.id,2 AS for_order
        FROM po_barang_masuk as pb,po_detail as pd,barang as o,po as p
        WHERE
        pb.id_po_detail = pd.id
        AND pb.id_item = o.id
        AND p.id = pd.id_po
        AND pb.table_reff_item = 'barang'
        ".$con_satuan1.$con_no_batch1.$con_expired1.$con_item.$con_tgl1."

        UNION

        SELECT
        o.id as id_reff,'obat' as reff,o.nama AS item,pen.no as ket,po.hpp as nominal,'tr' as type,
        0 as masuk,po.qty as keluar,po.satuan,
        DATE(p.datetime) as date,
        po.no_batch,po.date_expired,
        po.stock_awal,
        po.last_stock,
        po.date_insert,po.id_user,po.id,5 AS for_order
        FROM penjualan_detail as po,penjualan as p,obat as o,penjualan as pen
        WHERE p.id = po.id_penjualan
        AND po.id_item = o.id
        AND pen.id = po.id_penjualan
        AND po.table_reff_item = 'obat'
        ".$con_satuan2.$con_no_batch.$con_expired.$con_item.$con_tgl2."

        UNION

        SELECT
        o.id as id_reff,'barang' as reff,o.nama AS item,pen.no AS ket,po.hpp as nominal,'tr' as type,
        0 as masuk,po.qty as keluar,po.satuan,
        DATE(p.datetime) as date,
        po.no_batch,po.date_expired,
        po.stock_awal,
        po.last_stock,
        po.date_insert,po.id_user,po.id,6 AS for_order
        FROM penjualan_detail as po,penjualan as p,barang as o,penjualan as pen
        WHERE p.id = po.id_penjualan
        AND po.id_item = o.id
        AND pen.id = po.id_penjualan
        AND po.table_reff_item = 'barang'
        ".$con_satuan2.$con_no_batch.$con_expired.$con_item.$con_tgl2."

        UNION

        SELECT
        o.id as id_reff,'barang' as reff,o.nama AS item,po.ket,po.harga_satuan as nominal,'op' as type,
        po.masuk as masuk,po.keluar as keluar,po.satuan,
        DATE(po.date_opname) as date,
        po.no_batch,po.date_expired,
        po.stock_awal,
        po.last_stock,
        po.date_insert,po.id_user,po.id,
        CASE
        WHEN po.masuk > 0 THEN 3
        ELSE 4
        END AS for_order
        FROM stock_opname as po,barang as o
        WHERE po.id_item = o.id
        AND po.table_reff_item = 'barang'
        ".$con_satuan2.$con_no_batch.$con_expired.$con_item.$con_tgl3."

        UNION

        SELECT
        o.id as id_reff,'obat' as reff,o.nama AS item,po.ket,po.harga_satuan as nominal,'op' as type,
        po.masuk as masuk,po.keluar as keluar,po.satuan,
        DATE(po.date_opname) as date,
        po.no_batch,po.date_expired,
        po.stock_awal,
        po.last_stock,
        po.date_insert,po.id_user,po.id,
        CASE
        WHEN po.keluar > 0 THEN 4
        ELSE 3
        END AS for_order
        FROM stock_opname as po,obat as o
        WHERE po.id_item = o.id
        AND po.table_reff_item = 'obat'
        ".$con_satuan2.$con_no_batch.$con_expired.$con_item.$con_tgl3."

      ) AS x
      ORDER BY DATE(x.`date`) ASC,DATE(x.`date_insert`) ASC,TIME(x.`date_insert`) ASC,for_order ASC
      ");

      $html = '<table class="table table-bordered">';
      $html.= '<tbody>';
      $html .= '<tr>';
      $html .= '<th></th>';
      $html .= '<th>Nama Item</th>';
      $html .= '<th>No Batch</th>';
      $html .= '<th>Tanggal</th>';
      $html .= '<th>Stok Awal</th>';
      $html .= '<th>Masuk</th>';
      $html .= '<th>Keluar</th>';
      $html .= '<th>Sisa Stok</th>';
      $html .= '<th>Satuan</th>';
      // if ($satuan =='' || $no_batch == '' || $expired == ''){
      //
      // } else {
      //   $html .= '<th>Debet</th>';
      //   $html .= '<th>Kredit</th>';
      //   $html .= '<th>Saldo</th>';
      // }
      $html .= '<th>Ket</th>';
      $html .= '</tr>';
      $html.= '</tbody>';
      $no = 1;
      $saldo = 0;
      $stock_before = 0;
      foreach ($q->getResult() as $row) {
        if ($row->date_expired == '' || $row->date_expired == null || $row->date_expired == '0000-00-00' || $row->date_expired == '1970-01-01'){
          $date_expired = '';
        } else {
          $date_expired = ' EXP : '.date('d/m/Y',strtotime($row->date_expired));
        }

        $satuan_terkecil = $this->general_model->get_data_by_field('satuan','satuan',array('id_reff'=>$row->id_reff,'table_reff'=>$row->reff,'ket'=>1));

        $ket_satuan = $this->general_model->get_data_by_field('satuan','ket',array('satuan'=>$row->satuan,'id_reff'=>$row->id_reff,'table_reff'=>$row->reff));
        $konversi_masuk = konversi_satuan_terkecil($row->id_reff,$row->reff,$ket_satuan,$row->masuk);
        $konversi_keluar = konversi_satuan_terkecil($row->id_reff,$row->reff,$ket_satuan,$row->keluar);
        $stok_akhir = $stock_before + ( $konversi_masuk - $konversi_keluar );
        $text_ket= '';
        if ($row->masuk>0){
          $text_ket = $row->masuk.' '.$row->satuan;
        }
        if ($row->keluar>0){
          $text_ket = $row->keluar.' '.$row->satuan;
        }
        $html .= '<tr>';
        $html .= '<td>'.$no.'</td>';
        $html .= '<td>'.$row->item.'</td>';
        $html .= '<td>'.$row->no_batch.' - '.$date_expired.'</td>';
        $html .= '<td>'.date('d/m/Y',strtotime($row->date)).'</td>';
        $html .= '<td class="text-center">'.$stock_before.'</td>';
        $html .= '<td class="text-center">'.$konversi_masuk.'</td>';
        $html .= '<td class="text-center">'.$konversi_keluar.'</td>';
        $html .= '<td class="text-center">'.$stok_akhir.'</td>';
        $html .= '<td>'.$satuan_terkecil.'</td>';
        // if ($satuan =='' || $no_batch == '' || $expired == ''){
        //
        // } else {
        //   $html .= '<td>'.$debet.'</td>';
        //   $html .= '<td>'.$credit.'</td>';
        //   $html .= '<td>'.$saldo.'</td>';
        // }
        $html .= '<td>'.$text_ket.' '.$row->ket.'<br><small style="font-size:10px;">'.date('d/m/Y H:i:s',strtotime($row->date_insert)).'</small></td>';
        $html .= '</tr>';
        $no++;
        $stock_before = $stok_akhir;
      }
      $html .= '</table>';

      echo $html;
    }

    public function get_pilihan_satuan_by_name(){
      $item = $this->request->getGet('item');
      $q = $this->general_model->do_query("SELECT *
        FROM
        ( SELECT s.satuan,s.ket FROM satuan as s,obat as o WHERE s.id_reff=o.id AND s.table_reff = 'obat' AND o.nama LIKE '%".$item."%'
          UNION
          SELECT s.satuan,s.ket FROM satuan as s,barang as o WHERE s.id_reff=o.id AND s.table_reff = 'barang' AND o.nama LIKE '%".$item."%'
        ) AS  x
        ORDER BY ket ASC"
      );
      $html = '<option value=""> Pilih Satuan </option>';
      $html = '<option value="all">Semua</option>';
      foreach ($q->getResult() as $row) {
        $html .= '<option value="'.$row->satuan.'"> '.$row->satuan.' </option>';
      }
      echo $html;
    }

    public function opname()
    {
      $auth = $this->authentication->is_login();
      if ($auth==FALSE){
        return redirect()->to('aptauth');
      }
      $data=['list_item'=>$this->general_model->do_query("SELECT
        nama FROM obat WHERE status IS NULL
        UNION
        SELECT nama FROM barang WHERE status IS NULL "),
      ];
      return $this->template->report("stock/opname", $data);
    }

    public function get_data_opname(){
      $item = $this->request->getGet('item');
      if ($item == ''){
        $con_item = " AND o.id IS NULL ";
      } else {
        $con_item = " AND o.nama='".$item."' ";
      }
      $q = $this->general_model->do_query("
      SELECT distinct 'obat' as reff,o.id,o.nama,gol.nama as nama_golongan,(SELECT gol2.nama FROM obat_gol AS gol2 WHERE gol2.id=gol.id_parent) as  parent_golongan
      FROM  satuan as s,obat as o,gudang_rak as gr,gudang as g,obat_gol as gol
      WHERE s.id_reff = o.id
      AND gr.id_gudang=g.id
      AND gol.id = o.id_golongan
      AND s.table_reff = 'obat'
      AND s.ket = '1'
      AND o.status IS NULL
      ".$con_item."

      UNION

      SELECT distinct 'barang' as reff,o.id,o.nama,gol.nama as nama_golongan,(SELECT gol2.nama FROM obat_gol AS gol2 WHERE gol2.id=gol.id_parent) as  parent_golongan
      FROM  satuan as s,barang as o,gudang_rak as gr,gudang as g,obat_gol as gol
      WHERE s.id_reff = o.id
      AND gr.id_gudang=g.id
      AND gol.id = o.id_golongan
      AND s.table_reff = 'barang'
      AND s.ket = '1'
      ".$con_item."
      AND o.status IS NULL

      ");
      $html = '<div class="col-md-12 table-responsive">';
      $html = '<table class="table table-bordered" id="table_stock_opname">';
      $html .= '<thead>';

      $html .= '<tr>';
      $html .= '<th rowspan="2">ITEM</th>';
      $html .= '<th rowspan="2">GOLONGAN</th>';
      $html .= '<th colspan="2" class="text-center">SATUAN TERBESAR</th>';
      $html .= '<th colspan="2" class="text-center">SATUAN 3</th>';
      $html .= '<th colspan="2" class="text-center">SATUAN 2</th>';
      $html .= '<th colspan="2" class="text-center">SATUAN TERKECIL</th>';
      $html .= '<th rowspan="2">Aksi</th>';
      $html .= '</tr>';

      $html .= '<tr>';
      $html .= '<th>STOK</th>';
      $html .= '<th>SATUAN</th>';
      $html .= '<th>STOK</th>';
      $html .= '<th>SATUAN</th>';
      $html .= '<th>STOK</th>';
      $html .= '<th>SATUAN</th>';
      $html .= '<th>STOK</th>';
      $html .= '<th>SATUAN</th>';
      $html .= '</tr>';

      $html .= '</thead>';
      $html .= '<tbody>';

      foreach ( $q->getResult() as $row ) {
        $satuan1 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$row->reff,'ket'=>1]);
        $satuan2 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$row->reff,'ket'=>2]);
        $satuan3 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$row->reff,'ket'=>3]);
        $satuan4 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$row->reff,'ket'=>4]);

        if ($satuan1 == '' || $satuan1 == null){
          $jumlah1 = '';
        } else {
          $jumlah1 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND s.id_reff='".$row->id."' AND s.table_reff='".$row->reff."' AND s.satuan='".$satuan1."'");
        }

        if ($satuan2 == '' || $satuan2 == null){
          $jumlah2 = '';
        } else {
          $jumlah2 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND s.id_reff='".$row->id."' AND s.table_reff='".$row->reff."' AND s.satuan='".$satuan2."'");
        }

        if ($satuan3 == '' || $satuan3 == null){
          $jumlah3 = '';
        } else {
          $jumlah3 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND s.id_reff='".$row->id."' AND s.table_reff='".$row->reff."' AND s.satuan='".$satuan3."'");
        }

        if ($satuan4 == '' || $satuan4 == null){
          $jumlah4 = '';
        } else {
          $jumlah4 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND s.id_reff='".$row->id."' AND s.table_reff='".$row->reff."' AND s.satuan='".$satuan4."'");
        }

        $html .= '<tr>';
        $html .= '<td>'.$row->nama.'</td>';
        $html .= '<td>'.$row->parent_golongan.' / '.$row->nama_golongan.'</td>';
        $html .= '<td class="text-center" style="border-right:none;">'.$jumlah4.' </span></td>';
        $html .= '<td class="text-center" style="border-left:none;">'.$satuan4.' </span></td>';
        $html .= '<td class="text-center" style="border-right:none;">'.$jumlah3.' </span></td>';
        $html .= '<td class="text-center" style="border-left:none;">'.$satuan3.' </span></td>';
        $html .= '<td class="text-center" style="border-right:none;">'.$jumlah2.' </span></td>';
        $html .= '<td class="text-center" style="border-left:none;">'.$satuan2.' </span></td>';
        $html .= '<td class="text-center" style="border-right:none;">'.$jumlah1.' </span></td>';
        $html .= '<td class="text-center" style="border-left:none;">'.$satuan1.' </span></td>';
        $html .= '<th><a href="'.base_url().'/report/stock/opname_detail/'.$row->reff.'/'.$row->id.'" class="btn btn-sm btn-primary" style="color:#fff;"><i class="fa fa-pen"></i> OPNAME</a></th>';
        $html .= '</tr>';
      }
      $html .= '</tbody>';
      $html .= '</table>';
      $html .= '</div>';

      echo $html;
    }

    public function opname_detail($reff=null,$id=null){
      $auth = $this->authentication->is_login();
      if ($auth==FALSE){
        return redirect()->to('aptauth');
      }
      $nama_item = $this->general_model->get_data_by_field($reff,'nama',['id'=>$id]);
      $id_golongan = $this->general_model->get_data_by_field($reff,'id_golongan',['id'=>$id]);
      $nama_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_golongan]);
      $id_parent_golongan = $this->general_model->get_data_by_field('obat_gol','id_parent',['id'=>$id_golongan]);
      $nama_parent_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_parent_golongan]);

      $data=[
        'id'=>$id,
        'reff'=>$reff,
        'nama_item'=>$nama_item,
        'nama_golongan'=>$nama_golongan,
        'nama_parent_golongan'=>$nama_parent_golongan,
      ];
      return $this->template->report("stock/opname_detail", $data);
    }

    public function get_data_opname_detail(){
      $id = $this->request->getGet('id');
      $reff = $this->request->getGet('reff');
      $con_item = " AND o.id='".$id."' ";
      $q = $this->general_model->do_query("SELECT DISTINCT 'obat' as reff,ms.id_gudang_rak,o.id,o.nama,gr.nama as nama_rak,g.nama as nama_gudang,ms.no_batch,ms.date_expired,gol.nama as nama_golongan,(SELECT gol2.nama FROM obat_gol AS gol2 WHERE gol2.id=gol.id_parent) as  parent_golongan
      FROM master_stok as ms, satuan as s,".$reff." as o,gudang_rak as gr,gudang as g,obat_gol as gol
      WHERE ms.id_satuan = s.id
      AND s.id_reff = o.id
      AND ms.id_gudang_rak=gr.id
      AND gr.id_gudang=g.id
      AND gol.id = o.id_golongan
      AND s.table_reff = '".$reff."'
      ".$con_item."
      "
    );

    $html = '<table class="table table-bordered" id="table_stock_opname">';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .= '<th rowspan="2">NO</th>';
    $html .= '<th rowspan="2">NO BATCH</th>';
    $html .= '<th rowspan="2">TGL EXPIRED</th>';
    $html .= '<th rowspan="2">LOKASI</th>';
    $html .= '<th colspan="2" class="text-center">SATUAN TERBESAR</th>';
    $html .= '<th colspan="2" class="text-center">SATUAN 3</th>';
    $html .= '<th colspan="2" class="text-center">SATUAN 2</th>';
    $html .= '<th colspan="2" class="text-center">SATUAN TERKECIL</th>';
    $html .= '<th rowspan="2">#</th>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<th class="text-center">STOK</th>';
    $html .= '<th class="text-center">SATUAN</th>';
    $html .= '<th class="text-center">STOK</th>';
    $html .= '<th class="text-center">SATUAN</th>';
    $html .= '<th class="text-center">STOK</th>';
    $html .= '<th class="text-center">SATUAN</th>';
    $html .= '<th class="text-center">STOK</th>';
    $html .= '<th class="text-center">SATUAN</th>';
    $html .= '</tr>';
    $html .= '</thead>';
    $html .= '<tbody>';

    $q_rak = $this->general_model->select_order('gudang AS g,gudang_rak AS r','g.nama as gudang,r.nama AS rak,r.id AS id',"g.id = r.id_gudang AND g.status IS NULL AND r.status IS NULL","g.nama ASC,r.nama ASC");
    $option_golongan = '';
    if ($q_rak->getNumrows() > 1){
      $option_golongan .= '<option value="">- Pilih Lokasi -</option>';
    }
    foreach ($q_rak->getResult() as $rowr) {
      $option_golongan .= '<option value="'.$rowr->id.'"> Gudang '.$rowr->gudang.' Rak '.$rowr->rak.' </opion>';
    }

    $satuan1 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>1]);
    $satuan2 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>2]);
    $satuan3 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>3]);
    $satuan4 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>4]);

    $id_satuan1 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>1]);
    $id_satuan2 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>2]);
    $id_satuan3 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>3]);
    $id_satuan4 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>4]);

    if ($satuan1 == '' || $satuan1 == null){
      $form1 = '';
    } else {
      $form1 = '<input type="number" class="form-control" id="jml1_awal" value="0" /><input type="hidden" class="form-control" id="sat1_awal" value="'.$id_satuan1.'" />';
    }

    if ($satuan2 == '' || $satuan2 == null){
      $form2 = '';
    } else {
      $form2 = '<input type="number" class="form-control" id="jml2_awal" value="0" /><input type="hidden" class="form-control" id="sat2_awal" value="'.$id_satuan2.'" />';
    }

    if ($satuan3 == '' || $satuan3 == null){
      $form3 = '';
    } else {
      $form3 = '<input type="number" class="form-control" id="jml3_awal" value="0" /><input type="hidden" class="form-control" id="sat3_awal" value="'.$id_satuan3.'" />';
    }

    if ($satuan4 == '' || $satuan4 == null){
      $form4 = '';
    } else {
      $form4 = '<input type="number" class="form-control" id="jml4_awal" value="0" /><input type="hidden" class="form-control" id="sat4_awal" value="'.$id_satuan4.'" />';
    }

    $param = "'".$reff."','".$id."'";

    $html .= '<tr style="padding:0 3px !important;height:40px;background:#e1ecfa;">';
    $html .= '<th style="color:white;">0</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" id="0"><input type="text" class="form-control" id="no_batch_awal" value="" /></th>';
    $html .= '<th style="padding:0 3px !important;height:40px"><input type="text" class="form-control" id="expired_awal" value="" /></th>';
    $html .= '<th style="padding:0 3px !important;height:40px"><select class="form-control" id="id_gudang_rak_awal">'.$option_golongan.'</select></th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form4.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$satuan4.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form3.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$satuan3.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form2.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$satuan2.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$form1.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px" class="text-center">'.$satuan1.'</th>';
    $html .= '<th style="padding:0 3px !important;height:40px"><a class="btn btn-round btn-danger" style="color:#fff;" onClick="save_stock_awal('.$param.')" id="btnsave_awal'.$id.'"><i class="fa fa-save"></i>  SIMPAN STOK AWAL</a></th>';
    $html .= '</tr>';
    $no=1;
    foreach ( $q->getResult() as $row ) {
      $satuan1 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>1]);
      $satuan2 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>2]);
      $satuan3 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>3]);
      $satuan4 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>4]);

      $id_satuan1 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>1]);
      $id_satuan2 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>2]);
      $id_satuan3 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>3]);
      $id_satuan4 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id,'table_reff'=>$reff,'ket'=>4]);

      if ( $row->date_expired == '' OR $row->date_expired == null OR $row->date_expired == '0000-00-00' ) {
        $date_expired = '';
      } else {
        $date_expired = $row->date_expired;
      }

      if ($satuan1 == '' || $satuan1 == null){
        $jumlah1 = '';
      } else {
        $jumlah1 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan1."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
        $jumlah1 = '<input type="number" class="form-control" id="jml1'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.''.'" value="'.$jumlah1.'" style="width:100px;"/>
        <input type="hidden" class="form-control" id="sat1'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.'" value="'.$id_satuan1.'" style="width:100px;"/>';
      }

      if ($satuan2 == '' || $satuan2 == null){
        $jumlah2 = '';
      } else {
        $jumlah2 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan2."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
        $jumlah2 = '<input type="number" class="form-control" id="jml2'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.'" value="'.$jumlah2.'" style="width:100px;"/>
        <input type="hidden" class="form-control" id="sat2'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.'" value="'.$id_satuan2.'" style="width:100px;"/>';
      }

      if ($satuan3 == '' || $satuan3 == null){
        $jumlah3 = '';
      } else {
        $jumlah3 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan3."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
        $jumlah3 = '<input type="number" class="form-control" id="jml3'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.'" value="'.$jumlah3.'" style="width:100px;"/>
        <input type="hidden" class="form-control" id="sat3'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.'" value="'.$id_satuan3.'" style="width:100px;"/>';}

        if ($satuan4 == '' || $satuan4 == null){
          $jumlah4 = '';
        } else {
          $jumlah4 = $this->general_model->select_sum('master_stok as ms,satuan as s','ms.qty',"ms.id_satuan=s.id AND ms.id_gudang_rak ='".$row->id_gudang_rak."' AND s.id_reff='".$id."' AND s.table_reff='".$reff."' AND s.satuan='".$satuan4."' AND ms.no_batch='".$row->no_batch."' AND ms.date_expired='".$row->date_expired."'");
          $jumlah4 = '<input type="number" class="form-control" id="jml4'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.'" value="'.$jumlah4.'" style="width:100px;"/>
          <input type="hidden" class="form-control" id="sat4'.$id.$reff.$row->id_gudang_rak.$row->no_batch.$row->date_expired.'" value="'.$id_satuan4.'" style="width:100px;"/>';
        }

        $param = "'".$id."','".$reff."','".$row->id_gudang_rak."','".$row->no_batch."','".$row->date_expired."'";

        $html .= '<tr style="padding:0 3px !important;height:40px;">';
        $html .= '<td>'.$no.'</td>';
        $html .= '<td style="padding:0 3px !important;height:40px;">'.$row->no_batch.'</td>';
        $html .= '<td style="padding:0 3px !important;height:40px;">'.$date_expired.'</td>';
        $html .= '<td style="padding:0 3px !important;height:40px;">'.$row->nama_gudang.' / '.$row->nama_rak.'</td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$jumlah4.' </span></td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$satuan4.' </span></td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$jumlah3.' </span></td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$satuan3.' </span></td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$jumlah2.' </span></td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$satuan2.' </span></td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$jumlah1.' </span></td>';
        $html .= '<td style="padding:0 3px !important;height:40px;" class="text-center" >'.$satuan1.' </span></td>';
        $html .= '<th style="padding:0 3px !important;height:40px;"><a class="btn btn-round btn-primary" style="color:#fff;" onClick="save_opname_detail('.$param.')" id="btnsaveopname'.$row->no_batch.$row->date_expired.'"><i class="fa fa-save"></i> <b> SIMPAN OPNAME</b></a></th>';
        $html .= '</tr>';
        $no++;
      }
      $html .= '</tbody>';
      $html .= '</table>';

      echo $html;
    }

    public function save_opname_detail(){
      helper(['form', 'url']);
      $input = $this->validate([
        // 'no_batch' => for_validate('no_batch','string'),
        // 'date_expired' => for_validate('date_expired','string'),
        // 'id_gudang_rak' => for_validate('Rak Gudang','string'),
        'id' => for_validate('id','required'),
        'reff' => for_validate('table','required'),
      ]);
      if (!$input) {
        echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
      } else {
        $no_batch = $this->request->getPost('no_batch');
        $id_gudang_rak = $this->request->getPost('id_gudang_rak');
        $date_expired = $this->request->getPost('date_expired');
        $cek_error = '';
        $this->db->transBegin();
        $stok1=$stok2=$stok3=$stok4=0;

        for ($i=1; $i < 5; $i++) {

          $qty = $this->request->getPost('jml'.$i);
          $id_satuan = $this->request->getPost('sat'.$i);
          if ( $qty == '' || $qty == null || $qty == 'undefined' || $id_satuan == '' || $id_satuan == null || $id_satuan == 'undefined' ){
            $cek_error = $cek_error.' '.$i.'='.$qty.'~'.$id_satuan;
          } else {
            $id_item = $this->request->getPost('id');
            $table_reff = $this->request->getPost('reff');
            // $id_item = $this->general_model->get_data_by_field('satuan','id_reff',['id'=>$id_satuan]);
            // $table_reff = $this->general_model->get_data_by_field('satuan','table_reff',['id'=>$id_satuan]);
            $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);
            $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
            if ($harga_beli === 0 || $harga_beli == '' || $harga_beli == null || $harga_beli == '0'){
              $this->db->transRollback();
              echo json_encode(array('response'=>FALSE,'message'=>'GAGAL. HNA satuan '.$satuan.' belum disetting.','error'=>$cek_error));
              exit;
            } else {
              $get_last_stock = $this->general_model->get_data_by_field('master_stok','qty',['id_satuan'=>$id_satuan,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
              $get_last_stock = $get_last_stock == '' ? 0 : (float)$get_last_stock;
              if ($qty < $get_last_stock){
                $keluar = $get_last_stock-$qty;
                $masuk = 0;
                $new_stock = $qty;
              } else if ($qty > $get_last_stock){
                $keluar = 0;
                $masuk = $qty-$get_last_stock;
                $new_stock = $qty;
              } else if ($qty === 0){
                $keluar = $get_last_stock;
                $masuk = 0;
                $new_stock = 0;
              } else {
                $masuk = 0;
                $keluar = 0;
                $new_stock = $get_last_stock;
              }
              if ($qty <> $get_last_stock){
                $data_stock_opname = [
                  'id_item'=>$id_item,
                  'table_reff_item'=>$table_reff,
                  'id_gudang_rak'=>$id_gudang_rak,
                  'masuk'=>$masuk,
                  'keluar'=>$keluar,
                  'stock_awal'=>$get_last_stock,
                  'last_stock'=>$new_stock,
                  'satuan'=>$satuan,
                  'no_batch'=>$no_batch,
                  'harga_satuan'=>$harga_beli,
                  'date_expired'=>$date_expired,
                  'ket'=>'Opname',
                  'date_opname'=>date('Y-m-d'),
                  'date_insert'=>date('Y-m-d H:i:s'),
                  'id_user'=>$this->id_user,
                ];
                $this->general_model->do_save('stock_opname',$data_stock_opname);
                $cek_error = $cek_error.implode(' ',$this->general_model->error());
                $check = $this->general_model->count_rows('master_stok',['id'],['id_satuan'=>$id_satuan,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
                if ($check>0){
                  if ($check > 1){
                    $this->general_model->do_delete('master_stok',['id_satuan'=>$id_satuan,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
                    $data_master_stok =[
                      'id_satuan'=>$id_satuan,
                      'id_gudang_rak'=>$id_gudang_rak,
                      'no_batch'=>$no_batch,
                      'date_expired'=>$date_expired,
                      'qty'=>$new_stock,
                      'date_update'=>date('Y-m-d H:i:s'),
                      'id_user_update'=>$this->id_user,
                    ];
                    $this->general_model->do_save('master_stok',$data_master_stok);
                  } else {
                    $this->general_model->do_update('master_stok',
                    [
                      'qty'=>$new_stock,
                      'date_update'=>date('Y-m-d H:i:s'),
                      'id_user_update'=>$this->id_user,
                    ]
                    ,['id_satuan'=>$id_satuan,'no_batch'=>$no_batch,'date_expired'=>$date_expired,'id_gudang_rak'=>$id_gudang_rak]);
                  }

                } else {
                  $data_master_stok =[
                    'id_satuan'=>$id_satuan,
                    'id_gudang_rak'=>$id_gudang_rak,
                    'no_batch'=>$no_batch,
                    'date_expired'=>$date_expired,
                    'qty'=>$new_stock,
                    'date_update'=>date('Y-m-d H:i:s'),
                    'id_user_update'=>$this->id_user,
                  ];
                  $this->general_model->do_save('master_stok',$data_master_stok);
                }
                $cek_error = $cek_error.implode(' ',$this->general_model->error());
                generate_journal_hpp($id_item,$table_reff,$satuan,$no_batch,$date_expired);
              } else {
                $cek_error = $cek_error.' jumlah'.$i;
              }
            }
          }
          $qty = '';
          $id_satuan = '';
        }

        if ($this->db->transStatus() === false) {
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error'=>$cek_error));
        } else {
          $this->db->transCommit();
          echo json_encode(array('response'=>TRUE,'message'=>'OK','error'=>$cek_error));
        }


      }
    }


  }
