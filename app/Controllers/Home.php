<?php

namespace App\Controllers;

class Home extends BaseController
{
  public function __construct()
  {

  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->main("welcome_message", $data);
  }



}
