<?php
namespace App\Controllers\Apotek;
use App\Controllers\BaseController;
use App\Models\General_model;

class Supplier extends BaseController
{

  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array(
      'province'=>$this->general_model->select_data('alamat_province','*'," id IS NOT NULL ")
    );
    return $this->template->apotek("supplier/list", $data);
  }

  public function for_validate($label='',$rules=''){
    return ['label'  => $label,'rules'  => $rules,'errors' => ['required' => ' {field} harus diisi','numeric' => ' {field} harus diisi angka']];
  }

  public function action_create(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => $this->for_validate('nama','required'),
      // 'tlp' => $this->for_validate('tlp','numeric'),
      // 'hp' => $this->for_validate('hp','numeric'),
      // 'jalan' => $this->for_validate('jalan',''),
      // 'blok' => $this->for_validate('blok',''),
      // 'rt' => $this->for_validate('rt',''),
      // 'rw' => $this->for_validate('rw',''),
      // 'province' => $this->for_validate('province',''),
      // 'city' => $this->for_validate('city',''),
      // 'subdistric' => $this->for_validate('subdistric',''),
      // 'kelurahan' => $this->for_validate('kelurahan',''),
      // 'kode_pos' => $this->for_validate('kode_pos',''),
      // 'bank' => $this->for_validate('bank',''),
      // 'bank_name' => $this->for_validate('bank_name',''),
      // 'bank_norek' => $this->for_validate('bank_norek',''),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $action = $this->request->getPost('action');
      $id_supplier = $this->request->getPost('id_supplier');
      $nama       = $this->request->getPost('nama');
      $tlp        = $this->request->getPost('tlp');
      $hp         = $this->request->getPost('hp');
      $jenis_alamat = $this->request->getPost('jenis_alamat');
      $jalan      = $this->request->getPost('jalan');
      $blok       = $this->request->getPost('blok');
      $no         = $this->request->getPost('no');
      $rt         = $this->request->getPost('rt');
      $rw         = $this->request->getPost('rw');
      $province   = $this->request->getPost('province');
      $city       = $this->request->getPost('city');
      $subdistrict = $this->request->getPost('subdistrict');
      $kelurahan  = $this->request->getPost('kelurahan');
      $kode_pos   = $this->request->getPost('kode_pos');
      $bank   = $this->request->getPost('bank');
      $bank_name   = $this->request->getPost('bank_name');
      $bank_norek   = $this->request->getPost('bank_norek');

      $this->db->transBegin();
      $data = [
        'nama'=>$nama,
        'telp'=>$tlp,
        'hp'=>$hp,
        'bank'=>$bank,
        'bank_name'=>$bank_name,
        'bank_norek'=>$bank_norek,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      if ($action == 'edit'){
        $save = $this->general_model->do_update('supplier',$data,['id'=>$id_supplier]);
        $error1 = $this->general_model->error();
      } else {
        $save = $this->general_model->do_save('supplier',$data);
        $error1 = $this->general_model->error();
        $id_supplier = $this->general_model->insert_id();
      }


      $data_alamat=[
        'id_reff'=>$id_supplier,
        'table_reff'=>'supplier',
        'jenis'=> $jenis_alamat,
        'alamat' => $jalan,
        'blok' => $blok,
        'no' => $no,
        'rt' => $rt,
        'rw' => $rw,
        'id_province' => $province,
        'id_city' => $city,
        'id_subdistrict'=> $subdistrict,
        'kelurahan' => $kelurahan,
        'kd_pos' => $kode_pos,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      if ($action == 'edit'){
        $save2 = $this->general_model->do_update('alamat',$data_alamat,['id_reff'=>$id_supplier,'table_reff'=>'supplier']);
        $error2 = $this->general_model->error();
      } else {
        $save2 = $this->general_model->do_save('alamat',$data_alamat);
        $error2 = $this->general_model->error();
      }


      if ($this->db->transStatus() === false) {
        $this->db->transRollback();

        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1),'error2'=>$error2));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }

    }
  }

  public function get_data(){
    $field = ['ID','Nama','Telp','Alamat','Rekening Bank','Status','-'];
    $query = $this->general_model->select_order('supplier','*',['id'>0],"status ASC");
    echo '<table id="basic-datatables" class="display table table-bordered table-hover ">';
    echo '<thead>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</thead>';
    echo '<tfoot>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</tfoot>';
    echo '<tbody>';
    $no = 1;
    foreach ($query->getResult() as $row) {
      $alamat = '';
      $q_alamat = $this->general_model->select_data('alamat','*',"id_reff = '".$row->id."' AND table_reff = 'supplier'");
      $data_alamat=[
        'id_reff'=>$row->id,
        'table_reff'=>'supplier',
        'jenis'=> '',
        'alamat' => '',
        'blok' => '',
        'no' => '',
        'rt' => '',
        'rw' => '',
        'id_province' => '',
        'id_city' => '',
        'id_subdistrict'=> '',
        'kelurahan' => '',
        'kd_pos' => '',
      ];

      if ($q_alamat->getNumRows()>0){
        foreach ($q_alamat->getResult() as $rows) {
          $kecamatan = $this->general_model->get_data_by_field('alamat_subdistrict','subdistrict',array('id'=>$rows->id_subdistrict));
          $kota = $this->general_model->get_data_by_field('alamat_city','city',array('id'=>$rows->id_city));
          $provinsi = $this->general_model->get_data_by_field('alamat_province','province',array('id'=>$rows->id_province));
          $param = "'".$rows->alamat." ".$rows->blok." ".$rows->no." RT.".$rows->rt." RW.".$rows->rt." ".$rows->kelurahan." ".$kota." ".$kecamatan." ".$provinsi." ".$rows->kd_pos."'";
          $alamat = $alamat .' '. $kecamatan .' - '. $kota.' <br><a class="btn btn-info btn-xs btn-border btn-round" onClick="detail_alamat('.$param.')">Detail</a>	 <br>';

          $data_alamat=[
            'id_reff'=>$row->id,
            'table_reff'=>'supplier',
            'jenis'=> $rows->jenis,
            'alamat' => $rows->alamat,
            'blok' => $rows->blok,
            'no' => $rows->no,
            'rt' => $rows->rt,
            'rw' => $rows->rw,
            'id_province' => $rows->id_province,
            'id_city' => $rows->id_city,
            'id_subdistrict'=> $rows->id_subdistrict,
            'kelurahan' => $rows->kelurahan,
            'kd_pos' => $rows->kd_pos,
          ];

        }
      }

      if ($row->status == '0'){
        $aktif = '<span style="color:red;">tidak_aktif</span>';
        $tombol = '<a class="btn btn-success btn-xs btn-border btn-round col-md-12" style="margin-top:5px;" onClick="activate('.$row->id.')"><b>AKTIFKAN</b></a>';
      } else {
        $aktif = 'aktif';
        $tombol = '<a class="btn btn-danger btn-xs btn-border btn-round col-md-12" style="margin-top:5px;" onClick="activate('.$row->id.')"><b>NONAKTIFKAN</b></a>';
      }

      $edit = '<a class="btn btn-info btn-xs btn-border btn-round col-md-12" href="#modaleditdata" id="tmblmodaleditdata"
      data-id_supplier="'.$row->id.'"
      data-nama="'.$row->nama.'"
      data-telp="'.$row->telp.'"
      data-hp="'.$row->hp.'"
      data-bank="'.$row->bank.'"
      data-bank_name="'.$row->bank_name.'"
      data-bank_norek="'.$row->bank_norek.'"
      data-jenis="'.$data_alamat['jenis'].'"
      data-alamat="'.$data_alamat['alamat'].'"
      data-blok="'.$data_alamat['blok'].'"
      data-no="'.$data_alamat['no'].'"
      data-rt="'.$data_alamat['rt'].'"
      data-rw="'.$data_alamat['rw'].'"
      data-id_province="'.$data_alamat['id_province'].'"
      data-id_city="'.$data_alamat['id_city'].'"
      data-id_subdistrict="'.$data_alamat['id_subdistrict'].'"
      data-kelurahan="'.$data_alamat['kelurahan'].'"
      data-kd_pos="'.$data_alamat['kd_pos'].'"
      data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal">
      <b>EDIT</b></a>';

      echo '<tr>
        <td>'.$no.'</td>
        <td>'.$row->nama.'</td>
        <td>- '.$row->telp.' <br>- '.$row->hp.'</td>
        <td>'.$alamat.'</td>
        <td>'.$row->bank.' - '.$row->bank_norek.' '.$row->bank_name.'</td>
        <td>'.$aktif.'</td>
        <td>'.$edit.''.$tombol.'
        </td>

      </tr>';
      $no++;

    }
    echo '</tbody>';
    echo '</table>';
  }

  public function activate(){
    $id = $this->request->getPost('id');
    $get_status = $this->general_model->get_data_by_field('supplier','status',['id'=>$id]);
    if ($get_status=='0'){
      $data = ['status'=>null];
    } else {
      $data = ['status'=>0];
    }
    $this->general_model->do_update('supplier',$data,['id'=>$id]);
  }

  public function get_data_supplier(){
    $this->authentication->is_login_option();
    $q = $this->general_model->select_order('supplier','*',"status IS NULL",'nama ASC');
    echo '<option value="">-- Pilih Supplier --</option>';
    foreach ($q->getResult() as $row) {
      echo '<option value="'.$row->id.'">'.$row->nama.'</option>';
    }
  }

}
