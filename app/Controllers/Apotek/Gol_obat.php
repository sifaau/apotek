<?php
namespace App\Controllers\Apotek;
use App\Controllers\BaseController;
use App\Models\General_model;

class Gol_obat extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    $this->main_table = 'obat_gol';
    helper("general");
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->apotek("gol_obat/list", $data);
  }

  public function action_create(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_parent' => for_validate('id_parent','required'),
      'nama' => for_validate('nama','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $action = $this->request->getPost('action');
      $id = $this->request->getPost('id');
      $id_parent = $this->request->getPost('id_parent');
      $nama       = $this->request->getPost('nama');
      $keterangan = $this->request->getPost('keterangan');

      $this->db->transBegin();
      $data = [
        'id_parent'=>$id_parent,
        'nama'=>$nama,
        'keterangan'=>$keterangan,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      if ($action == 'edit'){
        $save = $this->general_model->do_update($this->main_table,$data,['id'=>$id]);
        $error1 = $this->general_model->error();
      } else {
        $check = $this->general_model->count_rows($this->main_table,'id',['nama'=>$nama,'id_parent'=>$id_parent]);
        if ($check>0){
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, nama golongan sudah ada','error1'=>''));
          exit;
        } else {
          $save = $this->general_model->do_save($this->main_table,$data);
          $error1 = $this->general_model->error();
          $id_supplier = $this->general_model->insert_id();
        }
      }


      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>$action.' GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>$action.' OK'));
      }

    }
  }

  public function get_data(){
    $field = ['','Golongan - Sub Golongan','Keterangan','-'];
    $query = $this->general_model->select_order($this->main_table,'*',['id_parent'=>'0'],"status ASC");
    echo '<table id="basic-datatables" class="display table">';
    echo '<thead>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</thead>';
    echo '<tfoot>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</tfoot>';
    echo '<tbody>';
    $no = 1;
    foreach ( $query->getResult() as $row ) {

      if ($row->status == '0'){
        $aktif = '<span style="color:red;">(tidak_aktif)</span>';
        $tombol = '<button class="btn btn-success btn-xs btn-icon btn-round" onClick="activate('.$row->id.')"><i class="fa fa-check"></i></button>';
      } else {
        $aktif = '<i style="color:#48abf7;">aktif</i>';
        $tombol = '<button class="btn btn-danger btn-xs btn-icon  btn-round" onClick="activate('.$row->id.')"><i class="fas fa-window-close"></i></button>';
      }

      $edit = '<button class="btn btn-info btn-xs btn-icon btn-round" href="#modaleditdata" id="tmblmodaleditdata"
      data-id="'.$row->id.'"
      data-id_parent="'.$row->id_parent.'"
      data-nama="'.$row->nama.'"
      data-keterangan="'.$row->keterangan.'"
      data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal">
      <i class="fa fa-pen"></i></button>';

      echo '<tr>';
      echo '<td style="padding:0 3px !important;height:35px" >'.$no.'.    '.$row->nama.'  <span class="pull-right">'.$edit.' '.$tombol.'</span></td>';
      echo '<td style="padding:0 3px !important;height:35px" >'.$aktif.'</td>';
      echo '<td style="padding:0 3px !important;height:35px" >'.$row->keterangan.'</td>';
      echo '</tr>';

      $query2 = $this->general_model->select_order($this->main_table,'*',['id_parent'=>$row->id],"status ASC");
      $no2=1;
      foreach ( $query2->getResult() as $row2 ) {
        if ($row2->status == '0'){
          $aktif2 = '<span style="color:red;">(tidak_aktif)</span>';
          $tombol2 = '<button class="btn btn-success btn-xs btn-icon btn-round" onClick="activate('.$row2->id.')"><i class="fa fa-check"></i></button>';
        } else {
          $aktif2 = '<i style="color:#48abf7;">aktif</i>';
          $tombol2 = '<button class="btn btn-danger btn-xs btn-icon btn-round" onClick="activate('.$row2->id.')"><i class="fa fa-window-close"></i></button>';
        }

        $edit2 = '<button class="btn btn-info btn-xs btn-icon btn-round" href="#modaleditdata" id="tmblmodaleditdata"
        data-id="'.$row2->id.'"
        data-id_parent="'.$row2->id_parent.'"
        data-nama="'.$row2->nama.'"
        data-keterangan="'.$row2->keterangan.'"
        data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal">
        <i class="fa fa-pen"></i></button>';

        echo '<tr>';
        echo '<td style="padding:0 3px !important;height:35px" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$no.'.'.$no2.'.    '.$row2->nama.' <span class="pull-right">'.$edit2.' '.$tombol2.'</span></td>';
        echo '<td style="padding:0 3px !important;height:35px" >'.$aktif2.'</td>';
        echo '<td style="padding:0 3px !important;height:35px" >'.$row2->keterangan.'</td>';
        echo '</tr>';
        $no2++;
      }
      $no++;

    }
    echo '</tbody>';
    echo '</table>';
  }

  public function get_golongan(){
    $id_parent = $this->request->getPost('id_parent');
    $this->authentication->is_login_option();
    $q = $this->general_model->select_order('obat_gol','*',['id_parent'=>$id_parent],'nama ASC');
    if ($id_parent == '0'){
      echo '<option value="0">-- LEVEL 1 --</option>';
    }
    foreach ($q->getResult() as $row) {
      echo '<option value="'.$row->id.'">'.$row->nama.'</option>';
    }
  }

  public function get_golongan_by_reff(){
    $reff = $this->request->getPost('reff');
    $this->authentication->is_login_option();
    $q = $this->general_model->select_distinct_order($reff.' as o, obat_gol as g','g.id,g.nama,g.id_parent'," o.id_golongan=g.id AND o.status IS NULL",'g.nama ASC');
    echo '<option value="">- Pilih Golongan-</option>';
    foreach ($q->getResult() as $row) {
      $nama_parent = $this->general_model->get_data_by_field('obat_gol','nama',"id='".$row->id_parent."'");
      echo '<option value="'.$row->id.'">'.$nama_parent.' - '.$row->nama.'</option>';
    }
  }

  public function get_golongan_option(){
    $this->authentication->is_login_option();
    $q = $this->general_model->select_order('obat_gol','id,nama,id_parent',"id_parent > 0 AND status IS NULL",'nama ASC');
    echo '<option value="">- Pilih Golongan -</option>';
    foreach ($q->getResult() as $row) {
      $parent = $this->general_model->get_data_by_field('obat_gol','nama',"id = '".$row->id_parent."' AND status IS NULL");
      echo '<option value="'.$row->id.'">'.$parent.' - '.$row->nama.'</option>';
    }
  }

  public function activate(){
    $id = $this->request->getPost('id');
    $get_status = $this->general_model->get_data_by_field($this->main_table,'status',['id'=>$id]);
    if ($get_status=='0'){
      $data = ['status'=>null];
    } else {
      $data = ['status'=>0];
    }
    $this->general_model->do_update($this->main_table,$data,['id'=>$id]);
    $this->general_model->do_update($this->main_table,$data,['id_parent'=>$id]);
  }


}
