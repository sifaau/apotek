<?php
namespace App\Controllers\Apotek;
use App\Controllers\BaseController;
use App\Models\General_model;

class Obat_perusahaan extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    $this->main_table = 'obat_perusahaan';
    helper("general");
  }


  public function action_create(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => for_validate('nama','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $action = $this->request->getPost('action');
      $id = $this->request->getPost('id');
      $nama       = $this->request->getPost('nama');
      $alamat        = $this->request->getPost('alamat');
      $kota        = $this->request->getPost('kota');
      $telp        = $this->request->getPost('telp');
      $hp        = $this->request->getPost('hp');

      $this->db->transBegin();
      $data = [
        'nama'=>$nama,
        'alamat'=>$alamat,
        'kota'=>$kota,
        'hp'=>$hp,
        'telp'=>$telp,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];

      if ($action == 'edit'){
        $save = $this->general_model->do_update($this->main_table,$data,['id'=>$id]);
        $error1 = $this->general_model->error();
      } else {
        $save = $this->general_model->do_save($this->main_table,$data);
        $error1 = $this->general_model->error();
        $id_supplier = $this->general_model->insert_id();
      }


      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }

    }
  }

  public function get_perusahaan(){
    $this->authentication->is_login_option();
    $q = $this->general_model->select_order($this->main_table,'*',"id > 0",'nama ASC');
      echo '<option value="0">- Pilih -</option>';
    foreach ($q->getResult() as $row) {
      echo '<option value="'.$row->id.'">'.$row->nama.'</option>';
    }
  }



}
