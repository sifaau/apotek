<?php
namespace App\Controllers\Apotek;
use App\Controllers\BaseController;
use App\Models\General_model;

class Gudang extends BaseController
{

  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    $this->main_table = 'gudang';
  }

  public function for_validate($label='',$rules=''){
    return ['label'  => $label,'rules'  => $rules,'errors' => ['required' => ' {field} harus diisi','numeric' => ' {field} harus diisi angka']];
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->apotek("gudang/list", $data);
  }

  public function action_create(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => $this->for_validate('nama','required'),
    ]);

    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id = $this->request->getPost('id');
      $action = $this->request->getPost('action');
      $nama = $this->request->getPost('nama');

      $this->db->transBegin();
      $data = [
        'nama'=>$nama,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      if ($action == 'edit'){
        $save = $this->general_model->do_update($this->main_table,$data,['id'=>$id]);
        $error1 = $this->general_model->error();
      } else {
        $save = $this->general_model->do_save($this->main_table,$data);
        $error1 = $this->general_model->error();
        $id = $this->general_model->insert_id();
      }

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1),'error2'=>$error2));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }
    }
  }

  public function get_data(){
    $field = ['ID','Nama Gudang','Label Rak','-'];
    $query = $this->general_model->select_order($this->main_table,'*',['id'>0],"status ASC");
    echo '<table id="basic-datatables" class="display table">';
    echo '<thead>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</thead>';
    echo '<tfoot>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</tfoot>';
    echo '<tbody>';
    $no = 1;
    foreach ( $query->getResult() as $row ) {

      if ($row->status == '0'){
        $aktif = '<span style="color:red;">(tidak_aktif)</span>';
        $tombol = '<button class="btn btn-success btn-round btn-xs" style="margin-top:5px;" onClick="activate('.$row->id.')"><b>AKTIFKAN GUDANG</b></button>';
      } else {
        $aktif = '<i style="color:#48abf7;">aktif</i>';
        $tombol = '<button class="btn btn-danger btn-round btn-xs" style="margin-top:5px;" onClick="activate('.$row->id.')"><b>NON AKTIFKAN GUDANG</b></button>';
      }

      $edit = '<button class="btn btn-info btn-round btn-xs" href="#modaleditdata" id="tmblmodaleditdata"
      data-id="'.$row->id.'"
      data-nama="'.$row->nama.'"
      data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal" style="margin-top:5px;">
      <b>EDIT GUDANG</b></button>';

      $list_rak = '<table class="table" style="width:100%;">';
      $rak = $this->general_model->select_order('gudang_rak','*',"id_gudang = '".$row->id."'","status ASC");
      foreach ($rak->getResult() as $rowr) {
        if ($row->status == '0'){
          $edit_rak = '';
          $nontaktif_rak = '';
          $rak_aktif = '';
        } else {
          $edit_rak = '<button class="btn btn-icon btn-primary btn-round btn-xs" href="#modaleditrak" id="tmblmodaleditrak"
          data-id="'.$rowr->id.'"
          data-id_gudang="'.$rowr->id_gudang.'"
          data-nama="'.$rowr->nama.'"
          data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal">
          <i class="fa fa-pen"></i></button>';
          if ($rowr->status == '0'){
            $rak_aktif = '<span style="color:red;">(tidak_aktif)</span>';
            $nontaktif_rak = '
            <button class="btn btn-icon btn-success btn-round btn-xs" onClick="activate_rak('.$rowr->id.')"><i class="fa fa-check"></i>
            </button>';
          } else {
            $rak_aktif = '';
            $nontaktif_rak = '
            <button class="btn btn-icon btn-danger btn-round btn-xs" onClick="activate_rak('.$rowr->id.')"><i class="fa fa-minus"></i>
            </button>';
          }
        }

        $check_stock = $this->general_model->select_sum('master_stok','qty',['id_gudang_rak'=>$rowr->id]);
        if ($check_stock>0){
          $btn_lihat = '<button class="btn btn-success btn-round btn-xs" onClick="lihat_item_rak('.$rowr->id.')">
          <b>LIHAT ITEM</b>
          </button>';
        } else {
          $btn_lihat = '<button class="btn btn-default btn-round btn-xs" onClick="lihat_item_rak('.$rowr->id.')" disabled="disabled">
          <b>LIHAT ITEM</b>
          </button>';
        }


        $list_rak .= '<tr style="padding:0 5px !important;height:40px">';
        $list_rak .= '<td style="padding:0 5px !important;height:40px"><span clas="label"><b>'.$rowr->nama.'</b> '.$rak_aktif.'</span></td>';
        $list_rak .= '<td style="padding:0 5px !important;height:40px" class="text-right">
        '.$btn_lihat.'
        '.$edit_rak.'
         '.$nontaktif_rak.'  </td>';
        $list_rak .= '</tr>';
      }





      $list_rak .= '</table>';

      if ($row->status == '0'){
        $add_rak = '';
      } else {
        $add_rak = '<button
        class="btn btn-primary btn-xs btn-round"
        id="tmblmodaladdrak"
        href="#modaladdrak"
        data-id="'.$row->id.'"
        data-nama="'.$row->nama.'"
        data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal"
        >
        <i class="fa fa-plus"></i> <b>TAMBAH RAK</b>
        </button>';
      }

      echo '<tr style="vertical-align: top !important;">';
      echo '<td style="vertical-align: top !important;">'.$row->id.'</td>';
      echo '<td style="vertical-align: top !important;"><b style="font-size:20px;">'.$row->nama.'</b><br>'.$aktif.'</td>';
      echo '<td style="vertical-align: top !important;">'.$list_rak.'</td>';
      echo '<td style="vertical-align: top !important;">'.$add_rak.'<br>'.$edit.'<br>'.$tombol.' </td>';
      echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
  }

  public function activate(){
    $id = $this->request->getPost('id');
    $get_status = $this->general_model->get_data_by_field($this->main_table,'status',['id'=>$id]);
    if ($get_status=='0'){
      $data = ['status'=>null];
    } else {
      $data = ['status'=>0];
    }
    $this->general_model->do_update($this->main_table,$data,['id'=>$id]);
  }


  public function action_create_rak(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => $this->for_validate('nama','required'),
    ]);

    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_gudang = $this->request->getPost('id_gudang');
      $id = $this->request->getPost('id');
      $action = $this->request->getPost('action');
      $nama = $this->request->getPost('nama');

      $this->db->transBegin();
      $data = [
        'id_gudang'=>$id_gudang,
        'nama'=>$nama,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      if ($action == 'edit'){
        $save = $this->general_model->do_update('gudang_rak',$data,['id'=>$id]);
        $error1 = $this->general_model->error();
      } else {
        $save = $this->general_model->do_save('gudang_rak',$data);
        $error1 = $this->general_model->error();
        $id = $this->general_model->insert_id();
      }

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1),'error2'=>$error2));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }
    }
  }

  public function activate_rak(){
    $id = $this->request->getPost('id');
    $get_status = $this->general_model->get_data_by_field('gudang_rak','status',['id'=>$id]);
    if ($get_status=='0'){
      $data = ['status'=>null];
    } else {
      $data = ['status'=>0];
    }
    $this->general_model->do_update('gudang_rak',$data,['id'=>$id]);
  }


  public function option_rak(){
    $rak = $this->general_model->select_order('gudang AS g,gudang_rak AS r','g.nama as gudang,r.nama AS rak,r.id AS id',"g.id = r.id_gudang AND g.status IS NULL AND r.status IS NULL","g.nama ASC,r.nama ASC");
    $html = '';
    if ($rak->getNumRows()>1){
      $html .= '<option value=""> - pilih - </opion>';
    }
    foreach ($rak->getResult() as $rowr) {
      $html .= '<option value="'.$rowr->id.'"> Gudang '.$rowr->gudang.' Rak '.$rowr->rak.' </opion>';
    }
    echo $html;
  }

  public function list_item(){
    $id_rak = $this->request->getGet('id_rak');
    $q = $this->general_model->select_order('master_stok as ms,satuan as s','ms.qty,s.satuan,s.id_reff,s.table_reff,ms.no_batch,ms.date_expired',"ms.id_satuan=s.id AND ms.id_gudang_rak='".$id_rak."'",'ms.id ASC');
    $html = '';
    if ($q->getNumRows() > 0){
      $html .= '<table class="table table-bordered basic-datatables" >';
      $no=1;
      $html .= '<thead>';
      $html .= '<tr>';
      $html .= '<th></th>';
      $html .= '<th>ITEM</th>';
      $html .= '<th>JUMLAH</th>';
      $html .= '<th>SATUAN</th>';
      $html .= '<th>NO BATCH</th>';
      $html .= '<th>EXPIRED</th>';
      $html .= '</tr>';
      $html .= '</thead>';
      $html .= '<tbody>';
      foreach ($q->getResult() as $row) {
        if ($row->table_reff == 'obat'){
          $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_reff]);
        } else if ($row->table_reff == 'barang'){
          $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_reff]);
        } else {
          $nama_item = '';
        }
        if ($row->date_expired == '' OR $row->date_expired == null OR $row->date_expired == '0000-00-00'){
          $date_expired = '';
        } else {
          $date_expired = date('d/m/Y',strtotime($row->date_expired));
        }
        $html .= '<tr>';
        $html .= '<td>#</td>';
        $html .= '<td>'.$nama_item.'</td>';
        $html .= '<td>'.$row->qty.'</td>';
        $html .= '<td>'.$row->satuan.'</td>';
        $html .= '<td>'.$row->no_batch.'</td>';
        $html .= '<td>'.$date_expired.'</td>';
        $html .= '</tr>';
      }
      $html .= '</tbody>';
      $html .= '</table>';
    } else {
      $html .= '<center>Tidak ada item di rak ini.</center>';
    }

    echo $html;
  }





}
