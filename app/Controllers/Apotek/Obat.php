<?php
namespace App\Controllers\Apotek;
use App\Controllers\BaseController;
use App\Models\General_model;
use CodeIgniter\Files\File;

class Obat extends BaseController
{
  protected $helpers = ['form'];

  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    $this->main_table = 'obat';
    helper("general");
    helper("item_apotek");
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->apotek("obat/list", $data);
  }

  public function action_create(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => for_validate('nama','required'),
      'golongan' => for_validate('golongan','required'),
      'perusahaan' => for_validate('perusahaan','required'),
      // 'upload'=>for_validate('Gambar Obat','uploaded[upload]|is_image[upload]|mime_in[upload,image/jpg,image/jpeg,image/gif,image/png,image/webp]'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $action = $this->request->getPost('action');
      $id = $this->request->getPost('id');
      $nama         = $this->request->getPost('nama');
      $golongan     = $this->request->getPost('golongan');
      $perusahaan   = $this->request->getPost('perusahaan');
      $deskripsi    = $this->request->getPost('deskripsi');
      $indikasi     = $this->request->getPost('indikasi');
      $kandungan    = $this->request->getPost('kandungan');
      $zat_aktif    = $this->request->getPost('zat_aktif');
      $efek_samping = $this->request->getPost('efek_samping');
      $dosis        = $this->request->getPost('dosis');
      $aturan_pakai = $this->request->getPost('aturan_pakai');
      $waktu_1 = $this->request->getPost('waktu_1');
      $waktu_2 = $this->request->getPost('waktu_2');
      $waktu_3 = $this->request->getPost('waktu_3');
      $waktu_4 = $this->request->getPost('waktu_4');
      $waktu_5 = $this->request->getPost('waktu_5');
      $syarat = $this->request->getPost('syarat');
      // $img = $this->request->getFile('upload');



      $this->db->transBegin();
      $data = [
        'nama'=>$nama,
        'id_golongan'=>$golongan,
        'id_perusahaan'=>$perusahaan,
        'deskripsi'=>$deskripsi,
        'indikasi'=>$indikasi,
        'kandungan'=>$kandungan,
        'zat_aktif'=>$zat_aktif,
        'efek_samping'=>$efek_samping,
        'dosis'=>$dosis,
        'aturan_pakai'=>$aturan_pakai,
        'waktu_1'=>$waktu_1,
        'waktu_2'=>$waktu_2,
        'waktu_3'=>$waktu_3,
        'waktu_4'=>$waktu_4,
        'waktu_5'=>$waktu_5,
        'syarat_pakai'=>$syarat,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      if ($action == 'edit'){
        $save = $this->general_model->do_update($this->main_table,$data,['id'=>$id]);
        $id_item = $id;
        $error1 = $this->general_model->error();
      } else {
        $save = $this->general_model->do_save($this->main_table,$data);
        $error1 = $this->general_model->error();
        $id_item = $this->general_model->insert_id();
      }


      if ($this->db->transStatus() === false ) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        // $newName = $img->getRandomName();
        // $img->move(WRITEPATH . 'uploads/obat', $newName);
        // $filepath = WRITEPATH . 'uploads/obat/' . $img->store();
        // $data_file = new File($filepath);
        echo json_encode(array('response'=>TRUE,'message'=>'OK','id_item'=>$id_item));
      }

    }
  }

  public function get_data(){
    $search = $this->request->getGet('search');
    if ($search == ''){
      $condition = " id > 0 ";
      $limit = 50;
    } else {
      $condition = " nama LIKE '%".$search."%'
      OR deskripsi LIKE '%".$search."%'
      OR indikasi LIKE '%".$search."%'
      OR kandungan LIKE '%".$search."%'
      OR zat_aktif LIKE '%".$search."%'
      OR efek_samping LIKE '%".$search."%'
      OR dosis LIKE '%".$search."%'
      OR aturan_pakai LIKE '%".$search."%'
      OR syarat_pakai LIKE '%".$search."%'
      OR waktu_1 LIKE '%".$search."%'
      OR waktu_2 LIKE '%".$search."%'
      OR waktu_3 LIKE '%".$search."%'
      OR waktu_4 LIKE '%".$search."%'
      ";
      $limit = 100;
    }

    $field = ['ID','Gambar','Nama Obat','Golongan','Kandungan & Zat Aktif','Stok','HNA','Margin','Harga Jual','Last Update',''];
    $query = $this->general_model->select_order_limit($this->main_table,'*',$condition,"status ASC,DATE(date_update) DESC,nama ASC",$limit);
    echo '<i>Hanya menampilkan maksimal 50 data tergantung performa komputer, gunakan fitur cari untuk lebih spesifik.</i><br><br>';
    echo '<table id="basic-datatables" class="display table table-bordered table-hover" >';
    echo '<thead>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</thead>';
    echo '<tfoot>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</tfoot>';
    echo '<tbody>';
    $no=1;
    foreach ($query->getResult() as $row) {

      $golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$row->id_golongan]);
      $perusahaan = $this->general_model->get_data_by_field('obat_perusahaan','nama',['id'=>$row->id_perusahaan]);
      if ($perusahaan == ''){
        $perusahaan = '';
      } else {
        $perusahaan = 'Diproduksi oleh : '.$perusahaan.'<br>';
      }
      if ($golongan == ''){
        $golongan = '';
      } else {
        $golongan = $golongan;
      }

      $edit = '<button type="button" class="btn btn-icon btn-round btn-info btn-xs" href="#modaleditdata" id="tmblmodaleditdata"
      data-id="'.$row->id.'"
      data-nama="'.$row->nama.'"
      data-id_golongan="'.$row->id_golongan.'"
      data-id_perusahaan="'.$row->id_perusahaan.'"
      data-deskripsi="'.$row->deskripsi.'"
      data-indikasi="'.$row->indikasi.'"
      data-zat_aktif="'.$row->zat_aktif.'"
      data-kandungan="'.$row->kandungan.'"
      data-efek_samping="'.$row->efek_samping.'"
      data-dosis="'.$row->dosis.'"
      data-aturan_pakai="'.$row->aturan_pakai.'"
      data-waktu_1="'.$row->waktu_1.'"
      data-waktu_2="'.$row->waktu_2.'"
      data-waktu_3="'.$row->waktu_3.'"
      data-waktu_4="'.$row->waktu_4.'"
      data-waktu_5="'.$row->waktu_5.'"
      data-syarat_pakai="'.$row->syarat_pakai.'"
      data-backdrop="static" data-keyboard="false" role="button" data-toggle="modal">
      <i class="fa fa-pen"></i></button>';

      $edit_satuan = '<button type="button" class="btn btn-icon btn-round btn-danger btn-xs" onClick="modal_edit_satuan('.$row->id.')"><i class="fa fa-cog"></i></button>';
      if ($row->image == '' || $row->image == null){
        $image = '<img src="'.base_url().'/assets/img/noimage.png" alt="..." class="avatar-img rounded-circle">';
      } else {
        $image = '<img src="'.base_url().'/assets/img/obat/'.$row->image.'" alt="..." class="avatar-img rounded-circle">';
      }
      $stok = text_stock_item_terkecil($row->id,'obat');
      $hna = $this->general_model->get_data_by_field('satuan','harga_beli',['id_reff'=>$row->id,'table_reff'=>'obat','ket'=>1]);
      if ($hna == '' || $hna == null){
        $hna = 0;
      }
      $margin = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id_reff'=>$row->id,'table_reff'=>'obat','ket'=>1]);
      if ($margin == '' || $margin == null){
        $margin = 0;
      }
      $harga_jual = $hna + ($hna*($margin/100));
      $style_hrj = '';
      $harga_jual = $this->general_model->get_data_by_field('satuan','harga_jual',['id_reff'=>$row->id,'table_reff'=>'obat','ket'=>1]);

      if ($harga_jual === $hna){
        $style_hrj = 'style="color:red;"';
      }
      $btn_view = '<button class="btn btn-icon btn-xs btn-round btn-default" onClick="modal_detail_obat('.$row->id.')"><i class="fa fa-eye"></i></button>';

      if ($row->status == '0'){
        $btn_delete = '<button class="btn btn-icon btn-xs btn-round" onClick="modal_delete_obat('.$row->id.','.$row->status.')"><i class="fa fa-check"></i></button>';
        $btn_view = '<span class="badge badge-danger">nontaktif</span>';
        $edit_satuan = $edit = '';
        $style_row = 'style="background:#ffdfdf;"';
      } else {
        $btn_delete = '<button class="btn btn-icon btn-xs btn-round btn-default" onClick="modal_delete_obat('.$row->id.','.$row->status.')"><i class="fa fa-trash"></i></button>';
        $style_row = '';
      }
      if ($row->date_update =='' || $row->date_update == null){
        $date_update = '';
      } else {
        $date_update = date('d/m/Y',strtotime($row->date_update));
      }

      echo '<tr '.$style_row.'>';
      echo '<td>'.$no.'</td>';
      echo '<td>
      <a class="avatar" onClick="open_modal_ganti_gambar('.$row->id.')" id="imageobat'.$row->id.'">
      '.$image.'
      </a>
      </td>';
      echo '<td>'.$row->nama.'</td>';
      echo '<td>'.$golongan.'</td>';
      echo '<td>'.$row->kandungan.'<br><span style="color:#b34f06;">'.$row->zat_aktif.'</span></td>';
      echo '<td style="padding: 0 5px !important;" class="text-left">'.$stok.'</td>';
      echo '<td class="text-right">'.$hna.'</td>';
      echo '<td class="text-right">'.$margin.'%</td>';
      echo '<td class="text-right" '.$style_hrj.'>'.round($harga_jual).'</td>';
      echo '<td class="text-right">'.$date_update.'</td>';
      echo '<td>'.$btn_view.'  '.$edit_satuan.'  '.$edit.'  '.$btn_delete.'</td>';
      echo '</tr>';
      $no++;
    }
    echo '</tbody>
    </table>';
  }

  public function open_edit_data_obat(){
    $id = $this->request->getPost('id');
    $query = $this->general_model->select_data('obat','*',['id'=>$id]);
    $x = [];
    foreach ($query->getResult() as $row) {
      $x['id'] = $row->id;
      $x['nama'] = $row->nama;
      $x['id_golongan'] = $row->id_golongan;
      $x['id_perusahaan'] = $row->id_perusahaan;
      $x['deskripsi'] = $row->deskripsi;
      $x['zat_aktif'] = $row->zat_aktif;
      $x['kandungan'] = $row->kandungan;
      $x['efek_samping'] = $row->efek_samping;
      $x['dosis'] = $row->dosis;
      $x['aturan_pakai'] = $row->aturan_pakai;
      $x['waktu_1'] = $row->waktu_1;
      $x['waktu_2'] = $row->waktu_2;
      $x['waktu_3'] = $row->waktu_3;
      $x['waktu_4'] = $row->waktu_4;
      $x['waktu_5'] = $row->waktu_5;
      $x['syarat_pakai'] = $row->syarat_pakai;
    }
    echo json_encode($x);
  }

  public function action_create_satuan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => for_validate('nama','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $action = $this->request->getPost('action');
      $id = $this->request->getPost('id');
      $nama         = $this->request->getPost('nama');

      $check = $this->general_model->count_rows('satuan_master','id',['nama'=>$nama]);
      if ($check>0){
        echo json_encode(array('response'=>FALSE,'message'=>'Nama satuan sudah ada'));
      } else {

        $this->db->transBegin();
        $data = [
          'nama'=>$nama,
          'date_insert'=>date('Y-m-d H:i:s'),
          'id_user'=>$this->id_user,
        ];
        if ($action == 'edit'){
          $save = $this->general_model->do_update('satuan_master',$data,['id'=>$id]);
          $error1 = $this->general_model->error();
        } else {
          $save = $this->general_model->do_save('satuan_master',$data);
          $error1 = $this->general_model->error();
          $id = $this->general_model->insert_id();
        }

        if ($this->db->transStatus() === false) {
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
        } else {
          $this->db->transCommit();
          echo json_encode(array('response'=>TRUE,'message'=>'OK'));
        }

      }
    }
  }

  public function get_satuan(){
    $this->authentication->is_login_option();
    $q = $this->general_model->select_order('satuan_master','*',"id>0",'nama ASC');
    echo '<option value="">-- Pilih Satuan --</option>';
    foreach ($q->getResult() as $row) {
      echo '<option value="'.$row->nama.'">'.$row->nama.'</option>';
    }
  }

  public function get_satuan_by_id(){
    $this->authentication->is_login_option();
    $id  = $this->request->getPost('id');
    $reff = 'obat';
    $q = $this->general_model->select_order('satuan','id,satuan,ket',['id_reff'=>$id,'table_reff'=>$reff],'ket ASC');
    echo '<option value="">-- Pilih Satuan --</option>';
    foreach ($q->getResult() as $row) {
      echo '<option value="'.$row->id.'">- '.$row->satuan.'</option>';
    }
  }

  public function edit_satuan_obat(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_reff' => for_validate('ID Reff','required'),
      'satuan' => for_validate('Satuan','required'),
      'i' => for_validate('i','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_reff = $this->request->getPost('id_reff');
      $satuan = $this->request->getPost('satuan');
      $i = (int)$this->request->getPost('i');

      $data = array(
        'id_reff'=>$id_reff,
        'table_reff'=>'obat',
        'satuan'=>$satuan,
        'ket'=>$i,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      );
      if ($i === 1){
        $check_satuan_sebelumnya = 1;
      } else {
        $check_satuan_sebelumnya = $this->general_model->count_rows('satuan','id',"id_reff='".$id_reff."' AND table_reff='obat' AND ket < '".$i."'");
        if ($check_satuan_sebelumnya === $i-1){

        } else {
          $check_satuan_sebelumnya = 0;
        }
      }

      if ($check_satuan_sebelumnya>0){
        $check_satuan_lain  = $this->general_model->count_rows('satuan','id',"id_reff='".$id_reff."' AND table_reff='obat' AND satuan='".$satuan."' AND ket <> '".$i."'");
        if ($check_satuan_lain>0){
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL,satuan sudah dipakai.'.$id_reff));
        } else {
          $check = $this->general_model->count_rows('satuan','id',['id_reff'=>$id_reff,'table_reff'=>'obat','ket'=>$i]);
          if ( $check > 0 ){
            $this->general_model->do_update('satuan',$data,['id_reff'=>$id_reff,'table_reff'=>'obat','ket'=>$i]);
          } else {
            $this->general_model->do_save('satuan',$data);
          }
          if ($this->db->transStatus() === false) {
            $this->db->transRollback();
            echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
          } else {
            $this->db->transCommit();
            echo json_encode(array('response'=>TRUE,'message'=>'OK'));
          }
        }
      } else {
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL,satuan sebelumnya belum disetting.'.$i));
      }


    }
  }

  public function action_save_satuan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_reff' => for_validate('ID Reff','required'),
      'satuan1' => for_validate('Satuan Terkecil','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_reff        = $this->request->getPost('id_reff');

      $this->db->transBegin();

      for ( $i=1;$i<5;$i++ ) {

        ${'satuan'.$i}            = $this->request->getPost('satuan'.$i);
        ${'konversi'.$i}          = $this->request->getPost('konversi'.$i);
        ${'harga_beli'.$i}        = $this->request->getPost('harga_beli'.$i);
        ${'persen_harga_jual'.$i} = $this->request->getPost('persen_harga_jual'.$i);
        ${'harga_jual'.$i}        = $this->request->getPost('harga_jual'.$i);
        ${'persen_diskon'.$i}     = $this->request->getPost('persen_diskon'.$i);

        if ( ${'satuan'.$i} != '' && ${'satuan'.$i} != NULL ){
          $data = array(
            'id_reff'=>$id_reff,
            'table_reff'=>'obat',
            'satuan'=>${'satuan'.$i},
            'harga_beli'=>${'harga_beli'.$i},
            'persen_harga_jual'=>${'persen_harga_jual'.$i},
            'harga_jual'=>${'harga_jual'.$i},
            'persen_diskon'=>${'persen_diskon'.$i},
            'konversi'=>${'konversi'.$i},
            'ket'=>$i,
            'date_insert'=>date('Y-m-d H:i:s'),
            'id_user'=>$this->id_user,
          );
          $check = $this->general_model->count_rows('satuan','id',['id_reff'=>$id_reff,'table_reff'=>'obat','ket'=>$i]);
          if ( $check > 0 ){
            $this->general_model->do_update('satuan',$data,['id_reff'=>$id_reff,'table_reff'=>'obat','ket'=>$i]);
          } else {
            $this->general_model->do_save('satuan',$data);
          }
          $check = $this->general_model->do_update('obat',['date_update'=>date('Y-m-d H:i:s')],['id'=>$id_reff]);

        }

      }

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }

    }
  }

  public function action_save_harga_jual_satuan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_reff' => for_validate('ID Reff','required'),
      'ket' => for_validate('ket','required'),
      'harga_jual' => for_validate('harga_jual','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_reff        = $this->request->getPost('id_reff');

      $this->db->transBegin();
      $ket = $this->request->getPost('ket');
      $harga_jual = $this->request->getPost('harga_jual');

      $data = array(
        'harga_jual'=>$harga_jual,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      );
      $this->general_model->do_update('satuan',$data,['id_reff'=>$id_reff,'table_reff'=>'obat','ket'=>$ket]);

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }

    }
  }

  public function get_detail_satuan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id_reff' => for_validate('ID Reff','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_reff        = $this->request->getPost('id_reff');
      $q = $this->general_model->select_data('satuan','*',['id_reff'=>$id_reff,'table_reff'=>'obat']);
      echo json_encode(array('response'=>TRUE,'message'=>'OK','data'=>$q->getResult()));
    }
  }

  public function get_pilih_obat_for_pembelian(){
    helper(['form', 'url']);
    $input = $this->validate([
      'search' => for_validate('Cari','string'),
      'jenis_trans' => for_validate('Jenis Trans','required'),
    ]);

    if (!$input) {
      echo '<div class="card-title fw-mediumbold">'.$this->validator->listErrors().'</div>';
    } else {
      $search = $this->request->getGet('search');
      $jenis_trans = $this->request->getGet('jenis_trans');
      $golongan = $this->request->getGet('golongan');
      if ($golongan == '' || $golongan == null || $golongan == 'null'){
        $con_golongan = '';
      } else {
        $con_golongan = " AND o.id_golongan = '".$golongan."' ";
      }
      if ($search == '' || $search == null || $search == 'null'){
        $con_search = '';
      } else {
        $con_search = " AND ( o.nama LIKE '%".$search."%' OR o.deskripsi LIKE '%".$search."%' OR o.indikasi LIKE '%".$search."%' OR o.kandungan LIKE '%".$search."%' ) ";
      }
      $q = $this->general_model->select_order_limit('obat as o,obat_gol as g','o.*,g.nama as nama_gol',"o.id_golongan=g.id AND o.status IS NULL AND (SELECT COUNT(s.id) FROM satuan as s WHERE s.id_reff=o.id AND s.table_reff = 'obat' ) > 0   ".$con_search.$con_golongan,'o.nama ASC',100);
      if ($q->getNumRows() > 0){
        echo '<table class="table table-bordered">';
        echo '<tr>
        <th>Gambar</th>
        <th>Nama</th>
        <th>STOK</th>
        <th></th>
        </tr>
        ';
        foreach ($q->getResult() as $row){
          if ($row->image == '' || $row->image == null){
            $image = '<img src="'.base_url().'/assets/img/noimage.png" class="avatar-img rounded-circle">';
          } else {
            $image = '<img src="'.base_url().'/assets/img/obat/'.$row->image.'" class="avatar-img rounded-circle">';
          }

          if ($jenis_trans=='opb'){
            $param = "'".$row->id."','obat'";
            $add_btn = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item('.$param.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i>
            </button>';
          } else {
            $add_btn = '';
          }

          if ( $row->syarat_pakai == '' OR $row->syarat_pakai == 'null' OR $row->syarat_pakai == null ){
            $syarat_pakai = '-';
          } else {
            $syarat_pakai = $row->syarat_pakai.' makan.';
          }

          if ( $row->aturan_pakai == '' OR $row->aturan_pakai == 'null' OR $row->aturan_pakai == null ){
            $aturan_pakai = '-';
          } else {
            $aturan_pakai = $row->aturan_pakai.' x ';
          }
          $stok = '';
          $stok = text_stock_item_terkecil($row->id,'obat');

          $param = "'".$row->id."','obat'";
          echo '<tr>
          <td>
          <div class="avatar text-center">
          '.$image.'
          </div>
          </td>
          <td><b>'.$row->nama.'</b><br><i class="fas fa-box-open"></i> '.$row->nama_gol.'</td>
          <td>
          '.$stok.'
          </td>
          <td class="text-center">
          '.$add_btn .'

          </td>
          </tr>';
        }
        echo '</table>';
      } else {
        echo '<div class="card-title fw-mediumbold">Data tidak ditemukan, silakan cek data master.</div>';
      }
    }
  }

  public function get_pilih_obat(){
    helper(['form', 'url']);
    $input = $this->validate([
      'search' => for_validate('Cari','string'),
      'jenis_trans' => for_validate('Jenis Trans','required'),
    ]);

    if (!$input) {
      echo '<div class="card-title fw-mediumbold">'.$this->validator->listErrors().'</div>';
    } else {
      $reff = 'obat';
      $search = $this->request->getGet('search');
      $jenis_trans = $this->request->getGet('jenis_trans');
      $golongan = $this->request->getGet('golongan');
      if ($golongan == '' || $golongan == null || $golongan == 'null'){
        $con_golongan = '';
      } else {
        $con_golongan = " AND id_golongan = '".$golongan."' ";
      }
      if ($search == '' || $search == null || $search == 'null'){
        $con_search = '';
      } else {
        $con_search = " AND ( nama LIKE '%".$search."%' OR deskripsi LIKE '%".$search."%' OR indikasi LIKE '%".$search."%' OR kandungan LIKE '%".$search."%' ) ";
      }
      $q = $this->general_model->select_order_limit($reff,'*',"status IS NULL AND (SELECT COUNT(s.id) FROM satuan as s WHERE s.id_reff=".$reff.".id AND s.table_reff = '".$reff."' ) > 0   ".$con_search.$con_golongan,'nama ASC',50);
      if ($q->getNumRows() > 0){
        echo '<div class="row">';
        echo '<div class="col-md-12">';
        echo '<i Hanya akan menampilkan 50 data saja, silakan gunankan fitur cari untuk lebih spesifik.></i>';
        echo '</div>';
        echo '<div class="col-md-12">';
        echo '<table class="table table-bordered">';
        echo '<tr><th>ITEM</th><th>NO BATCH</th> <th>EXP</th> <th>LOKASI</th> <th style="width:150px;" class="text-right">Satuan terkecil</th> <th style="width:150px;" class="text-right">satuan_2</th> <th style="width:150px;" class="text-right">satuan_3</th> <th style="width:150px;" class="text-right">satuan terbesar</th></tr>';
        foreach ($q->getResult() as $row){

          $satuan1 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>1]);
          $satuan2 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>2]);
          $satuan3 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>3]);
          $satuan4 = $this->general_model->get_data_by_field('satuan','satuan',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>4]);

          $id_satuan1 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>1]);
          $id_satuan2 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>2]);
          $id_satuan3 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>3]);
          $id_satuan4 = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$row->id,'table_reff'=>$reff,'ket'=>4]);

          if ($row->image == '' || $row->image == null){
            $image = '<img src="'.base_url().'/assets/img/noimage.png" class="avatar-img rounded-circle">';
          } else {
            $image = '<img src="'.base_url().'/assets/img/obat/'.$row->image.'" class="avatar-img rounded-circle">';
          }

          $qstock = $this->general_model->select_distinct($reff.' as o,satuan as s,gudang_rak as gr,gudang as g,master_stok as ms','ms.no_batch,ms.date_expired,g.nama as gudang,gr.id as id_rak,gr.nama as rak',
          "s.id_reff=o.id AND s.table_reff='".$reff."' AND gr.id_gudang=g.id
          AND ms.id_satuan=s.id AND gr.id=ms.id_gudang_rak
          AND o.id= '".$row->id."' AND s.id_reff='".$row->id."' AND ms.qty > 0
          ");
          $stok = '';
          if ($qstock->getNumRows() > 0){

            // $stok = '<table class="table table-striped">';
            // $stok .= '<tr></tr>';
            $jml_stok = 0;
            foreach ($qstock->getResult() as $rows) {
              // if ($jenis_trans=='opb'){
              //   $add_btn_batch = '';
              // } else {
              // $param2 = "'".$rows->id."','obat','".$rows->date_expired."','".$rows->no_batch."'";
              // $add_btn_batch = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param2.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i>
              // </button>';
              // }
              if($rows->date_expired == '' OR $rows->date_expired == null OR $rows->date_expired == '0000-00-00' OR $rows->date_expired == '1970-01-01'){
                $date_expired = '';
              } else {
                $date_expired = date('d/m/Y',strtotime($rows->date_expired));
              }

              $stok1 = text_stock_item_terkecil_by_satuan_batch($id_satuan1,$rows->id_rak,$rows->no_batch,$rows->date_expired);
              $stok2 = text_stock_item_terkecil_by_satuan_batch($id_satuan2,$rows->id_rak,$rows->no_batch,$rows->date_expired);
              $stok3 = text_stock_item_terkecil_by_satuan_batch($id_satuan3,$rows->id_rak,$rows->no_batch,$rows->date_expired);
              $stok4 = text_stock_item_terkecil_by_satuan_batch($id_satuan4,$rows->id_rak,$rows->no_batch,$rows->date_expired);

              $param1 = "'".$row->id."','".$reff."','".$satuan1."','".$rows->date_expired."','".$rows->no_batch."'";
              $param2 = "'".$row->id."','".$reff."','".$satuan2."','".$rows->date_expired."','".$rows->no_batch."'";
              $param3 = "'".$row->id."','".$reff."','".$satuan3."','".$rows->date_expired."','".$rows->no_batch."'";
              $param4 = "'".$row->id."','".$reff."','".$satuan4."','".$rows->date_expired."','".$rows->no_batch."'";





              if ((int)$stok4 > 0){
                $add_btn_batch4 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param4.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';
                $add_btn_batch3 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param3.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';
                $add_btn_batch2 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param2.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';
                $add_btn_batch1 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param1.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';


              } else {

                $add_btn_batch4 = '';
                $add_btn_batch3 = '';
                $add_btn_batch2 = '';
                $add_btn_batch1 = '';

                if ((int)$stok3 > 0){
                  $add_btn_batch3 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param3.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';
                  $add_btn_batch2 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param2.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';
                  $add_btn_batch1 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param1.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';

                } else {

                  if ((int)$stok2 > 0){
                    $add_btn_batch2 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param2.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';
                    $add_btn_batch1 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param1.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';
                  } else {

                    if ((int)$stok1 > 0){
                      $add_btn_batch1 = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_kasir('.$param1.')" id="btn_add_item'.$row->id.'"><i class="fa fa-plus"></i></button>';

                    }

                  }

                }

              }

              $stok .= '<tr style="padding:0 5px !important;height:35px;">
              <td>'.$row->nama.' <br></td>
              <td style="padding:0 5px !important;height:35px;">'.$rows->no_batch.'</td>
              <td style="padding:0 5px !important;height:35px;">'.$date_expired.'</td>
              <td style="padding:0 5px !important;height:35px;"><span class="text-primary pl-1">di '.$rows->rak.' </span><br><span class="text-info pl-1">'.$rows->gudang.'</span></td>
              <td style="padding:0 5px !important;height:35px;width:100px;" class="text-right">'.$stok1.' '.$satuan1.'        '.$add_btn_batch1.'</td>
              <td style="padding:0 5px !important;height:35px;width:100px;" class="text-right">'.$stok2.' '.$satuan2.'        '.$add_btn_batch2.'</td>
              <td style="padding:0 5px !important;height:35px;width:100px;" class="text-right">'.$stok3.' '.$satuan3.'        '.$add_btn_batch3.'</td>
              <td style="padding:0 5px !important;height:35px;width:100px;" class="text-right">'.$stok4.' '.$satuan4.'        '.$add_btn_batch4.'</td>
              </tr>';
              // $jml_stok += $rows->qty;
            }
            // $stok .= '</table>';
          } else {
            $jml_stok = 0;
            $stok .= '<tr style="padding:0 5px !important;height:35px;border:none;">
            <td>'.$row->nama.'</td>
            <td style="padding:0 5px !important;height:35px;"></td>
            <td style="padding:0 5px !important;height:35px;"></td>
            <td style="padding:0 5px !important;height:35px;"></td>
            <td colspan="4"><span class="text-danger pl-3">STOK HABIS</span></span>
            </tr>';
          }

          if ( $row->syarat_pakai == '' OR $row->syarat_pakai == 'null' OR $row->syarat_pakai == null ){
            $syarat_pakai = '-';
          } else {
            $syarat_pakai = $row->syarat_pakai.' makan.';
          }

          if ( $row->aturan_pakai == '' OR $row->aturan_pakai == 'null' OR $row->aturan_pakai == null ){
            $aturan_pakai = '-';
          } else {
            $aturan_pakai = $row->aturan_pakai.' x ';
          }

          echo $stok;

        }

        echo '</div>';
        echo '</div>';


        echo '</table>';
      } else {
        echo '<div class="col-md-12 card-title fw-mediumbold text-center">Data tidak ditemukan, silakan cek data master.</div>';
      }
    }
  }

  public function get_detail_obat(){
    $id = $this->request->getGet('id');
    $q = $this->general_model->select_data('obat','*',['id'=>$id]);
    $html = '';
    if ($q->getNumRows() > 0){
      foreach ($q->getResult() as $row) {
        $html .= '<div class="col-md-12">';
        $html .= '<table class="table table-striped">';

        $html .= '<tr>';
        $html .= '<th>Nama</th>';
        $html .= '<td>: '.$row->nama.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Deskripsi</th>';
        $html .= '<td>: '.$row->deskripsi.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Indikasi</th>';
        $html .= '<td>: '.$row->indikasi.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Kandungan</th>';
        $html .= '<td>: '.$row->kandungan.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Zat Aktif</th>';
        $html .= '<td>: '.$row->zat_aktif.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Efek Samping</th>';
        $html .= '<td>: '.$row->efek_samping.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Dosis</th>';
        $html .= '<td>: '.$row->dosis.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Dosis</th>';
        $html .= '<td>: '.$row->dosis.'</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<th>Aturan Pakai</th>';
        $html .= '<td>
        : '.$row->aturan_pakai.' kali Sehari '.$row->syarat_pakai.' makan
        <ul>
        <li>waktu 1 : '.$row->waktu_1.'</li>
        <li>waktu 2 : '.$row->waktu_2.'</li>
        <li>waktu 3 : '.$row->waktu_3.'</li>
        <li>waktu 4 : '.$row->waktu_4.'</li>
        </ul>
        </td>';
        $html .= '</tr>';

        $html .= '</table>';
        $html .= '</div>';
      }
    }
    echo $html;
  }

  public function get_image(){
    $id = $this->request->getGet('id');
    $nama = $this->general_model->get_data_by_field('obat','nama',['id'=>$id]);
    $image = $this->general_model->get_data_by_field('obat','image',['id'=>$id]);
    if ($image == '' || $image == null){
      $image = '<img src="'.base_url().'/assets/img/noimage.png" alt="..." width="100%">';
    } else {
      $image = '<img src="'.base_url().'/assets/img/obat/'.$image.'" alt="..." width="100%">';
    }
    echo json_encode(['image'=>$image,'nama'=>$nama]);
  }

  public function action_edit_gambar(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('id','required'),
      'upload'=>for_validate('Gambar Obat','uploaded[upload]|is_image[upload]|mime_in[upload,image/jpg,image/jpeg,image/gif,image/png]'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id = $this->request->getPost('id');
      $img = $this->request->getFile('upload');
      $path = ROOTPATH.'public/assets/img/obat/';
      $gambar_lama = $this->general_model->get_data_by_field('obat','image',['id'=>$id]);
      if ($gambar_lama == '' || $gambar_lama == null){

      } else {
        if (file_exists($path.$gambar_lama)) {
          unlink($path.$gambar_lama);
        }
      }

      if (! $img->isValid()) {
        echo json_encode(array('response'=>FALSE,'message'=>$img->getErrorString() . '(' . $img->getError() . ')'));
      } else {
        if (! $img->hasMoved()) {
          $newName = 'tes';
          // $img->move(WRITEPATH . 'uploads', $newName);
          $newName = $img->getRandomName();
          $img->move($path,$newName);
          $this->general_model->do_update('obat',['image'=>$newName],['id'=>$id]);
          $image2 = '<img src="'.base_url().'/assets/img/obat/'.$newName.'" alt="..." class="avatar-img rounded-circle">';
          $image = '<img src="'.base_url().'/assets/img/obat/'.$newName.'" alt="..." alt="..." width="100%">';
          echo json_encode(array('response'=>TRUE,'message'=>'Berhasil','image'=>$image,'image2'=>$image2));
        } else {
          echo json_encode(array('response'=>FALSE,'message'=>'Gagal Upload'));
        }
      }
    }
  }

  public function action_delete(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('id','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {

      $id = $this->request->getPost('id');

      $get_status = $this->general_model->get_data_by_field('obat','status',['id'=>$id]);
      if ($get_status == '0'){
        $data = ['status'=>null];
        $cek_stok = 0;
      } else {
        $data = ['status'=>'0'];
        $cek_stok = get_stock_item_terkecil($id,'obat');
      }

      if ($cek_stok > 0){
        echo json_encode(array('response'=>FALSE,'message'=>'Tidak bisa dihapus, ada stoknya,'));
      } else {
        $this->db->transBegin();
        $this->general_model->do_update('obat',$data,['id'=>$id]);
        if ($this->db->transStatus() === false) {
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
        } else {
          $this->db->transCommit();
          echo json_encode(array('response'=>TRUE,'message'=>'OK'));
        }
      }

    }
  }


}
