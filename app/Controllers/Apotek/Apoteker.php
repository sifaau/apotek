<?php
namespace App\Controllers\Apotek;
use App\Controllers\BaseController;

class Apoteker extends BaseController
{
  public function __construct()
  {

  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->apotek("apoteker/list", $data);
  }



}
