<?php
namespace App\Controllers\Apotek;
use App\Controllers\BaseController;
use App\Models\General_model;

class Racikan extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    $this->main_table = 'obat_racikan';
    helper("general");
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->apotek("racikan/list", $data);
  }

  public function action_create(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => for_validate('nama','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $action = $this->request->getPost('action');
      $id = $this->request->getPost('id');
      $nama         = $this->request->getPost('nama');
      $this->db->transBegin();
      $data = [
        'nama'=>$nama,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      if ($action == 'edit'){
        $save = $this->general_model->do_update($this->main_table,$data,['id'=>$id]);
        $error1 = $this->general_model->error();
      } else {
        $save = $this->general_model->do_save($this->main_table,$data);
        $error1 = $this->general_model->error();
        $id = $this->general_model->insert_id();
      }

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK','id'=>$id));
      }

    }
  }

  public function get_data(){
    $field = ['No','Nama Racikan','Detail'];
    $query = $this->general_model->select_order($this->main_table,'*',['id'>0],"status ASC");
    echo '<table id="basic-datatables" class="display table table-striped table-hover" >';
    echo '<thead>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</thead>';
    echo '<tfoot>';
    echo '<tr>';
    foreach ($field as $key => $value) {
      echo '<th>'.$value.'</th>';
    }
    echo '</tr>';
    echo '</tfoot>';
    echo '<tbody>';
    $no=1;
    foreach ($query->getResult() as $row) {

      $edit = '<a class="btn btn-info btn-md btn-round" href="'.base_url().'/apotek/racikan/create/'.$row->id.'"><b>DETAIL RACIKAN</b></a>';

      echo '<tr>';
      echo '<td>'.$no.'</td>';
      echo '<td>'.$row->nama.'</td>';
      echo '<td>'.$edit.'</td>';
      echo '</tr>';
      $no++;
    }
    echo '</tbody>
    </table>';
  }


public function create($id)
{
  $auth = $this->authentication->is_login();
  if ($auth==FALSE){
    return redirect()->to('aptauth');
  }
  $data=array(
    'id'=>$id,
    'nama_racikan'=>$this->general_model->get_data_by_field('obat_racikan','nama',['id'=>$id]),
  );
  return $this->template->apotek("racikan/create", $data);
}

public function add_item(){
  helper(['form', 'url']);
  $input = $this->validate([
    'id' => for_validate('ID Reff','required'),
    'id_racikan' => for_validate('ID Racikan','required'),
    'reff' => for_validate('Reff','required'),
  ]);
  if (!$input) {
    echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
  } else {
    $id_racikan = $this->request->getPost('id_racikan');
    $id = $this->request->getPost('id');
    $reff = $this->request->getPost('reff');
    $this->db->transBegin();

    $data = [
      'id_racikan'=>$id_racikan,
      'id_item'=>$id,
      'table_reff_item'=>$reff,
      'status'=>1,
      'date_insert'=>date('Y-m-d H:i:s'),
      'id_user'=>$this->id_user,
    ];
    $save = $this->general_model->do_save('obat_racikan_detail',$data);
    $error1 = $this->general_model->error();
    if ($this->db->transStatus() === false) {
      $this->db->transRollback();
      echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
    } else {
      $this->db->transCommit();
      echo json_encode(array('response'=>TRUE,'message'=>'OK'));
    }
  }
}

public function get_detail_item_racikan(){
  $id_racikan = $this->request->getGet('id_racikan');
  $q = $this->general_model->select_data('obat_racikan_detail','*'," status = '1' AND id_racikan = '".$id_racikan."' ");

  $html = '<table class="table table-bordered">';
  $html .= '<thead>';
  $html .= '<tr>';
  $html .= '<th>#</th>';
  $html .= '<th>Nama Item</th>';
  $html .= '<th>Spesifikasi</th>';
  $html .= '<th>Jumlah</th>';
  $html .= '<th>Satuan</th>';
  $html .= '<th>#</th>';
  $html .= '</tr>';
  $html .= '</thead>';

  $no = 1;
  foreach ($q->getResult() as $row) {
    if($row->table_reff_item == 'obat'){
      $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
      $spesifikasi = '';
    } else if($row->table_reff_item == 'barang'){
      $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
      $aspesifikasi = $this->general_model->get_data_by_field('barang','spesifikasi',['id'=>$row->id_item]);
      $merek = $this->general_model->get_data_by_field('barang','merek',['id'=>$row->id_item]);
      $spesifikasi = $merek.' - '.$aspesifikasi;
    } else {
      $nama_item = '';
      $spesifikasi = '';
    }
    $satuan = $this->general_model->select_data('satuan','*',['id_reff'=>$row->id_item,'table_reff'=>$row->table_reff_item]);
    $option = '<option value="">pilih satuan</option>';
    foreach ($satuan->getResult() as $rowsatuan) {
      $selected = $row->satuan == $rowsatuan->satuan ? 'selected' : '';
      $option.= '<option value="'.$rowsatuan->id.'" '.$selected.'>'.$rowsatuan->satuan.'</option>';
    }
    $param_get_satuan = "'".$row->id."','".$row->table_reff_item."'";
    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<td style="padding:0 5px !important;height:40px;" class="text-center">'.$no.'</td>';
    $html .= '<td style="padding:0 5px !important;height:40px;">'.$nama_item.'</td>';
    $html .= '<td style="padding:0 5px !important;height:40px;">'.$spesifikasi.'</td>';
    $html .= '<td style="padding:0 5px !important;height:40px;"><input type="number" class="form-control form-control-sm" onKeyup="save_jml_item('.$row->id.')" id="jumlah'.$row->id.'" value="'.$row->qty.'"/></td>';
    $html .= '<td style="padding:0 5px !important;height:40px;"><select class="form-control form-control-sm sss" onChange="set_satuan('.$param_get_satuan.')" id="optionsatuan'.$row->id.'" data-backdrop="static" data-keyboard="false" style="padding:0px;">'.$option.'</select></td>';
    $html .= '<td><button class="btn btn-icon btn-danger btn-round btn-xs" id="delete_item'.$row->id.'" onClick="delete_item('.$row->id.')"><i class="fa fa-times"></i></button></td>';
    $html .= '</tr>';
    $no++;
  }
  $html .= '</table>';
  echo $html;
}

public function delete_item(){
  $id = $this->request->getPost('id');
  $this->general_model->do_delete('obat_racikan_detail',['id'=>$id]);
}

public function insert_jml_item(){
  $jumlah = $this->request->getPost('jumlah');
  $id_racikan_detail = $this->request->getPost('id_racikan_detail');
  $save = $this->general_model->do_update('obat_racikan_detail',['qty'=>$jumlah],['id'=>$id_racikan_detail]);
  if ($save){
    echo json_encode(['response'=>true,'message'=>'Berhasil','jumlah'=>$jumlah]);
  } else {
    echo json_encode(['response'=>false,'message'=>'Gagal']);
  }
}

public function insert_satuan_item(){
  $id_satuan = $this->request->getPost('id_satuan');
  $id_racikan_detail = $this->request->getPost('id_racikan_detail');
  $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);
  $save = $this->general_model->do_update('obat_racikan_detail',['satuan'=>$satuan],['id'=>$id_racikan_detail]);
  if ($save){
    echo json_encode(['response'=>true,'message'=>'Berhasil']);
  } else {
    echo json_encode(['response'=>false,'message'=>'Gagal']);
  }
}

public function get_pilih_racikan(){
  helper(['form', 'url']);
  $input = $this->validate([
    'search' => for_validate('Cari','string'),
  ]);

  if (!$input) {
    echo '<div class="card-title fw-mediumbold">'.$this->validator->listErrors().'</div>';
  } else {
    $search = $this->request->getGet('search');
    if ($search == '' || $search == null || $search == 'null'){
      $con_search = ' status IS NULL';
    } else {
      $con_search = " status IS NULL AND nama LIKE '%".$search."%' ";
    }
    $q = $this->general_model->select_order_limit('obat_racikan','*',$con_search,'nama ASC',50);
    if ($q->getNumRows() > 0){
      echo '<table class="table table-bordered">';
      foreach ($q->getResult() as $row){
        $param = "'".$row->id."'";

        $q_detail = $this->general_model->select_data('obat_racikan_detail','*',['id_racikan'=>$row->id]);
        $detail = '<ul style="width:100%;padding:0px;">';
        $cannot_add = 0;
        if ($q_detail->getNumRows() > 0){
          foreach ( $q_detail->getResult() as $rows ) {
            if ($rows->table_reff_item == 'obat'){
              $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$rows->id_item]);
              $stock = $this->general_model->select_sum('obat as o,satuan as s,gudang_rak as gr,gudang as g,master_stok as ms','ms.qty',
              "s.id_reff=o.id AND s.table_reff='obat' AND gr.id_gudang=g.id
              AND ms.id_satuan=s.id AND gr.id=ms.id_gudang_rak
              AND o.id= '".$rows->id_item."' AND s.id_reff='".$rows->id_item."' AND s.satuan='".$rows->satuan."' AND ms.qty > 0
              ");
              $q_stock = $this->general_model->select_order('obat as o,satuan as s,gudang_rak as gr,gudang as g,master_stok as ms','ms.id,ms.qty,ms.no_batch,ms.date_expired',
              "s.id_reff=o.id AND s.table_reff='obat' AND gr.id_gudang=g.id
              AND ms.id_satuan=s.id AND gr.id=ms.id_gudang_rak
              AND o.id= '".$rows->id_item."' AND s.id_reff='".$rows->id_item."' AND s.satuan='".$rows->satuan."' AND ms.qty > 0
              ",'DATE(ms.date_expired) ASC,ms.id DESC');
            } elseif ($rows->table_reff_item == 'barang'){
              $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$rows->id_item]);
              $stock = $this->general_model->select_sum('barang as o,satuan as s,gudang_rak as gr,gudang as g,master_stok as ms','ms.qty',
              "s.id_reff=o.id AND s.table_reff='barang' AND gr.id_gudang=g.id
              AND ms.id_satuan=s.id AND gr.id=ms.id_gudang_rak
              AND o.id= '".$rows->id_item."' AND s.id_reff='".$rows->id_item."' AND s.satuan='".$rows->satuan."' AND ms.qty > 0
              ");
              $q_stock = $this->general_model->select_order('barang as o,satuan as s,gudang_rak as gr,gudang as g,master_stok as ms','ms.id,ms.qty,ms.no_batch,ms.date_expired',
              "s.id_reff=o.id AND s.table_reff='barang' AND gr.id_gudang=g.id
              AND ms.id_satuan=s.id AND gr.id=ms.id_gudang_rak
              AND o.id= '".$rows->id_item."' AND s.id_reff='".$rows->id_item."' AND s.satuan='".$rows->satuan."' AND ms.qty > 0
              ",'DATE(ms.date_expired) ASC,ms.id DESC');
            } else {
              $nama_item = '';
              $stock = 0;
            }
            if ($stock === 0){
              $ket = '<span class="text-danger pl-3">HABIS</span></h6>';
              $cannot_add = $cannot_add + 1;
            } else {
              if ($stock < $rows->qty){
                $ket = '<span class="text-warning pl-3">STOK KURANG</span></h6>';
                $cannot_add = $cannot_add + 1;
              } else {
                $ket = '( stok '.$stock.' )';
              }
            }
            $detail .= '<li>';
            $detail .= '<div style="margin-bottom:5px;">Membutuhkan : <span class="text-primary pl-3"><b>'.$rows->qty.'</b> '.$rows->satuan.' </span> '.$nama_item.' '.$ket.'</div>';
            if ($q_stock->getNumRows()>0){
              $detail .= '<table class="table table-striped">';
              $detail .= '<tbody>';
              $diambil = $rows->qty;
              foreach ($q_stock->getResult() as $rowstock) {
                if ($rowstock->date_expired == '' OR $rowstock->date_expired == null OR $rowstock->date_expired == '0000-00-00'){
                  $date_expiredd= '';
                } else {
                  $date_expiredd= date('d/m/Y',strtotime($rowstock->date_expired));
                }
                if ($diambil > 0 ){
                  $sisa_barang = $diambil - $rowstock->qty;
                  if ( $sisa_barang >= 0 ){
                    $diambil = $sisa_barang;
                    $value_ambil = $rowstock->qty;
                  } else {
                    $value_ambil = $diambil;
                  }
                } else {
                  $value_ambil = 0;
                }
                // if ($value_ambil <= 0){
                //   $value_ambil = 0;
                // }

                $detail .= '<tr style="padding:0 5px !important;height:35px;border:none;">';
                $detail .= '<td style="padding:0 5px !important;height:35px;border:none;" class="text-center"><b class="pull-left">Stock :</b> '.$rowstock->qty.'</td>';
                $detail .= '<td style="padding:0 5px !important;height:35px;border:none;">'.$rows->satuan.'</td>' ;
                $detail .= '<td style="padding:0 5px !important;height:35px;border:none;">'.$rowstock->no_batch.' EXP : '.$date_expiredd.'</td>';
                $detail .= '<td style="padding:0 5px !important;height:35px;border:none;" class="text-right">diambil <i class="fas fa-arrow-alt-circle-right bg-green"></i> </td>';
                $detail .= '<td style="padding:0 5px !important;height:35px;border:none;width:100px;" class="text-right">
                <input type="number" class="form-control input-full" id="value_item_racikan'.$rowstock->id.''.$rows->id.'" placeholder="0" style="height:30px !important;" value="'.$value_ambil.'">
                </td>';
                $detail .= '<td style="padding:0 5px !important;height:35px;border:none;width:100px;">'.$rows->satuan.'</td>';
                $detail .= '<td style="padding:0 5px !important;height:35px;border:none;width:50px;" class="text-center">
                <button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_item_racikan('.$rowstock->id.','.$rows->id.')" id="btn_add_item_racikan'.$rowstock->id.''.$rows->id.'">
                <i class="fa fa-plus"></i>
                </button>
                </td>';
                $detail .= '</tr>';
              }
              $detail .= '</tbody>';
              $detail .= '</table>';
            }

            $detail .= '</li>';
          }
        } else {
          $cannot_add=1;
        }

        // if ($cannot_add>0){
        //   $add_item = '';
        // } else {
        //   $add_item = '<button class="btn btn-icon btn-primary btn-round btn-xs" onClick="add_racikan('.$row->id.')" id="btn_add_racikan'.$row->id.'">
        //   <i class="fa fa-plus"></i>
        //   </button>';
        // }
        $detail .= '</ul>';
        echo '<tr>';
        echo '<td style="vertical-align:top !important;">'.$row->nama.'</td>';
        echo '<td>'.$detail.'</td>';
        echo '</tr>';

      }
      echo '</table>';
    } else {
      echo '<div class="card-title fw-mediumbold">Data tidak ditemukan, silakan cek data master.</div>';
    }
  }
}


}
