<?php
namespace App\Controllers\Penjualan;
use App\Controllers\BaseController;
use App\Models\General_model;

class Report extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    helper("general");
    helper("penjualan");
  }

  public function list_()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=array();
    return $this->template->penjualan("list", $data);
  }

  public function get_data(){
    $start = $this->request->getGET('start');
    $end = $this->request->getGET('end');
    $nomor = $this->request->getGET('nomor');
    if ($start == '' && $end == '' ){
      $con_date = '';
    } elseif ( $start == '' && $end != ''){
      $con_date = " AND DATE(`datetime`) = '".date('Y-m-d',strtotime($end))."'";
    } elseif ( $start != '' && $end == ''){
      $con_date = " AND DATE(`datetime`) = '".date('Y-m-d',strtotime($start))."'";
    } elseif ( $start != '' && $end != ''){
      $con_date = " AND DATE(`datetime`) >= '".date('Y-m-d',strtotime($start))."' AND DATE(`datetime`) <= '".date('Y-m-d',strtotime($end))."'";
    } else {
      $con_date = '';
    }
    if ($nomor == '' OR $nomor == null){
      $con_nomor = '';
    } else {
      $con_nomor = " AND no LIKE '%".$nomor."%'";
    }
    if ($nomor == '' && $start == '' && $end == ''){
      $limit = 50;
    } else {
      $limit = 100000;
    }

    $con = $con_nomor.$con_date;
    $field = ['ID','Tanggal','Nomor','Nilai Transaksi','Golongan'];
    $query = $this->general_model->select_order_limit('penjualan','*','status IS NULL'.$con,"DATE(datetime) DESC,id DESC",$limit);
    $html = '<table id="basic-datatables" class="display table table-striped table-hover" >';
    $html .= '<thead>';
    $html .= '<tr>';
    foreach ($field as $key => $value) {
      $html .= '<th>'.$value.'</th>';
    }
    $html .= '</tr>';
    $html .= '</thead>';
    $html .= '<tfoot>';
    $html .= '<tr>';
    foreach ($field as $key => $value) {
      $html .= '<th>'.$value.'</th>';
    }
    $html .= '</tr>';
    $html .= '</tfoot>';
    $html .= '<tbody>';
    $no=1;
    foreach ($query->getResult() as $row) {
      $harga = get_harga_kesepakatan_penjualan($row->id);
      if ($harga <> $row->harga_kesepakatan){
        $this->general_model->do_update('penjualan',['harga_kesepakatan'=>$harga],['id'=>$row->id]);
      }
      $html .= '<tr>';
      $html .= '<td>'.$row->id.'</td>';
      $html .= '<td>'.date('d/m/Y',strtotime($row->datetime)).'</td>';
      $html .= '<td>'.$row->no.'</td>';
      $html .= '<td>'.number_format($harga,2,',','.').'</td>';
      $html .= '<td>
      <button class="btn btn-info btn-xs btn-border btn-round" onClick="detail_penjualan('.$row->id.')" ><b>LIHAT</b></button>
      <button class="btn btn-danger btn-xs btn-border btn-round" ><b>CETAK STRUK</b></button>
      </td>';
      $html .= '</tr>';

    }
    $html .= '</tbody>
    </table>';
    $jml_trans = $this->general_model->count_rows('penjualan','id','status IS NULL'.$con);
    $nominal_trans = $this->general_model->select_sum('penjualan','harga_kesepakatan','status IS NULL'.$con);
    echo json_encode(['data'=>$html,'nominal'=>'Rp '.number_format($nominal_trans,2,'.',','),'jml_trans'=>$jml_trans]);
  }

  public function detail_penjualan(){
    $id_penjualan = $this->request->getGet('id_penjualan');
    $q = $this->general_model->select_data('penjualan_detail','*'," status = '1' AND id_penjualan = '".$id_penjualan."' ");

    $nomor = $this->general_model->get_data_by_field('penjualan','no'," id = '".$id_penjualan."' ");
    $diskon = $this->general_model->get_data_by_field('penjualan','diskon'," id = '".$id_penjualan."' ");
    $ppn = $this->general_model->get_data_by_field('penjualan','ppn'," id = '".$id_penjualan."' ");
    $tuslah = $this->general_model->get_data_by_field('penjualan','tuslah'," id = '".$id_penjualan."' ");
    $tunai = $this->general_model->get_data_by_field('penjualan','tunai'," id = '".$id_penjualan."' ");
    $id_user = $this->general_model->get_data_by_field('penjualan','id_user'," id = '".$id_penjualan."' ");
    $tanggal = $this->general_model->get_data_by_field('penjualan','datetime'," id = '".$id_penjualan."' ");
    $diskon = $diskon == ''  ? 0 : $diskon;
    $ppn = $ppn == ''  ? 0 : $ppn;

    $kasir = $this->general_model->get_data_by_field("user",'nama',['id'=>$id_user]);
    $html = '<div class="col-md-12 table-responsive">';
    $html .= '<table class="table table-condensed">';
    $html .= '<tr>';
    $html .= '<td>Nomor </td>';
    $html .= '<td>: '.$nomor.'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td>Kasir </td>';
    $html .= '<td>: '.$kasir.'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td>Tanggal </td>';
    $html .= '<td>: '.date('d/m/Y',strtotime($tanggal)).'</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';

    $html .= '<div class="col-md-12">';
    $html .= '<table class="table table-bordered">';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .= '<th>#</th>';
    $html .= '<th>Nama Item</th>';
    $html .= '<th>No Batch</th>';
    $html .= '<th>Jumlah</th>';
    $html .= '<th>Satuan</th>';
    $html .= '<th>Harga Satuan (Rp)</th>';
    $html .= '<th>Subtotal (Rp)</th>';
    $html .= '<th>Diskon (%)</th>';
    $html .= '<th>Tuslah (Rp)</th>';
    $html .= '<th>Total (Rp)</th>';
    $html .= '</tr>';
    $html .= '</thead>';

    $no = 1;
    $totalharga = 0;
    $not_done = 0;
    $cek_total_qty_come=0;
    foreach ($q->getResult() as $row) {
      if($row->table_reff_item == 'obat'){
        $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
        $spesifikasi = $this->general_model->get_data_by_field('obat','deskripsi',['id'=>$row->id_item]);
      } else if($row->table_reff_item == 'barang'){
        $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
        $aspesifikasi = $this->general_model->get_data_by_field('barang','spesifikasi',['id'=>$row->id_item]);
        $merek = $this->general_model->get_data_by_field('barang','merek',['id'=>$row->id_item]);
        $spesifikasi = $merek.' - '.$aspesifikasi;
      } else {
        $nama_item = '';
        $spesifikasi = '';
      }

      $qty_come = $this->general_model->select_sum('po_barang_masuk','qty',['id_po_detail'=>$row->id,'status'=>1]);
      $qty_come = $qty_come == '' ? 0 : $qty_come;
      $cek_total_qty_come=$cek_total_qty_come+$qty_come;
      if ($qty_come < $row->qty){
        $not_done = 1;
      }

      if ($qty_come > 0 ){
        $label_come = '<button class="btn btn-xs btn-success btn-round" onClick="show_history_data_barang('.$row->id.')"><b>'.$qty_come.'</b></button>';
      } else {
        $label_come = '<button class="btn btn-xs btn-danger btn-round" ><b>0</b></button>';
      }

      $param_get_satuan = "'".$row->id."','".$row->table_reff_item."'";
      $subtotal = $row->harga_jual*$row->qty;
      if ($row->persen_diskon == '' && $row->persen_diskon == null){
        $persen_diskon = 0;
        $nominal_diskon = 0;
      } else {
        $persen_diskon = $row->persen_diskon;
        $nominal_diskon = $subtotal*($persen_diskon/100);
      }
      if ($row->tuslah == '' && $row->tuslah == null){
        $tuslah = 0;
      } else {
        $tuslah = $row->tuslah;
      }
      $total = ( $subtotal - $nominal_diskon ) + $tuslah;

      $html .= '<tr style="padding:0 5px !important;height:40px;">';
      $html .= '<td style="padding:0 5px !important;height:40px;" class="text-center">'.$no.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$nama_item.' '.$spesifikasi.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$row->no_batch.'<br>EXP:'.$row->date_expired.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;" class="text-center">'.$row->qty.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;">'.$row->satuan.'</td>';
      $html .= '<td style="padding:0 5px !important;height:40px;" class="text-right">'.number_format($row->harga_jual,2,',','.').'</td>';
      $html .= '<td id="subtotalharga'.$row->id.'" class="text-right">'.number_format($subtotal,2,',','.').'</td>';
      $html .= '<td id="persen_diskon'.$row->id.'" class="text-center">'.$persen_diskon.'%</td>';
      $html .= '<td id="tuslah'.$row->id.'" class="text-right">'.number_format($tuslah,2,',','.').'</td>';
      $html .= '<td id="total'.$row->id.'" class="text-right">'.number_format($total,2,',','.').'</td>';
      $html .= '</tr>';
      $no++;
      $totalharga += $total;
    }

    $nominal_diskon = $totalharga * ($diskon/100);
    $harga_net = ( $totalharga - $nominal_diskon ) + $tuslah;
    $nominal_ppn = $harga_net * ($ppn/100);
    $harga_kesepakatan = $harga_net + $nominal_ppn ;
    $kembalian = $tunai - $harga_kesepakatan;

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">TOTAL : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($totalharga,2,',','.').'</th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">DISKON '.$diskon.'% : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($nominal_diskon,2,',','.').'</th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">TUSLAH : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($tuslah,2,',','.').'</th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">HARGA NET : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($harga_net,2,',','.').'</th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">PPN '.$ppn.'% : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($nominal_ppn,2,',','.').'</th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">HARGA KESEPAKATAN (Rp): </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($harga_kesepakatan,2,',','.').'</th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">TUNAI (Rp): </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($tunai,2,',','.').'</th>';
    $html .= '</tr>';

    $html .= '<tr style="padding:0 5px !important;height:40px;">';
    $html .= '<th style="padding:0 5px !important;height:40px;" colspan="9" class="text-right">KEMBALIAN (Rp) : </th>';
    $html .= '<th id="totalharga" class="text-right">'.number_format($kembalian,2,',','.').'</th>';
    $html .= '</tr>';


    $html .= '</table>';
    $html .= '</div>';

    $html .= '<div class="col-md-12 text-right">';
    $html .= '<button class="btn btn-lg btn-danger btn-round col-md-6" ><b><i class="fa fa-print"></i> CETAK STRUK</b></button>';
    $html .= '</div>';

    echo $html;
  }


}
