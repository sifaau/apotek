<?php
namespace App\Controllers\Penjualan;
use App\Controllers\BaseController;
use App\Models\General_model;

class Kasir extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    helper("general");
    helper("item_apotek");
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data=[
      'nama_kasir'=>$this->general_model->get_data_by_field('user','nama',['id'=>$this->id_user]),
    ];
    return $this->template->kasir("kasir", $data);
  }

  public function add_item(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('ID Reff','required'),
      'reff' => for_validate('Reff','required'),
      'satuan' => for_validate('satuan','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      // $id_master_stock = $this->request->getPost('id');
      $reff = $this->request->getPost('reff');
      $no_batch = $this->request->getPost('no_batch');
      $date_expired = $this->request->getPost('expired');
      $this->db->transBegin();


      // $no_batch = $this->general_model->get_data_by_field('master_stok','no_batch',['id'=>$id_master_stock]);
      // $date_expired = $this->general_model->get_data_by_field('master_stok','date_expired',['id'=>$id_master_stock]);

      // $id_item = $this->general_model->get_data_by_field('satuan','id_reff',['id'=>$id_satuan]);
      // $table_reff_item = $this->general_model->get_data_by_field('satuan','table_reff',['id'=>$id_satuan]);
      // $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);
      $id_item = $this->request->getPost('id');
      $satuan = $this->request->getPost('satuan');
      $table_reff_item = $reff;
      $id_satuan = $this->general_model->get_data_by_field('satuan','id',['id_reff'=>$id_item,'table_reff'=>$table_reff_item,'satuan'=>$satuan]);

      $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
      $persen_harga_jual = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id'=>$id_satuan]);
      // $harga = $harga_beli + ( $harga_beli * ($persen_harga_jual/100));
      $harga = $this->general_model->get_data_by_field('satuan','harga_jual',['id'=>$id_satuan]);

      $data = [
        'id_item'=>$id_item,
        'table_reff_item'=>$table_reff_item,
        'no_batch'=>$no_batch,
        'date_expired'=>$date_expired,
        'satuan'=>$satuan,
        'harga_jual'=>$harga,
        'status'=>1,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      $save = $this->general_model->do_save('penjualan_detail',$data);
      $error1 = $this->general_model->error();
      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL ditambahkan.','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK, berhasil ditambahkan.'));
      }
    }
  }

  public function add_racikan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('ID Reff','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id = $this->request->getPost('id');

      $this->db->transBegin();
      $q_detail_racikan = $this->general_model->select_data('obat_racikan_detail','*',['id_racikan'=>$id,'status'=>1]);
      foreach ($q_detail_racikan->getResult() as $row) {

        $id_satuan = $this->general_model->get_data_by_field('satuan','id',['satuan'=>$row->satuan,'id_reff'=>$row->id_item,'table_reff'=>$row->table_reff_item]);
        $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
        $persen_harga_jual = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id'=>$id_satuan]);
        $harga = $harga_beli + ( $harga_beli * ($persen_harga_jual/100));

        $data = [
          'id_item'=>$row->id_item,
          'table_reff_item'=>$row->table_reff_item,
          'qty'=>$row->qty,
          'satuan'=>$row->satuan,
          'harga_jual'=>$harga,
          'status'=>1,
          'date_insert'=>date('Y-m-d H:i:s'),
          'id_user'=>$this->id_user,
        ];
        $save = $this->general_model->do_save('penjualan_detail',$data);
      }

      $error1 = $this->general_model->error();
      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }
    }
  }

  public function add_item_racikan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('ID Reff','required'),
      'qty' => for_validate('Jumlah','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id = $this->request->getPost('id');
      $qty = $this->request->getPost('qty');

      $this->db->transBegin();

      $id_satuan = $this->general_model->get_data_by_field('master_stok','id_satuan',['id'=>$id]);
      $no_batch = $this->general_model->get_data_by_field('master_stok','no_batch',['id'=>$id]);
      $date_expired = $this->general_model->get_data_by_field('master_stok','date_expired',['id'=>$id]);

      $id_item = $this->general_model->get_data_by_field('satuan','id_reff',['id'=>$id_satuan]);
      $table_reff_item = $this->general_model->get_data_by_field('satuan','table_reff',['id'=>$id_satuan]);
      $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);

      $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
      $persen_harga_jual = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id'=>$id_satuan]);
      $harga = $harga_beli + ( $harga_beli * ($persen_harga_jual/100));

      $data = [
        'id_item'=>$id_item,
        'table_reff_item'=>$table_reff_item,
        'qty'=>$qty,
        'no_batch'=>$no_batch,
        'date_expired'=>$date_expired,
        'satuan'=>$satuan,
        'harga_jual'=>$harga,
        'status'=>1,
        'date_insert'=>date('Y-m-d H:i:s'),
        'id_user'=>$this->id_user,
      ];
      $save = $this->general_model->do_save('penjualan_detail',$data);

      // $q_detail_racikan = $this->general_model->select_data('obat_racikan_detail','*',['id_racikan'=>$id,'status'=>1]);
      // foreach ($q_detail_racikan->getResult() as $row) {
      //
      //   $id_satuan = $this->general_model->get_data_by_field('satuan','id',['satuan'=>$row->satuan,'id_reff'=>$row->id_item,'table_reff'=>$row->table_reff_item]);
      //   $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
      //   $persen_harga_jual = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id'=>$id_satuan]);
      //   $harga = $harga_beli + ( $harga_beli * ($persen_harga_jual/100));
      //
      //   $data = [
      //     'id_item'=>$row->id_item,
      //     'table_reff_item'=>$row->table_reff_item,
      //     'qty'=>$row->qty,
      //     'satuan'=>$row->satuan,
      //     'harga_jual'=>$harga,
      //     'status'=>1,
      //     'date_insert'=>date('Y-m-d H:i:s'),
      //     'id_user'=>$this->id_user,
      //   ];
      //   $save = $this->general_model->do_save('penjualan_detail',$data);
      // }

      $error1 = $this->general_model->error();
      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }
    }
  }

  public function get_new_item_penjualan(){
    $q = $this->general_model->select_data('penjualan_detail','*'," status = '1' AND id_penjualan IS NULL ");

    $html = '<table class="table table-bordered">';
    $html .= '<thead>';
    $html .= '<tr >';
    $html .= '<th >#</th>';
    $html .= '<th >Nama Item</th>';
    $html .= '<th >Golongan</th>';
    $html .= '<th >No Batch-EXP</th>';
    $html .= '<th >Jumlah Jual</th>';
    $html .= '<th >Satuan</th>';
    $html .= '<th >Harga JuaL Satuan (Rp)</th>';
    $html .= '<th >Subtotal (Rp)</th>';
    $html .= '<th >Diskon (%)</th>';
    $html .= '<th >Diskon (Rp)</th>';
    $html .= '<th >Tuslah (Rp)</th>';
    $html .= '<th >Total</th>';
    $html .= '<th ></th>';
    $html .= '</tr>';
    $html .= '</thead>';

    $no = 1;
    $totalharga = 0;
    $totaltuslah = 0;
    foreach ($q->getResult() as $row) {
      if($row->table_reff_item == 'obat'){
        $nama_item = $this->general_model->get_data_by_field('obat','nama',['id'=>$row->id_item]);
        $spesifikasi = $this->general_model->get_data_by_field('obat','deskripsi',['id'=>$row->id_item]);
        $id_golongan = $this->general_model->get_data_by_field('obat','id_golongan',['id'=>$row->id_item]);
        $nama_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_golongan]);
        $txt_golongan = '<i class="fas fa-capsules"></i> '.$nama_golongan;
      } else if($row->table_reff_item == 'barang'){
        $nama_item = $this->general_model->get_data_by_field('barang','nama',['id'=>$row->id_item]);
        $aspesifikasi = $this->general_model->get_data_by_field('barang','spesifikasi',['id'=>$row->id_item]);
        $merek = $this->general_model->get_data_by_field('barang','merek',['id'=>$row->id_item]);
        $spesifikasi = $merek.' - '.$aspesifikasi;
        $id_golongan = $this->general_model->get_data_by_field('barang','id_golongan',['id'=>$row->id_item]);
        $nama_golongan = $this->general_model->get_data_by_field('obat_gol','nama',['id'=>$id_golongan]);
        $txt_golongan = '<i class="fas fa-stethoscope"></i> '.$nama_golongan;
      } else {
        $nama_item = '';
        $spesifikasi = '';
        $txt_golongan = '';
      }
      // $satuan = $this->general_model->select_data('satuan','*',['id_reff'=>$row->id_item,'table_reff'=>$row->table_reff_item]);


      if($row->table_reff_item == 'obat'){
        // $satuan = $this->general_model->select_distinct('obat as o,satuan as s,gudang_rak as gr,gudang as g,master_stok as ms','s.id,s.satuan',
        // "s.id_reff=o.id AND s.table_reff='obat' AND gr.id_gudang=g.id
        // AND ms.id_satuan=s.id AND gr.id=ms.id_gudang_rak
        // AND o.id= '".$row->id_item."' AND s.id_reff='".$row->id_item."' AND ms.qty > 0
        // ");
        $satuan = $this->general_model->select_distinct('obat as o,satuan as s','s.id,s.satuan',
        "s.id_reff=o.id AND s.table_reff='obat'
        AND o.id= '".$row->id_item."' AND s.id_reff='".$row->id_item."'
        ");
        if ($satuan->getNumRows()>0){
          $option = '<option value="">-pilih satuan-</option>';
          foreach ($satuan->getResult() as $rowsatuan) {
            $selected = $row->satuan == $rowsatuan->satuan ? 'selected' : '';
            $option.= '<option value="'.$rowsatuan->id.'" '.$selected.'>'.$rowsatuan->satuan.'</option>';
          }
        } else {
          $option = '<option value="" style="color:red;">Stok Habis</option>';
        }

      } else {
        $satuan = $this->general_model->select_distinct('barang as o,satuan as s,gudang_rak as gr,gudang as g,master_stok as ms','s.id,s.satuan',
        "s.id_reff=o.id AND s.table_reff='barang' AND gr.id_gudang=g.id
        AND ms.id_satuan=s.id AND gr.id=ms.id_gudang_rak
        AND o.id= '".$row->id_item."' AND s.id_reff='".$row->id_item."' AND ms.qty > 0
        ");
        if ($satuan->getNumRows()>0){
          $option = '<option value="">pilih satuan</option>';
          foreach ($satuan->getResult() as $rowsatuan) {
            $selected = $row->satuan == $rowsatuan->satuan ? 'selected' : '';
            $option.= '<option value="'.$rowsatuan->id.'" '.$selected.'>'.$rowsatuan->satuan.'</option>';
          }
        } else {
          $option = '<option value="">Stok Habis</option>';
        }
      }

      if ($row->date_expired == '' or $row->date_expired == null OR $row->date_expired == '0000-00-00'){
        $date_expired = '';
      } else {
        $date_expired = 'Exp : '.date('d/m/Y',strtotime($row->date_expired));
      }
      $param_get_satuan = "'".$row->id."','".$row->table_reff_item."'";
      $subtotal = $row->harga_jual*$row->qty;

      if($row->persen_diskon == '' || $row->persen_diskon == null){
        $persen_diskon = 0;
        $nilai_diskon = 0;
      } else {
        $persen_diskon = $row->persen_diskon;
        $nilai_diskon = $subtotal * ($persen_diskon/100);
      }

      if($row->tuslah == '' || $row->tuslah == null){
        $tuslah = 0;
      } else {
        $tuslah = $row->tuslah;
      }

      $total = ( $subtotal - $nilai_diskon ) + $tuslah;

      $html .= '<tr style="padding:0 3px !important;height:35px;background:#f2f8ff;">';
      $html .= '<td style="padding:0 3px !important;height:35px;" class="text-center">'.$no.'</td>';
      $html .= '<td style="padding:0 3px !important;height:35px;"><b>'.$nama_item.'</b></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;">'.$txt_golongan.'</td>';
      $html .= '<td style="padding:0 3px !important;height:35px;"><span class="text-primary pl-1">'.$row->no_batch.'</span> <span style="color:#999999;" class="pull-right">- '.$date_expired.'</span></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;"><input type="number" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" id="jumlah'.$row->id.'" value="'.$row->qty.'"/></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;"><select class="form-control form-control-sm sss" onChange="get_harga_satuan('.$param_get_satuan.')" id="optionsatuan'.$row->id.'" data-backdrop="static" data-keyboard="false" style="padding:0px;" disabled>'.$option.'</select></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;width:200px;"><input type="number" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" id="harga_jual'.$row->id.'" value="'.$row->harga_jual.'"/></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;" id="subtotalharga'.$row->id.'" class="text-right">'.number_format($subtotal,2,',','.').'</td>';
      $html .= '<td style="padding:0 3px !important;height:35px;width:200px;" class="text-right"><input type="number" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" id="persen_diskon'.$row->id.'" value="'.$persen_diskon.'"/></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;" class="text-right"><input type="number" class="form-control form-control-sm" id="nilai_diskon'.$row->id.'" value="'.$nilai_diskon.'" readonly/></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;" class="text-right"><input type="number" class="form-control form-control-sm" onKeyup="hitung_sub_total_harga('.$row->id.')" id="tuslah'.$row->id.'" value="'.$tuslah.'"/></td>';
      $html .= '<td style="padding:0 3px !important;height:35px;" id="total'.$row->id.'" class="text-right">'.number_format($total,2,',','.').'</td>';
      $html .= '<td style="padding:0 3px !important;height:35px;" class="text-center"><button class="btn btn-icon btn-danger btn-round btn-xs" id="delete_item'.$row->id.'" onClick="delete_item('.$row->id.')"><i class="fa fa-times"></i></button></td>';
      $html .= '</tr>';
      $no++;
      $totalharga = $totalharga + $total;
      $totaltuslah += $tuslah;
    }
    $html .= '<tr style="padding:0 3px !important;height:35px;">';
    $html .= '<th style="padding:0 3px !important;height:35px;" colspan="10" class="text-right">TOTAL : </th>';
    $html .= '<th style="padding:0 3px !important;height:35px;" id="totaltuslah" class="text-right">'.number_format($totaltuslah,2,',','.').'</th>';
    $html .= '<th style="padding:0 3px !important;height:35px;" id="totalharga" class="text-right">'.number_format($totalharga,2,',','.').'</th>';
    $html .= '<td style="padding:0 3px !important;height:35px;" ><input type="hidden" id="harga_total" value="'.$totalharga.'" /></td>';
    $html .= '</tr>';
    $html .= '</table>';
    echo $html;
  }

  public function delete_item(){
    $id = $this->request->getPost('id');
    $this->general_model->do_delete('penjualan_detail',['id'=>$id]);
  }

  public function insert_jml_jual_item(){
    $jumlah = $this->request->getPost('jumlah');
    $harga_jual = $this->request->getPost('harga_jual');
    $persen_diskon = $this->request->getPost('persen_diskon');
    $tuslah = $this->request->getPost('tuslah');
    $id_penjualan_detail = $this->request->getPost('id_penjualan_detail');
    $this->general_model->do_update('penjualan_detail',['qty'=>$jumlah,'harga_jual'=>$harga_jual,'persen_diskon'=>$persen_diskon,'tuslah'=>$tuslah],['id'=>$id_penjualan_detail]);
    $q = $this->general_model->select_data('penjualan_detail','*'," status = '1' AND id_penjualan IS NULL ");
    $totalharga = 0;
    $total_tuslah = 0;
    foreach ($q->getResult() as $row) {
      $subtotal = $row->harga_jual*$row->qty;
      if ($row->persen_diskon == '' || $row->persen_diskon == null){
        $persen_diskon = 0;
        $nilai_diskon = 0;
      } else {
        $persen_diskon = $row->persen_diskon;
        $nilai_diskon = $subtotal * ($persen_diskon/100);
      }
      if ($row->tuslah == '' | $row->tuslah == null){
        $tuslah = 0;
      } else {
        $tuslah = $row->tuslah;
      }
      $total = ($subtotal - $nilai_diskon) + $tuslah;
      $totalharga += $total;
      $total_tuslah += $tuslah;
    }
    echo json_encode(['response'=>true,'jumlah'=>$jumlah,'harga_total'=>$totalharga,'total_tuslah'=>$total_tuslah]);
  }

  public function get_harga_jual_satuan(){
    $id_satuan = $this->request->getPost('id_satuan');
    $id_penjualan_detail = $this->request->getPost('id_penjualan_detail');
    $satuan = $this->general_model->get_data_by_field('satuan','satuan',['id'=>$id_satuan]);
    $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
    $persen_harga_jual = $this->general_model->get_data_by_field('satuan','persen_harga_jual',['id'=>$id_satuan]);
    $harga = $harga_beli + ( $harga_beli * ($persen_harga_jual/100));
    $this->general_model->do_update('penjualan_detail',['harga_jual'=>$harga,'satuan'=>$satuan],['id'=>$id_penjualan_detail]);
    echo json_encode(['response'=>true,'harga_jual'=>$harga,'persen_harga_jual'=>$persen_harga_jual]);
  }

  public function action_create_penjualan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'diskon' => for_validate('Diskon','required|numeric'),
      'ppn' => for_validate('PPN','required|numeric'),
      'tuslah' => for_validate('Tuslah','required|numeric'),
      'tunai' => for_validate('Tunai','required|numeric'),
      'date' => for_validate('date','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $supplier = $this->request->getPost('supplier');
      $diskon = $this->request->getPost('diskon');
      $ppn = $this->request->getPost('ppn');
      $tuslah = $this->request->getPost('tuslah');
      $tunai = $this->request->getPost('tunai');
      $date = $this->request->getPost('date');
      $date_trans = date('Y-m-d',strtotime($date));
      $count = $this->general_model->count_rows('penjualan','id'," id>0");
      $no = ($count+1).'/TR/'.date('m/Y');

      $this->db->transBegin();
      $data = [
        'diskon'=>$diskon,
        'ppn'=>$ppn,
        'tuslah'=>$tuslah,
        'tunai'=>$tunai,
        'no'=>$no,
        'datetime'=>date('Y-m-d H:i:s',strtotime($date_trans)),
        'id_user'=>$this->id_user,
      ];
      $this->general_model->do_save('penjualan',$data);
      $id_penjualan = $this->general_model->insert_id();

      $q = $this->general_model->select_data('penjualan_detail','*'," status = '1' AND harga_jual > 0 AND id_penjualan IS NULL ");
      $totalharga = 0;
      $cek_stok_tak_cukup = 0;
      $nama_item_stock_tak_cukup = '';
      foreach ($q->getResult() as $row) {
        $id_satuan = $this->general_model->get_data_by_field('satuan','id',"satuan='".$row->satuan."' AND id_reff='".$row->id_item."' AND table_reff='".$row->table_reff_item."' ");
        $satuan = $this->general_model->get_data_by_field('satuan','satuan',"satuan='".$row->satuan."' AND id_reff='".$row->id_item."' AND table_reff='".$row->table_reff_item."' ");
        $harga_beli = $this->general_model->get_data_by_field('satuan','harga_beli',['id'=>$id_satuan]);
        $satuan_ke = $this->general_model->get_data_by_field('satuan','ket',"satuan='".$row->satuan."' AND id_reff='".$row->id_item."' AND table_reff='".$row->table_reff_item."' ");
        $cek_stok = $this->general_model->select_sum('master_stok','qty',"id_satuan='".$id_satuan."' AND no_batch = '".$row->no_batch."' AND date_expired = '".$row->date_expired."' ");
        $nama_item = $this->general_model->get_data_by_field($row->table_reff_item,'nama',['id'=>$row->id_item]);

        $max_looping = get_max_loop($row->id_item,$row->table_reff_item,$satuan_ke,$row->no_batch,$row->date_expired);
        // echo $max_looping;
        // $cek_stok_for_penjualan = cek_stok_for_penjualan($row->id_item,$row->table_reff_item,$satuan_ke,$id_satuan,$row->no_batch,$row->date_expired,$row->qty,$date_trans,1,$max_looping);

        $cek_stok_for_penjualan = cek_stok_for_penjualan_2($row->qty,$row->id_item,$row->table_reff_item,$satuan_ke,$row->no_batch,$row->date_expired,$date_trans);
        if ($cek_stok_for_penjualan != 'true'){
          $nama_item_stock_tak_cukup = $nama_item_stock_tak_cukup.','.$nama_item;
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, stok '.$nama_item_stock_tak_cukup.' tak cukup. CODE:'.$cek_stok_for_penjualan));
          exit;
        }
        $new_stock = $this->general_model->select_sum('master_stok','qty',"id_satuan='".$id_satuan."' AND no_batch = '".$row->no_batch."' AND date_expired = '".$row->date_expired."' ");


        // if ( $cek_stok < $row->qty){
        //   $hasil_hitung =  hitung_konversi($row->id_item,$row->table_reff_item,$row->no_batch,$row->date_expired,$date_trans,$satuan_ke);
        //   if ($hasil_hitung === 0){
        //     $nama_item_stock_tak_cukup = $nama_item_stock_tak_cukup.','.$nama_item;
        //     $this->db->transRollback();
        //     echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, stok '.$nama_item_stock_tak_cukup.' tak cukup'));
        //     exit;
        //   }
        //   $cek_stok = $this->general_model->select_sum('master_stok','qty',"id_satuan='".$id_satuan."' AND no_batch = '".$row->no_batch."' AND date_expired = '".$row->date_expired."' ");
        // }


        // pengurangan barang di rak
        // $list_rak = $this->general_model->select_order('master_stok','id_gudang_rak,qty,id',"id_satuan='".$id_satuan."' AND no_batch = '".$row->no_batch."' AND date_expired = '".$row->date_expired."' AND qty >= ".$row->qty." ",'qty ASC');
        // if ($list_rak->getNumRows()>0){
        //   $new_stock      = 0;
        //   $diambil        = $row->qty;
        //   $id_gudang_rak  = 0;
        //   foreach ($list_rak->getResult() as $row_stock) {
        //     $sisa_barang = $row_stock->qty - $diambil;
        //     if ( $sisa_barang <= 0 ){
        //       $diambil = -$sisa_barang;
        //       $update_stock = $this->general_model->do_update('master_stok',['qty'=>0,'date_update'=>date('Y-m-d H:i:s'),'id_user_update'=>$this->id_user],['id'=>$row_stock->id]);
        //       $new_stock = 0;
        //     } else {
        //       $update_stock = $this->general_model->do_update('master_stok',['qty'=>$sisa_barang,'date_update'=>date('Y-m-d H:i:s'),'id_user_update'=>$this->id_user],['id'=>$row_stock->id]);
        //       $new_stock = $sisa_barang;
        //     }
        //     $id_gudang_rak = $row_stock->id_gudang_rak;
        //   }
        //
        //   $subtotal = $row->harga_jual*$row->qty;
        //   $totalharga += $subtotal;
        //   $data_penjualan_detail = [
        //     'id_penjualan'=>$id_penjualan,
        //     'stock_awal'=>$cek_stok,
        //     'last_stock'=>$new_stock,
        //     'date_insert'=>date('Y-m-d H:i:s'),
        //     'id_gudang_rak'=>$id_gudang_rak,
        //     'id_user'=>$this->id_user,
        //   ];
        //   $this->general_model->do_update('penjualan_detail',$data_penjualan_detail,['id'=>$row->id]);
        // } else {
        //   // $this->db->transRollback();
        //   echo json_encode(array('response'=>FALSE,'message'=>'GAGAL mengkonversi, atau gunakan satuan diatasnya saja.'.$cek_stok_for_penjualan));
        //   exit;
        // }
        $id_gudang_rak = $this->general_model->get_data_by_field('master_stok','id_gudang_rak',"id_satuan='".$id_satuan."' AND no_batch = '".$row->no_batch."' AND date_expired = '".$row->date_expired."' ");

        $subtotal = $row->harga_jual*$row->qty;
        $totalharga += $subtotal;
        $data_penjualan_detail = [
          'id_penjualan'=>$id_penjualan,
          'stock_awal'=>$cek_stok,
          'last_stock'=>$new_stock,
          'date_insert'=>date('Y-m-d H:i:s'),
          'id_gudang_rak'=>$id_gudang_rak,
          'id_user'=>$this->id_user,
        ];
        $this->general_model->do_update('penjualan_detail',$data_penjualan_detail,['id'=>$row->id]);

      }
    }

    // if ($cek_stok_tak_cukup>0){
    //   // $this->db->transRollback();
    //   echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, stok '.$nama_item_stock_tak_cukup.' tak cukup.'));
    // } else {
      if ($totalharga <= 0 ){
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL, Total harga 0'));
      } else {
        $nominal_diskon = $totalharga * ($diskon/100);
        $harga_net = $totalharga - $nominal_diskon;
        $nominal_ppn = $harga_net * ($ppn/100);
        $harga_kesepakatan = $harga_net + $nominal_ppn + $tuslah;
        $this->general_model->do_update('penjualan',['harga_kesepakatan'=>$harga_kesepakatan],['id'=>$id_penjualan]);

        if ($this->db->transStatus() === false) {
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
        } else {
          $this->db->transCommit();
          echo json_encode(array('response'=>TRUE,'message'=>'OK'));
        }
      }
    // }
  }





}
