<?php
namespace App\Controllers\Superadmin;
use App\Controllers\BaseController;
use App\Models\General_model;

class Users extends BaseController
{
  public function __construct()
  {
    $this->session = session();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
    helper("general");
  }

  public function index()
  {
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data = [];
    return $this->template->superadmin("user/list", $data);
  }

  public function get_data(){

    $search = $this->request->getGet('search');
    if ($search == ''){
      $condition = " id > 0 ";
      $limit = 100;
    } else {
      $condition = " nama LIKE '%".$search."%'
      OR alamat LIKE '%".$search."%'
      OR jabatan LIKE '%".$search."%'
      OR hp LIKE '%".$search."%'
       ";
      $limit = 100;
    }

    $field = ['ID','Nama','HP','Jabatan','Alamat','username','#','#'];
    $query = $this->general_model->select_order_limit('user','*',$condition,"nama ASC",$limit);
    $html = '<table id="basic-datatables" class="display table table-striped table-hover" >';
    $html .= '<thead>';
    $html .= '<tr>';
    foreach ($field as $key => $value) {
      $html .= '<th>'.$value.'</th>';
    }
    $html .= '</tr>';
    $html .= '</thead>';
    $html .= '<tfoot>';
    $html .= '<tr>';
    foreach ($field as $key => $value) {
      $html .= '<th>'.$value.'</th>';
    }
    $html .= '</tr>';
    $html .= '</tfoot>';
    $html .= '<tbody>';
    $no=1;
    foreach ($query->getResult() as $row) {
      $jabatan = '<select class="form-control" style="height:40px !important;" id="jabatan'.$row->id.'" onChange="ubah_jabatan('.$row->id.')">';
      $jabatan .= '<option value="" > -Pilih Jabatan-</option>';
      $jabatan .= '<option value="owner" '.($row->jabatan == 'owner' ? 'selected' : '').'> Owner </option>';
      $jabatan .= '<option value="apoteker" '.($row->jabatan == 'apoteker' ? 'selected' : '').'> Apoteker </option>';
      $jabatan .= '<option value="staff" '.($row->jabatan == 'staff' ? 'selected' : '').'> Staff </option>';
      $jabatan .= '<option value="admin" '.($row->jabatan == 'admin' ? 'selected' : '').'> Admin </option>';
      $jabatan .= '</select>';

      $html .= '<tr>';
      $html .= '<td>'.$row->id.'</td>';
      $html .= '<td>'.$row->nama.'</td>';
      $html .= '<td>'.$row->hp.'</td>';
      $html .= '<td>'.$jabatan.'</td>';
      $html .= '<td>'.$row->alamat.'</td>';
      $html .= '<td>'.$row->username.'</td>';
      $html .= '<td><button class="btn btn-info btn-xs btn-round" onClick="modal_ubah_password('.$row->id.')"><b>UBAH PAWSSWORD</b></button></td>';
      $html .= '<td><a class="btn btn-success btn-xs btn-round" style="color:#fff;" href="'.base_url('superadmin/users/hak_akses/'.$row->id).'"><b>HAK AKSES</b></a></a></td>';
      $html .= '</tr>';
    }
    $html .= '</tbody>
    </table>';
    echo $html;
  }

  public function action_create(){
    helper(['form', 'url']);
    $input = $this->validate([
      'nama' => for_validate('nama','required'),
      'hp' => for_validate('hp','required'),
      'username' => for_validate('username','required'),
      'password' => for_validate('password','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $action = $this->request->getPost('action');
      $id = $this->request->getPost('id');
      $nama     = $this->request->getPost('nama');
      $jabatan = $this->request->getPost('jabatan');
      $hp = $this->request->getPost('hp');
      $alamat     = $this->request->getPost('alamat');
      $username   = $this->request->getPost('username');
      $password   = $this->request->getPost('password');

      $check = $this->general_model->count_rows('user','id',['username'=>$username]);
      if ($check > 0){
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL. Username sudah ada','error1'=>''));
      } else {
      $this->db->transBegin();

        if ($action == 'edit'){
          $data = [
            'nama'=>$nama,
            'hp'=>$hp,
            'jabatan'=>$jabatan,
            'alamat'=>$alamat,
            'status'=>1,
          ];
          $save = $this->general_model->do_update('user',$data,['id'=>$id]);
          $error1 = $this->general_model->error();
        } else {
          $data = [
            'nama'=>$nama,
            'hp'=>$hp,
            'jabatan'=>$jabatan,
            'alamat'=>$alamat,
            'username'=>$username,
            'password'=>hash('sha512',$password),
            'status'=>1,
          ];
          $save = $this->general_model->do_save('user',$data);
          $error1 = $this->general_model->error();
          $id = $this->general_model->insert_id();
        }

        if ($this->db->transStatus() === false) {
          $this->db->transRollback();
          echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
        } else {
          $this->db->transCommit();
          echo json_encode(array('response'=>TRUE,'message'=>'OK'));
        }
      }


    }
  }

  public function ubah_jabatan(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('id','required'),
      'jabatan' => for_validate('jabatan','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $this->db->transBegin();
      $id = $this->request->getPost('id');
      $jabatan = $this->request->getPost('jabatan');
      $this->general_model->do_update('user',['jabatan'=>$jabatan],['id'=>$id]);

      if ($this->db->transStatus() === false) {
        $jabatan = $this->general_model->get_data_by_field('user','jabatan',['id'=>$id]);
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','jabatan'=>$jabatan,'error1'=>json_encode($error1)));
      } else {
        $jabatan = $this->general_model->get_data_by_field('user','jabatan',['id'=>$id]);
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK','jabatan'=>$jabatan));
      }
    }
  }

  public function change_password(){
    helper(['form', 'url']);
    $input = $this->validate([
      'id' => for_validate('id','required'),
      'password' => for_validate('password','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $this->db->transBegin();
      $id = $this->request->getPost('id');
      $password = $this->request->getPost('password');
      $this->general_model->do_update('user',['password'=>hash('sha512',$password)],['id'=>$id]);
      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL','error1'=>json_encode($error1)));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'));
      }
    }
  }

  public function hak_akses($id_user){
    $auth = $this->authentication->is_login();
    if ($auth==FALSE){
      return redirect()->to('aptauth');
    }
    $data = [
      'id_user'=>$id_user,
      'nama'=>$this->general_model->get_data_by_field('user','nama',['id'=>$id_user]),
      'username'=>$this->general_model->get_data_by_field('user','username',['id'=>$id_user]),
    ];
    return $this->template->superadmin("user/hak_akses", $data);
  }

  public function get_menu(){
    helper(['form', 'url','sidebar']);
    $input = $this->validate([
      'id_user' => for_validate('id user','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_user = $this->request->getGet('id_user');
      $menu_utama  = get_menu_utama();
      $html = '';
      foreach ($menu_utama->getResult() as $row) {
        $check_akses = $this->general_model->count_rows('zpb_access_role_group_user','id',['id_user'=>$id_user,'id_access_role_group'=>$row->id]);
        if ($check_akses > 0){
          $html .= '
          <button class="btn btn-danger btn-md col-md-2" onClick="add_menu('.$id_user.','.$row->id.')"><i class="fa fa-minus"></i></button>
          <button class="btn btn-primary btn-md col-md-6" onClick="get_sub_menu('.$row->id.')">'.$row->name.'</button>
          <br><br>
          ';
        } else {
          $html .= '
          <button class="btn btn-primary btn-md col-md-2" onClick="add_menu('.$id_user.','.$row->id.')"><i class="fa fa-plus"></i></button>
          <button class="btn btn-primary btn-md btn-border col-md-6" onClick="get_sub_menu('.$row->id.')">'.$row->name.'</button>
          <br><br>
          ';

        }

      }
      echo $html;
    }
  }

  public function get_sub_menu(){
    helper(['form', 'url','sidebar']);
    $input = $this->validate([
      'id_user' => for_validate('id user','required'),
      'id_group' => for_validate('id group','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_user = $this->request->getGet('id_user');
      $id_group = $this->request->getGet('id_group');
      $sub_menu_utama  = get_sub_menu($id_group);
      $html = '';
      foreach ($sub_menu_utama->getResult() as $row) {
        $check_akses = $this->general_model->count_rows('zpb_access_role_permission','id',['id_user'=>$id_user,'id_access_role'=>$row->id]);
        if ($check_akses > 0){
          $html .= '
          <button class="btn btn-danger btn-md col-md-2" onClick="add_sub_menu('.$id_user.','.$row->id.','.$id_group.')"><i class="fa fa-minus"></i></button>
          <button class="btn btn-primary btn-md col-md-6">'.$row->name.'</button>
          <br><br>
          ';
        } else {
          $html .= '
          <button class="btn btn-primary btn-md col-md-2" onClick="add_sub_menu('.$id_user.','.$row->id.','.$id_group.')"><i class="fa fa-plus"></i></button>
          <button class="btn btn-primary btn-md btn-border col-md-6">'.$row->name.'</button>
          <br><br>
          ';

        }

      }
      echo $html;
    }
  }

  public function add_menu(){
    helper(['form', 'url','sidebar']);
    $input = $this->validate([
      'id_user' => for_validate('id user','required'),
      'id' => for_validate('ID','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_user = $this->request->getPost('id_user');
      $id = $this->request->getPost('id');

      $this->db->transBegin();

      $check_akses = $this->general_model->count_rows('zpb_access_role_group_user','id',['id_user'=>$id_user,'id_access_role_group'=>$id]);
      if ($check_akses > 0){
        $this->general_model->do_delete('zpb_access_role_group_user',['id_user'=>$id_user,'id_access_role_group'=>$id]);
        $this->general_model->do_delete('zpb_access_role_permission',['id_user'=>$id_user]);
      } else {
        $data_new = [
          'id_user'=>$id_user,
          'id_access_role_group'=>$id,
        ];
        $this->general_model->do_save("zpb_access_role_group_user",$data_new);
      }

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'.$id.$id_user));
      }
    }
  }

  public function add_sub_menu(){
    helper(['form', 'url','sidebar']);
    $input = $this->validate([
      'id_user' => for_validate('id user','required'),
      'id' => for_validate('ID','required'),
    ]);
    if (!$input) {
      echo json_encode(array('response'=>FALSE,'message'=>$this->validator->listErrors()));
    } else {
      $id_user = $this->request->getPost('id_user');
      $id = $this->request->getPost('id');

      $this->db->transBegin();

      $check_akses = $this->general_model->count_rows('zpb_access_role_permission','id',['id_user'=>$id_user,'id_access_role'=>$id]);
      if ($check_akses > 0){
        $this->general_model->do_delete('zpb_access_role_permission',['id_user'=>$id_user,'id_access_role'=>$id]);
      } else {
        $data_new = [
          'id_user'=>$id_user,
          'id_access_role'=>$id,
        ];
        $this->general_model->do_save("zpb_access_role_permission",$data_new);
      }

      if ($this->db->transStatus() === false) {
        $this->db->transRollback();
        echo json_encode(array('response'=>FALSE,'message'=>'GAGAL'));
      } else {
        $this->db->transCommit();
        echo json_encode(array('response'=>TRUE,'message'=>'OK'.$id.$id_user));
      }
    }
  }


}
