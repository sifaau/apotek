<?php
$name_menu = 'Upload Data Obat';
?>
<div class="main-panel">
  <div class="content">

    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Utility</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">
      <div class="row">

        <div class="col-md-12">
          <div class="card">

            <div class="card-header">
              <div class="row">
                <div class="col-md-4">
                  FORMAT EXCEL
                  <form action="<?php echo base_url();?>/utility/excel/action_upload_excel" method="post" enctype="multipart/form-data">
                    <div class="form-gruop">
                      <label>&nbsp;</label>
                      <input type="file" name="file" class="form-control"/>
                    </div>
                    <br>
                    <input type="submit" value="Upload file2" class="btn btn-success btn-flat btn-lg"/>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>
