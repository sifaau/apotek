<?php
$name_menu = 'Backup Database';
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="col-md-12">
              <a class="btn btn-primary btn-round pull-left" href="<?php echo base_url();?>/superadmin/backup/backup_db" ><i class="fa fa-database"></i>  BACKUP DATABASE</a>
            </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>
