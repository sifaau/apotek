<?php
$name_menu = 'Users';
echo view('layout/timepicker', array());
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-primary btn-round pull-right" onClick="modal_tambah_data()"><i class="fa fa-plus"></i>  TAMBAH DATA</button>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title"></h4>
              <div class="col-md-12">
                <div class="form-group">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" id="search" placeholder="Cari data user" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" id="basic-addon2" onClick="get_data()">CARI</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">

              <div class="row">

                <div class="col-md-12">
                  <div class="table-responsive" id="root">

                  </div>
                </div>
              </div>



            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>




<?php
echo view('superadmin/user/modal_add', array());
echo view('superadmin/user/modal_ubah_password', array());
?>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
  get_data();
});

function get_data(){
  var search = $('#search').val();
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/superadmin/users/get_data',
    data : 'search='+search,
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root').html(data);
      // $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root').html('<center>'+c.toString()+'</center>');
    }
  });
}

function ubah_jabatan(id){
  var jabatan = $('#jabatan'+id).val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/superadmin/users/ubah_jabatan',
    data : 'id='+id+'&jabatan='+jabatan,
    dataType : 'json',
    beforeSend:function(){
    },
    success: function(JSONObject) {
      $.notify({message: JSONObject.message },{type: 'info',z_index:10000});
      $('#jabatan'+id).val(JSONObject.jabatan);
      // $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:10000});
    }
  });
}

</script>
