<?php
$name_menu = 'Hak Akses';
echo view('layout/timepicker', array());
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?> <?php echo $nama.' ('.$username.')';?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Superadmin</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?> <?php echo $nama.' ('.$username.')';?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-4">

          <div class="card">
            <div class="card-header">

            </div>
            <div class="card-body">
              <div class="table-responsive" id="root">

              </div>
            </div>
          </div>
        </div>

        <div class="col-md-8">

          <div class="card">
            <div class="card-header">

            </div>
            <div class="card-body">
              <div class="table-responsive" id="root_sub">

              </div>
            </div>
          </div>

        </div>

      </div>

    </div>
  </div>
</div>


<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
  get_menu();
});

function get_menu(id_user){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/superadmin/users/get_menu',
    data : 'id_user=<?php echo $id_user;?>',
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root').html(data);
      // $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root').html('<center>'+c.toString()+'</center>');
    }
  });
}

function get_sub_menu(id_group){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/superadmin/users/get_sub_menu',
    data : 'id_user=<?php echo $id_user;?>&id_group='+id_group,
    beforeSend:function(){
      $('#root_sub').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root_sub').html(data);
      // $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root_sub').html('<center>'+c.toString()+'</center>');
    }
  });
}

function add_menu(id_user,id){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/superadmin/users/add_menu',
    data : 'id_user='+id_user+'&id='+id,
    dataType : 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $.notify({message:JSONObject.message },{type: 'info',z_index:10000});
      get_menu(id_user);
      get_sub_menu(id);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:10000});
    }
  });
}

function add_sub_menu(id_user,id,id_group){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/superadmin/users/add_sub_menu',
    data : 'id_user='+id_user+'&id='+id,
    dataType : 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $.notify({message:JSONObject.message },{type: 'info',z_index:10000});
      get_sub_menu(id_group);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:10000});
    }
  });
}

</script>
