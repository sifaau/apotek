<div class="modal" id="ubahpassword" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <label for="largeInput" >Password Baru</label>
          <input type="hidden" class="form-control" id="id_ubah_password" placeholder="">
          <input type="password" class="form-control" id="ubah_password" placeholder="">
        </div>

        <div class="col-md-12" id="error_form_password">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save_passsword" onClick="save_password()">SIMPAN</button>
        </div>


      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).ready(function() {

});

function modal_ubah_password(id){
  $('#id_ubah_password').val(id);
  $('#error_form_password').html('');
  $('#ubahpassword').modal('show');
}

function save_password(){
  var id = $('#id_ubah_password').val();
  var password = $('#ubah_password').val();

  var dataString = 'id='+id+'&password='+password;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/superadmin/users/change_password',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      // $.notify({message: 'Loading..'},{type: 'info',z_index:10000});
      $('#btn_save_passsword').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save_passsword').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index:10000});
        $('#error_form_password').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index:10000});
        $('#ubahpassword').modal('hide');
        $('.form-control').val('');
      }

      $('#btn_save_passsword').html('<center>SIMPAN</center>');
      $('#btn_save_passsword').attr('onClick','save_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:10000});
      $('#btn_save_passsword').html('<center>SIMPAN</center>');
      $('#btn_save_passsword').attr('onClick','save_data()');
    }
  });
}

</script>
