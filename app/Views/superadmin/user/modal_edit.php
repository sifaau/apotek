<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <label for="largeInput">Nama <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="nama" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput">Alamat <sup style="color:red;">*</sup></label>
          <textarea type="text" class="form-control" id="alamat" ></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput">HP <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="hp" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >Jabatan</label>
          <select class="form-control" id="jabatan">
            <option value=""> -Pilih Jabatan-</option>
            <option value="owner"> Owner </option>
            <option value="apoteker"> Apoteker </option>
            <option value="staff"> Staff </option>
            <option value="admin"> Admin </option>
          </select>
        </div>

        <div class="col-md-12" id="error_form">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save" onClick="save_data()">SIMPAN</button>
        </div>


      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).ready(function() {

});

function modal_tambah_data(){
  $('#tambahdata').modal('show');
  get_golongan();
  get_perusahaan();
}

function save_data(){

  var nama  = $('#nama').val();
  var alamat = $('#alamat').val();
  var hp = $('#hp').val();
  var jabatan = $('#jabatan').val();
  var username = $('#username').val();
  var password = $('#password').val();

  var dataString = 'nama='+nama+'&alamat='+alamat+'&hp='+hp+'&jabatan='+jabatan+'&username='+username+'&password='+password;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/superadmin/users/action_create',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      // $.notify({message: 'Loading..'},{type: 'info',z_index:10000});
      $('#btn_save').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index:10000});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index:10000});
        get_data();
        $('#tambahdata').modal('hide');
        $('.form-control').val('');
      }

      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:10000});
      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    }
  });
}

</script>
