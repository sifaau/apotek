<?php
$name_menu = 'Posting Laporan Keuangan';
$num=array('-1-','-2-','-3-','-4-','-5-','-6-','-7-','-8-','-9-','-10-','-11-','-12-');
$day=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
echo view('layout/datepicker', array());
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Akuntansi</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">

        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="card">
              <div class="card-body">

                <div class="form-group">
                  <label>Periode</label>
                  <select class="form-control" name="periode" id="periode" data-error=".errorTxt1">
                    <option value="">Pilih Periode</option>
                    <?php foreach ($periode->getResult() as $row) :?>
                      <option value="<?php echo $row->id;?>"><?php echo str_replace($num,$day,'-'.$row->month.'-').' '.$row->year;?></option>
                    <?php endforeach;?>
                  </select>
                  <div class="frmerrormsg errorTxt1"></div>
                </div>
                <div class="form-group" >
                  <button class="btn btn-lg btn-round btn-primary pull-right" onclick="action_posting()" id="button_posting" ><b>P O S T I N G</b></button>
                </div>

              </div>
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>

      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
$( document ).ready(function() {
  $('#periode').select2();
});

function action_posting(){
  var periode = $('#periode').val();
  var perusahaan = null;
  $.ajax({
    type    : 'POST',
    url     : '<?php echo base_url();?>/accounting/posting/action_posting',
    data    : 'perusahaan='+perusahaan+'&periode='+periode,
    dataType: 'json',
    beforeSend:function(){
      $('#button_posting').html('<i class="fa fa-spin fa-spinner"></i> loading... Tolong jangan di tutup sampai selesai');
      $('#button_posting').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if ( JSONObject.response == true ){
        // get_data_tutup_buku();
        $.notify(JSONObject.message,'success');
      } else {
        $.notify(JSONObject.message,'error');
      }
      $('#button_posting').html('<b>P O S T I N G</b>');
      $('#button_posting').attr('onClick',"action_posting()");
    },
    error:function(a,b,c){
      $.notify(c.toString(),'error');
      $('#button_posting').html('<b>P O S T I N G</b>');
      $('#button_posting').attr('onClick',"action_posting()");
    }
  });
}

</script>
