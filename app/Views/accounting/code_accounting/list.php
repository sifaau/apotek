<?php
use App\Models\General_model;
$this->general_model = new General_model();
$name_menu = 'Kode Akuntansi';
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Akuntansi</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <a class="btn btn-primary btn-round pull-right" href="<?php echo base_url();?>/accounting/code_accounting/create" style="color:#ffffff;"><i class="fa fa-plus"></i>  TAMBAH KODE</a>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

                <table id="codeacc" class="table table-condensed">
                  <thead>
                    <tr>
                      <th>KODE AKUN</th>
                      <th>BALANCE</th>
                      <th>POSISI</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php

                    foreach ($code_accounting->getResult() as $row) {
                      $show = 1;
                      if ($row->code_parent == '0' OR $row->code_parent == '' OR $row->code_parent == null OR $row->code_parent == 'null'){

                      } else {
                        $cek_status_code_parent = $this->general_model->get_data_by_field('zacc_code_accounting','status',array('code'=>$row->code_parent));
                        if ( $cek_status_code_parent === '0'){
                          $this->general_model->update('zacc_code_accounting',array('status'=>'0'),array('id'=>$row->id));
                          $show = 0;
                        }
                      }
                      if ($show === 1){
                        $repeat = (((int)$row->level)-1);
                        if ($repeat > 0){
                          $space = str_repeat('&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ',$repeat);
                        } else {
                          $space = '';
                        }
                        if ($row->balance =='K') {
                          $balance='KREDIT';
                        } elseif ($row->balance=='D'){
                          $balance='DEBET';
                        } else {
                          $balance='';
                        }
                        if ($row->type_report =='N') {
                          $type_report='NERACA';
                        } elseif ($row->type_report=='LR'){
                          $type_report='LABA-RUGI';
                        } else {
                          $type_report='';
                        }

                        echo '<tr>';
                        echo '<td style="padding:5px 10px !important;height:35px;" >'.$space.'<span id="text_name'.$row->id.'"><b>'.$row->code.'</b> - '.$row->name.'</span></td>';
                        echo '<td style="padding:5px 10px !important;height:35px;" id="text_balance'.$row->id.'">'.$balance.'</td>';
                        echo '<td style="padding:5px 10px !important;height:35px;" id="text_type_report'.$row->id.'">'.$type_report.'</td>';
                        echo '<td style="padding:5px 10px !important;height:35px;" ></td>';
                        echo '</tr>';
                      }

                    }
                    ;?>
                  </tbody>
                </table>

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script class="text/javascript">

$(document).ready(function() {
  $('#codeacc').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "iDisplayLength": 100
  });
});

// var table = $('#codeacc').DataTable({
//   "paging": true,
//   "lengthChange": true,
//   "searching": true,
//   "ordering": false,
//   "info": true,
//   "autoWidth": false,
//   "iDisplayLength": 100
// });

</script>
