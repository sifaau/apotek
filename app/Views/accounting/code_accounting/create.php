<?php
use App\Models\General_model;
$this->general_model = new General_model();
helper('form');
$validation =  \Config\Services::validation();
$name_menu = 'Input Kode Akun';
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Akuntansi</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url();?>/accounting/code_accounting/create"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">


        <div class="row">
          <div class="col-md-2">

          </div>
          <div class="col-md-8">

            <div class="card">
              <div class="card-body">
                <div class="col-md-12">
                  <span class="text-info pl-3">
                    <?= $validation->listErrors() ?>
                  </div>
                </div>
                <div class="col-md-12">
                  <?php if ($level == 1 ):?>
                    <h3><b>INPUT KODE REKENING INDUK</b></h3>
                  <?php else:?>
                    <h3><b>INPUT TURUNAN KODE REKENING INDUK <?php echo $code_account;?></b></h3>

                    <?php
                    $the_name = $this->general_model->get_data_by_field('zacc_code_accounting','name',array('code'=>$code_account));
                    $the_balance = $this->general_model->get_data_by_field('zacc_code_accounting','balance',array('code'=>$code_account));
                    $the_type_report = $this->general_model->get_data_by_field('zacc_code_accounting','type_report',array('code'=>$code_account));
                    $text_balance =  '';
                    if ($the_balance == 'D'){
                      $text_balance =  'DEBET';
                    } if ($the_balance == 'K'){
                      $text_balance = 'KREDIT';
                    }
                    $text_type_report =  '';
                    if ($the_type_report == 'N'){
                      $text_type_report =  'NERACA';
                    } if ($the_balance == 'LR'){
                      $text_type_report = 'LABA RUGI';
                    }
                    ;?>
                    <table class="table table-bordered">
                      <tr>
                        <th><i class="fa fa-pencil"></i> <?php echo $the_name;?></th><th clas="text-center"><i class="fa fa-book"></i> <?php echo $text_type_report;?></td><th clas="text-center"><i class="fa fa-send"></i> <?php echo $text_balance;?></td>
                        </tr>
                      </table>

                    <?php endif;?>

                    <hr>
                  </div>

                  <?php echo form_open('',array('id'=>'form_create_account'));?>

                  <div class="col-md-12">
                    <div class="form-group">
                        <label>Kode Rekening</label>

                        <?php if ($level == 1 ):?>
                          <span class="text-info pl-3">( Isikan 2 digit kode induk saja )</span>
                        <?php elseif ($level == 2 ):?>
                          <span class="text-info pl-3">( Isikan 2 digit terakhir saja, kode depanya otomatis terisi )</span>
                        <?php elseif ($level == 3 ):?>
                          <span class="text-info pl-3">( Isikan 3 digit terakhir saja, kode depanya otomatis terisi )</span>
                        <?php else:?>
                          <span class="text-info pl-3">( Isikan minimal 3 digit dan maksimal 4 digit terakhir saja, kode depanya otomatis terisi )</span>
                        <?php endif;?>
												<div class="input-group mb-3">
                          <div class="input-group-append">
														<span class="input-group-text" id="basic-addon2"><?php echo $code_account == 0 ? '' : $code_parent;?></span>
													</div>
                          <?php if ($level == 1 ):?>
                            <input type="text" id="code_account" name="code_account" value="<?php echo $code;?>" class="form-control" data-error=".errorTxt1" minlength="2" maxlength="2" placeholder="contoh : 11">
                          <?php elseif ($level == 2 ):?>
                            <input type="text" id="code_account" name="code_account" value="<?php echo $code;?>" class="form-control" data-error=".errorTxt1" minlength="2" maxlength="2" placeholder="contoh : 11">
                          <?php elseif ($level == 3 ):?>
                            <input type="text" id="code_account" name="code_account" value="<?php echo $code;?>" class="form-control" data-error=".errorTxt1" minlength="3" maxlength="3" placeholder="contoh : 111">
                          <?php else:?>
                            <input type="text" id="code_account" name="code_account" value="<?php echo $code;?>" class="form-control" data-error=".errorTxt1" minlength="3" maxlength="4" placeholder="contoh : 111 atau 111X">
                          <?php endif;?>

												</div>
											</div>
                  </div>



                  <div class="col-md-12">
                    <div class="form-group" id="col_item_account" >
                      <label>Nama Akun</label>
                      <input type="text" id="account_name" name="account_name" value="<?php echo $name;?>" class="form-control" data-error=".errorTxt2">
                      <div class="frmerrormsg errorTxt2"></div>
                    </div>
                  </div>

                  <?php if ($level === 1 ):?>

                    <div class="col-md-12">
                      <div class="form-group" id="col_item_account" >
                        <label>Jenis Laporan</label>
                        <select class="form-control" id="pos" name="pos" data-error=".errorTxt4">
                          <option value="">- Pilih -</option>
                          <option value="N">Neraca</option>
                          <option value="LR">Laba Rugi</option>
                        </select>
                        <div class="frmerrormsg errorTxt4"></div>
                      </div>
                    </div>

                    <?php endif;?>

                    <?php if ($level < 3 ):?>
                    <div class="col-md-12">
                      <div class="form-group" id="col_item_account" >
                        <label>Normal Balance</label>
                        <select class="form-control" id="balance" name="balance"  data-error=".errorTxt3">
                          <option value="">- Pilih -</option>
                          <option value="D">Debet</option>
                          <option value="K">Kredit</option>
                        </select>
                        <div class="frmerrormsg errorTxt3"></div>
                      </div>
                    </div>
                    <?php endif;?>




                  <div class="col-md-12">
                    <div class="form-group" id="col_save_button">
                      <button type="submit" class="btn btn-flat btn-primary btn-lg pull-right">SIMPAN</button>
                      <br></br>
                      <br></br>
                    </div>
                  </div>
                  <?php echo form_close();?>

                </div>

              </div>

            </div>
            <div class="col-md-2">
            </div>

          </div>

          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <div class="card-body">

                  <table class="table table-condensed">
                    <tr>
                      <th></th>
                      <th>KODE</th>
                      <th>NAMA AKUN</th>
                      <th>BALANCE</th>
                      <th>LAPORAN</th>
                      <th>STATUS</th>
                      <th>TAMBAH KODE REKENING TURUNAN DISINI</th>
                      <th></th>
                    </tr>
                    <?php foreach ($code_accounting->getResult() as $row) : ?>
                      <?php
                      if ($row->status == '0'){
                        $style = 'style="background:#edeff1;color:#999;"';
                      } else {
                        $style = 'style="background:white;"';
                      }
                      ;?>
                      <tr id="baris<?php echo $row->id;?>" <?php echo $style;?> >
                        <td></td>
                        <td>
                          <?php echo $row->code;?>
                        </td>
                        <td><?php echo $row->name;?></td>
                        <td>
                          <?php
                          if ($row->balance =='K') {
                            echo 'KREDIT';
                          } elseif ($row->balance=='D'){
                            echo 'DEBET';
                          }
                          ;?>
                        </td>
                        <td>
                          <?php
                          if ($row->type_report =='N') {
                            echo 'NERACA';
                          } elseif ($row->type_report=='LR'){
                            echo 'LABA RUGI';
                          }
                          ;?>
                        </td>
                        <td id="kolom_status<?php echo $row->id;?>"> <?php echo $row->status == '0' ? 'tdk aktif' : 'aktif';?></td>
                        <td>
                          <?php if ( $check_last_item ==  FALSE AND $row->status != '0' AND $level < $max_level ) :?>
                            <a href="<?php echo base_url();?>/accounting/code_accounting/create/<?php echo $row->id;?>" class="btn btn-primary btn-round btn-sm"><b><i class="fa fa-plus"></i> INPUT KODE TURUNAN</b></a>
                          <?php endif;?>
                          &nbsp;&nbsp;
                          <!-- <a class="btn btn-primary btn-xs" onclick="open_edit_modal('<?php echo $row->id;?>')"><i class="fa fa-pencil"></i></a> -->
                          &nbsp;&nbsp;
                          <!-- <a class="btn btn-danger btn-xs"  onclick="open_delete_modal('<?php echo $row->id;?>')"><i class="fa fa-close"></i></a> -->
                        </td>
                        <td>
                          <?php
                          if ( $row->level > 2 ):
                            if ($row->status == '0'){
                              $text_tombol = 'AKTIFKAN';
                              $class_tombol = 'btn btn-xs btn-success';
                            } else {
                              $text_tombol = 'NONAKTIFKAN';
                              $class_tombol = 'btn btn-xs btn-danger';
                            }
                            $tombol_param = "'".$row->id."','".$text_tombol."'";
                            ?>
                            <button id="tombol_aktifkan<?php echo $row->id;?>" class="<?php echo $class_tombol;?>" onClick="action_aktifkan_kode(<?php echo $row->id;?>)"><?php echo $text_tombol;?></button>
                          <?php endif;?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </table>


                </div>
              </div>
            </div>
          </div>


        </div>

      </div>
    </div>
  </div>

  <script type="text/javascript">

  function action_aktifkan_kode(id){
    $.ajax({
      url   : '<?php echo base_url();?>/accounting/code_accounting/action_change_status',
      type  : 'POST',
      dataType : 'json',
      data  : 'id='+id,
      success : function(JSONObject){
        if(JSONObject.response == true){
          if ( JSONObject.new_status == '0' ){
            $('#baris'+id).attr('style','background:#edeff1;color:#999;');
            $('#tombol_aktifkan'+id).attr('class','btn btn-xs btn-success');
            $('#tombol_aktifkan'+id).html('AKTIFKAN');
            $('#kolom_status'+id).html('tdk aktif');
          } else {
            $('#baris'+id).attr('style','background:white;');
            $('#tombol_aktifkan'+id).attr('class','btn btn-xs btn-danger');
            $('#tombol_aktifkan'+id).html('NONAKTIFKAN');
            $('#kolom_status'+id).html('aktif');
          }
          $.notify(JSONObject.message,'success');
        } else {
          $.notify(JSONObject.message,'error');
        }
      },
      error : function(a,b,c){
        $.notify(c.toString(),'error');
      }
    })
  }

</script>
