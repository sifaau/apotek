<?php
$name_menu = 'Buku Besar';
helper('form');
helper('journal_accounting');
echo view('layout/datepicker', array());

$this->request = \Config\Services::request();
$id_periode         =  ( $this->request->getGet('periode'));
$id_periode2        =  ( $this->request->getGet('periode2'));
$code_accounting    =  ( $this->request->getGet('code_accounting'));
$type_filter        =  ( $this->request->getGet('type_filter'));
$id_proyek          =  ( $this->request->getGet('proyek'));
$desc               =  ( $this->request->getGet('desc'));

$total_transaksi    = sum_transaksi_from_journal_detail_by_code_accounting_and_two_date($date_start,$date_end,$code_accounting,'');
$nilai_saldo_akhir  = $saldo_awal + $total_transaksi;

$array_id_journal_detail = '';
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Akuntansi</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">
        <div class="card">
          <div class="card-body">

            <?php echo form_open('accounting/ledger/search/',array('method'=>'get'));?>

            <div class="row">

              <div class="col-md-2" id="col_periode">
                <div class="form-group">
                  <label>dari</label>
                  <select class="form-control form-control-sm col-md-12" name="periode" id="periode" placeholder="dari periode">
                  </select>
                </div>
              </div>

              <div class="col-md-2" id="col_periode2">
                <div class="form-group">
                  <label>sampai</label>
                  <select class="form-control form-control-sm col-md-12" name="periode2" id="periode2" placeholder="sampai periode">
                  </select>
                </div>
              </div>

              <div class="col-md-3" id="col_code_account">
                <div class="form-group">
                  <label>Kode Rekening</label>
                  <select class="form-control form-control-sm col-md-12" name="code_accounting" id="code_accounting" >
                  </select>
                </div>
              </div>

              <div class="col-md-3" id="col_code_account">
                <div class="form-group">
                  <label>Cari Keterangan</label>
                  <input type="text" class="form-control form-control-sm col-md-12" name="desc" id="desc" value="<?php echo $desc;?>" placeholder="Cari keterangan">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label>#</label>
                  <button type="submit" id="button_filter" class="form-control btn btn-primary btn-round btn-sm"><i class="fa fa-search">&nbsp;</i>CARI</button>
                </div>
              </div>

            </div>

            <?php
            echo form_close();
            ?>

          </div>
        </div>


        <div class="card">
          <div class="card-body">

            <?php if ($list->getNumRows() > 0) :?>
            <div class="row">

              <div class="col-md-12 table-responsive">
                <table  class="table table-bordered">
                  <thead>
                    <tr>
                      <td></td>
                      <th>Tanggal</th>
                      <th>NO Bukti</th>
                      <th>Ref</th>
                      <th width="15%">Keterangan</th>
                      <th class="text-right" width="13%">Debet</th>
                      <th class="text-right" width="13%">Kredit</th>
                      <th class="text-right" width="15%">Saldo</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr style="background:#fdffe5;">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>SALDO AWAL</td>
                      <th class="text-right"></th>
                      <th class="text-right"></th>
                      <th class="text-right"><?php echo number_format($saldo_awal, 2, ',','.');?></th>
                      <th></th>
                      <th></th>
                    </tr>

                    <?php
                    $total_debet = 0;
                    $total_credit = 0;

                    foreach ($list->getResult() as $row) :
                      $line=check_line_journal_detail($row->id_journal_detail);?>
                      <?php $nilai_debet = 0;?>
                      <?php $nilai_credit = 0;
                      ?>

                      <tr>
                        <td></td>
                        <td><?php echo date('d M y',strtotime($row->date_trans));?>  </td>
                        <td><?php echo $row->no_bukti;?></td>
                        <td id="code_ref<?php echo $row->id_journal_detail;?>" style="color:green;">
                          <i class="fa fa-spin fa-spinner"></i><small>Loading.</small>
                        </td>
                        <td id="desc_ref<?php echo $row->id_journal_detail;?>">
                          <?php echo $row->desc_detail;?>
                        </td>
                        <td class="text-right">
                          <?php
                          if($row->debet != 0):?>
                            <a data-toggle="tooltip"
                            data-placement="bottom" title="<?php echo number_format($row->debet, 2, ',','.');?>"
                            id="link_balance_debet<?php echo $row->id_journal_detail;?>" style="color:#000;">
                            <span id="total_balance_debet<?php echo $row->id_journal_detail;?>" style="display: none;">
                              Rp <?php echo number_format($row->debet, 2, ',','.');?>
                            </span>
                            <?php echo number_format($row->debet, 2, ',','.');?>
                          </a>
                          <?php $total_debet = $total_debet+$row->debet;?>
                        <?php endif;?>
                      </td>
                      <td class="text-right">
                        <?php if($row->credit != 0):?>
                          <a data-toggle="tooltip"
                          data-placement="bottom" title="<?php echo number_format($row->credit, 2, ',','.');?>"
                          id="link_balance_credit<?php echo $row->id_journal_detail;?>" style="color:#000;">
                          <span id="total_balance_credit<?php echo $row->id_journal_detail;?>" style="display: none;">Rp <?php echo number_format($row->credit, 2, ',','.');?>
                        </span>
                        <?php echo number_format($row->credit, 2, ',','.');?>
                      </a>
                      <?php $total_credit = $total_credit+$row->credit;?>
                    <?php endif;?>
                  </td>
                  <th class="text-right">
                    <?php
                    if ($row->balance == 'D') {
                      $saldo_awal = $saldo_awal+( $row->debet -  $row->credit );
                    } else if ($row->balance == 'K'){
                      $saldo_awal = $saldo_awal+( $row->credit -  $row->debet );
                    }
                    echo number_format($saldo_awal, 2, ',','.')
                    ;?>
                  </th>
                  <td >&nbsp;&nbsp;
                    <!-- <a class="label bg-olive" onclick="open_detail_journal_modal(<?php echo $row->id_journal;?>)"><i class="fa fa-search"></i> JURNAL</a> -->
                  </td>
                  <td class="hidden-print text-left">
                    <small>
                      <?php
                      if ($row->id_user_update != ''){
                        if ($row->id_user_update != '29'){
                          echo 'edit '.get_username_user($row->id_user_update).'<br>'.date('d/m/Y',strtotime($row->date_update)).'<br>';
                        }
                      }
                      ?>
                    </small>

                    <?php
                    $month_trans = date('m',strtotime($row->date_trans));
                    $year_trans = date('Y',strtotime($row->date_trans));
                    // echo '<a class="label label-primary" onclick="open_edit_modal_2021('.$row->id_journal.')"><i class="fa fa-pencil"></i> EDIT.</a>';
                    ;?>

                    <?php
                    $deb = round(sum_debet_or_credit_detail_journal($row->id_journal,'D'), 2);
                    $cre = round(sum_debet_or_credit_detail_journal($row->id_journal,'K'), 2);
                    if (abs($deb-$cre) > 500){
                      echo '<span class="badge bg-red">JURNAL BELUM BALANCE</span>';
                    }
                    ?>
                  </td>
                </tr>
                <?php
                $array_id_journal_detail = $array_id_journal_detail.'"'.$row->id_journal_detail.'~'.$row->id_journal.'~'.$line.'",';
              endforeach;
              ?>



            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><small><i>Total di Halaman ini</i></small></td>
              <th class="text-right"><?php echo number_format($total_debet, 2, ',','.');?></th>
              <th class="text-right"><?php echo number_format($total_credit, 2, ',','.');?></th>
              <th class="text-right"></th>
              <th></th>
              <th></th>
            </tr>
            <tr style="background:#fdffe5;">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>SALDO AKHIR</td>
              <td></td>
              <th class="text-right"></th>
              <th class="text-right"><h4><b><?php echo number_format($nilai_saldo_akhir, 2, ',','.');?></b></h4></th>
              <th></th>
              <th></th>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    <?php else:?>
      <center>Pilih Periode dan kode rekening</center>
    <?php endif;?>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">

$( document ).ready(function() {
  load_periode_for_cookies();
  set_code_acc();
});

function set_code_acc(){
  $.ajax({
    url : "<?php echo base_url();?>/accounting/journal_apt/load_code_accounting_for_journal_by_company",
    type:'GET',
    beforeSend: function(){
      $('#code_accounting').html('<option>Loading..</option>');
    },
    success: function(hasil){
      $('#code_accounting').html(hasil);
      $("#code_accounting").select2();
      $('#code_accounting').val('<?php echo $code_accounting;?>').trigger('change');
    },
    error: function(){
      alert('Error, menampilkan code acc');
    }
  });
}


function load_periode_for_cookies(){
  if ( localStorage.getItem('ledger_search_bulan_ini') == '' || localStorage.getItem('ledger_search_bulan_ini') == null || localStorage.getItem('ledger_search_bulan_ini') != '<?php echo date('Y-m');?>' ){
    localStorage.removeItem('ledger_search_bulan_ini');
    localStorage.setItem('ledger_search_bulan_ini', '<?php echo date('Y-m');?>');
    list_periode();
    var hasil = localStorage.getItem('list_periode');
  } else {
    var hasil = localStorage.getItem('list_periode');
    if ( hasil == '' ){
      list_periode();
      var hasil = localStorage.getItem('list_periode');
    }
  }
  list_periode();
  var hasil = localStorage.getItem('list_periode');
  console.log(hasil);
  $('#periode').html(hasil);
  $('#periode').select2();
  $('#periode').val('<?php echo $id_periode;?>').trigger('change');
  $('#periode2').html(hasil);
  $('#periode2').select2();
  $('#periode2').val('<?php echo $id_periode2;?>').trigger('change');
}

function list_periode(){
  $.ajax({
    url : "<?php echo base_url();?>/accounting/journal_apt/list_periode",
    beforeSend:function(){
    },
    success: function(hasil){
      localStorage.removeItem('list_periode');
      localStorage.setItem('list_periode', hasil);
      $('#periode').html(hasil);
      $('#periode').select2();
      $('#periode').val('<?php echo $id_periode;?>').trigger('change');
      $('#periode2').html(hasil);
      $('#periode2').select2();
      $('#periode2').val('<?php echo $id_periode2;?>').trigger('change');
    },
    error:function(){
      alert('Error, menampilkan periode');
    }
  });
}

$(document).ready(function(){
  var my_array_id_journal = <?php echo "[".substr($array_id_journal_detail, 0, -1)."]";?>;
  console.log(my_array_id_journal);
// for (var i=0; i<my_array_id_journal.length; i++) {
  var i = 0;
  console.log(my_array_id_journal[i]);
  var array_length_id_journal = my_array_id_journal.length;
  code_ref(i);
// }



function code_ref(i){
  if (my_array_id_journal.length === 0 ){

  } else {

    if ( my_array_id_journal[i] ) {
      var myarr = my_array_id_journal[i].split("~");
      console.log(myarr[0]);
      var id_journal_detail = myarr[0];
      var id_journal = myarr[1];
      var line = myarr[2];

      $.ajax({
        url:'<?php echo base_url();?>/accounting/ledger/list_ref_ledger',
        type:'POST',
        data:'id_journal_detail='+id_journal_detail+'&id_journal='+id_journal+'&line='+line,
        dataType: 'json',
        async: false,
        beforeSend:function(){
          $('#code_ref'+id_journal_detail).html('<i class="fa fa-spin fa-spinner"></i>Menampilkan..');
        },
        success:function(JSONObject){
          $('#code_ref'+id_journal_detail).html(JSONObject.code_ref);
          setTimeout(function(){ code_ref(i+1); }, 500);
        },
        error:function(a,b,c){
          $('#code_ref'+id_journal_detail).html('<small><i>Error. Gagal Koneksi</i></small>');
          setTimeout(function(){ code_ref(i+1); }, 500);
        }
      })
    }
  }
}

});


</script>
