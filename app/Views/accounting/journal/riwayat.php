<?php
use App\Models\General_model;
$name_menu = 'Riwayat Jurnal';
helper('form');
// helper('journal_accounting');
helper('code_accounting');
echo view('layout/datepicker', array());

$this->general_model = new General_model();
$this->request = \Config\Services::request();
$tanggal  = $this->request->getGet('t');
$search   = $this->request->getGet('s');
?>

<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Akuntansi</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">
        <div class="card">
          <div class="card-body">

            <?php echo form_open('accounting/journal_apt/riwayat/',array('method'=>'get'));?>
            <div class="row">
              <div class="col-md-4">
                <input type="text" name="t" class="form-control pull-right" id="range_date" placeholder='rentang tanggal' autocomplete="off" >
              </div>
              <div class="col-md-4">
                <div class="input-group">
                  <input type="text" name="s" class="form-control pull-right" id="search" value="<?php echo $search;?>" placeholder='cari keterangan' autocomplete="off" >
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search">&nbsp;</i>CARI</button>
                  </div>
                  <div class="input-group-btn">
                    <!-- <button type="button" onclick="download_excel()" id="button_print" class="btn btn-default"><i class="fa fa-print">&nbsp;</i>CSV</button> -->
                  </div>
                </div>
              </div>
            </div>

            <?php echo form_close();?>

          </div>
        </div>

        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-md-12">
                <table  class="table table-bordered">
                  <thead>
                    <tr>
                      <th width="10%">TANGGAL</th>
                      <th width="10%">NO BUKTI</th>
                      <th>KODE REK</th>
                      <th width="">NAMA AKUN</th>
                      <th class="text-right" width="15%">DEBET</th>
                      <th class="text-right" width="15%">KREDIT</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $sum_debet  = 0;
                    $sum_credit = 0;
                    foreach ($list->getResult() as $row) : ?>
                    <tr style="background: #F5F5F5;">
                      <th style="padding:5px 10px !important;height:35px;"><?php echo date('d-m-Y',strtotime($row->date_trans));?></th>
                      <th style="padding:5px 10px !important;height:35px;"><?php echo $row->no_bukti;?></th>
                      <td style="padding:5px 10px !important;height:35px;color:#ddd;"><?php echo $row->id;?></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                    </tr>
                    <?php
                    $sub_sum_debet  = 0;
                    $sub_sum_credit = 0;
                    if (detail_journal($row->id)->getNumRows() === 0 ){
                      $this->general_model->do_update('zacc_journal',array('status'=>0),array('id'=>$row->id));
                    } else {
                      foreach (detail_journal($row->id)->getResult() as $row2) {
                        $sum_debet      = $sum_debet + $row2->debet;
                        $sum_credit     = $sum_credit + $row2->credit;
                        $sub_sum_debet  = $sub_sum_debet + $row2->debet;
                        $sub_sum_credit = $sub_sum_credit + $row2->credit;
                        ?>
                        <tr>
                          <td style="padding:5px 10px !important;height:35px;"></td>
                          <td class="text-right" style="padding:5px 10px !important;height:35px;color:orage;"><?php echo $row2->type_report;?></td>
                          <td style="padding:5px 10px !important;height:35px;"><?php echo $row2->code_accounting;?></td>
                          <td style="padding:5px 10px !important;height:35px;"><?php echo ($row2->credit != 0 ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '').$row2->account_name;?>
                            <br>
                            <?php if ($row2->desc!=''){
                              if ($row2->credit != 0){
                                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                              }
                              echo '<small style="color:#447144;">( '.$row2->desc.' )</small>';
                            };?>
                          </td>
                          <th style="padding:5px 10px !important;height:35px;" class="text-right"><?php echo $row2->debet != 0 ? number_format($row2->debet, 2, ',','.') : '';?></th>
                          <th style="padding:5px 10px !important;height:35px;" class="text-right"><?php echo $row2->credit != 0 ? number_format($row2->credit, 2, ',','.') : '';?></th>
                          <td style="padding:5px 10px !important;height:35px;" >
                            <small>
                              <?php if ($row2->id_user_update != NULL OR $row2->id_user_update != ''):?>
                                terakhir direvisi oleh <?php echo '<b>'.strtoupper(get_username_user($row2->id_user_update)).'</b> '.date('d/m/Y H:i:s',strtotime($row2->date_update));?>
                              <?php endif;?>
                            </small>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                    ?>

                    <tr style="color:#207a96;">
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;" class="text-right"><i>SUM DEBET / CREDIT</i></td>
                      <td style="padding:5px 10px !important;height:35px;" class="text-right"><i><?php echo number_format($sub_sum_debet, 2, ',','.');?></i></td>
                      <td style="padding:5px 10px !important;height:35px;" class="text-right"><i><?php echo number_format($sub_sum_credit, 2, ',','.');?></i></td>
                    </tr>

                    <tr>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;"></td>
                      <td style="padding:5px 10px !important;height:35px;">
                      </td>
                      <td colspan="2" class="text-center" style="padding:5px 10px !important;height:35px;">
                        <?php
                        $selisih = round($sub_sum_debet,2) - round($sub_sum_credit,2) ;
                        if ( $selisih ){
                          echo '(<i style="color:red;"> tidak balance '.$sub_sum_debet.' != '.$sub_sum_credit.'</i>)';
                        }
                        ?>

                      </td>
                      <td style="padding:5px 10px !important;height:35px;">

                      </td>
                    </tr>

                  <?php endforeach;?>
                </tbody>
                <tfoot>
                  <tr style="border-top: solid 3px #00a65a;border-bottom: solid 3px #00a65a;color:#000;">
                    <th colspan="3" style="padding:5px 10px !important;height:35px;"></th>
                    <th style="padding:5px 10px !important;height:35px;">TOTAL : </th>
                    <th style="padding:5px 10px !important;height:35px;" class="text-right"><?php echo number_format($sum_debet, 2, ',','.') ;?></th>
                    <th style="padding:5px 10px !important;height:35px;" class="text-right"><?php echo number_format($sum_credit, 2, ',','.') ;?></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>


        </div>
      </div>

    </div>

  </div>
</div>
</div>

<script type="text/javascript">

$( document ).ready(function() {
  $('#range_date').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'DD-MMM-YYYY'
  });
  $('#range_date').val('<?php echo $tanggal;?>');
});

</script>
