<?php $name_menu = 'Jurnal';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>


      <div class="page-body">
        <div class="card">
          <div class="card-header">

            <div class='row'>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tanggal</label>&nbsp;&nbsp;<span style="color:red;">*</span><small>&nbsp;( Sesuaikan dengan tanggal transaksi )</small>
                  <input type="text" name="date_trans" id="date_trans" class="form-control" data-error=".errorTxt3" autocomplete="off">
                  <div class="frmerrormsg errorTxt3"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kode Bukti</label>&nbsp;&nbsp;<span style="color:red;">*</span><small>&nbsp; / Kode Voucher </small>
                  <input type="text" class="form-control" name="no_bukti" id="no_bukti" data-error=".errorTxt2" autocomplete="off">
                  <div class="frmerrormsg errorTxt2"></div>
                </div>
              </div>
            </div>

          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <small style="color:#b31a1a;" class="badge bg-green pull-right"> PENULISAN NOMINAL DIBELAKANG KOMA MENGGUNAKAN "TITIK", CONTOH : <b>1000000.02</b></small>
                <table class="table table-stripped">
                  <thead>
                    <tr style="padding:0 3px !important;height:35px;">
                      <th  style="padding:0 3px !important;height:35px" width="24%">KODE REK DEBET</th>
                      <th  style="padding:0 3px !important;height:35px" width="24%">KODE REK KREDIT</th>
                      <th style="padding:0 3px !important;height:35px" width="24%">KETERANGAN</th>
                      <th style="padding:0 3px !important;height:35px" width="24%">NOMINAL <small>&nbsp;( Rp )</small></th>
                      <th style="padding:0 3px !important;height:35px"></th>
                    </tr>
                    <tr style="padding:0 3px !important;height:40px;background:#f3ffe6;">
                      <td style="padding:0 3px !important;height:40px">
                        <select class="form-control col-md-12" name="code_accounting" id="code_accounting_debet" onkeydown="if (event.keyCode == 13) save_detail_journal();">
                          <option value="">Pilih Kode Rek Debet</option>
                        </select>
                      </td>
                      <td style="padding:0 3px !important;height:40px">
                        <select class="form-control col-md-12" name="code_accounting" id="code_accounting_credit" onkeydown="if (event.keyCode == 13) save_detail_journal();">
                          <option value="">Pilih Kode Rek Kredit</option>
                        </select>
                      </td>
                      <td style="padding:0 3px !important;height:40px">
                        <textarea class="form-control" id="keterangan" onkeydown="if (event.keyCode == 13) save_detail_journal();"></textarea>
                      <td style="padding:0 3px !important;height:40px"><input type="number" class="form-control " id="value" value="" onkeydown="if (event.keyCode == 13) save_detail_journal();"></td>
                      <td style="padding:0 3px !important;height:40px" id="button_save"><a class="btn btn-primary btn-round btn-md" onclick="save_detail_journal()" style="color:white;"><i class="fa fa-save"></i>&nbsp;<b>ENTER</b></a></td>
                    </tr>
                  </thead>
                  <tbody id="list_item">
                  </tbody>
                  <tfoot>
                    <tr style="padding:0 3px !important;height:35px">
                      <th style="padding:0 3px !important;height:35px" colspan="3" class="text-right">TOTAL</th>
                      <th style="padding:0 3px !important;height:35px" class="text-right" id="sum_value">0</th>
                      <th style="padding:0 3px !important;height:35px" ></th>
                      <th style="padding:0 3px !important;height:35px"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <div class="col-md-12">
                <button type="button" onclick="save_journal()" class="btn btn-lg btn-danger pull-right" id="btn_save_journal" style="display: none;"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;<b>SIMPAN JURNAL</b></button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<div class="modal fade" id="detail_journal_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border:none;">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" id="message_detail">
          </div>
        </div>
      </div>
      <div class="modal-footer" style="border:none;">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;&nbsp;TUTUP</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="loading_kd_rek_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border:none;background:#43945b;">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center">
            <b style="font-size:20px;color:white;"><i class="fa fa-spin fa-spinner"></i> Tunggu, Sedang Menyiapkan Data Kode Rekening</b>
          </div>
        </div>
      </div>
      <div class="modal-footer" style="border:none;">
        <!-- <button type="button" class="" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;&nbsp;TUTUP</button> -->
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function currency_format2(n) {
  return parseFloat(n).toFixed(2).replace(/./g, function(c, i, a) {
    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
  });
}

$("#date_trans").datepicker({
  format: "dd-mm-yyyy",
  autoclose: true,
}).datepicker(
  'setDate',new Date()
);

var arraynya = [];
var array_journal_detail = [];

function save_detail_journal(){
  var val_code_accounting_debet = $("#code_accounting_debet").val();
  var code_accounting_debet     = $("#code_accounting_debet option:selected").text();
  var val_code_accounting_credit = $("#code_accounting_credit").val();
  var code_accounting_credit     = $("#code_accounting_credit option:selected").text();
  var keterangan          = $("#keterangan").val();
  var value               = $("#value").val();
  var dataString          = 'code_accounting_debet='+ code_accounting_debet + '&code_accounting_credit='+ code_accounting_credit + '&keterangan='+ keterangan +'&value='+ value;
  var button_save         = '<a class="btn btn-success" onclick="save_detail_journal()"><i class="fa fa-save"></i>&nbsp;TAMBAH</a>';

  if ( val_code_accounting_debet == '' || val_code_accounting_debet == null || val_code_accounting_credit == '' || val_code_accounting_credit == null || keterangan == ''  ){
    $.notify('KODE REKENING BELUM DIPILIH &/ keterangan kosong', "error");
  } else if ( keterangan == '' || keterangan == null){
    $.notify('KETERANGAN BELUM DIISI', "error");
  } else if ( ( value == '0' || value == '') ){
    $.notify('Nominal harus lebih dari 0', "error");
  } else if ( val_code_accounting_debet == val_code_accounting_credit ){
    $.notify('KODE REKENING DEBET DAN KREDIT TAK BOLEH SAMA', "error");
  } else {
    arraynya.push({'code_accounting_debet':val_code_accounting_debet,'code_accounting_credit':val_code_accounting_credit,'text_code_accounting_debet':code_accounting_debet,'text_code_accounting_credit':code_accounting_credit,'desc':keterangan,'value':value});
    array_journal_detail.push({'code_accounting_debet':val_code_accounting_debet,'code_accounting_credit':val_code_accounting_credit,'desc':keterangan,'value':value});
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem('journal_detail_2021',JSON.stringify(arraynya));
      localStorage.setItem('val_journal_detail_2021',JSON.stringify(array_journal_detail));
    }
    view_journal_detail(arraynya);

    $('#code_accounting_debet').val(''); // Select the option with a value of '1'
    $('#code_accounting_debet').trigger('change');
    $('#code_accounting_credit').val('1'); // Select the option with a value of '1'
    $('#code_accounting_credit').trigger('change');
    // $("#code_accounting_debet").select2("val", "");
    // $("#code_accounting_credit").select2("val", "");
    $("#value").val('');
    show_sum();
    $("#code_accounting_debet").select2('open');
  }
}

function remove_journal_detail(id){
  arraynya.splice(id, 1);
  array_journal_detail.splice(id, 1);
  if (typeof(Storage) !== "undefined") {
    localStorage.setItem('journal_detail_2021',JSON.stringify(arraynya));
    localStorage.setItem('val_journal_detail_2021',JSON.stringify(array_journal_detail));
  }
  view_journal_detail(arraynya);
  show_sum();
}

function view_journal_detail(arraynya){
  console.log(array_journal_detail);
  $("#list_item").html('');
  for (i = 0; i < arraynya.length; i++) {
    list_item = '<tr id="order'+i+'"><td>'+arraynya[i].text_code_accounting_debet+'</td><td>'+arraynya[i].text_code_accounting_credit+'</td><td>'+arraynya[i].desc+'</td><td class="text-right">'+(arraynya[i].value)+'</td><td><a class="btn btn-sm btn-round btn-danger" onclick="remove_journal_detail('+i+')"><i class="fas fa-trash" style="color:#FFFFFF;"></i></a></td></tr>';
    $("#list_item").append(list_item);
  }
}

function sumColumn(index){
  var total = 0;
  $("td:nth-child("+ index +")").each(function() {
    var text = $(this).text();
    var nilai = text;
    total += parseFloat(nilai) || 0;
    // alert(nilai);
  });
  return (total);
}

function show_sum(){

  $("#sum_value_").val(sumColumn(4));
  $("#sum_value").html(currency_format2(sumColumn(4)));
  var sum = sumColumn(4);
  if ( sum > 0 ){
    $("#btn_save_journal").show();
  } else {
    $("#btn_save_journal").hide();
  }

}


function cek_storage(){
  if (typeof(Storage) !== "undefined") {
    local_arraynya = localStorage.getItem("journal_detail_2021");
    val_local_arraynya = localStorage.getItem("val_journal_detail_2021");
    if (local_arraynya != null ){
      arraynya = JSON.parse(local_arraynya);
      array_journal_detail = JSON.parse(val_local_arraynya);
      view_journal_detail(arraynya);
      show_sum();
    }
    // console.log(arraynya);
  } else {
    console.log('not supported local storage');
  }
}

cek_storage();

function save_journal(){
  var tr = document.getElementById('list_item').rows;
  var td = null;
  var date_trans  = $("#date_trans").val();
  var no_bukti    = $("#no_bukti").val();
  var id_company  = $("#id_company").val();
  if (date_trans == '' || date_trans == 0 || no_bukti == '' ) {
    $.notify('TANGGAL DAN KODE BUKTI HARUS DIISI', "error");
  } else {
    var tr = document.getElementById('list_item').rows;
    if ( tr.length == 0 ) {
      $.notify('TIDAK ADA DATA DETAIL JURNAL', "error");
    } else {

      $("#btn_save_journal").html('<i class="fa fa-spin fa-refresh"></i>&nbsp;&nbsp;&nbsp;MENYIMPAN DATA, MOHON TUNGGU..');
      $("#btn_save_journal").removeAttr('onclick');
      var dataJournal = 'no_bukti='+ no_bukti + '&date_trans='+ date_trans+ '&detail_journal='+ JSON.stringify(array_journal_detail);
      $.ajax({
        type      : 'POST',
        url       : '<?php echo base_url();?>/accounting/journal_apt/ajax_action_create_journal',
        data      : dataJournal,
        dataType  : 'json',
        success   : function(JSONObject){
          if (JSONObject.response == true){
            $.notify(JSONObject.message, "info");
            var id_journal = JSONObject.id_journal;
            var td = null;
            if (typeof(Storage) !== "undefined") {
              localStorage.removeItem("journal_detail_2021");
              localStorage.removeItem("val_journal_detail_2021");
              $("#sum_value_").val(0);
              $("#sum_value").html('0');
              $("#btn_save_journal").hide();
            }
            arraynya              = [];
            array_journal_detail  = [];
            $("#no_bukti").val('')
            view_journal_detail(arraynya);
            cek_hasil_journal_modal(id_journal);
          }
          if (JSONObject.response == false){
            $.notify(JSONObject.message, "error");
          }
          $("#btn_save_journal").html('<i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;<b>SIMPAN JURNAL</b>');
          $("#btn_save_journal").attr('onclick','save_journal()');
        },
        error:function(x,y,z){
          $.notify(z.toString(), "error");
          $("#btn_save_journal").html('<i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;<b>SIMPAN JURNAL</b>');
          $("#btn_save_journal").attr('onclick','save_journal()');
        }
      });
    }
  }
}

function cek_hasil_journal_modal(id_journal){
  $.ajax({
    data        : 'id_journal='+id_journal,
    type        : 'POST',
    url         : "<?php echo base_url();?>accounting/journal_2021/get_detail_journal_html",
    beforeSend  : function(){
      $("#message_detail").html('<i class="fa fa-spin fa-refresh"></i>');
    },
    success     : function(data){
      $("#detail_journal_modal").modal("show");
      $('#message_detail').html(data);
    },
    error : function(a,b,c){
      $.notify(c.toString(),'error');
    }
  });
}

</script>

<script type="text/javascript">

$(document).ready(function(){
  load_edit_code_accounting();
});


function load_edit_code_accounting(){
  var hasil = localStorage.getItem('localcodeaccapp2haljurnalumum');
  var datestorage = localStorage.getItem('datelocalcodeaccapp2haljurnalumum');
  console.log(hasil);
  if ( datestorage == "<?php echo date('Y-m-d');?>"){
    if ( hasil == '' || hasil == null ){
      get_new_code_acc();
      // console.log('b');
    } else {
      $('#code_accounting_debet').html(hasil);
      $("#code_accounting_debet").select2();
      $('#code_accounting_credit').html(hasil);
      $("#code_accounting_credit").select2();
    }
  } else {
    get_new_code_acc();
  }
}

function get_new_code_acc(){
  $.ajax({
    url : "<?php echo base_url();?>/accounting/journal_apt/load_code_accounting_for_journal_by_company/",
    beforeSend:function(){
      $('#loading_kd_rek_modal').modal('show');
    },
    success: function(hasilx){
      $('#loading_kd_rek_modal').modal('hide');
      localStorage.setItem('localcodeaccapp2haljurnalumum', hasilx);
      localStorage.setItem('datelocalcodeaccapp2haljurnalumum', '<?php echo date('Y-m-d');?>');

      $('#code_accounting_debet').html(hasilx);
      $("#code_accounting_debet").select2();
      $('#code_accounting_credit').html(hasilx);
      $("#code_accounting_credit").select2();
    },
    error:function(a,b,c){
      alert('Error, menampilkan code acc'+c.toString());
      $('#loading_kd_rek_modal').modal('hide');
    },
    abort:function(){
      $('#loading_kd_rek_modal').modal('hide');
    }
  });
}





</script>
