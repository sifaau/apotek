<?php $name_menu = 'Generate Journal';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Akuntansi</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12 table-responsive" id="root">
              </div>
            </div>
          </div>
          <div class="card-footer">
            <button class="btn btn-primary btn-lg pull-right">GENERATEL SEMUA SEKALIGUS</button>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">

$(document).ready(function() {
  get_data();
});

function get_data(){
  $.ajax({
    url       : '<?php echo base_url();?>/accounting/generate/get_data',
    beforeSend: function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(hasil){
      $('#root').html(hasil);
    },
    error: function(){
      $('#root').html('');
    }
  });

}

function generate_barang_masuk(id){
  $.ajax({
    url  : '<?php echo base_url();?>/accounting/generate/generate_barang_masuk',
    type : 'POST',
    data : 'id='+id,
    dataType : 'json',
    beforeSend: function(){
      $('#tbl_barang_masuk'+id).html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(JSONObject){
      if (JSONObject.response==true){
        $('#barang_masuk'+id).hide();
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'error',z_index: 10000});
      }
      $('#tbl_barang_masuk'+id).html('<b>GENERATE</b>');
    },
    error: function(a,b,c){
      $('#tbl_barang_masuk'+id).html('<b>GENERATE</b>');
      $.notify(c.toString(),{type: 'error',z_index: 10000});
    }
  });
}

function generate_penjualan(id){
  $.ajax({
    url  : '<?php echo base_url();?>/accounting/generate/generate_penjualan',
    type : 'POST',
    data : 'id='+id,
    dataType : 'json',
    beforeSend: function(){
      $('#tbl_penjualan'+id).html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(JSONObject){
      if (JSONObject.response==true){
        $('#penjualan'+id).hide();
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'error',z_index: 10000});
      }
      $('#tbl_penjualan'+id).html('<b>GENERATE</b>');
    },
    error: function(a,b,c){
      $('#tbl_penjualan'+id).html('<b>GENERATE</b>');
      $.notify(c.toString(),{type: 'error',z_index: 10000});
    }
  });
}

function generate_pembayaran_po(id){
  $.ajax({
    url  : '<?php echo base_url();?>/accounting/generate/generate_pembayaran_po',
    type : 'POST',
    data : 'id='+id,
    dataType : 'json',
    beforeSend: function(){
      $('#tbl_pembayaran_po'+id).html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(JSONObject){
      if (JSONObject.response==true){
        $('#pembayaran_po'+id).hide();
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'error',z_index: 10000});
      }
      $('#tbl_pembayaran_po'+id).html('<b>GENERATE</b>');
    },
    error: function(a,b,c){
      $('#tbl_pembayaran_po'+id).html('<b>GENERATE</b>');
      $.notify(c.toString(),{type: 'error',z_index: 10000});
    }
  });
}

</script>
