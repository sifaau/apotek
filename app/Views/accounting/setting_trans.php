<?php $name_menu = 'Setting Akun Transaksi';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Akuntansi</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">


        <div class="row">

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Obat</div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Kode Persediaan Obat</label>
                  <select class="form-control form-setting" id="persediaan_obat" onChange="ubah_kode('persediaan_obat')"></select>
                </div>
                <div class="form-group">
                  <label>Kode Harga Pokok Penjualan Obat</label>
                  <select class="form-control form-setting" id="hpp_obat" onChange="ubah_kode('hpp_obat')"></select>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Barang</div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Kode Persediaan Barang</label>
                  <select class="form-control form-setting" id="persediaan_barang" onChange="ubah_kode('persediaan_barang')"></select>
                </div>
                <div class="form-group">
                  <label>Kode Harga Pokok Penjualan Barang</label>
                  <select class="form-control form-setting" id="hpp_barang" onChange="ubah_kode('hpp_barang')"></select>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Supplier</div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Kode Hutang Supplier</label>
                  <select class="form-control form-setting" id="hutang_supplier" onChange="ubah_kode('hutang_supplier')"></select>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Penjualan</div>
              </div>
              <div class="card-body">

                <div class="form-group">
                  <label>Kode Kas Masuk</label>
                  <select class="form-control form-setting" id="kas_penjualan" onChange="ubah_kode('kas_penjualan')"></select>
                </div>

                <div class="form-group">
                  <label>Kode Diskon</label>
                  <select class="form-control form-setting" id="diskon_penjualan" onChange="ubah_kode('diskon_penjualan')"></select>
                </div>

                <div class="form-group">
                  <label>Kode Penjualan Obat</label>
                  <select class="form-control form-setting" id="penjualan_obat" onChange="ubah_kode('penjualan_obat')"></select>
                </div>

                <div class="form-group">
                  <label>Kode Penjualan Barang</label>
                  <select class="form-control form-setting" id="penjualan_barang" onChange="ubah_kode('penjualan_barang')"></select>
                </div>
                <!-- <div class="form-group">
                  <label>Kode Biaya Diskon</label>
                  <select class="form-control form-setting" id="biaya_diskon_penjualan" onChange="ubah_kode('biaya_diskon_penjualan')"></select>
                </div> -->

                <div class="form-group">
                  <label>Kode Tuslah</label>
                  <select class="form-control form-setting" id="tuslah_penjualan" onChange="ubah_kode('tuslah_penjualan')"></select>
                </div>

              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Pembelian</div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Kode Beban biaya PPN</label>
                  <select class="form-control form-setting" id="biaya_ppn_pembelian" onChange="ubah_kode('biaya_ppn_pembelian')"></select>
                </div>
              </div>

              <div class="card-body">
                <div class="form-group">
                  <label>Kode Pembayaran Kas</label>
                  <select class="form-control form-setting" id="kode_kas_pembayaran" onChange="ubah_kode('kode_kas_pembayaran')"></select>
                </div>
              </div>

              <div class="card-body">
                <div class="form-group">
                  <label>Kode Pembayaran Bank</label>
                  <select class="form-control form-setting" id="kode_bank_pembayaran" onChange="ubah_kode('kode_bank_pembayaran')"></select>
                </div>
              </div>

            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Pajak</div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Kode Hutang PPH 21</label>
                  <select class="form-control form-setting" id="hutang_pph21" onChange="ubah_kode('hutang_pph21')"></select>
                </div>

                <div class="form-group">
                  <label>Kode Hutang PPH 23</label>
                  <select class="form-control form-setting" id="hutang_pph23" onChange="ubah_kode('hutang_pph23')"></select>
                </div>

                <div class="form-group">
                  <label>Kode Hutang PPN</label>
                  <select class="form-control form-setting" id="hutang_ppn" onChange="ubah_kode('hutang_ppn')"></select>
                </div>
              </div>
            </div>
          </div>


          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Laba Ditahan</div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Kode Laba Ditahan</label>
                  <select class="form-control form-setting" id="code_parent_laba_ditahan" onChange="ubah_kode('code_parent_laba_ditahan')"></select>
                </div>

                <div class="form-group">
                  <label>Kode Laba Rugi Bulan Berjalan</label>
                  <select class="form-control form-setting" id="code_parent_laba_periode" onChange="ubah_kode('code_parent_laba_periode')"></select>
                </div>

                <div class="form-group">
                  <label>Kode Laba Rugi Bulan Berjalan Lalu</label>
                  <select class="form-control form-setting" id="code_parent_laba_sd_periode_lalu" onChange="ubah_kode('code_parent_laba_sd_periode_lalu')"></select>
                </div>

              </div>
            </div>
          </div>



        </div>

      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
$('.form-setting').html("<option>--LOADING--</option>");
$(document).ready(function() {
  set_code_acc();
  cek_setting();
});

function set_code_acc(){
  $.ajax({
    url : "<?php echo base_url();?>/accounting/journal_apt/load_code_accounting_for_journal_by_company",
    type:'GET',
    beforeSend: function(){
      $('#code_accounting').html('<option>Loading..</option>');
    },
    success: function(hasil){
      $('#persediaan_obat').html(hasil);
      $('#hpp_obat').html(hasil);
      $('#persediaan_barang').html(hasil);
      $('#hpp_barang').html(hasil);
      $('#hutang_supplier').html(hasil);
      $('#diskon_penjualan').html(hasil);
      $('#tuslah_penjualan').html(hasil);
      $('#hutang_pph21').html(hasil);
      $('#hutang_pph23').html(hasil);
      $('#hutang_ppn').html(hasil);
      $('#biaya_ppn_pembelian').html(hasil);
      $('#kas_penjualan').html(hasil);
      $('#biaya_diskon_penjualan').html(hasil);
      $('#penjualan_obat').html(hasil);
      $('#penjualan_barang').html(hasil);
      $('#kode_kas_pembayaran').html(hasil);
      $('#kode_bank_pembayaran').html(hasil);
      $('#code_parent_laba_ditahan').html(hasil);
      $('#code_parent_laba_periode').html(hasil);
      $('#code_parent_laba_sd_periode_lalu').html(hasil);

      $('#persediaan_obat').select2();
      $('#hpp_obat').select2();
      $('#persediaan_barang').select2();
      $('#hpp_barang').select2();
      $('#hutang_supplier').select2();
      $('#diskon_penjualan').select2();
      $('#tuslah_penjualan').select2();
      $('#hutang_pph21').select2();
      $('#hutang_pph23').select2();
      $('#hutang_ppn').select2();
      $('#biaya_ppn_pembelian').select2();
      $('#kas_penjualan').select2();
      $('#biaya_diskon_penjualan').select2();
      $('#penjualan_obat').select2();
      $('#penjualan_barang').select2();
      $('#kode_kas_pembayaran').select2();
      $('#kode_bank_pembayaran').select2();
      $('#code_parent_laba_ditahan').select2();
      $('#code_parent_laba_periode').select2();
      $('#code_parent_laba_sd_periode_lalu').select2();

    },
    error: function(){
      alert('Error, menampilkan code acc');
    }
  });
}

function cek_setting(){
  $.ajax({
    url : "<?php echo base_url();?>/accounting/setting/cek_setting",
    dataType:'json',
    beforeSend: function(){

    },
    success: function(JSONObject){
      if (JSONObject.response == true){
        $('#persediaan_obat').val(JSONObject.persediaan_obat).trigger('change');
        $('#hpp_obat').val(JSONObject.hpp_obat).trigger('change');
        $('#persediaan_barang').val(JSONObject.persediaan_barang).trigger('change');
        $('#hpp_barang').val(JSONObject.hpp_barang).trigger('change');
        $('#hutang_supplier').val(JSONObject.hutang_supplier).trigger('change');
        $('#diskon_penjualan').val(JSONObject.diskon_penjualan).trigger('change');
        $('#tuslah_penjualan').val(JSONObject.tuslah_penjualan).trigger('change');
        $('#hutang_pph21').val(JSONObject.hutang_pph21).trigger('change');
        $('#hutang_pph23').val(JSONObject.hutang_pph23).trigger('change');
        $('#hutang_ppn').val(JSONObject.hutang_ppn).trigger('change');
        $('#biaya_ppn_pembelian').val(JSONObject.biaya_ppn_pembelian).trigger('change');
        $('#kas_penjualan').val(JSONObject.kas_penjualan).trigger('change');
        $('#biaya_diskon_penjualan').val(JSONObject.biaya_diskon_penjualan).trigger('change');
        $('#penjualan_obat').val(JSONObject.penjualan_obat).trigger('change');
        $('#penjualan_barang').val(JSONObject.penjualan_barang).trigger('change');
        $('#kode_kas_pembayaran').val(JSONObject.kode_kas_pembayaran).trigger('change');
        $('#kode_bank_pembayaran').val(JSONObject.kode_bank_pembayaran).trigger('change');
        $('#code_parent_laba_ditahan').val(JSONObject.code_parent_laba_ditahan).trigger('change');
        $('#code_parent_laba_periode').val(JSONObject.code_parent_laba_periode).trigger('change');
        $('#code_parent_laba_sd_periode_lalu').val(JSONObject.code_parent_laba_sd_periode_lalu).trigger('change');
      } else {
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      }
    },
    error: function(){
      alert('Error, menampilkan code acc');
    }
  });
}

function ubah_kode(param){
  var value = $('#'+param).val();
  $.ajax({
    url       : '<?php echo base_url();?>/accounting/setting/change_setting',
    type      : 'POST',
    data      : 'param='+param+'&value='+value,
    dataType  : 'json',
    beforeSend: function(){

    },
    success: function(JSONObject){
      if (JSONObject.response == true){
        $.notify({message: param+' '+JSONObject.message},{type: 'success',z_index: 10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      }
    },
    error: function(){
      // alert('Error, menampilkan code acc');
    }
  });

}



</script>
