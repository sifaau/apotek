
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Login</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?php echo base_url('favicon.ico');?>" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?php echo base_url('plugin/atlantis/js/plugin/webfont/webfont.min.js'); ?>"></script>
	<script>
	WebFont.load({
		google: {"families":["Lato:300,400,700,900"]},
		custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?php echo base_url('plugin/atlantis/css/fonts.min.css');?>']},
		active: function() {
			sessionStorage.fonts = true;
		}
	});
	</script>

	<!-- CSS Files -->
	<?php echo view('layout/atlantis_css');?>
</head>
<body class="login_page" style="background:#efefee;">
	<div class="container h-100">
		<div id="overlay"></div>
		<div class="row h-100 justify-content-center align-items-center">
			<div class="box_login" style="background:#fff;padding:30px;border-radius:20px;margin-top : 40px;">
					<div>
						<center>
							<span>Maaf anda tidak segera login</span>
						</center>
					</div>
					<div class="text-center">
						<a  class="btn form-control btn-primary btn-round" href="<?php echo base_url();?>/aptauth">KEMBALI KE HALAMAN LOGIN</a>
					</div>
				</div>
		</div>
	</div>
	<?php echo view('layout/atlantis_js');?>


</body>
</html>
