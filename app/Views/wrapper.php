<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>1.1</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="icon" href="<?php echo base_url('favicon.ico');?>" type="image/x-icon"/>
  <!-- CSS Files -->
  <?php echo $header;?>
</head>
<body>
  <div class="wrapper sidebar_minimize">
    <?php echo $header_content;?>
    <?php if ($sidebar):?>
      <?php echo $sidebar;?>
    <?php endif;?>
    <div class="content-wrapper">
      <?php echo $content;?>
    </div>
    <?php echo $footer;?>
  </div>
</body>
</html>
