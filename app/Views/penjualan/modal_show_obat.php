<div class="modal" id="pilihan_data_obat" tabindex="30000" role="dialog" style="z-index:30000;">
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content" style="width:1080px;left:-20%;">
      <div class="modal-header">
        <h5 class="modal-title">Cari Data Obat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" id="jenis_trans" />
          <!-- <div class="col-md-12">
            <div class="form-group">
              <label>Pilih Golongan</label>
              <select class="form-control" id="id_parent" placeholder="">
              </select>
            </div>
          </div> -->

          <div class="col-md-12">
            <div class="form-group">
              <label>Pilih Golongan</label>
              <select class="form-control" id="golongan_obat" onChange="get_pilih_obat()" placeholder="">

              </select>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <div class="input-group mb-3">
                <input type="text" class="form-control" id="search_obat" placeholder="Cari Nama Obat" onKeyup="if (event.keyCode == 13) get_pilih_obat();" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" id="basic-addon2" onClick="get_pilih_obat()" onKeyup="if (event.keyCode == 13) get_pilih_obat();"><b>C A R I.</b></button>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12 table-responsive" id="root_table_pilih_obat">
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function show_pilihan_obat(jenis_trans){
  // get_parent_golongan(0,'id_parent');
  get_golongan_obat();
  $('#jenis_trans').val(jenis_trans);
  $('#root_table_pilih_obat').html('');
  $('#pilihan_data_obat').modal('show');

}

$( "#id_parent" ).change(function() {
  var parent = $('#id_parent').val();
  get_parent_golongan(parent,'golongan');
});

  function get_pilih_obat(){
    var search = $('#search_obat').val();
    var golongan = $('#golongan_obat').val();
    var jenis_trans = $('#jenis_trans').val();
    $.ajax({
      type:'GET',
      url : '<?php echo base_url();?>/apotek/obat/get_pilih_obat',
      data : 'search='+search+'&golongan='+golongan+'&jenis_trans='+jenis_trans,
      beforeSend:function(){
        $('#root_table_pilih_obat').html('<center><i class="fa fa-spin fa-spinner"></i> Loading...</center>');
      },
      success: function(result) {
        $('#root_table_pilih_obat').html(result);
      },
      error:function(a,b,c){
        $('#root_table_pilih_obat').html('<tr><td>'+c.toString()+'</td></tr>');
      }
    });
  }

  function get_parent_golongan(id_parent,html){
    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan',
      data : 'id_parent='+id_parent,
      beforeSend:function(){
        $('#'+html).html('<option>Loading</option>');
      },
      success: function(data) {
        $('#'+html).html(data);
        $('#'+html).select2();
      },
      error:function(a,b,c){
        $('#'+html).html('<option>Error</option>');
      }
    });
  }

  function get_golongan_obat(){
    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan_by_reff',
      data : 'reff=obat',
      beforeSend:function(){
        $('#golongan_obat').html('<option>Loading</option>');
      },
      success: function(data) {
        $('#golongan_obat').html(data);
        // $('#golongan_obat').select2();
      },
      error:function(a,b,c){
        $('#golongan_obat').html('<option>Error</option>');
      }
    });
  }

</script>
