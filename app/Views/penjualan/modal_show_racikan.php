<div class="modal" id="pilihan_data_racikan" tabindex="30000" role="dialog" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cari Data Racikan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">

          <div class="col-md-12">
            <div class="form-group">
              <div class="input-group mb-3">
                <input type="text" class="form-control" id="search_racikan" placeholder="Cari Nama Racikan" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" id="basic-addon2" onClick="get_pilih_racikan()"><b>C A R I</b></button>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12" id="root_table_pilih_racikan">
            <table class="table">
              <body id="">
              </body>
            </table>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function show_pilihan_racikan(){
  get_parent_golongan(0,'id_parent');
  $('#root_table_pilih_racikan').html('');
  $('#pilihan_data_racikan').modal('show');
}

$( "#id_parent" ).change(function() {
  var parent = $('#id_parent').val();
  get_parent_golongan(parent,'golongan');
});

  function get_pilih_racikan(){
    var search = $('#search_racikan').val();
    $.ajax({
      type:'GET',
      url : '<?php echo base_url();?>/apotek/racikan/get_pilih_racikan',
      data : 'search='+search,
      beforeSend:function(){
        $('#root_table_pilih_racikan').html('<tr><td><option> - Loading -</option></td></tr>');
      },
      success: function(result) {
        $('#root_table_pilih_racikan').html(result);
      },
      error:function(a,b,c){
        $('#root_table_pilih_racikan').html('<tr><td>'+c.toString()+'</td></tr>');
      }
    });
  }

  function get_parent_golongan(id_parent,html){
    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan',
      data : 'id_parent='+id_parent,
      beforeSend:function(){
        $('#'+html).html('<option>Loading</option>');
      },
      success: function(data) {
        $('#'+html).html(data);
        $('#'+html).select2();
      },
      error:function(a,b,c){
        $('#'+html).html('<option>Error</option>');
      }
    });
  }

  function add_item_racikan(id,idr){
    var qty = $('#value_item_racikan'+id+idr).val();
    if (parseFloat(qty)>0){
      $.ajax({
        type:'POST',
        url : '<?php echo base_url();?>/penjualan/kasir/add_item_racikan',
        data : 'id='+id+'&qty='+qty,
        dataType: 'json',
        beforeSend:function(){
          $('#btn_add_item_racikan'+id+idr).html('<i class="fa fa-spin fa-spinner"></i>');
          $('#btn_add_item_racikan'+id+idr).removeAttr('onClick');
        },
        success: function(JSONObject) {
          if (JSONObject.response == true){
            $('#value_item_racikan'+id+idr).val(0);
            list_item();
            $.notify({message: JSONObject.message},{type: 'success',z_index: 100000});
          } else {
            $.notify({message: JSONObject.message},{type: 'error',z_index: 100000});
          }

          $('#btn_add_item_racikan'+id+idr).html('<i class="fa fa-plus"></i>');
          $('#btn_add_item_racikan'+id+idr).attr('onClick','add_item_racikan('+id+','+idr+')');
        },
        error:function(a,b,c){
          $('#btn_add_item_racikan'+id+idr).html('<i class="fa fa-plus"></i>');
          $('#btn_add_item_racikan'+id+idr).attr('onClick','add_item_racikan('+id+','+idr+')');
        }
      });
    } else {
      $.notify({message: 'Jumlah kurang.'},{type: 'error',z_index: 100000});
    }

  }




</script>
