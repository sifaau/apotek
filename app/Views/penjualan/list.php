<?php $name_menu = 'Data Penjualan';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">

    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Penjualan</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">

            <div class="card-header">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Cari Nomor</label>
                    <input type="text" class="form-control" id="nomor" placeholder="Cari Nomor Transaksi" autocomplete="off" aria-describedby="basic-addon2">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Dari Tanggal</label>
                    <input type="text" class="form-control" id="start" placeholder="Sampai Tanggal" autocomplete="off" aria-describedby="basic-addon2">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Sampai Tanggal</label>
                    <input type="text" class="form-control" id="end" placeholder="Sampai Tanggal"  autocomplete="off" aria-describedby="basic-addon2">
                  </div>
                </div>
                <div class="col-md-33">

                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button class="btn btn-primary form-control" id="basic-addon2" onClick="get_data()"> C E K </button>
                  </div>
                </div>
              </div>

            </div>

            <div class="card-body">
              <div class="col-md-12 text-right">
                <div class="row">
                <div class="col-sm-6 col-md-6">
                  <div class="card card-stats card-round">
								<div class="card-body">
									<div class="row align-items-center">
										<div class="col-icon">
											<div class="icon-big text-center icon-info bubble-shadow-small">
												<i class="flaticon-interface-6"></i>
											</div>
										</div>
										<div class="col col-stats ml-3 ml-sm-0">
											<div class="numbers">
												<p class="card-category">Jumlah Transaksi</p>
												<h4 class="card-title" id="jml_trans">0</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
                </div>

                <div class="col-sm-6 col-md-6">
                  <div class="card card-stats card-round">
								<div class="card-body">
									<div class="row align-items-center">
										<div class="col-icon">
											<div class="icon-big text-center icon-info bubble-shadow-small">
												<i class="flaticon-coins text-success"></i>
											</div>
										</div>
										<div class="col col-stats ml-3 ml-sm-0">
											<div class="numbers">
												<p class="card-category">Nomimal</p>
												<h4 class="card-title" id="nominal_trans"></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
                </div>
              </div>

              </div>
              <div class="col-md-12">
                <div class="table-responsive" id="root">
                </div>
              </div>


            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</div>

<?php
echo view('penjualan/modal_detail_penjualan', array());
?>


<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">

function modal_detail_data(){
  $('#detaildata').modal('show');
}

$('#start').datepicker({
  format: "dd-mm-yyyy",
  autoclose: true
});

$('#end').datepicker({
  format: "dd-mm-yyyy",
  autoclose: true
});

function get_data(){
  var start = $('#start').val();
  var end = $('#end').val();
  var nomor = $('#nomor').val();
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/penjualan/report/get_data',
    data : 'start='+start+'&end='+end+'&nomor='+nomor,
    dataType : 'json',
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
      $('#nominal_trans').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
      $('#jml_trans').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(JSONObject) {
      $('#root').html(JSONObject.data);
      $('#nominal_trans').html(JSONObject.nominal);
      $('#jml_trans').html(JSONObject.jml_trans);
      $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root').html('<center>'+c.toString()+'</center>');
      $('#nominal_trans').html('error');
      $('#jml_trans').html('error');
    }
  });
}

$(document).ready(function() {
  get_data();
});

</script>
