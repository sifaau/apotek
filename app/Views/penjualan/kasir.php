<?php $name_menu = 'Kasir';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel" style="left:0;width:100%;top:-70px;padding-top:0px;margin-top:0px;">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url();?>/penjualan/report/list_">Penjualan</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?>  </a>
          </li>
        </ul>
      </div>


      <div class="row" style="padding-bottom:200px;">

        <div class="col-md-12" style="display:none;">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <table class="table table-border">
                    <tr>
                      <td>Petugas </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Tanggal </td>
                      <td><?php echo date('d M Y');?></td>
                    </tr>
                  </table>
                </div>

                <div class="col-md-6 text-right">
                  <b style="font-size:50px;">Rp. <span id="harga_kesepakatan2">0</span></b>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">

              <div class="row">
                <div class="col-md-5">
                  *) Nomor Transaksi di Generate Otomatis
                </div>
                <div class="col-md-3">
                  <div class="form-group form-inline" style="margin-top:-10px;">
                      <label for="inlineinput" class="col-md-3 col-form-label">Kasir :&nbsp;&nbsp;&nbsp;</label>
                      <div class="col-md-9 p-0">
                        <input type="text" class="form-control input-full" id="" value="<?php echo $nama_kasir;?>" readonly>
                      </div>
                    </div>

                </div>

                <div class="col-md-4">
                  <div class="form-group form-inline" style="margin-top:-10px;">
                      <label for="inlineinput" class="col-md-3 col-form-label">Tanggal :&nbsp;&nbsp;&nbsp;</label>
                      <div class="col-md-9 p-0">
                        <input type="text" class="form-control input-full" id="tgl_sekarang" value="" placeholder="Isi Tanggal">
                      </div>
                    </div>
                </div>

                </div>

            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card">
            <div class="card-header">

              <div class="row">
                <div class="col-md-11">
                <button class="btn btn-sm btn-primary col-md-3" onClick="show_pilihan_obat('kasir')"><i class="fa fa-capsules"></i> <b>OBAT</b></button>
                <!-- <button class="btn btn-sm btn-info col-md-3" onClick="show_pilihan_racikan('kasir')"><i class="fas fa-fill-drip"></i> <b>OBAT RACIKAN</b></button> -->
                <button class="btn btn-sm btn-success col-md-3" onClick="show_pilihan_barang('kasir')"><i class="fa fa-stethoscope"></i> <b>BARANG</b></button>
              </div>
                <div class="col-md-1">
                  <button class="btn btn-sm btn-info btn-round btn-icon pull-right" onClick="list_item()"><i class="fas fa-sync"></i></button>
                </div>

              </div>

            </div>
            <div class="card-body table-responsive" id="list_item">
            </div>
          </div>
        </div>

      </div>

      <div class="row" >
        <div class="col-md-12" style="position: fixed;
        height: auto;
        bottom:-50px;
        /* left: 0; */
        right: 0;
        z-index: 1000;
        width: 100%;
        /* padding-bottom:20px; */

        ">

        <div class="card" style="background: #1572e8;">
          <div class="card-body" style="padding:10px 10px 20px 10px;">

            <div class="col-md-12">

              <table class="table table-striped">
                <tr style="padding:0 5px !important;height:35px;background:transparent;">
                  <td class="text-right" style="padding:0 5px !important;height:35px;color:#fff;"><b>Diskon 2 (dalam persen %)</b></td>
                  <td style="padding:0 5px !important;height:35px;"><input type="number" class="form-control input-full" id="diskon" placeholder="0" style="height:30px !important;"></td>
                  <td style="padding:0 5px !important;height:35px;">
                    <input type="hidden" class="form-control form-control-sm text-right" id="value_diskon" placeholder="0" readonly style="height:30px !important;">
                    <input type="text" class="form-control input-full text-right" id="txt_diskon" placeholder="0.00" readonly style="height:30px !important;color:#000;">
                  </td>
                  <td class="text-right" style="padding:0 5px !important;height:35px;color:#fff;"><b>Harga Net (Rp)</b></td>
                  <td style="padding:0 5px !important;height:35px;color:#fff;" class="text-right">
                    <input type="hidden" class="form-control form-control-sm text-right" id="harga_net" placeholder="0" readonly style="height:30px !important;color:#000;">
                    <div class="col-md-9 p-0 text-right" style="font-size:20px;font-style:bold;" id="txt_harga_net">
                      <b>0</b>
                    </div>
                  </td>
                </tr>

                <tr style="padding:0 5px !important;height:35px;background:transparent;display:none;">
                  <td class="text-right" style="padding:0 5px !important;height:35px;color:#fff;"><b style="display:none;">Tuslah (Rp)</b></td>
                  <td style="padding:0 5px !important;height:40px"><input type="hidden" class="form-control input-full" id="tuslah" value="0" style="height:30px !important;"></td>
                  <td style="padding:0 5px !important;height:40px">
                    <input type="hidden" class="form-control input-full text-right" id="rp_tuslah" placeholder="0" readonly style="height:30px !important;">
                  </td>

                </tr>

                <tr style="padding:0 5px !important;height:35px;background:transparent;">
                  <td class="text-right" style="padding:0 5px !important;height:35px;color:#fff;"><b>PPN (dalam persen %)</b></td>
                  <td style="padding:0 5px !important;height:40px"><input type="number" class="form-control input-full" id="ppn" value="0" style="height:30px !important;"></td>
                  <td style="padding:0 5px !important;height:40px">
                    <input type="text" class="form-control input-full text-right" id="rp_ppn" placeholder="0" readonly style="height:30px !important;">
                  </td>
                  <td class="text-right" style="padding:0 5px !important;height:35px;color:#000;background:#daffda;"><b></b>GRAND TOTAL (Rp)</td>
                  <td style="padding:0 5px !important;height:35px;color:#000;background:#daffda;" class="text-right">
                    <input type="hidden" class="form-control input-full" id="value_harga_kesepakatan" value="0" style="height:30px !important;">
                    <div class="col-md-9 p-0 text-right" style="font-size:20px;font-style:bold;" id="harga_kesepakatan">
                      <b>0</b>
                    </div>
                  </td>
                </tr>

                <tr id="kembalian_form" style="padding:0 5px !important;height:35px;background:transparent;background:green;">
                  <td class="text-right" style="padding:0 5px !important;height:35px;color:#fff;"><b>Tunai (Rp)</b></td>
                  <td style="padding:0 5px !important;height:40px"><input type="number" class="form-control input-full" id="tunai" value="0" style="height:30px !important;"></td>
                  <td style="padding:0 5px !important;height:40px">
                    <input type="text" class="form-control input-full text-right" id="rp_tunai" placeholder="0" readonly style="height:30px !important;">
                  </td>
                  <td class="text-right" style="padding:0 5px !important;height:35px;color:#fff;"><b></b>Kembalian (Rp)</td>
                  <td style="padding:0 5px !important;height:35px;color:#fff;" class="text-right">
                    <div class="col-md-9 p-0 text-right" style="font-size:20px;font-style:bold;" id="kembalian">
                      <b>0</b>
                    </div>
                  </td>
                </tr>

                <tr style="padding:5px 5px !important;height:35px;background:transparent;">
                  <td class="text-right" colspan="3" style="padding:5px 5px !important;height:35px;color:#fff;"><i>Pastikan data diatas sudah benar, lalu klik SIMPAN.<i></td>
                  <td class="text-right" colspan="2" style="padding:5px 5px !important;height:35px;color:#fff;">
                    <button class="btn btn-md btn-danger btn-round col-md-12" id="tombol_register" onClick="register()"><b><i class="fa fa-save"></i> SIMPAN</b>
                    </button>
                  </td>

                </tr>

              </table>
          </div>
        </div>



      </div>
    </div>


  </div>
</div>
</div>

<?php
echo view('penjualan/modal_show_obat', array());
echo view('penjualan/modal_show_racikan', array());
echo view('penjualan/modal_show_barang', array());
?>

<script type="text/javascript">

$('#tgl_sekarang').datepicker({
  format: 'yyyy-mm-dd',
    todayHighlight:'TRUE',
    autoclose: true,
}).datepicker('setDate', new Date());

function add_item_kasir(id,reff,satuan,expired=null,no_batch=null){
  var param = "'"+id+"','"+reff+"'";
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/penjualan/kasir/add_item',
    data : 'id='+id+'&reff='+reff+'&expired='+expired+'&no_batch='+no_batch+'&satuan='+satuan,
    dataType: 'json',
    beforeSend:function(){
      $('#btn_add_item'+id).html('<i class="fa fa-spin fa-spinner"></i>');
      $('#btn_add_item'+id).removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response == true){
        list_item();
        $.notify({message: JSONObject.message},{type: 'success',z_index: 100000});
      } else {
        $.notify({message: JSONObject.message},{type: 'error',z_index: 100000});
      }
      $('#btn_add_item'+id).html('<i class="fa fa-plus"></i>');
      $('#btn_add_item'+id).attr('onClick','add_item('+param+')');

    },
    error:function(a,b,c){
      $('#btn_add_item'+id).html('<i class="fa fa-plus"></i>');
      $('#btn_add_item'+id).attr('onClick','add_item('+param+')');
    }
  });
}

// function add_racikan(id){
//   $.ajax({
//     type:'POST',
//     url : '<?php echo base_url();?>/penjualan/kasir/add_racikan',
//     data : 'id='+id,
//     dataType: 'json',
//     beforeSend:function(){
//       $('#btn_add_racikan'+id).html('<i class="fa fa-spin fa-spinner"></i>');
//       $('#btn_add_racikan'+id).removeAttr('onClick');
//     },
//     success: function(JSONObject) {
//       if (JSONObject.response == true){
//         list_item();
//         $('#pilihan_data_racikan').modal('hide');
//         $('#root_table_pilih_racikan').html('');
//         $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
//       } else {
//         $.notify({message: JSONObject.message},{type: 'error',z_index: 10000});
//       }
//
//       $('#btn_add_racikan'+id).html('<i class="fa fa-plus"></i>');
//       $('#btn_add_racikan'+id).attr('onClick','add_racikan('+id+')');
//
//     },
//     error:function(a,b,c){
//       $('#btn_add_racikan'+id).html('<i class="fa fa-plus"></i>');
//       $('#btn_add_racikan'+id).attr('onClick','add_racikan('+id+')');
//     }
//   });
// }

function list_item(){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/penjualan/kasir/get_new_item_penjualan',
    beforeSend:function(){
      $('#list_item').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
    },
    success: function(data) {
      $('#list_item').html(data);
      hitung_harga_kesepakatan();
    },
    error:function(a,b,c){
      $('#list_item').html('<center>'+c.toString()+'</center>');
    }
  });
}

function delete_item(id){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/penjualan/kasir/delete_item',
    data : 'id='+id,
    beforeSend:function(){
      $('#delete_item'+id).html('<i class="fa fa-spin fa-spinner"></i>');
    },
    success: function() {
      list_item();
    },
    error:function(a,b,c){
      list_item();
    }
  });
}

function hitung_sub_total_harga(id_penjualan_detail){
  var harga_jual = $('#harga_jual'+id_penjualan_detail).val();
  var jumlah = $('#jumlah'+id_penjualan_detail).val();
  var persen_diskon = $('#persen_diskon'+id_penjualan_detail).val();
  var tuslah = $('#tuslah'+id_penjualan_detail).val();
  var subtotalharga = parseFloat(harga_jual) * parseFloat(jumlah);
  if (persen_diskon == '' || persen_diskon == null){
    persen_diskon = 0;
    nilai_diskon = 0;
  } else {
    nilai_diskon = parseFloat(subtotalharga) * ( parseFloat(persen_diskon) / 100 );
  }
  if (tuslah == '' || tuslah == null){
    tuslah = 0;
  } else {
    tuslah = parseFloat(tuslah);
  }
  var total = ( subtotalharga - nilai_diskon ) + tuslah;
  $('#subtotalharga'+id_penjualan_detail).html(currency_format(subtotalharga,2,',','.'));
  $('#nilai_diskon'+id_penjualan_detail).val(nilai_diskon);
  $('#total'+id_penjualan_detail).html(currency_format(total,2,',','.'));

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/penjualan/kasir/insert_jml_jual_item',
    data : 'id_penjualan_detail='+id_penjualan_detail+'&jumlah='+jumlah+'&harga_jual='+harga_jual+'&persen_diskon='+persen_diskon+'&tuslah='+tuslah,
    dataType: 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $('#jumlah'+id_penjualan_detail).val(JSONObject.jumlah);
      $('#totalharga').html(currency_format(JSONObject.harga_total,2,',','.'));
      $('#harga_total').val(JSONObject.harga_total);
	  $('#totaltuslah').html(currency_format(JSONObject.total_tuslah,2,',','.'));
      hitung_harga_kesepakatan();
    },
    error:function(a,b,c){

    }
  });

}

function get_harga_satuan(id_penjualan_detail,jenis){
  var optionsatuan = $('#optionsatuan'+id_penjualan_detail).val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/penjualan/kasir/get_harga_jual_satuan',
    data : 'id_satuan='+optionsatuan+'&id_penjualan_detail='+id_penjualan_detail+'&jenis='+jenis,
    dataType: 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $.notify({message: 'Profit '+JSONObject.persen_harga_jual+'%'},{type: 'success',z_index: 10000});
      $('#harga_jual'+id_penjualan_detail).val(JSONObject.harga_jual);
      $('#totalharga').val(JSONObject.harga_total);
      hitung_sub_total_harga(id_penjualan_detail);
    },
    error:function(a,b,c){
      // list_item();
    }
  });
}

function hitung_harga_kesepakatan(){
  var totalharga = $('#harga_total').val();
  console.log(totalharga);
  var diskon = $('#diskon').val();
  diskon = diskon == '' ? 0 : parseFloat(diskon);
  var ppn = $('#ppn').val();
  ppn = ppn == '' ? 0 : parseFloat(ppn);
  var tuslah = $('#tuslah').val();
  tuslah = tuslah == '' ? 0 : parseFloat(tuslah);
  var harga_net = (parseFloat(totalharga) - diskon ) + tuslah;
  var nominal_ppn = harga_net * (ppn/100);
  var harga_kesepakatan = harga_net + nominal_ppn ;
  $('#rp_ppn').val(currency_format(nominal_ppn,2,',','.'));
  $('#rp_tuslah').val(currency_format(tuslah,2,',','.'));
  $('#harga_net').val(harga_net);
  $('#txt_harga_net').html('<b>'+currency_format(harga_net,2,',','.')+'</b>');
  $('#value_harga_kesepakatan').val(harga_kesepakatan);
  $('#harga_kesepakatan').html('<b>'+currency_format(harga_kesepakatan,2,',','.')+'</b>');
  $('#harga_kesepakatan2').html(currency_format(harga_kesepakatan,2,',','.'));

  var tunai = $('#tunai').val();
  var value_harga_kesepakatan = $('#value_harga_kesepakatan').val();
  tunai = tunai == '' ? 0 : tunai;
  var kembalian = tunai - value_harga_kesepakatan;

  if (kembalian < 0 ){
    $('#kembalian_form').attr('style',"padding:0 5px !important;height:35px;background:transparent;background:#d70b17;");
  } else {
    $('#kembalian_form').attr('style',"padding:0 5px !important;height:35px;background:transparent;background:#2a6e12;");
  }

  $('#rp_tunai').val(currency_format(tunai,2,',','.'));
  $('#kembalian').html(''+currency_format(kembalian,2,',','.')+'');
}

$('#diskon').keyup(function(){
  var diskon = $('#diskon').val();
  var harga_total = $('#harga_total').val();
  diskon = diskon == '' ? 0 : diskon;
  harga_total = harga_total == '' ? 0 : harga_total;
  var nominal_diskon = parseFloat(harga_total) * ( parseFloat(diskon)/100 );
  $('#value_diskon').val(diskon);
  $('#txt_diskon').val(currency_format(nominal_diskon,2,',','.'));
  hitung_harga_kesepakatan();
});

$('#ppn').keyup(function(){
  hitung_harga_kesepakatan();
})

$('#tuslah').keyup(function(){
  hitung_harga_kesepakatan();
})

$('#tunai').keyup(function(){
  hitung_harga_kesepakatan();
})

function register(){
  var diskon = $('#value_diskon').val();
  diskon = diskon == '' ? 0 : parseFloat(diskon);
  var ppn = $('#ppn').val();
  ppn = ppn == '' ? 0 : parseFloat(ppn);
  var tuslah = $('#tuslah').val();
  tuslah = tuslah == '' ? 0 : parseFloat(tuslah);
  var tunai = $('#tunai').val();
  tunai = tunai == '' ? 0 : parseFloat(tunai);
  var date = $('#tgl_sekarang').val();
  if (tunai <=0 ){
    $.notify({message: 'Belum ada angka pembayaran tunai'},{type: 'danger',z_index: 10000});
  } else {
    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/penjualan/kasir/action_create_penjualan',
      data : 'diskon='+diskon+'&ppn='+ppn+'&tuslah='+tuslah+'&tunai='+tunai+'&date='+date,
      dataType : 'json',
      beforeSend:function(){
        $('#tombol_register').html('<i class="fa fa-spin fa-spinner"></i> Loading..');
        $('#tombol_register').removeAttr('onClick');
      },
      success: function(JSONObject) {
        if(JSONObject.response==true){
          $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
          $('#diskon').val(0);
          $('#ppn').val(0);
          $('#tunai').val(0);
          $('#tuslah').val(0);
          list_item();
          hitung_harga_kesepakatan();
          // window.location.replace("<?php echo base_url('pembelian/po');?>");
        } else {
          $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
        }
        $('#tombol_register').html('<b><i class="fa fa-save"></i> REGISTER</b>');
        $('#tombol_register').attr('onClick','register()');
      },
      error:function(a,b,c){
        $('#tombol_register').html('<b><i class="fa fa-save"></i> REGISTER</b>');
        $('#tombol_register').attr('onClick','register()');
        $.notify({message: c.toString() },{type: 'danger',z_index: 10000});
      }
    });
  }

}


$(document).ready(function() {
  list_item();

});
</script>
