<div class="modal" id="pilihan_data_barang" tabindex="30000" role="dialog" style="z-index:30000">
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content" style="width:1080px;left:-20%;">
      <div class="modal-header">
        <h5 class="modal-title">Cari Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">

          <input type="hidden" id="jenis_trans_barang" />

          <!-- <div class="col-md-12">
            <div class="form-group">
              <label>Pilih Golongan</label>
              <select class="form-control" id="id_parent_barang" placeholder="">
              </select>
            </div>
          </div> -->

          <div class="col-md-12">
            <div class="form-group">
              <label>Pilih Sub Golongan</label>
              <select class="form-control" id="golongan_barang" onChange="get_pilih_barang()"  placeholder="">

              </select>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <div class="input-group mb-3">
                <input type="text" class="form-control" id="search_barang" placeholder="Cari Nama Barang" onKeyup="if (event.keyCode == 13) get_pilih_barang();"  aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" id="basic-addon2" onClick="get_pilih_barang()" onKeyup="if (event.keyCode == 13) get_pilih_barang();"><b>C A R I</b></button>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12" id="root_table_pilih_barang">
            <table class="table">
              <body id="">
              </body>
            </table>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function show_pilihan_barang(jenis_trans){
  // get_parent_golongan(0,'id_parent_barang');
  get_golongan_barang();
  $('#jenis_trans_barang').val(jenis_trans);
  $('#root_table_pilih_barang').html('');
  $('#pilihan_data_barang').modal('show');
}

$( "#id_parent_barang" ).change(function() {
  var parent = $('#id_parent_barang').val();
  get_parent_golongan(parent,'golongan_barang');
});

function get_pilih_barang(){
  var search = $('#search_barang').val();
  var jenis_trans = $('#jenis_trans_barang').val();
  var golongan = $('#golongan_barang').val();
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/apotek/barang/get_pilih_barang',
    data : 'search='+search+'&golongan='+golongan+'&jenis_trans='+jenis_trans,
    beforeSend:function(){
      $('#root_table_pilih_barang').html('<center><i class="fa fa-spin fa-spinner"></i> Loading...</center>');
    },
    success: function(result) {
      $('#root_table_pilih_barang').html(result);
    },
    error:function(a,b,c){
      $('#root_table_pilih_barang').html('<tr><td>'+c.toString()+'</td></tr>');
    }
  });
}

function get_golongan_barang(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan_by_reff',
    data : 'reff=barang',
    beforeSend:function(){
      $('#golongan_barang').html('<option>Loading</option>');
    },
    success: function(data) {
      $('#golongan_barang').html(data);
      // $('#golongan_obat').select2();
    },
    error:function(a,b,c){
      $('#golongan_barang').html('<option>Error</option>');
    }
  });
}






</script>
