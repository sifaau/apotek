<div class="modal" id="detaildatapenjualan" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="width:1080px;left:-15%;">
      <div class="modal-header">
        <h5 class="modal-title">Detail Penjualan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row table-responsive" id="root_detail_penjualan">
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function detail_penjualan(id){
  $('#detaildatapenjualan').modal('show');
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/penjualan/report/detail_penjualan',
    data : 'id_penjualan='+id,
    beforeSend:function(){
      $('#root_detail_penjualan').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root_detail_penjualan').html(data);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root_detail_penjualan').html('<center>'+c.toString()+'</center>');
    }
  });
}

</script>
