<div class="modal" id="detail_po" tabindex="-1" role="dialog" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" >
      <div class="modal-header" >
        <h5 class="modal-title">Detail PO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12 table-responsive" id="root_po_detail">
          </div>
        </div>
      </div>
      <div class="modal-footer" >
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  function modal_detail_po(id_po){
    $.ajax({
      type:'GET',
      url : '<?php echo base_url();?>/pembelian/po/detail_po',
      data : 'id_po='+id_po,
      beforeSend:function(){
        $('#root_po_detail').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
      },
      success: function(data) {
        $('#root_po_detail').html(data);
        $('#detail_po').modal('show');
      },
      error:function(a,b,c){
        $.notify({message: c.toString()},{type: 'error'});
      }
    });
  }

</script>
