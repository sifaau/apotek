<?php $name_menu = 'Pembelian';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">

            <div class="card-header">
              <div class="row">
                <div class="col-md-8">
                  <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                    <!-- <li class="nav-item">
                      <a onClick="get_data('new')" class="nav-link active" id="pills-home-tab" data-toggle="pill" role="tab" aria-controls="pills-home" aria-selected="true"><span class="btn btn-xs btn-primary btn-round" id="count_new">0</span> Data Baru</a>
                    </li> -->
                    <!-- <li class="nav-item">
                      <a onClick="get_data('proses')" class="nav-link" id="pills-profile-tab" data-toggle="pill" role="tab" aria-controls="pills-profile" aria-selected="false"><span class="btn btn-xs btn-danger btn-round" id="count_proses">0</span> Dalam Proses</a>
                    </li> -->
                    <li class="nav-item">
                      <a onClick="get_data('prosesbayar')" class="nav-link" id="pills-profile-tab" data-toggle="pill" role="tab" aria-controls="pills-profile" aria-selected="false"><span class="btn btn-xs btn-primary btn-round" id="count_prosesbayar">0</span> Belum Lunas</a>
                    </li>
                    <li class="nav-item">
                      <a onClick="get_data('done')" class="nav-link" id="pills-contact-tab" data-toggle="pill" role="tab" aria-controls="pills-done" aria-selected="false"><span class="btn btn-xs btn-primary btn-round" id="count_done">0</span> Selesai</a>
                    </li>
                    <li class="nav-item">
                      <a onClick="get_data('all')" class="nav-link" id="pills-contact-tab" data-toggle="pill" role="tab" aria-controls="pills-all" aria-selected="false"><span class="btn btn-xs btn-primary btn-round" id="count_all">0</span> Semua Data</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <a class="btn btn-primary btn-round pull-right" href="<?php echo base_url('pembelian/opb/create');?>"><i class="fa fa-plus"></i>  INPUT PEMBELIAN</a>
                </div>
              </div>

            </div>

            <div class="card-body">

              <div class="row">

                <div class="col-md-12">
                  Pencarian Data :
                  <div class="row">
                    <div class="col-md-4">
                      <input type="text" name="t" class="form-control pull-right" id="range_date" placeholder='rentang tanggal' autocomplete="off" >
                    </div>
                    <div class="col-md-4">
                      <select class="form-control" id="supplier">
                        <option value=""> - Cari Supplier - </option>
                        <?php
                        foreach ($supplier->getResult() as $rows) {
                          echo '<option value="'.$rows->id.'"> '.$rows->nama.' </option>';
                        }
                        ;?>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <input type="text" name="s" class="form-control pull-right" id="search" value="" placeholder='cari nomor PO atau faktur' autocomplete="off" >
                        <div class="input-group-btn">
                          <input type="hidden" id="parameter_search">
                          <button class="btn" onClick="get_data_search()"><i class="fa fa-search">&nbsp;</i>CARI</button>
                        </div>
                        <div class="input-group-btn">
                          <!-- <button type="button" onclick="download_excel()" id="button_print" class="btn btn-default"><i class="fa fa-print">&nbsp;</i>CSV</button> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                </div>

                <div class="col-md-12">

                  <div class="tab-content mt-2 mb-3" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-new" role="tabpanel" aria-labelledby="pills-home-tab">
                      <div class="table-responsive" id="root">
                      </div>
                    </div>
                  </div>

                </div>

              </div>


            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<?php echo view('pembelian/modal_pembayaran_po', array()); ?>
<?php echo view('pembelian/modal_detail_po', array()); ?>
<?php echo view('pembelian/modal_terima_barang', array()); ?>
<?php echo view('pembelian/modal_history_kedatangan_barang', array()); ?>


<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#supplier').select2();
  get_data('new');
  $('#range_date').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    format: 'DD-MMM-YYYY'
  });
  $('#range_date').val('');
});


function show_count(){
  count_data('new');
  count_data('prosesbayar');
  count_data('done');
  count_data('all');
}

function count_data(param){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/po/count_po/'+param,
    beforeSend:function(){
      $('#count_'+param).html('<i class="fa fa-spin fa-spinner"></i>');
    },
    success: function(data) {
      $('#count_'+param).html(data);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#count_').html('-');
    }
  });
}

function get_data_search(){
  var param = $('#parameter_search').val();
  get_data(param);
}

function get_data(param){
  $('#parameter_search').val(param);
  var date = $('#range_date').val();
  var search = $('#search').val();
  var supplier = $('#supplier').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/po/get_data',
    data : 'param='+param+'&date='+date+'&search='+search+'&supplier='+supplier,
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root').html(data);
      // $('#basic-datatables').DataTable({});
      show_count();
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root').html('<center>'+c.toString()+'</center>');
    }
  });
}

function modal_tambah_data(){
  $('#tambahdata').modal('show');
}

function modal_detail_data(){
  $('#detaildata').modal('show');
}
</script>
