<div class="modal" id="modal_terima_barang" tabindex="-1" role="dialog" style="background:#1572E8;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Terima Barang PO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row" style="display:none;">

          <div class="col-md-12">
            <div class="form-group">
              <label>No BATCH</label>
              <input type="text" class="form-control" id="no_batch" autocomplete="off">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Tanggal Expired</label>
              <input type="text" class="form-control" id="tanggal_expired_barang" autocomplete="off">
            </div>
          </div>


          <div class="col-md-12">
            <div class="form-group">
              <label>Jumlah Diterima</label>
              <input type="hidden" class="form-control" id="id_po_detail_terima_barang">
              <input type="number" class="form-control" id="jumlah_terima_barang" autocomplete="off">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Tanggal Masuk</label>
              <input type="text" class="form-control" id="tanggal_terima_barang" autocomplete="off">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Letakan di</label>
              <select class="form-control" id="rak_terima_barang"></select>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <button class="btn btn-primary pull-right btn-round" id="basic-addon2" id="btn_terima_barang" onClick="action_terima_barang()"><b>S  I  M P  A  N</b></button>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12" id="root_terima_barang">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$('#tanggal_terima_barang').datepicker({
  format: "dd-mm-yyyy",
  autoclose: true
});

$('#tanggal_expired_barang').datepicker({
  format: "dd-mm-yyyy",
  autoclose: true
});


function get_pilihan_rak(){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/apotek/gudang/option_rak',
    beforeSend:function(){
      $('#rak_terima_barang').html('<option value="">Loading</option>');
    },
    success: function(data) {
      $('#rak_terima_barang').html(data);
    },
    error:function(a,b,c){
      $('#rak_terima_barang').html('');
    }
  });
}

function modal_terima_barang(id){
  get_pilihan_rak();
  $('#id_po_detail_terima_barang').val(id);
  get_desc_item(id);
  $('#modal_terima_barang').modal('show');
}

function get_desc_item(id){
  $.ajax({
    type:'GET',
    // url : '<?php echo base_url();?>/pembelian/po/desc_item',
    url : '<?php echo base_url();?>/pembelian/po/history_barang_data',
    data : 'id_po_detail='+id,
    beforeSend:function(){
      $('#root_terima_barang').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root_terima_barang').html(data);
    },
    error:function(a,b,c){
      $('#root_terima_barang').html('');
    }
  });
}

function action_terima_barang(){
  var id_po_detal = $('#id_po_detail_terima_barang').val();
  var jumlah = $('#jumlah_terima_barang').val();
  var tanggal = $('#tanggal_terima_barang').val();
  var rak = $('#rak_terima_barang').val();
  var tanggal_expired = $('#tanggal_expired_barang').val();
  var no_batch = $('#no_batch').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/po/terima_barang',
    data : 'id_po_detail='+id_po_detal+'&jumlah='+jumlah+'&tanggal='+tanggal+'&rak='+rak+'&tanggal_expired='+tanggal_expired+'&no_batch='+no_batch,
    dataType : 'json',
    beforeSend:function(){
      $('#btn_terima_barang').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
    },
    success: function(JSONObject) {
      if (JSONObject.response==true) {
        $('#id_po_detail_terima_barang').val(id_po_detal);
        $('#jumlah_terima_barang').val('');
        $('#tanggal_terima_barang').val('');
        $('#rak_terima_barang').val('');
        // $('#modal_terima_barang').modal('hide');
        modal_detail_po(JSONObject.id_po);
        get_desc_item(id_po_detal);

        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      }
      $('#btn_terima_barang').modal('<b>B A Y A R</b>');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'success',z_index: 10000});
      $('#btn_terima_barang').modal('<b>B A Y A R</b>');
    }
  });

}

function change_lokasi_rak_po(id,sel){
  var value = sel.value;
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/po/ubah_rak_barang_datang',
    data : 'id_po_barang_masuk='+id+'&id_rak='+value,
    dataType:'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
        // show_history_data_barang(JSONObject.id_po_detail);
        get_desc_item(JSONObject.id_po_detail);
        modal_detail_po(JSONObject.id_po);
      }
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'danger',z_index: 10000});
    }
  });
}

</script>
