<div class="modal" id="history_data_barang" tabindex="-1" role="dialog" style="background:#1572E8;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">History Kedatangan Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">

          <div class="col-md-12" id="root_history_barang">
            <table class="table">
              <body id="">
              </body>
            </table>
          </div>



        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function show_history_data_barang(id){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/pembelian/po/history_barang_data',
    data : 'id_po_detail='+id,
    beforeSend:function(){
      $('#root_history_barang').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root_history_barang').html(data);
      $('#history_data_barang').modal('show');
    },
    error:function(a,b,c){
      $('#root_history_barang').html('');
    }
  });
}



</script>
