<?php
use App\Models\General_model;
$this->general_model = new General_model();
$kode_kas_pembayaran = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'kode_kas_pembayaran']);
$kode_pembayaran_bank = $this->general_model->get_data_by_field('zacc_setting','value',['param'=>'kode_bank_pembayaran']);
if ($kode_kas_pembayaran == '' || $kode_kas_pembayaran == null){
  $option_kas = '';
} else {
  $nama_kas = $this->general_model->get_data_by_field("zacc_code_accounting",'name',['code'=>$kode_kas_pembayaran]);
  $option_kas = '<option value="'.$kode_kas_pembayaran.'">'.$kode_kas_pembayaran.' '.$nama_kas.'</option>';
}
if ($kode_pembayaran_bank == '' || $kode_pembayaran_bank == null){
  $option_bank = '';
} else {
  $nama_bank = $this->general_model->get_data_by_field("zacc_code_accounting",'name',['code'=>$kode_pembayaran_bank]);
  $option_bank = '<option value="'.$kode_pembayaran_bank.'">'.$kode_pembayaran_bank.' '.$nama_bank.'</option>';
}
?>

<div class="modal" id="pembayaran_po" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Pembayaran PO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">

          <div class="col-md-12">
            <div class="form-group">
              <label>Nominal</label>
              <input type="hidden" class="form-control" id="id_po_bayar">
              <div class="input-group mb-3">
                <input type="number" class="form-control" id="nominal_bayar" onkeyup="text_nominal_bayar()">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="text_nominal_bayar">Rp 0</span>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Tanggal</label>
              <input type="text" class="form-control" id="tanggal_bayar">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Kode Akun Pembayaran</label>
              <select class="form-control" id="code_bayar">
                <option value=""> Pilih </option>
                <?php echo $option_kas.$option_bank;?>
              </select>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <button class="btn btn-primary pull-right" id="basic-addon2" onClick="action_bayar_po()"><b>B A Y A R</b></button>
            </div>
            <br><br>
            <hr>
          </div>

        </div>

        <div class="row">
          <div class="col-md-12" id="root_riwayat_bayar">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function modal_bayar_po(id){
  $('#id_po_bayar').val(id);
  $('#root_riwayat_bayar').html('');
  $('#pembayaran_po').modal('show');
  get_riwayat_bayar(id);


}

function get_riwayat_bayar(id){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/pembelian/po/riwayat_pembayaran',
    data : 'id='+id,
    beforeSend:function(){
      $('#root_riwayat_bayar').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root_riwayat_bayar').html(data);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index: 10000});
      $('#root_riwayat_bayar').html('<center>'+c.toString()+'</center>');
    }
  });
}

$('#tanggal_bayar').datepicker({
  format: "dd-mm-yyyy",
  autoclose: true
});

function action_bayar_po(){
  var id_po = $('#id_po_bayar').val();
  var nominal = $('#nominal_bayar').val();
  var tanggal = $('#tanggal_bayar').val();
  var code_bayar = $('#code_bayar').val();

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/po/bayar_po',
    data : 'id_po='+id_po+'&nominal='+nominal+'&tanggal='+tanggal+'&code_bayar='+code_bayar,
    dataType : 'json',
    beforeSend:function(){
      // $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(JSONObject) {
      if (JSONObject.response == true){
        $('#id_po_bayar').val('');
        $('#nominal_bayar').val('');
        $('#tanggal_bayar').val('');
        // $('#pembayaran_po').modal('hide');
        get_data('prosesbayar');
        get_riwayat_bayar(id_po);
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      }
      // $('#root').html(data);
      // $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index: 10000});
      // $('#root').html('<center>'+c.toString()+'</center>');
    }
  });

}

function text_nominal_bayar(){
  var nominal = $('#nominal_bayar').val();
  $('#text_nominal_bayar').html('<b>Rp '+currency_format(nominal,2,',','.')+'</b>');
}

</script>
