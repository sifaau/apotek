<?php $name_menu = 'Retur';?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-primary btn-round pull-right" onClick="modal_tambah_data()"><i class="fa fa-plus"></i>  RETUR BARANG</button>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Data Retur</h4>
            </div>
            <div class="card-body">
              <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-new" role="tab" aria-controls="pills-home" aria-selected="true"><span class="btn btn-xs btn-danger btn-round">3</span> Data Baru</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-done" role="tab" aria-controls="pills-done" aria-selected="false"><span class="btn btn-xs btn-danger btn-round">99+</span> Selesai</a>
                </li>
              </ul>
              <div class="tab-content mt-2 mb-3" id="pills-tabContent">

                <div class="tab-pane fade show active" id="pills-new" role="tabpanel" aria-labelledby="pills-home-tab">

                  <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover" >
                      <thead>
                        <tr>
                          <th>Nomor</th>
                          <th>Supplier</th>
                          <th>Nilai Tagihan</th>
                          <th>Detail</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Nomor</th>
                          <th>Supplier</th>
                          <th>Nilai Tagihan</th>
                          <th>Detail</th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                        <tr>
                          <th>01/PO</th>
                          <td>Joko</td>
                          <td>8.000.000</td>
                          <td><a class="btn btn-info btn-xs btn-border btn-round" onClick="modal_detail_data()"><b>LIHAT</b></a></td>
                          <td>
                            <button class="btn btn-success btn-xs btn-round"><b>PROSES</b></button>
                            <button class="btn btn-danger btn-xs btn-round"><b>BATALKAN</b></button>
                          </td>
                        </tr>
                        <tr>
                          <th>01/PO</th>
                          <td>Joko</td>
                          <td>8.000.000</td>
                          <td><a class="btn btn-info btn-xs btn-border btn-round" onClick="modal_detail_data()"><b>LIHAT</b></a></td>
                          <td>
                            <button class="btn btn-success btn-xs btn-round"><b>PROSES</b></button>
                            <button class="btn btn-danger btn-xs btn-round"><b>BATALKAN</b></button>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                </div>

                <div class="tab-pane fade" id="pills-done" role="tabpanel" aria-labelledby="pills-contact-tab">

                  <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover" >
                      <thead>
                        <tr>
                          <th>Nomor</th>
                          <th>Supplier</th>
                          <th>Nilai</th>
                          <th>Detail</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Nomor</th>
                          <th>Supplier</th>
                          <th>Nilai (Rp.)</th>
                          <th>Detail</th>
                          <th></th>
                        </tr>
                      </tfoot>
                      <tbody>
                        <tr>
                          <th>01/PO</th>
                          <td>Joko</td>
                          <td>8.000.000</td>
                          <td><a class="btn btn-info btn-xs btn-border btn-round" onClick="modal_detail_data()"><b>LIHAT</b></a></td>
                          <td>

                          </td>
                        </tr>
                        <tr>
                          <th>01/PO</th>
                          <td>Joko</td>
                          <td>8.000.000</td>
                          <td><a class="btn btn-info btn-xs btn-border btn-round" onClick="modal_detail_data()"><b>LIHAT</b></a></td>
                          <td>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                </div>


              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal beli barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">

            <div class="form-group">
              <label for="largeInput">Obat</label>
              <select class="form-control form-control">
                <option>Pilih Obat</option>
              </select>
            </div>
            <div class="form-group">
              <label for="smallInput">Jumlah</label>
              <input type="number" class="form-control" id="smallInput" />
            </div>
            <div class="form-group">
              <label for="smallInput">Satuan</label>
              <select class="form-control form-control">
                <option>Pilih Satuan</option>
              </select>
            </div>
            <div class="form-group">
              <label for="smallInput">Harga Beli</label>
              <input type="number" class="form-control" id="smallInput" />
            </div>
            <div class="form-group">
              <label for="smallInput">Keterangan</label>
              <textarea type="text" class="form-control" id="smallInput" ></textarea>
            </div>
            <div class="form-group">
              <label for="smallInput"></label>
              <button type="button" class="pull-right btn btn-primary btn-lg btn-round">TAMBAHKAN</button>
            </div>
          </div>

          <div class="col-md-12">
            <table id="basic-datatables" class="display table table-striped table-hover" >
              <thead>
                <tr>
                  <th>Nama Obat</th>
                  <th>Deskripsi</th>
                  <th>Jumlah</th>
                  <th>Satuan</th>
                  <th>Harga Beli</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Mixagrip</th>
                  <td></td>
                  <td>20</td>
                  <td>box</td>
                  <td>5.000</td>
                  <td>
                    <a class="btn btn-danger btn-xs btn-border btn-round"><b>HAPUS</b></a>
                  </td>
                </tr>
                <tr>
                  <th>Mixagrip</th>
                  <td></td>
                  <td>20</td>
                  <td>box</td>
                  <td>5.000</td>
                  <td>
                    <a class="btn btn-danger btn-xs btn-border btn-round"><b>HAPUS</b></a>
                  </td>
                </tr>
              </tbody>
            </table>

            <div class="form-group">
              <label for="largeInput">Supplier</label>
              <select class="form-control form-control">
                <option>Pilih Supplier</option>
              </select>
            </div>
            <div class="form-group">
              <label for="largeInput"></label>
              <button type="button" class="form-control btn btn-success btn-lg btn-round">PROSES</button>
            </div>

          </div>


        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal" id="detaildata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detail Pembelian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="basic-datatables" class="display table table-striped table-hover" >
          <thead>
            <tr>
              <th>Nama Obat</th>
              <th>Deskripsi</th>
              <th>Jumlah</th>
              <th>Satuan</th>
              <th>Harga Beli</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Mixagrip</th>
              <td></td>
              <td>20</td>
              <td>box</td>
              <td>5.000</td>
              <td>
                <a class="btn btn-danger btn-xs btn-border btn-round"><b>HAPUS</b></a>
              </td>
            </tr>
            <tr>
              <th>Mixagrip</th>
              <td></td>
              <td>20</td>
              <td>box</td>
              <td>5.000</td>
              <td>
                <a class="btn btn-danger btn-xs btn-border btn-round"><b>HAPUS</b></a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#basic-datatables').DataTable({
  });


});

function modal_tambah_data(){
  $('#tambahdata').modal('show');
}

function modal_detail_data(){
  $('#detaildata').modal('show');
}
</script>
