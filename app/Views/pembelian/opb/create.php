<?php $name_menu = 'Input';?>
<?php
echo view('layout/datepicker', array());
if ( strtotime(date('Y-m-d')) >= strtotime('2022-04-01') ){
  $persen_ppn = 11;
} else if ( strtotime(date('Y-m-d')) >= strtotime('2025-01-01') ) {
  $persen_ppn = 12;
} else {
  $persen_ppn = 10;
}
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url();?>/pembelian/po">Pembelian</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">



        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group form-inline" style="margin-top:-10px;">
												<label for="inlineinput" class="col-md-3 col-form-label">Nomor Faktur :&nbsp;&nbsp;&nbsp;</label>
												<div class="col-md-9 p-0">
													<input type="text" class="form-control input-full" id="faktur" value="" placeholder="Isi Nomor Faktur">
												</div>
											</div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-inline" style="margin-top:-10px;">
												<label for="inlineinput" class="col-md-3 col-form-label">Tanggal Faktur :&nbsp;&nbsp;&nbsp;</label>
												<div class="col-md-9 p-0">
													<input type="text" class="form-control input-full" id="tgl_sekarang" value="<?php echo date('d-m-Y');?>" placeholder="Isi Tanggal">
												</div>
											</div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group form-inline" style="margin-top:-10px;">
												<label for="inlineinput" class="col-md-3 col-form-label">Supplier :&nbsp;&nbsp;&nbsp;</label>
												<div class="col-md-9 p-0">
                          <select class="form-control col-md-12" id="supplier" style="width:100%;">
                            <option>Pilih Supplier</option>
                          </select>
                        </div>
											</div>
                  </div>

                  </div>

              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-11">
                <button class="btn btn-sm btn-primary col-md-4" onClick="show_pilihan_obat('opb')"><i class="fas fa-capsules"></i> <b>OBAT</b></button>
                <button class="btn btn-sm btn-success col-md-4" onClick="show_pilihan_barang('opb')"><i class="fas fa-stethoscope"></i></i> <b>BARANG</b></button>
              </div>
                <div class="col-md-1">
                  <button class="btn btn-sm btn-info btn-round btn-icon pull-right" onClick="list_item()"><i class="fas fa-sync"></i></button>
                </div>
              </div>
              </div>
              <div class="card-body table-responsive" id="list_item">
              </div>

              <div class="card-footer">

                <div class="form-group form-inline" style="padding:0 5px !important;height:35px;">
                  <label for="inlineinput" class="col-md-3 col-form-label">Diskon (%)</label>
                  <div class="col-md-4 p-0" style="padding:0 5px !important;height:35px;">
                    <input style="padding:0 5px !important;height:35px;" type="text" class="form-control input-full" id="diskon" placeholder="0">
                  </div>
                  <div class="col-md-5 p-0" style="padding:0 5px !important;height:35px;">
                    <input style="padding:0 5px !important;height:35px;" type="text" class="form-control input-full text-right" id="txt_diskon" placeholder="0" readonly>
                  </div>
                </div>

                <div class="form-group form-inline" style="padding:0 5px !important;height:35px;">
                  <label for="inlineinput" class="col-md-3 col-form-label">Harga Net (Rp)</label>
                  <div class="col-md-4 p-0" style="padding:0 5px !important;height:35px;">
                  </div>
                  <div class="col-md-5 p-0" style="padding:0 5px !important;height:35px;">
                    <input style="padding:0 5px !important;height:35px;" type="text" class="form-control input-full text-right" id="harga_net" placeholder="0" readonly>
                  </div>
                </div>

                <div class="form-group form-inline" style="padding:0 5px !important;height:35px;">
                  <label for="inlineinput" class="col-md-3 col-form-label">PPN (%)</label>
                  <div class="col-md-4 p-0" style="padding:0 5px !important;height:35px;">
                    <input style="padding:0 5px !important;height:35px;" type="text" class="form-control input-full" id="ppn" value="<?php echo $persen_ppn;?>">
                  </div>
                  <div class="col-md-5 p-0" style="padding:0 5px !important;height:35px;">
                    <input style="padding:0 5px !important;height:35px;" type="text" class="form-control input-full text-right" id="rp_ppn" placeholder="0" readonly>
                  </div>
                </div>

                <div class="form-group form-inline">
                  <label for="inlineinput" class="col-md-3 col-form-label">Harga Kesepakatan (Rp)</label>
                  <div class="col-md-9 p-0 text-right" style="font-style:bold;" id="harga_kesepakatan">
                    <b>0</b>
                  </div>
                </div>

              </div>


            </div>
          </div>

          <div class="col-md-12">
            <div class="card">


            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

                <div class="col-md-12">

                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="row" >
          <div class="col-md-12" style="position: fixed;
          height: auto;
          bottom:-60px;
          /* left: 0; */
          right: 0;
          z-index: 1000;
          width: 100%;
          border-top:solid 3px #1572e8;
          /* padding-bottom:20px; */
          ">
          <div class="card">
            <div class="card-body">
              <div class="row">
          <div class="col-md-6 text-right">
                HARGA KESEPAKATAN : <b>Rp</b>  <span id="harga_kesepakatan2" style="font-size:35px;">0</span>
          </div>
          <div class="col-md-6 text-right">
            <button class="btn btn-lg btn-primary btn-round col-md-12" id="tombol_register" onClick="register()"><b><i class="fa fa-save"></i> REGISTER</b></button>

          </div>
        </div>
        </div>
        <br><br>
      </div>

        </div>




      </div>

    </div>
  </div>
</div>

<?php
echo view('pembelian/modal_show_obat_pembelian', array());
echo view('pembelian/modal_show_barang_pembelian', array());
?>

<script type="text/javascript">

$(document).ready(function() {
  list_item();
  get_supplier();
  $('#supplier').select2();
  $('#tgl_sekarang').datepicker({
    format: "dd-mm-yyyy",
    autoclose: true,
    defaultDate : new Date()
  });
});




function get_parent_golongan(id_parent,html){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan',
    data : 'id_parent='+id_parent,
    beforeSend:function(){
      $('#'+html).html('<option>Loading</option>');
    },
    success: function(data) {
      $('#'+html).html(data);
    },
    error:function(a,b,c){
      $('#'+html).html('<option>Error</option>');
    }
  });
}




function get_supplier(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/supplier/get_data_supplier',
    beforeSend:function(){
      $('#supplier').html('<option> - Loading -</option>');
    },
    success: function(data) {
      $('#supplier').html(data);
    },
    error:function(a,b,c){
      $('#supplier').html('');
    }
  });
}

function add_item(id,reff){
  var param = "'"+id+"','"+reff+"'";
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/opb/add_item',
    data : 'id='+id+'&reff='+reff,
    dataType: 'json',
    beforeSend:function(){
      $('#btn_add_item'+id).html('<i class="fa fa-spin fa-spinner"></i>');
      $('#btn_add_item'+id).removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response == true){
        list_item();
        $.notify({message: JSONObject.message+' Berhasil di tambahkan'},{type: 'success',z_index:30000});
      } else {
        $.notify({message: JSONObject.message},{type: 'error',z_index:30000});
      }

      $('#btn_add_item'+id).html('<i class="fa fa-plus"></i>');
      $('#btn_add_item'+id).attr('onClick','add_item('+param+')');

    },
    error:function(a,b,c){
      $('#btn_add_item'+id).html('<i class="fa fa-plus"></i>');
      $('#btn_add_item'+id).attr('onClick','add_item('+param+')');
    }
  });
}

function list_item(){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/pembelian/opb/get_new_item_opb',
    beforeSend:function(){
      $('#list_item').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
    },
    success: function(data) {
      $('#list_item').html(data);
      hitung_harga_kesepakatan();
      $('.expired').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
      });
    },
    error:function(a,b,c){
      $('#list_item').html('<center>'+c.toString()+'</center>');
    }
  });
}

function get_harga_satuan(id_po_detail,jenis){
  var optionsatuan = $('#optionsatuan'+id_po_detail).val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/opb/get_harga_jual_satuan',
    data : 'id_satuan='+optionsatuan+'&id_po_detail='+id_po_detail+'&jenis='+jenis,
    dataType: 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $('#harga_beli'+id_po_detail).val(JSONObject.harga_beli);
      $('#totalharga').val(JSONObject.harga_total);
      hitung_sub_total_harga(id_po_detail);
    },
    error:function(a,b,c){
      // list_item();
    }
  });
}

function hitung_nominal_diskon(id_po_detail){
  var persen_diskon = $('#persen_diskon'+id_po_detail).val();
  var harga_beli = $('#harga_beli'+id_po_detail).val();
  var jumlah = $('#jumlah'+id_po_detail).val();
  var subtotalharga = parseFloat(harga_beli) * parseFloat(jumlah);
  var nominal_diskon = subtotalharga*(persen_diskon/100);
  nominal_diskon = Math.round(nominal_diskon);
  $('#nilai_diskon'+id_po_detail).val(nominal_diskon);
  hitung_sub_total_harga(id_po_detail);
}

function hitung_sub_total_harga(id_po_detail){
  var harga_beli = $('#harga_beli'+id_po_detail).val();
  var jumlah = $('#jumlah'+id_po_detail).val();
  var persen_diskon = $('#persen_diskon'+id_po_detail).val();
  var no_batch = $('#no_batch'+id_po_detail).val();
  var expired = $('#date_expired'+id_po_detail).val();
  var rak = $('#rak'+id_po_detail).val();
  var subtotalharga = parseFloat(harga_beli) * parseFloat(jumlah);
  // var nominal_diskon = subtotalharga*(persen_diskon/100);
  // nominal_diskon = Math.round(nominal_diskon);
  // $('#nilai_diskon'+id_po_detail).val(nominal_diskon);
  var nominal_diskon_fix = $('#nilai_diskon'+id_po_detail).val();
  var totalharga = subtotalharga - nominal_diskon_fix;
  $('#subtotalharga'+id_po_detail).html(currency_format(subtotalharga,2,',','.'));
  $('#totalharga'+id_po_detail).html(currency_format(totalharga,2,',','.'));

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/opb/insert_jml_beli_item',
    data : 'id_po_detail='+id_po_detail+'&jumlah='+jumlah+'&harga_beli='+harga_beli+'&persen_diskon='+persen_diskon+'&nilai_diskon='+nominal_diskon_fix+'&no_batch='+no_batch+'&expired='+expired+'&rak='+rak,
    dataType: 'json',
    beforeSend:function(){
      $('#delete_item'+id_po_detail).html('<i class="fa fa-spin fa-spinner"></i>');
    },
    success: function(JSONObject) {
      $('#jumlah'+id_po_detail).val(JSONObject.jumlah);
      $('#totalharga').html(currency_format(JSONObject.harga_total,2,',','.'));
      $('#harga_total').val(JSONObject.harga_total);
      hitung_harga_kesepakatan();
      $('#delete_item'+id_po_detail).html('<i class="fa fa-times"></i>');
    },
    error:function(a,b,c){
      $('#delete_item'+id_po_detail).html('<i class="fa fa-times"></i>');
    }
  });

}

function delete_item(id){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/opb/delete_item',
    data : 'id='+id,
    beforeSend:function(){
      $('#delete_item'+id).html('<i class="fa fa-spin fa-spinner"></i>');
    },
    success: function() {
      list_item();
    },
    error:function(a,b,c){
      list_item();
    }
  });
}

function hitung_harga_kesepakatan(){
  var totalharga = $('#harga_total').val();
  console.log(totalharga);
  var diskon = $('#diskon').val();
  diskon = diskon == '' ? 0 : parseFloat(diskon);
  var nominal_diskon = totalharga*(diskon/100);
  nominal_diskon = Math.round(nominal_diskon);
  var ppn = $('#ppn').val();
  ppn = ppn == '' ? 0 : parseFloat(ppn);
  var harga_net = parseFloat(totalharga) - nominal_diskon;
  var nominal_ppn = harga_net * (ppn/100);
  var harga_kesepakatan = harga_net + nominal_ppn;
  $('#txt_diskon').val(currency_format(nominal_diskon,2,',','.'));
  $('#rp_ppn').val(currency_format(nominal_ppn,2,',','.'));
  $('#harga_net').val(currency_format(harga_net,2,',','.'));
  $('#harga_kesepakatan').html('<b>'+currency_format(harga_kesepakatan,2,',','.')+'</b>');
  $('#harga_kesepakatan2').html('<b>'+currency_format(harga_kesepakatan,2,',','.')+'</b>');
}

$('#diskon').keyup(function(){
  hitung_harga_kesepakatan();
});

$('#ppn').keyup(function(){
  hitung_harga_kesepakatan();
})

function register(){
  var date_trans = $('#tgl_sekarang').val();
  var diskon = $('#diskon').val();
  var faktur = $('#faktur').val();
  diskon = diskon == '' ? 0 : parseFloat(diskon);
  var ppn = $('#ppn').val();
  ppn = ppn == '' ? 0 : parseFloat(ppn);
  var supplier = $('#supplier').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/po/action_create_po',
    data : 'supplier='+supplier+'&diskon='+diskon+'&ppn='+ppn+'&date_trans='+date_trans+'&faktur='+faktur,
    dataType : 'json',
    beforeSend:function(){
      $('#tombol_register').html('<i class="fa fa-spin fa-spinner"></i> Loading..');
      $('#tombol_register').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if(JSONObject.response==true){
        $.notify({message: JSONObject.message},{type: 'success'});
        $('#diskon').val(0);
        $('#faktur').val('');
        $('#supplier').val('').trigger("change");
        $('#supplier').select2('');
        list_item();
        // window.location.replace("<?php echo base_url('pembelian/opb/create');?>");
      } else {
        $.notify({message: JSONObject.message},{type: 'danger'});
      }
      $('#tombol_register').html('<b><i class="fa fa-save"></i> REGISTER</b>');
      $('#tombol_register').attr('onClick','register()');
    },
    error:function(a,b,c){
      $('#tombol_register').html('<b><i class="fa fa-save"></i> REGISTER</b>');
      $('#tombol_register').attr('onClick','register()');
      $.notify({message: c.toString() },{type: 'danger'});
    }
  });
}


</script>
