<?php $name_menu = 'Stok Opname';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">

    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Laporan</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">

        <div class="row">

          <div class="col-md-12">
            <div class="card">

              <div class="card-header">

                <div class="row">

                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Cari Barang</label>
                      <select class="form-control" id="item" onChange="list_data()">
                        <option value="">- Cari Barang -</option>
                        <?php
                        foreach ($list_item->getResult() as $rowi) {
                          echo '<option value="'.$rowi->nama.'">'.$rowi->nama.'</option>';
                        }
                        ?>
                      </select>
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                      <label>&nbsp;</label>
                      <button class="btn btn-primary form-control" id="basic-addon2" onClick="list_data()"> C E K </button>
                    </div>
                  </div>

                </div>

              </div>

              <div class="card-body tabke-responsive" id="root">

              </div>
            </div>
          </div>

      </div>

    </div>
  </div>
</div>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">

function list_data(){
  var item = $('#item').val();
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/report/stock/get_data_opname/',
    data : 'item='+item,
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading .. </center>');
    },
    success: function(data) {
      $('#root').html(data);
      $('#table_stock_opname').DataTable({});
    },
    error:function(a,b,c){
      $('#root').html('<center>'+c.toString()+'</center>');
    }
  });
}


$(document).ready(function() {
  list_data();
  $('#item').select2();
});

</script>
