<?php
$this->request = \Config\Services::request();
$name_menu = 'Kartu Stok';
$param_id_item = $this->request->getGet('id_item');

$no_batch = $this->request->getGet('no_batch');
$date_expired = $this->request->getGet('date_expired');
$date_expired = $date_expired == '0000-00-00' ? '' : $date_expired;
?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">

    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Laporan</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">

            <div class="card-header">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Cari Item</label><br>
                    <select class="form-control" id="item" onChange="get_pilihan_satuan_by_name()">
                      <option value="">- Pilih Item -</option>
                      <?php
                      foreach ($list_item->getResult() as $rowi) {
                        // $selected = ($rowi->id.'~'.$rowi->reff) == '2~obat' ? 'selected' : '';
                        echo '<option value="'.$rowi->id.'~'.$rowi->reff.'">'.$rowi->nama.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-3" style="display:none;">
                  <div class="form-group" >
                    <label>Satuan</label>
                    <select class="form-control" id="satuan" >
                    </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Dari Tanggal</label>
                    <input type="text" class="form-control" id="start" placeholder="Sampai Tanggal" autocomplete="off" aria-describedby="basic-addon2">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Sampai Tanggal</label>
                    <input type="text" class="form-control" id="end" placeholder="Sampai Tanggal"  autocomplete="off" aria-describedby="basic-addon2">
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>No Batch</label>
                    <input type="text" class="form-control" id="no_batch" placeholder="No Batch" value="<?php echo $no_batch;?>" autocomplete="off" aria-describedby="basic-addon2">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Tgl Expired</label>
                    <input type="text" class="form-control" id="expired" placeholder="Tgl Expired" value="<?php echo $date_expired;?>"  autocomplete="off" aria-describedby="basic-addon2">
                  </div>
                </div>

                <div class="col-md-6">

                  <div class="form-group">
                    <label>&nbsp;</label>
                    <button class="btn btn-primary form-control" id="basic-addon2" onClick="list_data()"> C E K </button>
                  </div>
                </div>
              </div>

            </div>

            <div class="card-body">

              <div class="col-md-12">
                <div class="table-responsive" id="root">
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</div>



<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">

$('#expired').datepicker({
  format: "yyyy-mm-dd",
  autoclose: true
});

$('#start').datepicker({
  format: "yyyy-mm-dd",
  autoclose: true
});

$('#end').datepicker({
  format: "yyyy-mm-dd",
  autoclose: true
});

function list_data(){
  var item = $('#item').val();
  var satuan = $('#satuan').val();
  var no_batch = $('#no_batch').val();
  var expired = $('#expired').val();
  var start = $('#start').val();
  var end = $('#end').val();

  if(item == '' || item == null || item == 'null'){
    $.notify({message: 'Pilih item dahulu.'},{type: 'warning',z_index: 10000});
  } else {
    $.ajax({
      type:'GET',
      url : '<?php echo base_url();?>/report/stock/get_kartu/',
      data : 'item='+item+'&satuan='+satuan+'&no_batch='+no_batch+'&expired='+expired+'&start='+start+'&end='+end,
      beforeSend:function(){
        $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading .. </center>');
      },
      success: function(data) {
        $('#root').html(data);
      },
      error:function(a,b,c){
        $('#root').html('<center>'+c.toString()+'</center>');
      }
    });
  }

}

function get_pilihan_satuan_by_name(){
  var item = $('#item').val();
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/report/stock/get_pilihan_satuan_by_name',
    data : 'item='+item,
    beforeSend:function(){
      $('#satuan').html('<option value="">Loading</option>');
    },
    success: function(data) {
      $('#satuan').html(data);
    },
    error:function(a,b,c){
      $('#satuan').html('<option>error</option>');
    }
  });
}

function get_pilihan_rak(){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/apotek/gudang/option_rak',
    beforeSend:function(){
      $('#pilihan_rak').html('<option value="">Loading</option>');
    },
    success: function(data) {
      $('#pilihan_rak').html(data);
    },
    error:function(a,b,c){
      $('#pilihan_rak').html('');
    }
  });
}

$(document).ready(function() {
  // get_pilihan_rak();

  $('#item').select2();
  $('#item').val('<?php echo $param_id_item;?>').trigger('change');
  list_data();

});



</script>
