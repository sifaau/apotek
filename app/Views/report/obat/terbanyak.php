<?php $name_menu = 'Stok Obat Dari Terbanyak Hingga yang Habis ';?>
<div class="main-panel">
  <div class="content">

    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Laporan</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">

        <table class="table table-striped" id="basic-datatables">
          <thead>
            <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Jumlah</th>
            <th>Satuan</th>
          </tr>
          </thead>
          <tbody>

            <?php
            $no = 1;
            foreach ($data->getResult() as $row) {
              echo '<tr>
              <td>'.$no.'</td>
              <td>'.$row->nama.'</td>
              <td>'.$row->stok_asli.'</td>
              <td>'.$row->s1.'</td>
              </tr>';
              $no++;
            }?>
          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
$('#basic-datatables').DataTable({});
});
</script>
