<?php $name_menu = 'Detail Stok & Lokasi';?>
<?php echo view('layout/datepicker', array()); ?>
<div class="main-panel">
  <div class="content">

    <div class="page-inner">

      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url();?>/apotek/<?php echo $reff;?>"><?php echo $reff;?></a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">

        <div class="row">

          <div class="col-md-12">
            <div class="card">

              <div class="card-header">
                <div class="row">
                  <div class="col-md-9">
                    <div class="card-title"><h1><i class="fas fa-briefcase-medical"></i> <?php echo $nama_item;?></h1></div>
                    <?php echo 'Golongan : '.$nama_parent_golongan.' / '.$nama_golongan;?>

                  </div>
                  <div class="col-md-2 text-right" style="font-size:25px;" id="info_stock_item">

                  </div>
                  <div class="col-md-1">
                    <button class="btn btn-sm btn-info btn-round btn-icon pull-right" onClick="list_data()"><i class="fas fa-sync"></i></button>
                  </div>
                </div>

              </div>

              <div class="card-body table-responsive" id="root">

              </div>
            </div>
          </div>

      </div>

    </div>
  </div>
</div>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">



function list_data(){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/report/stock_awal/get_data_stock_detail/',
    data : 'id=<?php echo $id;?>&reff=<?php echo $reff;?>',
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading .. </center>');
    },
    success: function(data){
      $('#root').html(data);
      $('#table_stock_opname').DataTable({});
      $('#golongan').select2();
      $('#expired').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
      text_stock_item_terkecil();
    },
    error:function(a,b,c){
      $('#root').html('<center>'+c.toString()+'</center>');
    }
  });
}

function text_stock_item_terkecil(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/report/stock_awal/text_stock_item_terkecil',
    data : 'id=<?php echo $id;?>&reff=<?php echo $reff;?>',
    beforeSend:function(){
      $('#info_stock_item').html('<center><i class="fa fa-spin fa-spinner"></i> Loading .. </center>');
    },
    success: function(data) {
      $('#info_stock_item').html(data);
    },
    error:function(a,b,c){
      $('#info_stock_item').html('<center>'+c.toString()+'</center>');
    }
  });
}

function save_stock_awal(reff,id){
  param = "'"+reff+"','"+id+"'";
  var jml1 = $('#jml1').val();
  var jml2 = $('#jml2').val();
  var jml3 = $('#jml3').val();
  var jml4 = $('#jml4').val();

  var sat1 = $('#sat1').val();
  var sat2 = $('#sat2').val();
  var sat3 = $('#sat3').val();
  var sat4 = $('#sat4').val();

  var no_batch = $('#no_batch').val();
  var date_expired = $('#expired').val();
  var id_gudang_rak = $('#id_gudang_rak').val();

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/report/stock_awal/save_stock_awal',
    data : 'reff='+reff+'&id='+id+'&no_batch='+no_batch+'&date_expired='+date_expired+'&id_gudang_rak='+id_gudang_rak+'&jml1='+jml1+'&jml2='+jml2+'&jml3='+jml3+'&jml4='+jml4+'&sat1='+sat1+'&sat2='+sat2+'&sat3='+sat3+'&sat4='+sat4,
    dataType:'json',
    beforeSend:function(){
      $('#btnsave'+id).removeAttr('onClick');
      $('#btnsave'+id).html('<center><i class="fa fa-spin fa-spinner"></i> LOADING</center>');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      } else {
        list_data();
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
      }
      $('#btnsave'+id).attr('onClick','save_stock_awal('+param+')');
      $('#btnsave'+id).html('<center><i class="fa fa-save"></i> SIMPAN</center>');
    },
    error:function(a,b,c){
      $('#btnsave'+id).attr('onClick','save_stock_awal('+param+')');
      $('#btnsave'+id).html('<center><i class="fa fa-save"></i> SIMPAN</center>');

    }
  });
}


$(document).ready(function() {
  list_data();
  $('#item').select2();
});

</script>
