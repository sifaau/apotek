<?php
$name_menu = 'Supplier';

?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-primary btn-round pull-right" onClick="modal_tambah_data()"><i class="fa fa-plus"></i>  TAMBAH DATA</button>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title"></h4>
            </div>
            <div class="card-body">
              <div class="table-responsive" id="root">
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<?php
echo view('apotek/supplier/modal_add', array());
echo view('apotek/supplier/modal_edit', array());
?>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">

function get_data(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/supplier/get_data',
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root').html(data);
      $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root').html('<center>'+c.toString()+'</center>');
    }
  });
}

function activate(id){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/supplier/activate',
    data:'id='+id,
    beforeSend:function(){

    },
    success: function(data) {
      get_data();
    },
    error:function(a,b,c){
    }
  });
}

$(document).ready(function() {
get_data();
});

</script>
