<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Data Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">



          <div class="col-md-12">
            <div class="form-group">
              <label for="largeInput">Nama</label>
              <input type="text" class="form-control form-control" id="nama"  placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">HP</label>
              <input type="text" class="form-control " id="hp" placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">Telp</label>
              <input type="text" class="form-control " id="telp" placeholder="">
            </div>
          </div>


          <div class="col-md-12">

            <hr>
            <h2><b>Alamat</b></h2>
            <div class="form-group">
              <label for="smallInput">Jenis alamat</label>
              <select class="form-control" id="jenis_alamat">
                <option value="ktp">Sesuai KTP</option>
                <option value="tempattinggal">Tempat Tinggal</option>
                <option value="kantor">Kantor</option>
              </select>
            </div>

            <div class="form-group">
              <label for="smallInput">Jalan</label>
              <input type="text" class="form-control"  id="jalan" placeholder="">
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Blok</label>
                  <input type="text" class="form-control " id="blok" placeholder="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Nomor</label>
                  <input type="text" class="form-control "  id="no" placeholder="">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">RT</label>
                  <input type="text" class="form-control " id="rt" placeholder="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">RW</label>
                  <input type="text" class="form-control " id="rw" placeholder="">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group ">
                  <label for="smallInput">Provinsi</label>
                  <div>
                    <select class="form-control select2" id="province" style="width:100%;">
                      <option value="">-Pilih-</option>
                      <?php
                      foreach ($province->getResult() as $row) {
                        echo '<option value="'.$row->id.'">'.$row->province.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Kota / Kabupaten</label>
                  <div class="select2-input">
                    <select class="form-control" id="city">
                      <option value="">-Pilih-</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Kecamatan</label>
                  <select class="form-control" id="subdistrict">
                    <option value="">-Pilih-</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Kelurahan</label>
                  <input type="text" class="form-control" id="kelurahan" placeholder="">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="smallInput">Kode POS</label>
              <input type="text" class="form-control" id="kode_pos" placeholder="">
            </div>

          </div>

          <div class="col-md-12">
            <hr>
            <h2><b>Data Bank</b></h2>
            <div class="form-group">
              <label for="smallInput">Nama Bank</label>
              <input type="text" class="form-control " id="bank"  placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">Nomor Rekening Bank</label>
              <input type="text" class="form-control " id="bank_norek" placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">Nama Pemilik Rekening Bank</label>
              <input type="text" class="form-control " id="bank_name" placeholder="">
            </div>

            <div class="col-md-12" id="error_form">
            </div>

            <div class="form-group">
              <label for="smallInput"></label>
              <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save" onClick="save_data()">SIMPAN</button>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$('#province').select2({
  placeholder: 'Select an option'
});

$("#province").change(function(){
  var id_province = $('#province').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/api/alamat/get_city',
    data : 'id_province='+id_province,
    beforeSend:function(){
      $('#city').html('<option> Loading </option>');
    },
    success: function(data) {
      $('#city').html(data);
    },
    error:function(a,b,c){
      $('#city').html('');
    }
  });

});

$("#city").change(function(){
  var id_city = $('#city').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/api/alamat/get_subdistrict',
    data : 'id_city='+id_city,
    beforeSend:function(){
      $('#subdistrict').html('<option> Loading </option>');
    },
    success: function(data) {
      $('#subdistrict').html(data);
    },
    error:function(a,b,c){
      $('#subdistrict').html('');
    }
  });

});


function modal_tambah_data(){
  $('#tambahdata').modal('show');
}

function save_data(){
  console.log('a');
  var nama = $('#nama').val();
  var tlp = $('#telp').val();
  var hp = $('#hp').val();
  var jenis_alamat = $('#jenis_alamat').val();
  var jalan = $('#jalan').val();
  var blok = $('#blok').val();
  var no = $('#no').val();
  var rt = $('#rt').val();
  var rw = $('#rw').val();
  var province = $('#province').val();
  var city = $('#city').val();
  var subdistrict = $('#subdistrict').val();
  var kelurahan = $('#kelurahan').val();
  var kode_pos = $('#kode_pos').val();
  var bank = $('#bank').val();
  var bank_norek = $('#bank_norek').val();
  var bank_name = $('#bank_name').val();

  var dataString = 'bank='+bank+'&bank_norek='+bank_norek+'&bank_name='+bank_name+'&nama='+nama+'&tlp='+tlp+'&hp='+hp+'&jenis_alamat='+jenis_alamat+'&jalan='+jalan+'&blok='+blok+'&no='+no+'&rt='+rt+'&rw='+rw+'&subdistrict='+subdistrict+'&province='+province+'&city='+city+'&kelurahan='+kelurahan+'&kode_pos='+kode_pos;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/supplier/action_create',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_save').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger'});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success'});
        get_data();
      }

      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    }
  });

}


</script>

<script type="text/javascript">
function detail_alamat(param){
  swal(param, {
    buttons: false,
    timer: 3000,
  });
}
</script>
