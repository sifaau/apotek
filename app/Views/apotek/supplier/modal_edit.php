<div class="modal" id="modaleditdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Data Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">

          <input type="hidden" id="id_supplier" />

          <div class="col-md-12">
            <div class="form-group">
              <label for="largeInput">Nama</label>
              <input type="text" class="form-control form-control" id="nama_edit"  placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">HP</label>
              <input type="text" class="form-control " id="hp_edit" placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">Telp</label>
              <input type="text" class="form-control " id="telp_edit" placeholder="">
            </div>
          </div>


          <div class="col-md-12">

            <hr>
            <h2><b>Alamat</b></h2>
            <div class="form-group">
              <label for="smallInput">Jenis alamat</label>
              <select class="form-control" id="jenis_alamat_edit">
                <option value="ktp">Sesuai KTP</option>
                <option value="tempattinggal">Tempat Tinggal</option>
                <option value="kantor">Kantor</option>
              </select>
            </div>

            <div class="form-group">
              <label for="smallInput">Jalan</label>
              <input type="text" class="form-control"  id="jalan_edit" placeholder="">
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Blok</label>
                  <input type="text" class="form-control " id="blok_edit" placeholder="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Nomor</label>
                  <input type="text" class="form-control "  id="no_edit" placeholder="">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">RT</label>
                  <input type="text" class="form-control " id="rt_edit" placeholder="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">RW</label>
                  <input type="text" class="form-control " id="rw_edit" placeholder="">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Provinsi</label>
                    <select class="form-control select2" id="province_edit">
                      <option value="">-Pilih-</option>
                      <?php
                      foreach ($province->getResult() as $row) {
                        echo '<option value="'.$row->id.'">'.$row->province.'</option>';
                      }
                      ?>
                    </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Kota / Kabupaten</label>
                  <select class="form-control" id="city_edit">
                    <option value="">-Pilih-</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Kecamatan</label>
                  <select class="form-control" id="subdistrict_edit">
                    <option value="">-Pilih-</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="smallInput">Kelurahan</label>
                  <input type="text" class="form-control" id="kelurahan_edit" placeholder="">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="smallInput">Kode POS</label>
              <input type="text" class="form-control" id="kode_pos_edit" placeholder="">
            </div>

          </div>

          <div class="col-md-12">
            <hr>
            <h2><b>Data Bank</b></h2>
            <div class="form-group">
              <label for="smallInput">Rekening Bank</label>
              <input type="text" class="form-control " id="bank_edit"  placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">Nomor Rekening Bank</label>
              <input type="text" class="form-control " id="bank_norek_edit" placeholder="">
            </div>
            <div class="form-group">
              <label for="smallInput">Nama Rekening Bank</label>
              <input type="text" class="form-control " id="bank_name_edit" placeholder="">
            </div>

            <div class="col-md-12" id="error_form">
            </div>

            <div class="form-group">
              <label for="smallInput"></label>
              <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save" onClick="edit_data()">SIMPAN</button>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function get_city(id_province,id_city=''){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/api/alamat/get_city',
    data : 'id_province='+id_province,
    beforeSend:function(){
      $('#city_edit').html('<option> Loading </option>');
    },
    success: function(data) {
      $('#city_edit').html(data);
      if(id_city!=''){
        $('#city_edit').val(id_city);
      }
    },
    error:function(a,b,c){
      $('#city_edit').html('');
    }
  });
}

function get_subdistrict(id_city,id_subdistrict=''){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/api/alamat/get_subdistrict',
    data : 'id_city='+id_city,
    beforeSend:function(){
      $('#subdistrict_edit').html('<option> Loading </option>');
    },
    success: function(data) {
      $('#subdistrict_edit').html(data);
      if(id_subdistrict!=''){
        $('#subdistrict_edit').val(id_subdistrict);
      }
    },
    error:function(a,b,c){
      $('#subdistrict_edit').html('');
    }
  });
}

$("#province_edit").change(function(){
  var id_province = $('#province_edit').val();
  get_city(id_province);
});

$("#city_edit").change(function(){
  var id_city = $('#city_edit').val();
  get_subdistrict(id_city);
});

$(document).on("click","#tmblmodaleditdata",function(){
  var id_supplier = $(this).data('id_supplier');
  var nama = $(this).data('nama');
	var telp = $(this).data('telp');
	var hp = $(this).data('hp');
	var bank = $(this).data('bank');
  var bank_name = $(this).data('bank_name');
	var bank_norek = $(this).data('bank_norek');
	var jenis = $(this).data('jenis');
	var alamat = $(this).data('alamat');
  var blok = $(this).data('blok');
	var no = $(this).data('no');
	var rt = $(this).data('rt');
	var rw = $(this).data('rw');
  var id_province = $(this).data('id_province');
  var id_city = $(this).data('id_city');
	var id_subdistrict = $(this).data('id_subdistrict');
	var kelurahan = $(this).data('kelurahan');
  var kd_pos = $(this).data('kd_pos');

  $('#id_supplier').val(id_supplier);
  $('#nama_edit').val(nama);
  $('#telp_edit').val(telp);
  $('#hp_edit').val(hp);
  $('#bank_edit').val(bank);
  $('#bank_norek_edit').val(bank_norek);
  $('#bank_name_edit').val(bank_name);
  $('#jenis_alamat_edit').val(jenis);
  $('#jalan_edit').val(alamat);
  $('#blok_edit').val(blok);
  $('#no_edit').val(no);
  $('#rt_edit').val(rt);
  $('#rw_edit').val(rw);
  $('#province_edit').val(id_province);
  get_city(id_province,id_city);
  get_subdistrict(id_city,id_subdistrict);
  // $('#city_edit').val(id_city);
  // $('#subdistrict_edit').val(id_subdistrict);
  $('#kelurahan_edit').val(kelurahan);
  $('#kode_pos_edit').val(kd_pos);

})

function edit_data(){
  console.log('a');
  var id_supplier = $('#id_supplier').val();
  var nama = $('#nama_edit').val();
  var tlp = $('#telp_edit').val();
  var hp = $('#hp_edit').val();
  var jenis_alamat = $('#jenis_alamat_edit').val();
  var jalan = $('#jalan_edit').val();
  var blok = $('#blok_edit').val();
  var no = $('#no_edit').val();
  var rt = $('#rt_edit').val();
  var rw = $('#rw_edit').val();
  var province = $('#province_edit').val();
  var city = $('#city_edit').val();
  var subdistrict = $('#subdistrict_edit').val();
  var kelurahan = $('#kelurahan_edit').val();
  var kode_pos = $('#kode_pos_edit').val();
  var bank = $('#bank_edit').val();
  var bank_norek = $('#bank_norek_edit').val();
  var bank_name = $('#bank_name_edit').val();

  var dataString = 'action=edit&id_supplier='+id_supplier+'&nama='+nama+'&tlp='+tlp+'&hp='+hp+'&jenis_alamat='+jenis_alamat+'&jalan='+jalan+'&blok='+blok+'&no='+no+'&rt='+rt+'&rw='+rw+'&province='+province+'&city='+city+'&subdistrict='+subdistrict+'&kelurahan='+kelurahan+'&kode_pos='+kode_pos+'&bank='+bank+'&bank_norek='+bank_norek+'&bank_name='+bank_name;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/supplier/action_create',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#tmblmodaleditdata').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#tmblmodaleditdata').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger'});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success'});
        get_data();
        $('#modaleditdata').modal('hide');
      }

      $('#tmblmodaleditdata').html('<center>SIMPAN</center>');
      $('#tmblmodaleditdata').attr('onClick','edit_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#tmblmodaleditdata').html('<center>SIMPAN</center>');
      $('#tmblmodaleditdata').attr('onClick','edit_data()');
    }
  });

}
</script>
