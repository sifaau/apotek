<?php $name_menu = 'Apoteker';?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-primary btn-round pull-right" onClick="modal_tambah_data()"><i class="fa fa-plus"></i>  TAMBAH DATA</button>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title"></h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="basic-datatables" class="display table table-striped table-hover" >
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Telp</th>
                      <th>Alamat</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Telp</th>
                      <th>Alamat</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <tr>
                      <th>#</th>
                      <td>Tiger Nixon</td>
                      <td>08123123123213</td>
                      <td>Texas</td>
                      <td>aktif</td>
                      <td><a class="btn btn-info btn-xs btn-border btn-round"><b>EDIT</b></a></td>
                    </tr>
                    <tr>
                      <th>#</th>
                      <td>Tiger Nixon</td>
                      <td>08123123123213</td>
                      <td>Texas</td>
                      <td>aktif</td>
                      <td><a class="btn btn-info btn-xs btn-border btn-round"><b>EDIT</b></a></td>
                    </tr>
                    <tr>
                      <th>#</th>
                      <td>Tiger Nixon</td>
                      <td>08123123123213</td>
                      <td>Texas</td>
                      <td>aktif</td>
                      <td><a class="btn btn-info btn-xs btn-border btn-round"><b>EDIT</b></a></td>
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <label for="largeInput">Nama</label>
          <input type="text" class="form-control form-control" id="defaultInput" placeholder="">
        </div>
        <div class="form-group">
          <label for="smallInput">Telp / HP</label>
          <input type="text" class="form-control " id="smallInput" placeholder="">
        </div>
        <div class="form-group">
          <label for="smallInput">Alamat</label>
          <textarea type="text" class="form-control" id="smallInput" ></textarea>
        </div>
        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round">SIMPAN</button>
        </div>


      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#basic-datatables').DataTable({
  });


});

function modal_tambah_data(){
  $('#tambahdata').modal('show');
}
</script>
