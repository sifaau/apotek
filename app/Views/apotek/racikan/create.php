<?php $name_menu = 'Detail Racikan '.$nama_racikan;?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Obat</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="page-body">



        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">

                <div class="row ">
                  <div class="col-md-12 text-right">
                  <button class="btn btn-md btn-primary col-md-4 pull-right" onClick="show_pilihan_obat()"><i class="fa fa-plus"></i> OBAT</button>
                </div>
                </div>

              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="card">
              <div class="card-body" id="list_item">
              </div>
            </div>
          </div>


        </div>



      </div>

    </div>
  </div>
</div>

<?php
echo view('pembelian/modal_show_obat', array());
?>

<script type="text/javascript">

function get_parent_golongan(id_parent,html){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan',
    data : 'id_parent='+id_parent,
    beforeSend:function(){
      $('#'+html).html('<option>Loading</option>');
    },
    success: function(data) {
      $('#'+html).html(data);
    },
    error:function(a,b,c){
      $('#'+html).html('<option>Error</option>');
    }
  });
}

$(document).ready(function() {
  list_item();
  get_supplier();
});


function get_supplier(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/supplier/get_data_supplier',
    beforeSend:function(){
      $('#supplier').html('<option> - Loading -</option>');
    },
    success: function(data) {
      $('#supplier').html(data);
    },
    error:function(a,b,c){
      $('#supplier').html('');
    }
  });
}

function add_item(id,reff){
  var param = "'"+id+"','"+reff+"'";
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/racikan/add_item',
    data : 'id='+id+'&reff='+reff+'&id_racikan=<?php echo $id;?>',
    dataType: 'json',
    beforeSend:function(){
      $('#btn_add_item'+id).html('<i class="fa fa-spin fa-spinner"></i>');
      $('#btn_add_item'+id).removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response == true){
        list_item();
        $.notify({message: JSONObject.message},{type: 'success'});
      } else {
        $.notify({message: JSONObject.message},{type: 'error'});
      }

      $('#btn_add_item'+id).html('<i class="fa fa-plus"></i>');
      $('#btn_add_item'+id).attr('onClick','add_item('+param+')');

    },
    error:function(a,b,c){
      $('#btn_add_item'+id).html('<i class="fa fa-plus"></i>');
      $('#btn_add_item'+id).attr('onClick','add_item('+param+')');
    }
  });
}

function list_item(){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/apotek/racikan/get_detail_item_racikan',
    data : 'id_racikan=<?php echo $id;?>',
    beforeSend:function(){
      $('#list_item').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
    },
    success: function(data) {
      $('#list_item').html(data);
      hitung_harga_kesepakatan();
    },
    error:function(a,b,c){
      $('#list_item').html('<center>'+c.toString()+'</center>');
    }
  });
}

function set_satuan(id_racikan_detail,jenis){
  var optionsatuan = $('#optionsatuan'+id_racikan_detail).val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/racikan/insert_satuan_item',
    data : 'id_satuan='+optionsatuan+'&id_racikan_detail='+id_racikan_detail,
    dataType: 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
    },
    error:function(a,b,c){
      // list_item();
    }
  });
}

function save_jml_item(id_racikan_detail){
  var jumlah = $('#jumlah'+id_racikan_detail).val();

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/racikan/insert_jml_item',
    data : 'id_racikan_detail='+id_racikan_detail+'&jumlah='+jumlah,
    dataType: 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $('#jumlah'+id_racikan_detail).val(JSONObject.jumlah);
      $.notify({message: 'Jumlah diubah '+JSONObject.jumlah},{type: 'danger',z_index: 10000});
    },
    error:function(a,b,c){

    }
  });

}

function delete_item(id){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/racikan/delete_item',
    data : 'id='+id,
    beforeSend:function(){
      $('#delete_item'+id).html('<i class="fa fa-spin fa-spinner"></i>');
    },
    success: function() {
      list_item();
    },
    error:function(a,b,c){
      list_item();
    }
  });
}

function hitung_harga_kesepakatan(){
  var totalharga = $('#harga_total').val();
  console.log(totalharga);
  var diskon = $('#diskon').val();
  diskon = diskon == '' ? 0 : parseFloat(diskon);
  var ppn = $('#ppn').val();
  ppn = ppn == '' ? 0 : parseFloat(ppn);
  var harga_net = parseFloat(totalharga) - diskon;
  var nominal_ppn = harga_net * (ppn/100);
  var harga_kesepakatan = harga_net + nominal_ppn;
  $('#rp_ppn').val(currency_format(nominal_ppn,2,',','.'));
  $('#harga_net').val(currency_format(harga_net,2,',','.'));
  $('#harga_kesepakatan').html('<b>'+currency_format(harga_kesepakatan,2,',','.')+'</b>');
}

$('#diskon').keyup(function(){
  var diskon = $('#diskon').val();
  $('#txt_diskon').val(currency_format(diskon,2,',','.'));
  hitung_harga_kesepakatan();
});

$('#ppn').keyup(function(){
  hitung_harga_kesepakatan();
})

function register(){
  var diskon = $('#diskon').val();
  diskon = diskon == '' ? 0 : parseFloat(diskon);
  var ppn = $('#ppn').val();
  ppn = ppn == '' ? 0 : parseFloat(ppn);
  var supplier = $('#supplier').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/pembelian/po/action_create_po',
    data : 'supplier='+supplier+'&diskon='+diskon+'&ppn='+ppn,
    dataType : 'json',
    beforeSend:function(){
      $('#tombol_register').html('<i class="fa fa-spin fa-spinner"></i> Loading..');
      $('#tombol_register').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if(JSONObject.response==true){
        $.notify({message: JSONObject.message},{type: 'success'});
        $('#diskon').val(0);
        $('#ppn').val(0);
        list_item();
        window.location.replace("<?php echo base_url('pembelian/po');?>");
      } else {
        $.notify({message: JSONObject.message},{type: 'danger'});
      }
      $('#tombol_register').html('<b><i class="fa fa-save"></i> REGISTER</b>');
      $('#tombol_register').attr('onClick','register()');
    },
    error:function(a,b,c){
      $('#tombol_register').html('<b><i class="fa fa-save"></i> REGISTER</b>');
      $('#tombol_register').attr('onClick','register()');
      $.notify({message: c.toString() },{type: 'danger'});
    }
  });
}


</script>
