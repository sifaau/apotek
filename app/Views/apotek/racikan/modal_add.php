<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Racikan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <label for="largeInput">Nama</label>
          <input type="text" class="form-control form-control" id="nama" placeholder="Nama Gudang atau Lokasi">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save" onClick="save_data()">SIMPAN</button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function modal_tambah_data(){
  $('#tambahdata').modal('show');
}

function save_data(){
  var nama = $('#nama').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/racikan/action_create',
    data : 'nama='+nama,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info',z_index: 10000});
      $('#btn_save').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
        // get_data();
        window.location = "<?php echo base_url();?>/apotek/racikan/create/"+JSONObject.id;
        // $('#tambahdata').modal('hide');
      }

      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index: 10000});
      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    }
  });
}

</script>
