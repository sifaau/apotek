<?php
$name_menu = 'Obat Racikan';
echo view('layout/timepicker', array());
?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#">Master</a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-primary btn-round pull-right" onClick="modal_tambah_data()"><i class="fa fa-plus"></i>  TAMBAH DATA</button>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title"></h4>
            </div>
            <div class="card-body">
              <div class="table-responsive" id="root">

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>




<?php
echo view('apotek/racikan/modal_add', array());
?>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/datatables/datatables.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
get_data();
});

function get_data(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/racikan/get_data',
    beforeSend:function(){
      $('#root').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root').html(data);
      $('#basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#root').html('<center>'+c.toString()+'</center>');
    }
  });
}

function get_perusahaan(){
  console.log('get_perusahaan');
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat_perusahaan/get_perusahaan',
    beforeSend:function(){
      $('.obat_perusahaan').html('<option>Loading</option>');
    },
    success: function(data) {
      $('.obat_perusahaan').html(data);
    },
    error:function(a,b,c){
      $('.obat_perusahaan').html('<option>Error</option>');
    }
  });
}

function get_golongan(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan_option',
    beforeSend:function(){
      $('.obat_golongan').html('<option>Loading</option>');
    },
    success: function(data) {
      $('.obat_golongan').html(data);
    },
    error:function(a,b,c){
      $('.obat_golongan').html('<option>Error</option>');
    }
  });
}


</script>
