<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <label for="largeInput">Parent <sup style="color:red;">*</sup></label>
          <select type="text" class="form-control parent" id="id_parent" placeholder="">
            <option value="0">LEVEL 1</option>
          </select>
        </div>

        <div class="form-group">
          <label for="largeInput">Nama <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="nama" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >Keterangan</label>
          <textarea class="form-control" id="keterangan"></textarea>
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" onClick="save_data()">SIMPAN</button>
        </div>


      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).ready(function() {

});

function modal_tambah_data(){
  $('#tambahdata').modal('show');
}

function modal_tambah_data(){
  get_parent_golongan('0');
  $('#tambahdata').modal('show');
}



function save_data(){
  var id_parent = $('#id_parent').val();
  var nama  = $('#nama').val();
  var keterangan = $('#keterangan').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gol_obat/action_create',
    data : 'id_parent='+id_parent+'&nama='+nama+'&keterangan='+keterangan,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_save').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index:20000});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index:20000});
        get_data();
        $('#tambahdata').modal('hide');
        $('#id_parent').val('');
        $('#nama').val('');
        $('#keterangan').val('');
      }

      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:20000});
      $('#btn_save').html('<center>SIMPAN</center>');
      $('#btn_save').attr('onClick','save_data()');
    }
  });
}

</script>
