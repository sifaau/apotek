<div class="modal" id="modaleditdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Golongan Obat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <input type="text" id="id_edit_gol" />

        <!-- <div class="form-group">
          <label for="largeInput">Parent <sup style="color:red;">*</sup></label>
          <select type="text" class="form-control parent" id="id_parent_edit_gol" placeholder="">
            <option value="0">LEVEL 1</option>
          </select>
        </div> -->

        <div class="form-group">
          <label for="largeInput">Nama <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="nama_edit_gol" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >Keterangan</label>
          <textarea class="form-control" id="keterangan_edit_gol"></textarea>
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" onClick="edit_data()">SIMPAN</button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).on("click","#tmblmodaleditdata",function(){
  var id = $(this).data('id');
  var id_parent = $(this).data('id_parent');
  var nama = $(this).data('nama');
  var keterangan = $(this).data('keterangan');
  isi_val(id,id_parent,nama,keterangan)
  // get_parent_golongan('0');

})

async function isi_val(id,id_parent,nama,keterangan){
    await get_parent_golongan('0');
    $('#id_edit_gol').val(id);
    $('#nama_edit_gol').val(nama);
    $('#keterangan_edit_gol').val(keterangan);
    $('#id_parent_edit_gol').val(id_parent);
    alert(id);
}

function edit_data(){
  var keterangan = $('#keterangan_edit_gol').val();
  var nama = $('#nama_edit_gol').val();
  var id = $('#id_edit_gol').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gol_obat/action_create',
    data : 'action=edit&id='+id+'&nama='+nama+'&keterangan='+keterangan,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info',z_index:20000});
      $('#btn_edit').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_edit').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index:20000});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index:20000});
        get_data();
        $('#modaleditdata').modal('hide');
      }
      $('#btn_edit').html('<center>SIMPAN</center>');
      $('#btn_edit').attr('onClick','edit_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:20000});
      $('#btn_edit').html('<center>SIMPAN</center>');
      $('#btn_edit').attr('onClick','edit_data()');
    }
  });
}

</script>
