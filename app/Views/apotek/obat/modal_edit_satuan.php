<div class="modal" id="modaleditsatuanobat" tabindex="-1" role="dialog" >
  <div class="modal-dialog modal-lg" role="document" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title"><b></b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="card">
								<div class="card-header">
									<div class="card-head-row">
										<div class="card-title">Setting Satuan Obat</div>
										<div class="card-tools">
											<ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
												<li class="nav-item">
													<a class="nav-link " aria-selected="false" onClick="go_open_modal_edit_data()"> <span class="badge badge-default">STEP 1</span> Form Data Obat <i class="fa fa-arrow-left"></i></a>
												</li>
												<li class="nav-item">
													<a class="nav-link active" aria-selected="true" > <span class="badge badge-danger">STEP 2</span> Setting Satuan</a>
												</li>
                        <li class="nav-item">
													<a class="nav-link "aria-selected="false" onClick="go_open_modal_stok_awal()"><i class="fa fa-arrow-right"></i> <span class="badge badge-default">STEP 3</span> Stok Awal</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
              </div>

        <div class="container">
          <input type="hidden" class="form-control form-control-sm" id="id_obat_for_satuan"/>
          <?php for($i=1;$i<5;$i++):

            if ($i === 1){
              $text = 'Terkecil';
            } else if ($i === 2) {
              $text = 'Kedua';
            } else if ($i === 3) {
              $text = 'Ketiga';
            } else {
              $text = 'Terbesar';
            }

            ?>
            <div class="row">

              <div class="col-md-12" id="satuan<?php echo $i;?>">

                <div class="form-group form-inline">
                  <label for="inlineinput" class="col-md-3 col-form-label">Satuan <?php echo $text;?></label>
                  <div class="col-md-9 p-0">
                    <div class="row">

                    <div class="input-group col-md-12">
                      <div class="input-group-append">
                         <button class="btn" id="basic-addon2" >1</button>
                      </div>
                      <select class="form-control satuan_obat" id="satuan_obat<?php echo $i;?>" onChange="edit_satuan(<?php echo $i?>)">
                        <option value=""> - pilih- </option>
                      </select>
                      <?php if ( $i>1 ):?>
                      <div class="input-group-append">
                         <button class="btn" id="basic-addon2" >berisi</button>
                      </div>
                      <input type="text" class="form-control form-control-sm" value="0" id="konversi<?php echo $i;?>"/>
                      <div class="input-group-append">
                        <button class="btn" id="txt_konversi<?php echo $i;?>"></button>
                      </div>
                      <?php endif;?>
                      <div class="input-group-append">
                        <button class="btn btn-primary" id="basic-addon2" onClick="modaltambahsatuan()"><i class="fa fa-plus"></i></button>
                      </div>
                    </div>

                  </div>

                  </div>
                </div>


                <div class="row">
                  <div class="col-md-3" >
                    <?php
                    // if ( $i===1 ):
                      ?>
                    <div class="form-group" style="background:#1572e8;border-radius:5px;<?php echo $i===1 ? 'display:block' : 'display:none;';?>">
                      <label><span style="color:#FFFFFF;">HNA</span></label>
                      <input type="text" class="form-control form-control-sm" value="0" id="hargabeli<?php echo $i;?>"/>
                    </div>
                    <?php
                  // endif;
                    ?>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Persen Harga Jual <?php echo $text;?> (%)</label>
                      <input type="text" class="form-control form-control-sm " value="0%" id="hargajualdisp<?php echo $i;?>"/>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group">
                      <label>Harga Jual <?php echo $text;?> (Rp)</label>
                      <input type="text" class="form-control form-control-sm" value="0" id="hargajual<?php echo $i;?>" />
                    </div>
                  </div>
                  <div class="col-md-3" style="display:none;">
                    <div class="form-group">
                      <label>Diskon</label>
                      <input type="text" class="form-control form-control-sm" value="0%" id="diskon<?php echo $i;?>"/>
                    </div>
                  </div>
                  <div class="col-md-3" style="display:none;">
                    <div class="form-group">
                      <label>Harga Net <?php echo $text;?></label>
                      <input type="text" class="form-control form-control-sm" value="0" id="harganet<?php echo $i;?>" readonly/>
                    </div>
                  </div>
                </div>
                <br>
                <hr>
                <br>

              </div>

            </div>
          <?php endfor;?>

          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-primary btn-lg btn-round col-md-12" id="btn_save_satuan" onClick="go_to_stok_awal()"><b>S I M P A N & LANJUT KE INPUT STOK AWAL</b></button>
            </div>

            <div class="col-md-12">
              <br><br>
              <button type="button" class="btn btn-default btn-lg btn-round col-md-12" onClick="go_open_modal_edit_data()"><b>KE EDIT DATA OBAT</b></button>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function go_open_modal_edit_data(){
  var id_obat = $('#id_obat_for_satuan').val();
  // $('#modaleditsatuanobat').modal('hide');
  open_modal_edit_data(id_obat);
}

function go_open_modal_stok_awal(){
  var id_obat = $('#id_obat_for_satuan').val();
  $('#modaleditsatuanobat').modal('hide');
  modal_stok_awal(id_obat);
}

<?php for ($i = 1; $i < 5; $i++) : ?>
$("#hargajualdisp<?php echo $i;?>").keyup(function(){
  // generate_harga_jual(<?php echo $i;?>);
  generate_harga_jual_new(<?php echo $i;?>);
});

$('#konversi<?php echo $i;?>').keyup(function(){
  // generate_harga_jual(<?php echo $i;?>);
  generate_harga_jual_new(<?php echo $i;?>);
});

$("#hargabeli<?php echo $i;?>").keyup(function(){
  // generate_harga_jual(<?php echo $i;?>);
  generate_harga_jual_new(<?php echo $i;?>);
});

$("#hargajual<?php echo $i;?>").keyup(function(){
  // generate_harga_jual(<?php echo $i;?>);
  save_harga_jual_khusus(<?php echo $i;?>);
});

// $("#diskon<?php echo $i;?>").keyup(function(){
//   generate_harga_jual(<?php echo $i;?>);
// });
<?php endfor;?>

function generate_harga_jual_new(num){
  var hargabeli1 = $('#hargabeli1').val();

  var konversi2 = $('#konversi2').val();
  var konversi3 = $('#konversi3').val();
  var konversi4 = $('#konversi4').val();

  var hargabeli4 = ((hargabeli1*konversi4)*konversi3)*konversi2;
  $('#hargabeli4').val(hargabeli4);
  var hargabeli3 = ((hargabeli1*konversi3)*konversi2);
  $('#hargabeli3').val(hargabeli3);
  var hargabeli2 = (hargabeli1*konversi2);
  $('#hargabeli2').val(hargabeli2);
  var hargabeli4 = hargabeli4 == '' ? 0 : hargabeli4;
  var hargabeli3 = hargabeli3 == '' ? 0 : hargabeli3;
  var hargabeli2 = hargabeli2 == '' ? 0 : hargabeli2;
  var hargabeli1 = hargabeli1 == '' ? 0 : hargabeli1;

  var hargajual4 = $('#hargajualdisp4').val();
  var persen4 = hargajual4.replace('%','');
  var hargajual3 = $('#hargajualdisp3').val();
  var persen3 = hargajual3.replace('%','');
  var hargajual2 = $('#hargajualdisp2').val();
  var persen2 = hargajual2.replace('%','');
  var hargajual1 = $('#hargajualdisp1').val();
  var persen1 = hargajual1.replace('%','');

  var persen4 = persen4 == '' ? 0 : persen4;
  var persen3 = persen3 == '' ? 0 : persen3;
  var persen2 = persen2 == '' ? 0 : persen2;
  var persen1 = persen1 == '' ? 0 : persen1;

if (num == '4' || num === 4){
  var persenharga4 = ((persen4/100)*hargabeli4);
  var hargajualreal4 = parseFloat(persenharga4)+parseFloat(hargabeli4);
  var harganet4 = parseFloat(hargajualreal4)
  $('#hargajual4').val(hargajualreal4);
  $('#harganet4').val(harganet4);
}

if (num == '3' || num === 3){
  var persenharga3 = ((persen3/100)*hargabeli3);
  var hargajualreal3 = parseFloat(persenharga3)+parseFloat(hargabeli3);
  var harganet3 = parseFloat(hargajualreal3)
  $('#hargajual3').val(hargajualreal3);
  $('#harganet3').val(harganet3);
}

if (num == '2' || num === 2){
  var persenharga2 = ((persen2/100)*hargabeli2);
  var hargajualreal2 = parseFloat(persenharga2)+parseFloat(hargabeli2);
  var harganet2 = parseFloat(hargajualreal2)
  $('#hargajual2').val(hargajualreal2);
  $('#harganet2').val(harganet2);
}

if (num == '1' || num === 1){
  var persenharga1 = ((persen1/100)*hargabeli1);
  var hargajualreal1 = parseFloat(persenharga1)+parseFloat(hargabeli1);
  var harganet1 = parseFloat(hargajualreal1)
  $('#hargajual1').val(hargajualreal1);
  $('#harganet1').val(harganet1);
}

  save_satuan_obat();
}

function generate_harga_jual(num){
  var konversi = $('#konversi'+num).val();
  var hargajual = $('#hargajualdisp'+num).val();
  var persen = hargajual.replace('%','');
  var hargabeli = $('#hargabeli'+num).val();
  var hargabeli = hargabeli == '' ? 0 : hargabeli;
  var persenharga = ((persen/100)*hargabeli);
  var hargajualreal = parseFloat(persenharga)+parseFloat(hargabeli);
  $('#hargajual'+num).val(hargajualreal);

  var diskon = $('#diskon'+num).val();
  var persendiskon = diskon.replace('%','');
  var nominal_diskon = ((persendiskon/100)*hargajualreal);
  var harganet = parseFloat(hargajualreal)-parseFloat(nominal_diskon);
  $('#harganet'+num).val(harganet);

}

function get_satuan_obat(){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/get_satuan',
    beforeSend:function(){
      $('.satuan_obat').html('<option>Loading</option>');
    },
    success: function(data) {
      $('.satuan_obat').html(data);
    },
    error:function(a,b,c){
      $('.satuan_obat').html('<option>Error</option>');
    }
  });
}

function clear_form_edit_satuan(){
  <?php for ($ii = 1; $ii < 5; $ii++) :?>
  $('#satuan_obat<?php echo $ii?>').val('');
  $("#hargabeli<?php echo $ii?>").val('');
  $("#hargajualdisp<?php echo $ii?>").val('');
  $("#diskon<?php echo $ii?>").val('');
  $("#harganet<?php echo $ii?>").val(0);
  $("#hargajual<?php echo $ii?>").val(0);
  $('#txt_konversi<?php echo $ii?>').html('');
  $('#konversi<?php echo $ii?>').val(0);
  <?php endfor;?>
}

function modal_edit_satuan(id){
  $('#id_obat_for_satuan').val(id);
  // get_satuan_obat();

   $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/get_satuan',
    beforeSend:function(){
      $('.satuan_obat').html('<option>Loading</option>');
    },
    success: function(data) {
      $('.satuan_obat').html(data);
      get_detail_satuan(id);
    },
    error:function(a,b,c){
      $('.satuan_obat').html('<option>Error</option>');
    }
  });



  $('#modaleditsatuanobat').modal('show');
}

function get_detail_satuan(id){
  var dataString = 'id_reff='+id;
$.ajax({
  type:'POST',
  url : '<?php echo base_url();?>/apotek/obat/get_detail_satuan',
  data : dataString,
  dataType:'json',
  beforeSend:function(){
    clear_form_edit_satuan();
    $("#hargabeli1").val('loading..');
    $("#hargajualdisp1").val('loading..');
    $("#hargajual").val('loading..');
  },
  success: function(JSONObject) {
  $("#hargabeli1").val('');
    $("#hargajualdisp1").val('');
    $("#hargajual").val('');
    for (var key in JSONObject.data) {
      if (JSONObject.data.hasOwnProperty(key)) {
        var i = ("'"+JSONObject.data[key]["ket"]+"'");
        var i = i.replace("'",'');
        var i = i.replace("'",'');
        var satuan = JSONObject.data[key]["satuan"];
        console.log(i);
        if (satuan != '' && satuan != null){
          var j = parseFloat(i)+1;
          $('#satuan_obat'+i).val(satuan);
          $('#konversi'+i).val(JSONObject.data[key]["konversi"]);
          $('#txt_konversi'+j).html(satuan);
          $("#hargabeli"+i).val(JSONObject.data[key]["harga_beli"]);
          var persen_harga_jual = JSONObject.data[key]["persen_harga_jual"];
          if ( JSONObject.data[key]["persen_harga_jual"] == '' || JSONObject.data[key]["persen_harga_jual"] == null || JSONObject.data[key]["persen_harga_jual"] == 'null' ){
             persen_harga_jual = 0;
          }
          $("#hargajualdisp"+i).val(persen_harga_jual);
          $("#diskon"+i).val(JSONObject.data[key]["persen_diskon"]);
          $('#harganet'+i).val(0);
          $('#hargajual'+i).val(JSONObject.data[key]["harga_jual"]);
          // generate_harga_jual(i);
        }
      }
    }
  },
  error:function(a,b,c){
    clear_form_edit_satuan();
    $("#hargabeli1").val('error..');
    $("#hargajualdisp1").val('error..');
    $("#hargajual").val('error..');
  }
});
}

function edit_satuan(i){
  var satuan = $('#satuan_obat'+i).val();
  var id_obat = $('#id_obat_for_satuan').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/edit_satuan_obat',
    data : 'id_reff='+id_obat+'&satuan='+satuan+'&i='+i,
    dataType:'json',
    beforeSend:function(){
    },
    success: function(JSONObject) {
      if (JSONObject.response == false){
        $('#satuan_obat'+i).val('');
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      } else {
        var j = parseFloat(i)+1;
        $('#txt_konversi'+j).html(satuan);
        $.notify({message: JSONObject.message+' success '+satuan},{type: 'info',z_index: 10000});
      }

    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'danger',z_index: 10000});
    }
  });
}

function save_satuan_obat(){
  var id_obat = $('#id_obat_for_satuan').val();
  <?php for($i=1; $i < 5; $i++) :?>
  var satuan<?php echo $i;?> = $('#satuan_obat<?php echo $i;?>').val();
  var konversi<?php echo $i;?> = $('#konversi<?php echo $i;?>').val();
  var hargabeli<?php echo $i;?> = $('#hargabeli<?php echo $i;?>').val();
  var persenhargajual<?php echo $i;?> = $('#hargajualdisp<?php echo $i;?>').val();
  var hargajual<?php echo $i;?> = $('#hargajual<?php echo $i;?>').val();
  var persenhargajual<?php echo $i;?> = persenhargajual<?php echo $i;?>.replace('%','');
  var diskon<?php echo $i;?> = $('#diskon<?php echo $i;?>').val();
  var diskon<?php echo $i;?> = diskon<?php echo $i;?>.replace('%','');
  <?php endfor;?>

  var dataString = 'id_reff='+id_obat+
  '&satuan1='+satuan1+'&harga_beli1='+hargabeli1+'&persen_harga_jual1='+persenhargajual1+'&persen_diskon1='+diskon1+'&konversi1='+konversi1+'&harga_jual1='+hargajual1+
  '&satuan2='+satuan2+'&harga_beli2='+hargabeli2+'&persen_harga_jual2='+persenhargajual2+'&persen_diskon2='+diskon2+'&konversi2='+konversi2+'&harga_jual2='+hargajual2+
  '&satuan3='+satuan3+'&harga_beli3='+hargabeli3+'&persen_harga_jual3='+persenhargajual3+'&persen_diskon3='+diskon3+'&konversi3='+konversi3+'&harga_jual3='+hargajual3+
  '&satuan4='+satuan4+'&harga_beli4='+hargabeli4+'&persen_harga_jual4='+persenhargajual4+'&persen_diskon4='+diskon4+'&konversi4='+konversi4+'&harga_jual4='+hargajual4
  ;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/action_save_satuan',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      $('#btn_save_satuan').html('<i class="fa fa-spin fa-spinner"></i> Loading');
    },
    success: function(JSONObject) {
      if (JSONObject.response == false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      } else {
        $.notify({message: 'OK'},{type: 'info',z_index: 10000});
        // clear_form_edit_satuan();
        // $('#modaleditsatuanobat').modal('hide');
        // go_open_modal_stok_awal();
        get_data();
      }
      $('#btn_save_satuan').html('<b>S I M P A N & LANJUT KE INPUT STOK AWAL</b>');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'danger',z_index: 10000});
      $('#btn_save_satuan').html('<b>S I M P A N & LANJUT KE INPUT STOK AWAL</b>');
    }
  });

}

function go_to_stok_awal(){
  var id_obat = $('#id_obat_for_satuan').val();
  <?php for($i=1; $i < 5; $i++) :?>
  var satuan<?php echo $i;?> = $('#satuan_obat<?php echo $i;?>').val();
  var konversi<?php echo $i;?> = $('#konversi<?php echo $i;?>').val();
  var hargabeli<?php echo $i;?> = $('#hargabeli<?php echo $i;?>').val();
  var persenhargajual<?php echo $i;?> = $('#hargajualdisp<?php echo $i;?>').val();
  var hargajual<?php echo $i;?> = $('#hargajual<?php echo $i;?>').val();
  var persenhargajual<?php echo $i;?> = persenhargajual<?php echo $i;?>.replace('%','');
  var diskon<?php echo $i;?> = $('#diskon<?php echo $i;?>').val();
  var diskon<?php echo $i;?> = diskon<?php echo $i;?>.replace('%','');
  <?php endfor;?>

  var dataString = 'id_reff='+id_obat+
  '&satuan1='+satuan1+'&harga_beli1='+hargabeli1+'&persen_harga_jual1='+persenhargajual1+'&persen_diskon1='+diskon1+'&konversi1='+konversi1+'&harga_jual1='+hargajual1+
  '&satuan2='+satuan2+'&harga_beli2='+hargabeli2+'&persen_harga_jual2='+persenhargajual2+'&persen_diskon2='+diskon2+'&konversi2='+konversi2+'&harga_jual2='+hargajual2+
  '&satuan3='+satuan3+'&harga_beli3='+hargabeli3+'&persen_harga_jual3='+persenhargajual3+'&persen_diskon3='+diskon3+'&konversi3='+konversi3+'&harga_jual3='+hargajual3+
  '&satuan4='+satuan4+'&harga_beli4='+hargabeli4+'&persen_harga_jual4='+persenhargajual4+'&persen_diskon4='+diskon4+'&konversi4='+konversi4+'&harga_jual4='+hargajual4
  ;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/action_save_satuan',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      $('#btn_save_satuan').html('<i class="fa fa-spin fa-spinner"></i> Loading');
    },
    success: function(JSONObject) {
      if (JSONObject.response == false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      } else {
        $.notify({message: 'OK'},{type: 'info',z_index: 10000});
        // clear_form_edit_satuan();
        // $('#modaleditsatuanobat').modal('hide');
        go_open_modal_stok_awal();
        get_data();
      }
      $('#btn_save_satuan').html('<b>S I M P A N & LANJUT KE INPUT STOK AWAL</b>');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'danger',z_index: 10000});
      $('#btn_save_satuan').html('<b>S I M P A N & LANJUT KE INPUT STOK AWAL</b>');
    }
  });

}

function save_harga_jual_khusus(i){
  var id_obat = $('#id_obat_for_satuan').val();
  var hargajual = $('#hargajual'+i).val();
  var dataString = 'id_reff='+id_obat+'&ket='+i+'&harga_jual='+hargajual;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/action_save_harga_jual_satuan',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
    },
    success: function(JSONObject) {
      if (JSONObject.response == false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      } else {
        $.notify({message: 'OK'},{type: 'info',z_index: 10000});
      }
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'danger',z_index: 10000});
    }
  });

}

</script>
