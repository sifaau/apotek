<style type="text/css">
.bg-smoth-red{background-color: #ff4f4f !important;color:#FFF;}
.bg-smoth-aqua{background-color: #4faeff !important;color:#FFF;}

.row.fullscreen {
	width: 100%;
	max-width: 100%;padding: 0px;margin:0px;
}

.buttonupload {
	min-height: 200px;
	position: relative;
	background: #FFF;
	border:dashed 2px #1572e8;
	border-radius: 2px;
}
.uploadproduk {
	height: 50px;
	display: block;
	cursor: pointer;
	background: #DDD;
	border-radius: 3px;
	font-size: 20px;
}
.buttonupload input[type="file"] {
	z-index: 2;
	opacity: 0;
	position: absolute;
	min-height: 100px;
	width: 100%;
}

#box_img_product{
	width: 100%;
	min-height: 100px;
	overflow: hidden;
	position: relative;
}

#previewgambar{
	max-width: 100%;height: auto;vertical-align: middle;border: 0;
	position: absolute;
	margin: auto;
	height: auto;
	width: 100%;
	left: -100%;
	right: -100%;
	top: -100%;
	bottom: -100%;
}
#closeimg{
	display: none;
}
#detail_product{
	color: #000;
	position: absolute;
}
</style>

<div class="modal" id="modaleditgambar" tabindex="-1" role="dialog" >
  <div class="modal-dialog modal-lg" role="document" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title" id="nama_obat_gambar">Edit Gambar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <input type="hidden" id="id_reff">
          <div class="col-md-12" id="root_show_image">
          </div>

          <div class="col-md-12">
          <div class="form-group">
            <div class="large-12 columns" id="box_img_product">
              <img id="previewgambar" src="" width="100%">
              <div id="detail_product ">
                <div class="buttonupload text-center" >
                  <input type='file' id="upload" name="gambar" required />
                  <span class="form-error" style="color:#1572e8;">
                    GANTI GAMBAR&nbsp;<span style="color:#1572e8;"> - <small><i></i></small></span><br>
                    <i class="fa fa-camera" aria-hidden="true" style="font-size:60px;"></i>
                  </span>
                </div>
                <div>

                </div>
              </div>
            </div>
            <button id="closeimg" class="btn btn-flat btn-warning btn-block" ><i class="fa fa-close" style="font-size: 15px;"></i>&nbsp;Ganti</button>
          </div>

          <div class="form-group">
            <label for="smallInput"></label>
            <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save_obat" onClick="save_gambar()">SIMPAN GAMBAR</button>
          </div>

        </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function open_modal_ganti_gambar(id){
  $('#id_reff').val(id);
  $('#modaleditgambar').modal('show');

    $.ajax({
      type:'GET',
      url : '<?php echo base_url();?>/apotek/obat/get_image/',
      data : 'id='+id,
      dataType : 'json',
      beforeSend:function(){
        $('#root_show_image').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
        $('#nama_obat_gambar').html('');
      },
      success: function(JSONObject) {
        $('#root_show_image').html(JSONObject.image);
        $('#nama_obat_gambar').html(JSONObject.nama);
      },
      error:function(a,b,c){
        $('#root_show_image').html('');
        $('#nama_obat_gambar').html(c.toString());
      }
    });

}

function save_gambar(){
  var id  = $('#id_reff').val();
  var file 			= document.getElementById('upload').files[0];
  var dataString = new FormData();
  dataString.append("upload", file);
  dataString.append("id", id);

  $.ajax({
    xhr: function() {
      var xhr = new window.XMLHttpRequest();
      xhr.upload.addEventListener("progress", function(evt) {
        if (evt.lengthComputable) {
          var percentComplete = evt.loaded / evt.total;
          percentComplete = parseInt(percentComplete * 100);
          $('#progressBar').css('width', percentComplete+'%');
          $('#progressBar').html(percentComplete+'%');
        }
      }, false);
      return xhr;
    },
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/action_edit_gambar',
    data : dataString,
    processData: false,
    contentType: false,
    dataType:'json',
    beforeSend:function(){

    },
    success:function(JSONObject){
      close_img();
      $('#root_show_image').html(JSONObject.image);
      $('#imageobat'+id).html(JSONObject.image2);
    },
    error:function(){

    }
  })
}

function previewgambar(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#previewgambar').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
		$('.buttonupload').hide();
		$('#closeimg').show();
	}
}

$("#upload").change(function(){
	previewgambar(this);
});

$("#closeimg").click(function(){
	close_img();
});

function close_img(){
	$('#previewgambar').attr('src', '');
	$('.buttonupload').show();
	$('#closeimg').hide();
	$('#upload').val("");
}
</script>
