<div class="modal" id="modalstokawal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="card">
								<div class="card-header">
									<div class="card-head-row">
										<div class="card-title">Form Stok Awal Obat</div>
										<div class="card-tools">
                      <ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link "aria-selected="false" onClick="go_open_modal_edit_data_2()"><i class="fa fa-arrow-left"></i> <span class="badge badge-default">STEP 1</span> Form Data Obat</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link "aria-selected="false" onClick="go_modal_edit_satuan()"><i class="fa fa-arrow-left"></i> <span class="badge badge-default">STEP 2</span> Setting Satuan</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link active"aria-selected="true" ><span class="badge badge-danger">STEP 3</span> Stok Awal</a>
                        </li>
                      </ul>
										</div>
									</div>
								</div>
              </div>

              <div class="row">
              <div class="col-md-6">
                <input type="hidden" id="id_stok_awal" />
                <div class="form-group">
                  <label>No Batch</label>
                  <input type="text" class="form-control" id="no_batch_stok_awal" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tgl Expired</label>
                  <input type="text" class="form-control" id="tgl_expired_stok_awal" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Stok</label>
                  <input type="text" class="form-control" id="jml_stok_awal" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Satuan</label>
                  <select class="form-control" id="satuan_stok_awal"></select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Lokasi</label>
                  <select class="form-control" id="id_gudang_rak_stok_awal"></select>
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label></label>
                  <button type="button" class="btn btn-round btn-lg btn-primary col-md-12" id="btn_save_stok_awal" onClick="save_stock_awal()"><i class="fa fa-save"></i> <b>S I M P A N</b></button>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <br>
              </div>
              <div class="col-md-12" id="root_stok_awal">
                <br>
              </div>
            </div>



      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function modal_stok_awal(id){
  $('#id_stok_awal').val(id);
  $('#tgl_expired_stok_awal').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
  });
  get_satuan_obat_by_id(id);
  get_pilihan_rak();
  list_data_stok_awal();
  $('#modalstokawal').modal('show');
}

function get_satuan_obat_by_id(id){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/get_satuan_by_id',
    data : 'id='+id,
    beforeSend:function(){
      $('#satuan_stok_awal').html('<option>Loading</option>');
    },
    success: function(data) {
      $('#satuan_stok_awal').html(data);
    },
    error:function(a,b,c){
      $('#satuan_stok_awal').html('<option>Error</option>');
    }
  });
}

function go_open_modal_edit_data_2(){
  var id = $('#id_stok_awal').val();
  $('#modalstokawal').modal('hide');
  open_modal_edit_data(id);
}

function go_modal_edit_satuan(){
  var id = $('#id_stok_awal').val();
  $('#modalstokawal').modal('hide');
  modal_edit_satuan(id);
}

function get_pilihan_rak(){
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/apotek/gudang/option_rak',
    beforeSend:function(){
      $('#id_gudang_rak_stok_awal').html('<option value="">Loading</option>');
    },
    success: function(data) {
      $('#id_gudang_rak_stok_awal').html(data);
    },
    error:function(a,b,c){
      $('#id_gudang_rak_stok_awal').html('');
    }
  });
}

function save_stock_awal(){
  var id = $('#id_stok_awal').val();
  var reff = 'obat';
  var jml = $('#jml_stok_awal').val();
  var satuan = $('#satuan_stok_awal').val();
  var no_batch = $('#no_batch_stok_awal').val();
  var date_expired = $('#tgl_expired_stok_awal').val();
  var id_gudang_rak = $('#id_gudang_rak_stok_awal').val();

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/report/stock_awal/save_stock_awal_by_one',
    data : 'reff='+reff+'&id='+id+'&no_batch='+no_batch+'&date_expired='+date_expired+'&id_gudang_rak='+id_gudang_rak+'&jml='+jml+'&satuan='+satuan,
    dataType:'json',
    beforeSend:function(){
      $('#btn_save_stok_awal').removeAttr('onClick');
      $('#btn_save_stok_awal').html('<center><i class="fa fa-spin fa-spinner"></i> LOADING</center>');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
      } else {
        $('#jml_stok_awal').val(0);
        $('#satuan_stok_awal').val('');
        $('#no_batch_stok_awal').val('');
        $('#tgl_expired_stok_awal').val('');
        get_data();
        list_data_stok_awal();
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
      }
      $('#btn_save_stok_awal').attr('onClick','save_stock_awal()');
      $('#btn_save_stok_awal').html('<center><i class="fa fa-save"></i> <b>S I M P A N</b></center>');
    },
    error:function(a,b,c){
      $('#btn_save_stok_awal').attr('onClick','save_stock_awal()');
      $('#btn_save_stok_awal').html('<center><i class="fa fa-save"></i> <b>S I M P A N</b></center>');
    }
  });
}

function list_data_stok_awal(){
  var id = $('#id_stok_awal').val();
  var reff = 'obat';
  $.ajax({
    url : '<?php echo base_url();?>/report/stock_awal/get_data_stok_by_no_batch',
    data : 'reff='+reff+'&id='+id,
    beforeSend:function(){
      $('#root_stok_awal').html('<center><i class="fa fa-spin fa-spinner"></i> Loading..</center>');
    },
    success: function(data) {
      $('#root_stok_awal').html(data);
    },
    error:function(a,b,c){
      $('#root_stok_awal').html('');
    }
  });
}

</script>
