

<div class="modal" id="modaldetailobat" tabindex="-1" role="dialog" >
  <div class="modal-dialog modal-lg" role="document" style="width:1000px;">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title" id="nama_obat_gambar">Detail Obat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container" id="root_detail_obat">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function modal_detail_obat(id){
  $('#modaldetailobat').modal('show');

    $.ajax({
      type:'GET',
      url : '<?php echo base_url();?>/apotek/obat/get_detail_obat/',
      data : 'id='+id,
      beforeSend:function(){
        $('#root_detail_obat').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
      },
      success: function(data) {
        $('#root_detail_obat').html(data);
      },
      error:function(a,b,c){
        $('#root_detail_obat').html(c.toString());
      }
    });

}

function save_gambar(){
  var id  = $('#id_reff').val();
  var file 			= document.getElementById('upload').files[0];
  var dataString = new FormData();
  dataString.append("upload", file);
  dataString.append("id", id);

  $.ajax({
    xhr: function() {
      var xhr = new window.XMLHttpRequest();
      xhr.upload.addEventListener("progress", function(evt) {
        if (evt.lengthComputable) {
          var percentComplete = evt.loaded / evt.total;
          percentComplete = parseInt(percentComplete * 100);
          $('#progressBar').css('width', percentComplete+'%');
          $('#progressBar').html(percentComplete+'%');
        }
      }, false);
      return xhr;
    },
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/action_edit_gambar',
    data : dataString,
    processData: false,
    contentType: false,
    dataType:'json',
    beforeSend:function(){

    },
    success:function(JSONObject){
      close_img();
      $('#root_show_image').html(JSONObject.image);
      $('#imageobat'+id).html(JSONObject.image2);
    },
    error:function(){

    }
  })
}

function previewgambar(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#previewgambar').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
		$('.buttonupload').hide();
		$('#closeimg').show();
	}
}

$("#upload").change(function(){
	previewgambar(this);
});

$("#closeimg").click(function(){
	close_img();
});

function close_img(){
	$('#previewgambar').attr('src', '');
	$('.buttonupload').show();
	$('#closeimg').hide();
	$('#upload').val("");
}
</script>
