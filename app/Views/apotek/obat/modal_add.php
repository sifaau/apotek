

<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


        <div class="card">
								<div class="card-header">
									<div class="card-head-row">
										<div class="card-title">Form Data Obat</div>
										<div class="card-tools">
											<ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
												<li class="nav-item">
													<a class="nav-link active"aria-selected="true"><span class="badge badge-danger">STEP 1</span> Form Data Obat</a>
												</li>
												<li class="nav-item">
													<a class="nav-link "aria-selected="false" onClick="save_data()"><i class="fa fa-arrow-right"></i> <span class="badge badge-default">STEP 2</span> Setting Satuan</a>
												</li>
                        <li class="nav-item">
													<a class="nav-link "aria-selected="false" onClick="edit_data()"><i class="fa fa-arrow-right"></i> <span class="badge badge-default">STEP 3</span> Stok Awal</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
              </div>


        <div class="form-group">
          <label for="largeInput">Nama <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control form-control" id="nama" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput">Golongan <sup style="color:red;">*</sup></label>
          <select type="text" class="form-control obat_golongan" id="golongan" placeholder="">
          </select>
        </div>

        <div class="form-group">
          <label for="largeInput" >Di Produksi Oleh <sup style="color:red;">*</sup></label>
          <div class="input-group mb-3">
            <select type="text" class="form-control parent obat_perusahaan" id="produksi" placeholder="">
            </select>
            <div class="input-group-append">
              <button class=" btn btn-primary" onClick="add_produksi_obat()"><i class="fa fa-plus"></i> TAMBAH</button>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="largeInput" >Deskripsi</label>
          <textarea class="form-control" id="deskripsi"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Indikasi</label>
          <textarea class="form-control" id="indikasi"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Kandungan</label>
          <textarea class="form-control" id="kandungan"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Zat Aktif (Prekusor dll)</label>
          <textarea class="form-control" id="zat_aktif"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Efek Samping</label>
          <textarea class="form-control" id="efek_samping"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Dosis</label>
          <textarea class="form-control" id="dosis"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Aturan Pakai </label>
          <div class="input-group mb-3">
            <select type="text" class="form-control parent" id="aturan_pakai" placeholder="">
              <option value="">- Pilih etiket-</option>
              <option value="1"> 1 </option>
              <option value="2"> 2 </option>
              <option value="3"> 3 </option>
              <option value="4"> 4 </option>
              <option value="5"> 5 </option>
            </select>
            <div class="input-group-append">
              <span class="input-group-text" id="basic-addon2">X Sehari</span>
            </div>
          </div>
        </div>

        <div class="col-md-12">

          <div class="form-group" id="displaywaktu1" style="display:none;">
            <label for="largeInput">Waktu 1</label>
            <select type="text" class="form-control" id="waktu1" placeholder="">
              <?php
              for ($i=0; $i < 25; $i++) {
                if (strlen($i) === 1){
                  $jam = '0'.$i;
                }  else {
                  $jam = $i;
                }
                echo '<option value="'.$jam.':00">'.$jam.':00</option>';
              }
              ?>
            </select>
          </div>

          <div class="form-group" id="displaywaktu2" style="display:none;">
            <label for="largeInput">Waktu 2</label>
            <select type="text" class="form-control" id="waktu2" placeholder="">
              <?php
              for ($i=0; $i < 25; $i++) {
                if (strlen($i) === 1){
                  $jam = '0'.$i;
                }  else {
                  $jam = $i;
                }
                echo '<option value="'.$jam.':00">'.$jam.':00</option>';
              }
              ?>
            </select>
          </div>

          <div class="form-group" id="displaywaktu3" style="display:none;">
            <label for="largeInput">Waktu 3</label>
            <select type="text" class="form-control" id="waktu3" placeholder="">
              <?php
              for ($i=0; $i < 25; $i++) {
                if (strlen($i) === 1){
                  $jam = '0'.$i;
                }  else {
                  $jam = $i;
                }
                echo '<option value="'.$jam.':00">'.$jam.':00</option>';
              }
              ?>
            </select>
          </div>

          <div class="form-group" id="displaywaktu4" style="display:none;">
            <label for="largeInput">Waktu 4</label>
            <select type="text" class="form-control" id="waktu4" placeholder="">
              <?php
              for ($i=0; $i < 25; $i++) {
                if (strlen($i) === 1){
                  $jam = '0'.$i;
                }  else {
                  $jam = $i;
                }
                echo '<option value="'.$jam.':00">'.$jam.':00</option>';
              }
              ?>
            </select>
          </div>

          <div class="form-group" id="displaywaktu5" style="display:none;">
            <label for="largeInput">Waktu 5</label>
            <select type="text" class="form-control" id="waktu5" placeholder="">
              <?php
              for ($i=0; $i < 25; $i++) {
                if (strlen($i) === 1){
                  $jam = '0'.$i;
                }  else {
                  $jam = $i;
                }
                echo '<option value="'.$jam.':00">'.$jam.':00</option>';
              }
              ?>
            </select>
          </div>

          <div class="form-group" id="displaysyarat" style="display:none;">
            <label for="largeInput" >Syarat Minum</label>
            <div class="input-group mb-3">
              <select type="text" class="form-control form-sm" id="syarat" placeholder="">
                <option value="sesudah">Sesudah</option>
                <option value="sebelum">Sebelum</option>
              </select>
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Makan</span>
              </div>
            </div>
          </div>

        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save_obat" onClick="save_data()"><b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i></button>
        </div>

        <div class="col-md-12" id="error_form_obat">
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

$(document).ready(function(){

})

function save_data(){
  // var file 			= document.getElementById('upload').files[0];
  var nama  = $('#nama').val();
  var golongan = $('#golongan').val();
  var perusahaan = $('#produksi').val();
  var deskripsi = $('#deskripsi').val();
  var indikasi = $('#indikasi').val();
  var kandungan = $('#kandungan').val();
  var zat_aktif = $('#zat_aktif').val();
  var efek_samping = $('#efek_samping').val();
  var dosis = $('#dosis').val();
  var aturan_pakai = $('#aturan_pakai').val();
  var waktu_1 = $('#waktu1').val();
  var waktu_2 = $('#waktu2').val();
  var waktu_3 = $('#waktu3').val();
  var waktu_4 = $('#waktu4').val();
  var waktu_5 = $('#waktu5').val();
  var syarat = $('#syarat').val();

  // var dataString = 'nama='+nama+'&golongan='+golongan+'&perusahaan='+perusahaan+'&deskripsi='+deskripsi+'&indikasi='+indikasi
  // +'&kandungan='+kandungan+'&zat_aktif='+zat_aktif+'&efek_samping='+efek_samping
  // +'&dosis='+dosis+'&aturan_pakai='+aturan_pakai+'&waktu_1='+waktu_1
  // +'&waktu_2='+waktu_2+'&waktu_3='+waktu_3+'&waktu_4='+waktu_4+'&waktu_5='+waktu_5+'&syarat='+syarat;

  var dataString = new FormData();
  // dataString.append("upload", file);
  dataString.append("nama", nama);
  dataString.append("golongan", golongan);
  dataString.append("perusahaan", perusahaan);
  dataString.append("deskripsi", deskripsi);
  dataString.append("indikasi", indikasi);
  dataString.append("kandungan", kandungan);
  dataString.append("zat_aktif", zat_aktif);
  dataString.append("efek_samping", efek_samping);
  dataString.append("dosis", dosis);
  dataString.append("aturan_pakai", aturan_pakai);
  dataString.append("waktu_1", waktu_1);
  dataString.append("waktu_2", waktu_2);
  dataString.append("waktu_3", waktu_3);
  dataString.append("waktu_4", waktu_4);
  dataString.append("waktu_5", waktu_5);
  dataString.append("syarat", syarat);

  $.ajax({
    // xhr: function() {
    //   var xhr = new window.XMLHttpRequest();
    //   xhr.upload.addEventListener("progress", function(evt) {
    //     if (evt.lengthComputable) {
    //       var percentComplete = evt.loaded / evt.total;
    //       percentComplete = parseInt(percentComplete * 100);
    //       $('#progressBar').css('width', percentComplete+'%');
    //       $('#progressBar').html(percentComplete+'%');
    //     }
    //   }, false);
    //   return xhr;
    // },
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/action_create',
    data : dataString,
    processData: false,
    contentType: false,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info',z_index: 10000});
      $('#btn_save_obat').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save_obat').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
        $('#error_form_obat').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
        get_data();
        $('#tambahdata').modal('hide');
        $('.form-control').val('');
        modal_edit_satuan(JSONObject.id_item);
      }

      $('#btn_save_obat').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
      $('#btn_save_obat').attr('onClick','save_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index: 10000});
      $('#error_form_obat').html(c.toString());
      $('#btn_save_obat').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
      $('#btn_save_obat').attr('onClick','save_data()');
    },
    abort:function(){
      $.notify({message: 'GAGAL SIMPAN'},{type: 'error',z_index: 10000});
      $('#error_form_obat').html('Gagal');
      $('#btn_save_obat').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
      $('#btn_save_obat').attr('onClick','save_data()');
    },
    timeout: 900000
  });
}

function modal_tambah_data(){
  get_golongan();
  get_perusahaan();
  console.log('get_perusahaan');
  $('#tambahdata').modal('show');
}

$("#aturan_pakai").change(function(){
  var aturan_pakai = $('#aturan_pakai').val();
  if (aturan_pakai=='1'){
    $('#displaysyarat').show();
    $('#displaywaktu1').show();
    $('#displaywaktu2').hide();
    $('#displaywaktu3').hide();
    $('#displaywaktu4').hide();
    $('#displaywaktu5').hide();
  } else if (aturan_pakai=='2'){
    $('#displaysyarat').show();
    $('#displaywaktu1').show();
    $('#displaywaktu2').show();
    $('#displaywaktu3').hide();
    $('#displaywaktu4').hide();
    $('#displaywaktu5').hide();
  } else if (aturan_pakai=='3'){
    $('#displaysyarat').show();
    $('#displaywaktu1').show();
    $('#displaywaktu2').show();
    $('#displaywaktu3').show();
    $('#displaywaktu4').hide();
    $('#displaywaktu5').hide();
  } else if (aturan_pakai=='4'){
    $('#displaysyarat').show();
    $('#displaywaktu1').show();
    $('#displaywaktu2').show();
    $('#displaywaktu3').show();
    $('#displaywaktu4').show();
    $('#displaywaktu5').hide();
  } else if (aturan_pakai=='5'){
    $('#displaysyarat').show();
    $('#displaywaktu1').show();
    $('#displaywaktu2').show();
    $('#displaywaktu3').show();
    $('#displaywaktu4').show();
    $('#displaywaktu5').show();
  } else {
    $('#displaysyarat').hide();
    $('#displaywaktu1').hide();
    $('#displaywaktu2').hide();
    $('#displaywaktu3').hide();
    $('#displaywaktu4').hide();
    $('#displaywaktu5').hide();
  }
});





</script>
