<div class="modal" id="modaleditdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="card">
								<div class="card-header">
									<div class="card-head-row">
										<div class="card-title">Form Data Obat</div>
										<div class="card-tools">
											<ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
												<li class="nav-item">
													<a class="nav-link active"aria-selected="true"><span class="badge badge-danger">STEP 1</span> Form Data Obat</a>
												</li>
                        <li class="nav-item">
													<a class="nav-link "aria-selected="false" onClick="edit_data()"><i class="fa fa-arrow-right"></i> <span class="badge badge-default">STEP 2</span> Setting Satuan</a>
												</li>
                        <li class="nav-item">
													<a class="nav-link "aria-selected="false" onClick="go_open_modal_stok_awal_2()"><i class="fa fa-arrow-right"></i> <span class="badge badge-default">STEP 3</span> Stok Awal</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
              </div>

        <div class="col-md-12">

        </div>
        <div class="col-md-12">
          <input type="hidden" id="id_edit" />

          <div class="form-group">
            <label for="largeInput">Nama <sup style="color:red;">*</sup></label>
            <input type="text" class="form-control form-control" id="nama_edit" placeholder="">
          </div>

          <div class="form-group">
            <label for="largeInput">Golongan <sup style="color:red;">*</sup></label>
            <select type="text" class="form-control obat_golongan" id="golongan_edit" placeholder="">
            </select>
          </div>

          <div class="form-group">
            <label for="largeInput" >Di Produksi Oleh <sup style="color:red;">*</sup></label>
            <div class="input-group mb-3">
              <select type="text" class="form-control parent obat_perusahaan" id="produksi_edit" placeholder="">
              </select>
              <div class="input-group-append">
                <button class=" btn btn-primary" onClick="add_produksi_obat()"><i class="fa fa-plus"></i> TAMBAH</button>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="largeInput" >Deskripsi</label>
            <textarea class="form-control" id="deskripsi_edit"></textarea>
          </div>

          <div class="form-group">
            <label for="largeInput" >Indikasi</label>
            <textarea class="form-control" id="indikasi_edit"></textarea>
          </div>

          <div class="form-group">
            <label for="largeInput" >Kandungan</label>
            <textarea class="form-control" id="kandungan_edit"></textarea>
          </div>

          <div class="form-group">
            <label for="largeInput" >Zat Aktif (Prekusor dll)</label>
            <textarea class="form-control" id="zat_aktif_edit"></textarea>
          </div>

          <div class="form-group">
            <label for="largeInput" >Efek Samping</label>
            <textarea class="form-control" id="efek_samping_edit"></textarea>
          </div>

          <div class="form-group">
            <label for="largeInput" >Dosis</label>
            <textarea class="form-control" id="dosis_edit"></textarea>
          </div>

          <div class="form-group">
            <label for="largeInput" >Aturan Pakai </label>
            <div class="input-group mb-3">
              <select type="text" class="form-control parent" id="aturan_pakai_edit" placeholder="">
                <option value="">- Pilih etiket-</option>
                <option value="1"> 1 </option>
                <option value="2"> 2 </option>
                <option value="3"> 3 </option>
                <option value="4"> 4 </option>
                <option value="5"> 5 </option>
              </select>
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">X Sehari</span>
              </div>
            </div>
          </div>

          <div class="col-md-12">

            <div class="form-group" id="displaywaktu1_edit" style="display:none;">
              <label for="largeInput">Waktu 1</label>
              <select type="text" class="form-control" id="waktu1_edit" placeholder="">
                <?php
                for ($i=0; $i < 25; $i++) {
                  if (strlen($i) === 1){
                    $jam = '0'.$i;
                  }  else {
                    $jam = $i;
                  }
                  echo '<option value="'.$jam.':00">'.$jam.':00</option>';
                }
                ?>
              </select>
            </div>

            <div class="form-group" id="displaywaktu2_edit" style="display:none;">
              <label for="largeInput">Waktu 2</label>
              <select type="text" class="form-control" id="waktu2_edit" placeholder="">
                <?php
                for ($i=0; $i < 25; $i++) {
                  if (strlen($i) === 1){
                    $jam = '0'.$i;
                  }  else {
                    $jam = $i;
                  }
                  echo '<option value="'.$jam.':00">'.$jam.':00</option>';
                }
                ?>
              </select>
            </div>

            <div class="form-group" id="displaywaktu3_edit" style="display:none;">
              <label for="largeInput">Waktu 3</label>
              <select type="text" class="form-control" id="waktu3_edit" placeholder="">
                <?php
                for ($i=0; $i < 25; $i++) {
                  if (strlen($i) === 1){
                    $jam = '0'.$i;
                  }  else {
                    $jam = $i;
                  }
                  echo '<option value="'.$jam.':00">'.$jam.':00</option>';
                }
                ?>
              </select>
            </div>

            <div class="form-group" id="displaywaktu4_edit" style="display:none;">
              <label for="largeInput">Waktu 4</label>
              <select type="text" class="form-control" id="waktu4_edit" placeholder="">
                <?php
                for ($i=0; $i < 25; $i++) {
                  if (strlen($i) === 1){
                    $jam = '0'.$i;
                  }  else {
                    $jam = $i;
                  }
                  echo '<option value="'.$jam.':00">'.$jam.':00</option>';
                }
                ?>
              </select>
            </div>

            <div class="form-group" id="displaywaktu5_edit" style="display:none;">
              <label for="largeInput">Waktu 5</label>
              <select type="text" class="form-control" id="waktu5_edit" placeholder="">
                <?php
                for ($i=0; $i < 25; $i++) {
                  if (strlen($i) === 1){
                    $jam = '0'.$i;
                  }  else {
                    $jam = $i;
                  }
                  echo '<option value="'.$jam.':00">'.$jam.':00</option>';
                }
                ?>
              </select>
            </div>

            <div class="form-group" id="displaysyarat_edit" style="display:none;">
              <label for="largeInput" >Syarat Minum</label>
              <div class="input-group mb-3">
                <select type="text" class="form-control form-sm" id="syarat_edit" placeholder="">
                  <option value="sesudah">Sesudah</option>
                  <option value="sebelum">Sebelum</option>
                </select>
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">Makan</span>
                </div>
              </div>
            </div>

            <div class="col-md-12" id="error_form_edit">
            </div>

            <div class="form-group">
              <label for="smallInput"></label>
              <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_edit" onClick="edit_data()"><b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i></button>
            </div>

          </div>

        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">

  function go_open_modal_stok_awal_2(){
    var id_obat = $('#id_edit').val();
    $('#modaleditdata').modal('hide');
    modal_stok_awal(id_obat);
  }

  function open_modal_edit_data(id){
    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/apotek/obat/open_edit_data_obat',
      data : 'id='+id,
      dataType : 'json',
      beforeSend:function(){

      },
      success: function(JSONObject) {
        $('#id_edit').val(JSONObject.id);
        $('#nama_edit').val(JSONObject.nama);
        get_golongan_edit(JSONObject.id_golongan);
        get_perusahaan_edit(JSONObject.id_perusahaan);
        $('#deskripsi_edit').val(JSONObject.deskripsi);
        $('#kandungan_edit').val(JSONObject.kandungan);
        $('#indikasi_edit').val(JSONObject.indikasi);
        $('#zat_aktif_edit').val(JSONObject.zat_aktif);
        $('#efek_samping_edit').val(JSONObject.efek_samping);
        $('#dosis_edit').val(JSONObject.dosis);
        $('#aturan_pakai_edit').val(JSONObject.aturan_pakai);
        aturan_pakai_edit();
        $('#waktu1_edit').val(JSONObject.waktu_1);
        $('#waktu2_edit').val(JSONObject.waktu_2);
        $('#waktu3_edit').val(JSONObject.waktu_3);
        $('#waktu4_edit').val(JSONObject.waktu_4);
        $('#waktu5_edit').val(JSONObject.waktu_5);
        $('#syarat_edit').val(JSONObject.syarat_pakai);
        $('#modaleditdata').modal('show');
      },
      error:function(a,b,c){
      }
    });
  }

  $(document).on("click","#tmblmodaleditdata",function(){
    var id = $(this).data('id');
    var nama = $(this).data('nama');
    var id_golongan = $(this).data('id_golongan');
    var id_perusahaan = $(this).data('id_perusahaan');
    var deskripsi = $(this).data('deskripsi');
    var kandungan = $(this).data('kandungan');
    var zat_aktif = $(this).data('zat_aktif');
    var indikasi = $(this).data('indikasi');
    var efek_samping = $(this).data('efek_samping');
    var dosis = $(this).data('dosis');
    var aturan_pakai = $(this).data('aturan_pakai');
    var waktu_1 = $(this).data('waktu_1');
    var waktu_2 = $(this).data('waktu_2');
    var waktu_3 = $(this).data('waktu_3');
    var waktu_4 = $(this).data('waktu_4');
    var waktu_5 = $(this).data('waktu_5');
    var syarat = $(this).data('syarat_pakai');

    $('#id_edit').val(id);
    $('#nama_edit').val(nama);
    get_golongan_edit(id_golongan);
    get_perusahaan_edit(id_perusahaan);
    $('#deskripsi_edit').val(deskripsi);
    $('#kandungan_edit').val(kandungan);
    $('#indikasi_edit').val(indikasi);
    $('#zat_aktif_edit').val(zat_aktif);
    $('#efek_samping_edit').val(efek_samping);
    $('#dosis_edit').val(dosis);
    $('#aturan_pakai_edit').val(aturan_pakai);
    aturan_pakai_edit();
    $('#waktu1_edit').val(waktu_1);
    $('#waktu2_edit').val(waktu_2);
    $('#waktu3_edit').val(waktu_3);
    $('#waktu4_edit').val(waktu_4);
    $('#waktu5_edit').val(waktu_5);
    $('#syarat_edit').val(syarat);


  })

  function aturan_pakai_edit(){
    var aturan_pakai = $('#aturan_pakai_edit').val();
    if (aturan_pakai=='1'){
      $('#displaysyarat_edit').show();
      $('#displaywaktu1_edit').show();
      $('#displaywaktu2_edit').hide();
      $('#displaywaktu3_edit').hide();
      $('#displaywaktu4_edit').hide();
      $('#displaywaktu5_edit').hide();
    } else if (aturan_pakai=='2'){
      $('#displaysyarat_edit').show();
      $('#displaywaktu1_edit').show();
      $('#displaywaktu2_edit').show();
      $('#displaywaktu3_edit').hide();
      $('#displaywaktu4_edit').hide();
      $('#displaywaktu5_edit').hide();
    } else if (aturan_pakai=='3'){
      $('#displaysyarat_edit').show();
      $('#displaywaktu1_edit').show();
      $('#displaywaktu2_edit').show();
      $('#displaywaktu3_edit').show();
      $('#displaywaktu4_edit').hide();
      $('#displaywaktu5_edit').hide();
    } else if (aturan_pakai=='4'){
      $('#displaysyarat_edit').show();
      $('#displaywaktu1_edit').show();
      $('#displaywaktu2_edit').show();
      $('#displaywaktu3_edit').show();
      $('#displaywaktu4_edit').show();
      $('#displaywaktu5_edit').hide();
    } else if (aturan_pakai=='5'){
      $('#displaysyarat_edit').show();
      $('#displaywaktu1_edit').show();
      $('#displaywaktu2_edit').show();
      $('#displaywaktu3_edit').show();
      $('#displaywaktu4_edit').show();
      $('#displaywaktu5_edit').show();
    } else {
      $('#displaysyarat_edit').hide();
      $('#displaywaktu1_edit').hide();
      $('#displaywaktu2_edit').hide();
      $('#displaywaktu3_edit').hide();
      $('#displaywaktu4_edit').hide();
      $('#displaywaktu5_edit').hide();
    }
  }
  $("#aturan_pakai_edit").change(function(){
    aturan_pakai_edit();
  });

  function get_perusahaan_edit(id_perusahaan){
    console.log('get_perusahaan');
    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/apotek/obat_perusahaan/get_perusahaan',
      beforeSend:function(){
        $('#produksi_edit').html('<option>Loading</option>');
      },
      success: function(data) {
        $('#produksi_edit').html(data);
        $('#produksi_edit').val(id_perusahaan);
      },
      error:function(a,b,c){
        $('#produksi_edit').html('<option>Error</option>');
      }
    });
  }

  function edit_data(){
    $('#error_form_edit').html('');
    var id  = $('#id_edit').val();
    var nama  = $('#nama_edit').val();
    var golongan = $('#golongan_edit').val();
    var perusahaan = $('#produksi_edit').val();
    var deskripsi = $('#deskripsi_edit').val();
    var indikasi = $('#indikasi_edit').val();
    var kandungan = $('#kandungan_edit').val();
    var zat_aktif = $('#zat_aktif_edit').val();
    var efek_samping = $('#efek_samping_edit').val();
    var dosis = $('#dosis_edit').val();
    var aturan_pakai = $('#aturan_pakai_edit').val();
    var waktu_1 = $('#waktu1_edit').val();
    var waktu_2 = $('#waktu2_edit').val();
    var waktu_3 = $('#waktu3_edit').val();
    var waktu_4 = $('#waktu4_edit').val();
    var waktu_5 = $('#waktu5_edit').val();
    var syarat = $('#syarat_edit').val();

    var dataString2 = 'action=edit&id='+id+'&nama='+nama+'&golongan='+golongan+'&perusahaan='+perusahaan+'&deskripsi='+deskripsi+'&indikasi='+indikasi
    +'&kandungan='+kandungan+'&zat_aktif='+zat_aktif+'&efek_samping='+efek_samping+'&dosis='+dosis+'&aturan_pakai='+aturan_pakai+'&waktu_1='+waktu_1
    +'&waktu_2='+waktu_2+'&waktu_3='+waktu_3+'&waktu_4='+waktu_4+'&waktu_5='+waktu_5+'&syarat='+syarat;

    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/apotek/obat/action_create',
      data : dataString2,
      dataType:'json',
      beforeSend:function(){
        $.notify({message: 'Loading..'},{type: 'info',z_index: 10000});
        $('#btn_edit').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
        $('#btn_edit').removeAttr('onClick');
      },
      success: function(JSONObject) {
        if (JSONObject.response==false){
          $.notify({message: JSONObject.message},{type: 'danger',z_index: 10000});
          $('#error_form_edit').html(JSONObject.message);
        } else {
          $.notify({message: JSONObject.message},{type: 'success',z_index: 10000});
          get_data();
          $('#modaleditdata').modal('hide');
          modal_edit_satuan(id);
        }

        $('#btn_edit').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
        $('#btn_edit').attr('onClick','edit_data()');
      },
      error:function(a,b,c){
        $.notify({message: c.toString()},{type: 'error',z_index: 10000});
        $('#btn_edit').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
        $('#btn_edit').attr('onClick','edit_data()');
      }
    });
  }

  function get_golongan_edit(id_golongan){
    $.ajax({
      type:'POST',
      url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan_option',
      beforeSend:function(){
        $('#golongan_edit').html('<option>Loading</option>');
      },
      success: function(data) {
        $('#golongan_edit').html(data);
        $('#golongan_edit').val(id_golongan);
      },
      error:function(a,b,c){
        $('#golongan_edit').html('<option>Error</option>');
      }
    });
  }


</script>
