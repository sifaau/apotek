<div class="modal" id="modaladdproduksi" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Perusahaan Pembuat Obat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <label for="largeInput">Nama Perusahaan<sup style="color:red;">*</sup></label>
          <input type="text" class="form-control form-control" id="nama_perusahaan" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >Alamat</label>
          <textarea class="form-control" id="alamat_perusahaan"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Kota</label>
          <input type="text" class="form-control form-control" id="kota_perusahaan" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >Telp</label>
          <input type="text" class="form-control form-control" id="telp_perusahaan" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >HP</label>
          <input type="text" class="form-control form-control" id="hp_perusahaan" placeholder="">
        </div>

        <div class="col-md-12" id="error_form_perusahaan">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save_perusahaan" onClick="save_data_perusahaan()">SIMPAN</button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function add_produksi_obat(){
  $('#modaladdproduksi').modal('show');
  $('#error_form_perusahaan').html('');
}

function save_data_perusahaan(){

  var nama  = $('#nama_perusahaan').val();
  var alamat = $('#alamat_perusahaan').val();
  var kota = $('#kota_perusahaan').val();
  var telp = $('#telp_perusahaan').val();
  var hp = $('#hp_perusahaan').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat_perusahaan/action_create',
    data : 'nama='+nama+'&alamat='+alamat+'&kota='+kota+'&telp='+telp+'&hp='+hp,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_save_perusahaan').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save_perusahaan').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger'});
        $('#error_form_perusahaan').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success'});
        get_perusahaan();
        $('#modaladdproduksi').modal('hide');
        $('#nama_perusahaan').val('');
        $('#alamat_perusahaan').val('');
        $('#kota_perusahaan').val('');
        $('#hp_perusahaan').val('');
        $('#telp_perusahaan').val('');
      }

      $('#btn_save_perusahaan').html('<center>SIMPAN</center>');
      $('#btn_save_perusahaan').attr('onClick','save_data_perusahaan()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#error_form_perusahaan').html(c.toString());
      $('#btn_save_perusahaan').html('<center>SIMPAN</center>');
      $('#btn_save_perusahaan').attr('onClick','save_data_perusahaan()');
    }
  });
}


</script>
