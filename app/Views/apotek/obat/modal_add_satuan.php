<div class="modal" id="tambahdatasatuan" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Master Satuan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
          <label for="largeInput">Nama Satuan<sup style="color:red;">*</sup></label>
          <input type="text" class="form-control form-control" id="nama_satuan" placeholder="">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save_satuan" onClick="save_data_satuan()">SIMPAN</button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

$(document).ready(function(){

})

function modaltambahsatuan(){
  $('.modal').modal('hide');
  $('#tambahdatasatuan').modal('show');
}


function save_data_satuan(){
  var nama  = $('#nama_satuan').val();
  var dataString = 'nama='+nama;
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat/action_create_satuan',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_save_satuan').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save_satuan').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger'});
      } else {
        $.notify({message: JSONObject.message},{type: 'success'});
        get_satuan_obat();
        $('#nama_satuan').val('');
        $('.modal').modal('hide');
      }
      $('#btn_save_satuan').html('<center>SIMPAN</center>');
      $('#btn_save_satuan').attr('onClick','save_data_satuan()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#btn_save_satuan').html('<center>SIMPAN</center>');
      $('#btn_save_satuan').attr('onClick','save_data_satuan()');
    }
  });
}



</script>
