
<div class="modal" id="modallistitemrak" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Data Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12" id="root_list_item">
          </div>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">


function lihat_item_rak(id){
  $('#modallistitemrak').modal('show');
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/apotek/gudang/list_item',
    data : 'id_rak='+id,
    beforeSend:function(){
      $('#root_list_item').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#root_list_item').html(data);
      $('.basic-datatables').DataTable({});
    },
    error:function(a,b,c){
      $('#root_list_item').html(c.toString());
    }
  });
}

</script>
