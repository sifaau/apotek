<div class="modal" id="modaleditdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Gudang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <input type="hidden" id="id_edit" />

        <div class="form-group">
          <label for="largeInput">Nama</label>
          <input type="text" class="form-control form-control" id="nama_edit" placeholder="Nama Gudang atau Lokasi">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_edit" onClick="edit_data()">EDIT</button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).on("click","#tmblmodaleditdata",function(){
  var id = $(this).data('id');
  var nama = $(this).data('nama');

  $('#id_edit').val(id);
  $('#nama_edit').val(nama);

})

function edit_data(){
  var nama = $('#nama_edit').val();
  var id = $('#id_edit').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gudang/action_create',
    data : 'action=edit&id='+id+'&nama='+nama,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_edit').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_edit').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger'});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success'});
        get_data();
        $('#modaleditdata').modal('hide');
      }

      $('#btn_edit').html('<center>SIMPAN</center>');
      $('#btn_edit').attr('onClick','edit_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#btn_edit').html('<center>SIMPAN</center>');
      $('#btn_edit').attr('onClick','edit_data()');
    }
  });
}

</script>
