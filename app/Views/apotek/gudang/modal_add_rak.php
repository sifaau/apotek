<div class="modal" id="modaladdrak" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Rak di <span id="nama_gudang_add_rak"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_gudang_add_rak">
        <div class="form-group">
          <label for="largeInput">Label / Nama</label>
          <input type="text" class="form-control form-control" id="nama_rak" placeholder="Nama Rak atau Label">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save_rak" onClick="save_data_rak()">SIMPAN</button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).on("click","#tmblmodaladdrak",function(){
  var id = $(this).data('id');
  var nama = $(this).data('nama');

  $('#id_gudang_add_rak').val(id);
  $('#nama_gudang_add_rak').html(nama);

})

function save_data_rak(){
  var id_gudang = $('#id_gudang_add_rak').val();
  var nama = $('#nama_rak').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gudang/action_create_rak',
    data : 'id_gudang='+id_gudang+'&nama='+nama,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_save_rak').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save_rak').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger'});
        // $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success'});
        get_data();
        $('#modaladdrak').modal('hide');
      }
      $('#btn_save_rak').html('<center>SIMPAN</center>');
      $('#btn_save_rak').attr('onClick','save_data_rak()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#btn_save_rak').html('<center>SIMPAN</center>');
      $('#btn_save_rak').attr('onClick','save_data_rak()');
    }
  });
}

</script>
