<div class="modal" id="modaleditrak" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Rak</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_rak_edit">
        <input type="hidden" id="id_gudang_rak_edit">
        <div class="form-group">
          <label for="largeInput">Label / Nama</label>
          <input type="text" class="form-control form-control" id="nama_rak_edit" placeholder="Nama Rak atau Label">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-info btn-lg btn-round" id="btn_edit_rak" onClick="edit_data_rak()">SIMPAN</button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).on("click","#tmblmodaleditrak",function(){
  var id = $(this).data('id');
  var id_gudang = $(this).data('id_gudang');
  var nama = $(this).data('nama');

  $('#id_rak_edit').val(id);
  $('#id_gudang_rak_edit').val(id_gudang);
  $('#nama_rak_edit').val(nama);

})

function edit_data_rak(){
  var id = $('#id_rak_edit').val();
  var id_gudang = $('#id_gudang_rak_edit').val();
  var nama = $('#nama_rak_edit').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gudang/action_create_rak',
    data : 'action=edit&id='+id+'&id_gudang='+id_gudang+'&nama='+nama,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_edit_rak').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_edit_rak').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger'});
        // $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success'});
        get_data();
        $('#modaleditrak').modal('hide');
      }
      $('#btn_edit_rak').html('<center>SIMPAN</center>');
      $('#btn_edit_rak').attr('onClick','edit_data_rak()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#btn_edit_rak').html('<center>SIMPAN</center>');
      $('#btn_edit_rak').attr('onClick','edit_data_rak()');
    }
  });
}

</script>
