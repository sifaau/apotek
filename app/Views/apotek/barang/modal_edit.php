<div class="modal" id="modaleditdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="card">
                <div class="card-header">
                  <div class="card-head-row">
                    <div class="card-title">Form Data Barang</div>
                    <div class="card-tools">
                      <ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active"aria-selected="true"><span class="badge badge-danger">STEP 1</span> Form Data Barang</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link "aria-selected="false" onClick="edit_data()"><i class="fa fa-arrow-right"></i> <span class="badge badge-default">STEP 2</span> Setting Satuan</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link"aria-selected="true" onClick="go_open_modal_stok_awal_2()"><span class="badge badge-default">STEP 3</span> Stok Awal</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

        <input type="hidden" id="id_edit" />

        <div class="form-group">
          <label for="largeInput">Nama <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="nama_edit" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput">Golongan <sup style="color:red;">*</sup></label>
          <select type="text" class="form-control obat_golongan" id="golongan_edit" placeholder="">
          </select>
        </div>

        <div class="form-group">
          <label for="largeInput">Merek <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="merek_edit" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >Spesifikasi</label>
          <textarea class="form-control" id="spesifikasi_edit"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Di Produksi Oleh <sup style="color:red;">*</sup></label>
          <div class="input-group mb-3">
            <select type="text" class="form-control parent obat_perusahaan" id="produksi_edit" placeholder="">
            </select>
            <div class="input-group-append">
              <button class=" btn btn-primary" onClick="add_produksi_obat()"><i class="fa fa-plus"></i> TAMBAH</button>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" onClick="edit_data()"><b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i></button>
        </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function go_open_modal_stok_awal_2(){
  var id_obat = $('#id_edit').val();
  $('#modaleditdata').modal('hide');
  modal_stok_awal(id_obat);
}

function open_modal_edit_data(id){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/barang/open_edit_data_barang',
    data : 'id='+id,
    dataType : 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $('#id_edit').val(JSONObject.id);
      $('#nama_edit').val(JSONObject.nama);
      $('#merek_edit').val(JSONObject.merek);
      $('#spesifikasi_edit').val(JSONObject.spesifikasi);
      $('#produksi_edit').val(JSONObject.id_perusahaan);
      get_golongan_edit(JSONObject.id_golongan);
      get_perusahaan_edit(JSONObject.id_perusahaan);
      $('#modaleditdata').modal('show');
    },
    error:function(a,b,c){
    }
  });
}

$(document).on("click","#tmblmodaleditdata",function(){
  var id = $(this).data('id');
  var nama = $(this).data('nama');
  var spesifikasi = $(this).data('spesifikasi');
  var merek = $(this).data('merek');
  var id_golongan = $(this).data('id_golongan');
  var id_perusahaan = $(this).data('id_perusahaan');
  isi_val(id,id_golongan,nama,merek,spesifikasi,id_perusahaan)
  // get_parent_golongan('0');
})

async function isi_val(id,id_golongan,nama,merek,spesifikasi,id_perusahaan){
    // await get_parent_golongan('0');
    $('#id_edit').val(id);
    $('#nama_edit').val(nama);
    $('#merek_edit').val(merek);
    $('#spesifikasi_edit').val(spesifikasi);
    $('#produksi_edit').val(id_perusahaan);
    get_golongan_edit(id_golongan);
    get_perusahaan_edit(id_perusahaan);
}

function edit_data(){
  var nama  = $('#nama_edit').val();
  var golongan = $('#golongan_edit').val();
  var perusahaan = $('#produksi_edit').val();
  var spesifikasi = $('#spesifikasi_edit').val();
  var merek = $('#merek_edit').val();
  var id = $('#id_edit').val();
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/barang/action_create',
    data : 'action=edit&id='+id+'&nama='+nama+'&golongan='+golongan+'&perusahaan='+perusahaan+'&spesifikasi='+spesifikasi+'&merek='+merek,
    dataType:'json',
    beforeSend:function(){
      $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_edit').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_edit').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index:10000});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index:10000});
        get_data();
        $('#modaleditdata').modal('hide');
        modal_edit_satuan(id);
      }

      $('#btn_edit').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
      $('#btn_edit').attr('onClick','edit_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:10000});
      $('#btn_edit').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
      $('#btn_edit').attr('onClick','edit_data()');
    }
  });
}

function get_perusahaan_edit(id_perusahaan){
  console.log('get_perusahaan');
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/obat_perusahaan/get_perusahaan',
    beforeSend:function(){
      $('#produksi_edit').html('<option>Loading</option>');
    },
    success: function(data) {
      $('#produksi_edit').html(data);
      $('#produksi_edit').val(id_perusahaan);
    },
    error:function(a,b,c){
      $('#produksi_edit').html('<option>Error</option>');
    }
  });
}

function get_golongan_edit(id_golongan){
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/gol_obat/get_golongan_option',
    beforeSend:function(){
      $('#golongan_edit').html('<option>Loading</option>');
    },
    success: function(data) {
      $('#golongan_edit').html(data);
      $('#golongan_edit').val(id_golongan);
    },
    error:function(a,b,c){
      $('#golongan_edit').html('<option>Error</option>');
    }
  });
}

</script>
