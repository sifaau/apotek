<div class="modal" id="tambahdata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="card">
								<div class="card-header">
									<div class="card-head-row">
										<div class="card-title">Form Data Barang</div>
										<div class="card-tools">
											<ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
												<li class="nav-item">
													<a class="nav-link active"aria-selected="true"><span class="badge badge-danger">STEP 1</span> Form Data Barang</a>
												</li>
												<li class="nav-item">
													<a class="nav-link "aria-selected="false" onClick="save_data()"><i class="fa fa-arrow-right"></i> <span class="badge badge-default">STEP 2</span> Setting Satuan</a>
												</li>
                        <li class="nav-item">
                          <a class="nav-link"aria-selected="true" onClick="edit_data()"><span class="badge badge-default">STEP 3</span> Stok Awal</a>
                        </li>
											</ul>
										</div>
									</div>
								</div>
              </div>

        <div class="form-group">
          <label for="largeInput">Golongan <sup style="color:red;">*</sup></label>
          <select type="text" class="form-control barang_golongan" id="golongan" placeholder="">
          </select>
        </div>

        <div class="form-group">
          <label for="largeInput">Nama <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="nama" placeholder="" >
        </div>

        <div class="form-group">
          <label for="largeInput">Merek <sup style="color:red;">*</sup></label>
          <input type="text" class="form-control" id="merek" placeholder="">
        </div>

        <div class="form-group">
          <label for="largeInput" >Spesifikasi</label>
          <textarea class="form-control" id="spesifikasi"></textarea>
        </div>

        <div class="form-group">
          <label for="largeInput" >Di Produksi Oleh <sup style="color:red;">*</sup></label>
          <div class="input-group mb-3">
            <select type="text" class="form-control parent barang_perusahaan" id="produksi" placeholder="">
            </select>
            <div class="input-group-append">
              <button class=" btn btn-primary" onClick="add_produksi_obat()"><i class="fa fa-plus"></i> TAMBAH</button>
            </div>
          </div>
        </div>

        <div class="col-md-12" id="error_form">
        </div>

        <div class="form-group">
          <label for="smallInput"></label>
          <button type="button" class="form-control btn btn-primary btn-lg btn-round" id="btn_save" onClick="save_data()"><b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i></button>
        </div>


      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn-sm btn-round" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).ready(function() {

});

function modal_tambah_data(){
  $('#tambahdata').modal('show');
  get_golongan();
  get_perusahaan();
}

function save_data(){

  var nama  = $('#nama').val();
  var golongan = $('#golongan').val();
  var perusahaan = $('#produksi').val();
  var spesifikasi = $('#spesifikasi').val();
  var merek = $('#merek').val();

  var dataString = 'nama='+nama+'&golongan='+golongan+'&perusahaan='+perusahaan+'&spesifikasi='+spesifikasi+'&merek='+merek;

  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/barang/action_create',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      // $.notify({message: 'Loading..'},{type: 'info'});
      $('#btn_save').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_save').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index:10000});
        $('#error_form').html(JSONObject.message);
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index:10000});
        get_data();
        $('#tambahdata').modal('hide');
        $('.form-control').val('');
        modal_edit_satuan(JSONObject.id_item);
      }

      $('#btn_save').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
      $('#btn_save').attr('onClick','save_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#btn_save').html('<b>LANJUT KE SETTING SATUAN</b> <i class="fa fa-arrow-right"></i>');
      $('#btn_save').attr('onClick','save_data()');
    }
  });
}

</script>
