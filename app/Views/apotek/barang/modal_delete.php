

<div class="modal" id="deletedata" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ubah Status Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <input type="hidden" class="form-control form-control" id="delete_id"  >
            </div>
          </div>
          <div class="col-md-12 text-center" id='txt_confirm'>

          </div>
          <div class="col-md-6">
            <a class="btn btn-danger btn-lg col-md-12" id="btn_delete_item" onClick="delete_data()">YA</a>
          </div>
          <div class="col-md-6">
            <a class="btn btn-default btn-lg col-md-12" data-dismiss="modal">BATAL</a>
          </div>


        </div>


      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

function modal_delete_barang(id,status){
  if (status == '0'){
    $('#txt_confirm').html('<h2><b>Yakin ingin diaktifkan kembali?</b></h2>');
  } else {
    $('#txt_confirm').html('<h2><b>Yakin ingin dihapus?</b></h2>');
  }

  $('#delete_id').val(id);
  $('#deletedata').modal('show');
}

function delete_data(){
  var id = $('#delete_id').val();
  var dataString = 'id='+id;
  $.ajax({
    type:'POST',
    url : '<?php echo base_url();?>/apotek/barang/action_delete',
    data : dataString,
    dataType:'json',
    beforeSend:function(){
      // $.notify({message: 'Loading..'},{type: 'info',z_index:10000});
      $('#btn_delete_item').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
      $('#btn_delete_item').removeAttr('onClick');
    },
    success: function(JSONObject) {
      if (JSONObject.response==false){
        $.notify({message: JSONObject.message},{type: 'danger',z_index:10000});
      } else {
        $.notify({message: JSONObject.message},{type: 'success',z_index:10000});
        get_data();
        $('#nama_satuan').val('');
        $('.modal').modal('hide');
      }
      $('#btn_delete_item').html('<center>YA</center>');
      $('#btn_delete_item').attr('onClick','delete_data()');
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error',z_index:10000});
      $('#btn_delete_item').html('<center>YA</center>');
      $('#btn_delete_item').attr('onClick','delete_data()');
    }
  });
}

</script>
