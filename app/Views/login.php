<?php
$session = session();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Login</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?php echo base_url('favicon.ico');?>" type="image/x-icon"/>
	<!-- Fonts and icons -->
	<!-- CSS Files -->
	<?php echo view('layout/atlantis_css');?>

	<script>
	WebFont.load({
		google: {"families":["Lato:300,400,700,900"]},
		custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?php echo base_url('plugin/atlantis/css/fonts.min.css');?>']},
		active: function() {
			sessionStorage.fonts = true;
		}
	});
	</script>

</head>
<body class="login_page" style="background:#efefee;">
	<div class="container h-100">
		<div id="overlay"></div>
		<div class="row h-100 justify-content-center align-items-center">
			<div class="box_login" style="background:#fff;padding:30px;border-radius:20px;margin-top : 40px;">
				<input type="hidden" name="tkn" value="<?php echo $session->get('tkn');?>">
				<div>
					<center>
						<span>Silahkan login</span>
					</center>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<span class="input-icon-addon"><i class="icon-user"></i></span>
						<input id="username" type="text" class="form-control" id="username" value="" placeholder="Username" required autocomplete="off" autofocus>
					</div>
				</div>
				<div class="form-group">
					<div class="input-icon">
						<span class="input-icon-addon"><i class="icon-key"></i></span>
						<input id="password" type="password" class="form-control" placeholder="Password" autocomplete="off" required>
					</div>
				</div>
				<div class="form-group">
					<button  class="btn form-control btn-primary btn-round" id="btn_login" onClick="login()"><b>Login</b></button>
				</div>
				<div class="text-center">
					<p>Sisa waktu anda untuk login <span id="sisa_waktu">30</span> detik.</p>
					Tampilan Terbaik Gunakan
					<a style="color:#536c79;text-decoration: underline;"
					href="https://www.google.com/chrome/browser/desktop/" target="blank">Google
					Chrome</a> Terbaru
				</div>
			</div>
		</div>
	</div>
	<?php echo view('layout/atlantis_js');?>

	<script type="text/javascript">
	var timeLeft = 30;
	// var timerId = setInterval(countdown, 1000);
	function countdown() {
		if (timeLeft == -1) {
			clearTimeout(timerId);
			window.location.replace("<?php echo base_url();?>/aptauth/backtologin");
		} else {
			console.log(timeLeft);
			$('#sisa_waktu').html(timeLeft);
			// elem.innerHTML = timeLeft + ' seconds remaining';
			timeLeft--;
		}
	}
	function login(){
		timeLeft = 15;
		var username = $("#username").val();
		var password = $("#password").val();
		var dataString = 'username='+ username + '&password=' + password + '&tkn=<?php echo $session->get('tkn');?>';

		$.ajax({
			url:'<?php echo base_url('aptauth/auth_login'); ?>',
			type: 'post',
			dataType:'json',
			data:dataString,
			beforeSend:function(){
				$('#btn_login').html('<i class="fa fa-spin fa-spinner"></i> Loading');
				$('#btn_login').removeAttr('onClick');
			},
			success:function(data){
				if (data.response == false ){
					$.notify({message: data.message},{type: 'danger'});
				} else {
					$.notify({message: data.message},{type: 'success'});
					window.location.href = '<?php echo base_url();?>';
				}
				$('#btn_login').html('<b>Login</b>');
				$('#btn_login').attr('onClick','login()');
			},
			error:function(a,b,c){
				$.notify({ message: c.toString() },{type: 'danger'});
				$('#btn_login').html('<b>Login</b>');
				$('#btn_login').attr('onClick','login()');
			}
		});
	}
	</script>

</body>
</html>
