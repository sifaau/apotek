

<script src="<?php echo base_url('plugin/atlantis/js/core/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/atlantis/js/core/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/atlantis/js/plugin/bootstrap-notify/bootstrap-notify.min.js');?>"></script>
<!-- <script src="<?php echo base_url('plugin/notify/notify.js');?>"></script> -->
<script src="<?php echo base_url('plugin/atlantis/js/atlantis.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/sweetalert/sweetalert.min.js'); ?>"></script>
<script type="text/javascript">
// $('.select2').select2({
//   placeholder: 'Select an option'
// });

$(".form-persen").keyup(function(){
  var hargajual = $(this).val();
  var newhargajual = hargajual.replace('%','')
  $(this).val(newhargajual+'%');
});

function currency_format(amount, decimalCount , decimal , thousands ) {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}


</script>
