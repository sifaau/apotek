<link rel="stylesheet" href="<?php echo base_url('plugin/datepicker/datepicker.css');?>">
<link rel="stylesheet" href="<?php echo base_url('plugin/datepicker/daterangepicker.css');?>">
<script src="<?php echo base_url('plugin/datepicker/moment.js'); ?>"></script>
<script src="<?php echo base_url('plugin/datepicker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('plugin/datepicker/daterangepicker.js'); ?>"></script>
