<?php
helper('user');
helper('sidebar');
$session = session();
$level_user = get_level_user();

?>

<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<div class="user">
				<div class="avatar-sm float-left mr-2">
					<img src="<?php echo base_url('plugin/atlantis//img/profile.jpg');?>" alt="..." class="avatar-img rounded-circle">
				</div>
				<div class="info">
					<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
						<span>
							<h4><?php echo $session->get('username');?></h4>
							<span class="user-level"></span>

						</span>
					</a>

				</div>
			</div>
			<ul class="nav nav-primary">

				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Menu</h4>
				</li>

				<li class="nav-item">

					<?php
					$menu_utama = get_menu_utama();
					foreach ($menu_utama->getResult() as $row) {
						if (check_akses_menu($row->id) > 0 ){
							echo '<a data-toggle="collapse" href="#sidebarLayouts'.$row->id.'">
							'.$row->icon.'
							<p>'.$row->name.'</p>
							<span class="caret"></span>
							</a>';
							echo '<div class="collapse" id="sidebarLayouts'.$row->id.'">
							<ul class="nav nav-collapse">';
							$sub_menu = get_sub_menu($row->id);
							foreach ($sub_menu->getResult() as $row2) {
								if (check_akses_sub_menu($row2->id) > 0){
									echo '<li>
									<a href="'.base_url($row2->directory.$row2->method).'">
									<span class="sub-item">'.$row2->name.'</span>
									</a>
									</li>';
								}
							}
							echo '</ul>
							</div>';
						}

					}
					?>

					<?php if ($level_user == 'superadmin'):?>
						<li class="nav-item">
							<a data-toggle="collapse" href="#sidebarSetting">
								<i class="fas fa-th-list"></i>
								<p>Setting</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="sidebarSetting">
								<ul class="nav nav-collapse">
									<li>
										<a href="<?php echo base_url('superadmin/users');?>">
											<span class="sub-item">User</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('superadmin/backup');?>">
											<span class="sub-item">Backup Data</span>
										</a>
									</li>
								</ul>
							</div>
						</li>


					<?php endif;?>

				</ul>
			</div>
		</div>
	</div>
	<!-- End Sidebar -->
