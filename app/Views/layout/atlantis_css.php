
<link rel="stylesheet" href="<?php echo base_url('plugin/atlantis/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('plugin/atlantis/css/atlantis.css');?>">
<link rel="stylesheet" href="<?php echo base_url('plugin/select2/select2.min.css');?>">
<script src="<?php echo base_url('plugin/atlantis/js/plugin/webfont/webfont.min.js'); ?>"></script>
<script>
WebFont.load({
  google: {"families":["Lato:300,400,700,900"]},
  custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?php echo base_url('plugin/atlantis/css/fonts.min.css');?>']},
  active: function() {
    sessionStorage.fonts = true;
  }
});
</script>
<script src="<?php echo base_url('plugin/atlantis/js/core/jquery.3.2.1.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/atlantis/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js');?>"></script>
<script src="<?php echo base_url('plugin/atlantis/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js');?>"></script>
<script src="<?php echo base_url('plugin/atlantis/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js');?>"></script>
<script src="<?php echo base_url('plugin/select2/select2.full.min.js');?>"></script>

<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?php echo base_url('plugin/atlantis/js/plugin/sweetalert/sweetalert.min.js');?>"></script>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" /> -->
