<?php $this->session = session(); ?>
<script type="text/javascript">
$(document).ready(function() {
  <?php if ($this->session->getFlashdata('message') != NULL) : $message = $this->session->getFlashdata('message'); ?>
  swal( "OK" ,  "<?php echo $message['message'];?>" ,  "success" );
  <?php endif;?>
  <?php if ($this->session->getFlashdata('message_error') != NULL) : $message = $this->session->getFlashdata('message_error'); ?>
  swal( "Gagal" ,  "<?php echo $message['message_error'];?>" ,  "error" );
  <?php endif;?>
});
</script>
