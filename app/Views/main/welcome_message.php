<?php $name_menu = 'Dashboard';?>
<div class="main-panel">
  <div class="content">
    <div class="page-inner">
      <div class="page-header">
        <h4 class="page-title"><?php echo $name_menu;?></h4>
        <ul class="breadcrumbs">
          <li class="nav-home">
            <a href="<?php echo base_url();?>">
              <i class="flaticon-home"></i>
            </a>
          </li>
          <li class="separator">
            <i class="flaticon-right-arrow"></i>
          </li>
          <li class="nav-item">
            <a href="#"><?php echo $name_menu;?></a>
          </li>
        </ul>
      </div>


      <div class="page-body">



        <div class="row">

          <div class="col-md-4">
          <div class="row">

            <div class="col-sm-12 col-lg-12">
              <div class="card p-3">
                <div class="d-flex align-items-center">
                  <span class="stamp stamp-md bg-primary mr-3">
                    <i class="fa fa-dollar-sign"></i>
                  </span>
                  <div>
                    <h5 class="mb-1"><b><a href="#"><span id="nominal_trans_today">0</span> <small></small></a></b></h5>
                    <small class="text-muted"><span id="jml_trans_today">0</span> Transaksi Penjualan Hari ini</small>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12 col-lg-12">
              <div class="card p-3">
                <div class="d-flex align-items-center">
                  <span class="stamp stamp-md bg-success mr-3">
                    <i class="fa fa-dollar-sign"></i>
                  </span>
                  <div>
                    <h5 class="mb-1"><b><a href="#"><span id="nominal_trans_month">0</span> <small></small></a></b></h5>
                    <small class="text-muted"><span id="jml_trans_month">0</span> Transaksi Penjualan Bulan ini</small>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

          <div class="col-md-4">
            <div class="card card-dark bg-primary-gradient">
              <div class="card-body skew-shadow">
                <i class="fas fa-capsules"></i> Obat
                <h2 class="py-4 mb-0" id="obat_all">0 item</h2>
                <div class="row">
                  <div class="col-8 pr-0">
                    <h3 class="fw-bold mb-1" id="obat_aktif">0</h3>
                    <div class="text-small text-uppercase fw-bold op-8">Aktif</div>
                  </div>
                  <div class="col-4 pl-0 text-right">
                    <h3 class="fw-bold mb-1" id="obat_nonaktif">0</h3>
                    <div class="text-small text-uppercase fw-bold op-8">Tidak Aktif</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card card-dark bg-success-gradient">
              <div class="card-body skew-shadow">
                <i class="fas fa-capsules"></i> Barang
                <h2 class="py-4 mb-0" id="barang_all">0 item</h2>
                <div class="row">
                  <div class="col-8 pr-0">
                    <h3 class="fw-bold mb-1" id="barang_aktif">0</h3>
                    <div class="text-small text-uppercase fw-bold op-8">Aktif</div>
                  </div>
                  <div class="col-4 pl-0 text-right">
                    <h3 class="fw-bold mb-1" id="barang_nonaktif">0</h3>
                    <div class="text-small text-uppercase fw-bold op-8">Tidak Aktif</div>
                  </div>
                </div>
              </div>
            </div>

          </div>



        </div>


        <div class="row" style="display:none;">

          <div class="col-md-4">
							<div class="card full-height">
								<div class="card-body">
									<div class="card-title">Grafik Transaksi Penjualan Dua Minggu Terakhir</div>
									<div class="row py-3">
										<div class="col-md-4 d-flex flex-column justify-content-around">
											<div>
												<h6 class="fw-bold text-uppercase text-success op-8">Total Transaksi</h6>
												<h3 class="fw-bold" id="total_trans_dua_minggu">0</h3>
											</div>
											<div>
												<h6 class="fw-bold text-uppercase text-danger op-8"></h6>
												<h3 class="fw-bold"></h3>
											</div>
										</div>
										<div class="col-md-8">
                      <div id="chart-container">
												<canvas id="totalIncomeChart"></canvas>
											</div>
                      <div id="chart-container">
												<canvas id="totalIncomeChart2"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

          <div class="col-md-4">
            <div class="card  full-height">
              <div class="card-header">
                <div class="card-title">5 Obat dengan Sisa Stok Terbanyak <br><small><i>( Dalam satuan terkecil )</i></small></div>
              </div>
              <div class="card-body pb-0" id="data_stok_terbanyak">

              </div>
              <div class="card-footer">
                <a href="<?php echo base_url();?>/report/stock_obat/terbanyak" class="btn btn-sm btn-danger pull-right" > <b>Lihat Semua</b> <i class="fa fa-arrow-right"></i></a>
              </div>

            </div>
          </div>

          <div class="col-md-4">
            <div class="card  full-height">
              <div class="card-header">
                <div class="card-title">10 Obat Lebih sering terjual 2 bulan terakhir</div>
              </div>
              <div class="card-body pb-0" id="data_sering_terjual">
                <div class="d-flex">
                  <div class="avatar">
                    <i class="fas fa-capsules"></i>
                  </div>
                  <div class="flex-1 pt-1 ml-2">
                    <h6 class="fw-bold mb-1">CSS</h6>
                    <small class="text-muted">Cascading Style Sheets</small>
                  </div>
                  <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">+$17</h3>
                  </div>
                </div>
                <div class="separator-dashed"></div>
              </div>
            </div>
          </div>






          </div>

      </div>

    </div>
  </div>
</div>

<script src="<?php echo base_url('plugin/atlantis/js/plugin/chart.js/chart.min.js');?>"></script>

<script type="text/javascript">

$(document).ready(function() {
  get_data_penjualan_hari_ini();
  get_data_penjualan_bulan_ini();
  get_summary_jumlah_item('obat');
  get_summary_jumlah_item('barang');
  // get_stok_obat_terbanyak();
  // sering_terjual_dua_bulan_terakhir();
  // grafik_transaksi();
});

function get_data_penjualan_hari_ini(){
  var start = '<?php echo date('d-m-Y');?>';
  var end = '<?php echo date('d-m-Y');?>';
  var nomor = '';
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/penjualan/report/get_data',
    data : 'start='+start+'&end='+end+'&nomor='+nomor,
    dataType : 'json',
    beforeSend:function(){
      $('#nominal_trans_today').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
      $('#jml_trans_today').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(JSONObject) {
      $('#nominal_trans_today').html(JSONObject.nominal);
      $('#jml_trans_today').html(JSONObject.jml_trans);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#nominal_trans_today').html('error');
      $('#jml_trans_today').html('error');
    }
  });
}

function get_data_penjualan_bulan_ini(){
  var start = '<?php echo '01'.date('-m-Y');?>';
  var end = '<?php echo date('t-m-Y');?>';
  var nomor = '';
  $.ajax({
    type:'GET',
    url : '<?php echo base_url();?>/penjualan/report/get_data',
    data : 'start='+start+'&end='+end+'&nomor='+nomor,
    dataType : 'json',
    beforeSend:function(){
      $('#nominal_trans_month').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
      $('#jml_trans_month').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(JSONObject) {
      $('#nominal_trans_month').html(JSONObject.nominal);
      $('#jml_trans_month').html(JSONObject.jml_trans);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});
      $('#nominal_trans_month').html('error');
      $('#jml_trans_month').html('error');
    }
  });
}

function get_summary_jumlah_item(table){
  $.ajax({
    url : '<?php echo base_url();?>/dashboard/info/get_summary_jumlah_item/'+table,
    dataType : 'json',
    beforeSend:function(){

    },
    success: function(JSONObject) {
      $('#'+table+'_all').html(JSONObject.all+' item');
      $('#'+table+'_aktif').html(JSONObject.aktif);
      $('#'+table+'_nonaktif').html(JSONObject.nonaktif);
    },
    error:function(a,b,c){
      $.notify({message: c.toString()},{type: 'error'});

    }
  });
}

function get_stok_obat_terbanyak(){
  $.ajax({
    url : '<?php echo base_url();?>/dashboard/info/get_stok_obat_terbanyak',
    beforeSend:function(){
      $('#data_stok_terbanyak').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#data_stok_terbanyak').html(data);
    },
    error:function(a,b,c){
      $('#data_stok_terbanyak').html('');
    }
  });
}

function grafik_transaksi(){
  $.ajax({
    url : '<?php echo base_url();?>/dashboard/info/grafik_transaksi',
    dataType : 'json',
    beforeSend:function(){
      $('#totalIncomeChart').html('');
      $("#total_trans_dua_minggu").html('<i class="fa fa-spin fa-spinner"></i>')
    },
    success: function(JSONObject) {
      $("#total_trans_dua_minggu").html(JSONObject.total);
      var totalIncomeChart = document.getElementById('totalIncomeChart').getContext('2d');
      var totalIncomeChart2 = document.getElementById('totalIncomeChart2').getContext('2d');

      var mytotalIncomeChart = new Chart(totalIncomeChart, {
        type: 'bar',
        data: {
          // labels: ["1", "1", "T", "W", "T", "F", "S", "S", "M", "T", "T", "T", "T", "T"],
          labels : JSONObject.tgl,
          datasets : [{
            label: "Jumlah Transaksi",
            backgroundColor: '#ff9e27',
            borderColor: 'rgb(23, 125, 255)',
            // data: [6, 4, 9, 5, 4, 6, 4, 3, 8, 10,3, 8, 10, 10],
            data : JSONObject.tr
          }],
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                display: false //this will remove only the label
              },
              gridLines : {
                drawBorder: false,
                display : false
              }
            }],
            xAxes : [ {
              gridLines : {
                drawBorder: false,
                display : false
              }
            }]
          },
        }
      });

      var mytotalIncomeChart2 = new Chart(totalIncomeChart2, {
        type: 'bar',
        data: {
          // labels: ["1", "1", "T", "W", "T", "F", "S", "S", "M", "T", "T", "T", "T", "T"],
          labels : JSONObject.tgl,
          datasets : [{
            label: "Nominal Rupiah",
            backgroundColor: '#179D08',
            borderColor: 'rgb(23, 125, 255)',
            // data: [6, 4, 9, 5, 4, 6, 4, 3, 8, 10,3, 8, 10, 10],
            data : JSONObject.nm
          }],
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                display: false //this will remove only the label
              },
              gridLines : {
                drawBorder: false,
                display : false
              }
            }],
            xAxes : [ {
              gridLines : {
                drawBorder: false,
                display : false
              }
            }]
          },
        }
      });

    },
    error:function(a,b,c){
        $('#totalIncomeChart').html('Gagal menampilkan grafik');
    }
  });
}


function sering_terjual_dua_bulan_terakhir(){
  $.ajax({
    url : '<?php echo base_url();?>/dashboard/info/sering_terjual_dua_bulan_terakhir/',
    beforeSend:function(){
      $('#data_sering_terjual').html('<center><i class="fa fa-spin fa-spinner"></i> Loading</center>');
    },
    success: function(data) {
      $('#data_sering_terjual').html(data);
    },
    error:function(a,b,c){
      $('#data_sering_terjual').html('');
    }
  });
}





</script>
