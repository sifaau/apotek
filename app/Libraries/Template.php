<?php

namespace App\Libraries;

class Template
{
    public function __construct()
    {
      $this->folder_main = 'main/';
      $this->folder_apotek = 'apotek/';
      $this->folder_pembelian = 'pembelian/';
      $this->folder_penjualan = 'penjualan/';
      $this->folder_accounting = 'accounting/';
      $this->folder_report = 'report/';
      $this->folder_superadmin = 'superadmin/';
      $this->folder_utility = 'utility/';
    }

    public function main($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_main.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

    public function apotek($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_apotek.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

    public function pembelian($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_pembelian.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

    public function penjualan($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_penjualan.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

    public function report($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_report.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

    public function kasir($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = '';
      $viewdata['sidebar'] = '';
      $viewdata['content'] = view($this->folder_penjualan.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper_kasir', $viewdata);
    }

    public function accounting_main_layout($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_accounting.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

    public function superadmin($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_superadmin.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

    public function utility($content, $data = null) {
      $viewdata['header'] = view('layout/header', $data);
      $viewdata['header_content'] = view('layout/header_content', $data);
      $viewdata['sidebar'] = view('layout/sidebar', $data);
      $viewdata['content'] = view($this->folder_utility.$content, $data);
      $viewdata['footer'] = view('layout/footer', $data);
      $viewdata['session'] = session();
      echo view('wrapper', $viewdata);
    }

}
