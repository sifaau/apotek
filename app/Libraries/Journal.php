<?php

/**
* By: Sifa
*/

namespace App\Libraries;
use App\Models\General_model;

class Journal
{

  public function __construct()
  {
    date_default_timezone_set("Asia/Jakarta");
    $this->session = session();
    $this->db = db_connect();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');

  }

  public function generate_id_journal($id_user,$date_payment,$code_ref,$kode_bb=null) {

    $username     = $this->general_model->get_data_by_field('user','username',array('id'=>$id_user));
    if ($username == '' OR $username == NULL){
      $username   = 'AUTO';
    }
    $bulan        = date('m',strtotime($date_payment));
    $tahun        = date('Y',strtotime($date_payment));
    $id_periode   = $this->general_model->get_data_by_field('zacc_periode','id',"`month` =  '".$bulan."' AND `year` = '".$tahun."'");

    if ( $kode_bb != '' OR $kode_bb != null ){
      // $count_code_ref   = $this->general_model->count_rows('zacc_journal','id',array('no_bukti'=>$kode_bb,'MONTH(date_trans)'=>$bulan,'YEAR(date_trans)'=>$tahun));
      $count_code_ref   = $this->general_model->count_rows('zacc_journal','id'," no_bukti LIKE '".$kode_bb."%' AND MONTH(date_trans)='".$bulan."' AND YEAR(date_trans)='".$tahun."'");
      if ($count_code_ref > 0){
        $no_bukti         = $kode_bb.'('.str_pad(($count_code_ref+1), 3, '0', STR_PAD_LEFT).')';
      } else {
        $no_bukti         = $kode_bb;
      }
    } else {
      $prefix_no_bukti  = $code_ref.'/'.date('y/m/d',strtotime($date_payment));
      // $count_code_ref   = $this->general_model->count_rows('zacc_journal','id',array('code_ref'=>$code_ref,'MONTH(date_trans)'=>$bulan,'YEAR(date_trans)'=>$tahun));
      $count_code_ref   = $this->general_model->count_rows('zacc_journal','id'," no_bukti LIKE '".$prefix_no_bukti."%' AND MONTH(date_trans)='".$bulan."' AND YEAR(date_trans)='".$tahun."'");
      $no_bukti         = $prefix_no_bukti.'('.str_pad(($count_code_ref+1), 3, '0', STR_PAD_LEFT).')';
    }

    $count_journal      = $this->general_model->count_rows('zacc_journal','id',array('id_user'=>$id_user));
    if ($count_journal > 0){
      $last_no_trans  = $this->general_model->last_field('zacc_journal','no_trans','id DESC');
      $explode        = explode('-', $last_no_trans);
      $no_trans       =  $count_journal + 1;
      $full_no_trans  = strtoupper($username).str_pad(($no_trans), 4, '0', STR_PAD_LEFT);
    } else {
      $full_no_trans  = strtoupper($username).'0001';
    }

    $data = array(
      'no_trans'    =>  $full_no_trans,
      'id_periode'  =>  $id_periode,
      'id_user'     =>  $id_user,
      'date_trans'  =>  $date_payment,
      'code_ref'    =>  $code_ref,
      'no_bukti'    =>  $no_bukti,
      'id_project'  =>  '',
      'date_insert' =>  date('Y-m-d H:i:s'),
      'desc'        =>  '',
      'auto'        =>  1,
    );
    $save_journal       = $this->general_model->do_save('zacc_journal',$data);
    if ($save_journal){
      $id_journal  = $this->general_model->insert_id();
      return $id_journal;
    } else {
      return '0';
    }
  }

  public function generate_id_journal_manual($id_user,$date_payment,$code_ref,$kode_bb=null) {

    $username     = $this->general_model->get_data_by_field('user','username',array('id'=>$id_user));
    if ($username == '' OR $username == NULL){
      $username   = 'AUTO';
    }
    $bulan        = date('m',strtotime($date_payment));
    $tahun        = date('Y',strtotime($date_payment));
    $id_periode   = $this->general_model->get_data_by_field('zacc_periode','id',"`month` =  '".$bulan."' AND `year` = '".$tahun."'");

    if ( $kode_bb != '' OR $kode_bb != null ){
      // $count_code_ref   = $this->general_model->count_rows('zacc_journal','id',array('no_bukti'=>$kode_bb,'MONTH(date_trans)'=>$bulan,'YEAR(date_trans)'=>$tahun));
      $count_code_ref   = $this->general_model->count_rows('zacc_journal','id'," no_bukti LIKE '".$kode_bb."%' AND MONTH(date_trans)='".$bulan."' AND YEAR(date_trans)='".$tahun."'");
      if ($count_code_ref > 0){
        $no_bukti         = $kode_bb.'('.str_pad(($count_code_ref+1), 3, '0', STR_PAD_LEFT).')';
      } else {
        $no_bukti         = $kode_bb;
      }
    } else {
      $prefix_no_bukti  = $code_ref.'/'.date('y/m/d',strtotime($date_payment));
      // $count_code_ref   = $this->general_model->count_rows('zacc_journal','id',array('code_ref'=>$code_ref,'MONTH(date_trans)'=>$bulan,'YEAR(date_trans)'=>$tahun));
      $count_code_ref   = $this->general_model->count_rows('zacc_journal','id'," no_bukti LIKE '".$prefix_no_bukti."%' AND MONTH(date_trans)='".$bulan."' AND YEAR(date_trans)='".$tahun."'");
      $no_bukti         = $prefix_no_bukti.'('.str_pad(($count_code_ref+1), 3, '0', STR_PAD_LEFT).')';
    }

    $count_journal      = $this->general_model->count_rows('zacc_journal','id',array('id_user'=>$id_user));
    if ($count_journal > 0){
      $last_no_trans  = $this->general_model->last_field('zacc_journal','no_trans','id DESC');
      $explode        = explode('-', $last_no_trans);
      $no_trans       =  $count_journal + 1;
      $full_no_trans  = strtoupper($username).str_pad(($no_trans), 4, '0', STR_PAD_LEFT);
    } else {
      $full_no_trans  = strtoupper($username).'0001';
    }

    $data = array(
      'no_trans'    =>  $full_no_trans,
      'id_periode'  =>  $id_periode,
      'id_user'     =>  $id_user,
      'date_trans'  =>  $date_payment,
      'code_ref'    =>  $code_ref,
      'no_bukti'    =>  $no_bukti,
      'id_project'  =>  '',
      'date_insert' =>  date('Y-m-d H:i:s'),
      'desc'        =>  '',
    );
    $save_journal       = $this->general_model->do_save('zacc_journal',$data);
    if ($save_journal){
      $id_journal  = $this->general_model->insert_id();
      return $id_journal;
    } else {
      return '0';
    }
  }

  public function generate_journal_detail($id_user,$id_journal,$code_debet,$code_credit,$value,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null){
    if ( $id_journal != '0'){
      helper('code_accounting');
      $min_length = code_accounting_journal_min_length();
      if ( strlen($code_debet) >= $min_length && strlen($code_credit) >= $min_length ){
        $id_journal_detail_debet  = $this->generate_pos_journal_detail($id_user,$id_journal,'debet',$code_debet,$value,$line,$desc);
        $id_journal_detail_credit = $this->generate_pos_journal_detail($id_user,$id_journal,'credit',$code_credit,$value,$line,$desc);
        if ( $pos_for_id_jurnal_detail_for_ledger=='debet' ){
          return $id_journal_detail_debet;
        } else if ( $pos_for_id_jurnal_detail_for_ledger=='credit' ) {
          return $id_journal_detail_credit;
        } else {
          return '';
        }
      } else {
        return '';
      }

    } else {
      return '';
    }
  }

  public function generate_journal_detail_by_user($id_user_debet,$id_user_credit,$id_journal,$code_debet,$code_credit,$value,$line,$desc,$pos_for_id_jurnal_detail_for_ledger=null){
    if ( $id_journal != '0'){
      helper('code_accounting');
      $min_length = code_accounting_journal_min_length();
      if ( strlen($code_debet) >= $min_length && strlen($code_credit) >= $min_length ){
        $id_journal_detail_debet  = $this->generate_pos_journal_detail($id_user_debet,$id_journal,'debet',$code_debet,$value,$line,$desc);
        $id_journal_detail_credit = $this->generate_pos_journal_detail($id_user_credit,$id_journal,'credit',$code_credit,$value,$line,$desc);
        if ( $pos_for_id_jurnal_detail_for_ledger=='debet' ){
          return $id_journal_detail_debet;
        } else if ( $pos_for_id_jurnal_detail_for_ledger=='credit' ) {
          return $id_journal_detail_credit;
        } else {
          return '';
        }
      } else {
        return '';
      }

    } else {
      return '';
    }
  }

  public function generate_pos_journal_detail($id_user,$id_journal,$pos,$code_acc,$value,$line,$desc){
    if ( $id_journal != '0'){
      if ( $pos == 'debet'){
        $debet  = $value;
        $credit = 0;
      } else if ( $pos == 'credit'){
        $debet  = 0;
        $credit = $value;
      } else {
        $data['status']= 0;
      }
      $data=array(
        'id_journal'      =>  $id_journal,
        'code_accounting' =>  $code_acc,
        'id_user'         =>  $id_user,
        'debet'           =>  $debet,
        'credit'          =>  $credit,
        'date_insert'     =>  date('Y-m-d H:i:s'),
        'line'            =>  $line,
        'desc'            =>  $desc
      );
      $save = $this->general_model->do_save('zacc_journal_detail',$data);
      if ($save){
        $id_journal_detail = $this->general_model->insert_id();
        return $id_journal_detail;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  public function generate_journal_sub_ledger($id_user,$type_trans,$id_client,$type_client,$debet_ledger,$credit_ledger,$date_trans,$desc,$code_accounting,$id_journal_detail=null,$no_kwitansi=null){
    if ( $id_journal_detail == '' || $id_journal_detail == null){
      return '';
    } else {
      $data_sub_ledger_hutang =  array(
        'type_trans'      =>  $type_trans,
        'id_client'       =>  $id_client,
        'type_client'     =>  $type_client,
        'debet'           =>  $debet_ledger,
        'credit'          =>  $credit_ledger,
        'date_trans'      =>  $date_trans,
        'date_insert'     =>  date('Y-m-d H:i:s'),
        'desc'            =>  $desc,
        'code_accounting' =>  $code_accounting,
        'id_journal_detail' =>  $id_journal_detail,
        'id_user'         =>  $id_user,
        'no_kwitansi'     =>  $no_kwitansi,
      );

      $save_sub_ledger_hutang = $this->general_model->save('zacc_sub_ledger',$data_sub_ledger_hutang);
      if ( $save_sub_ledger_hutang ){
        $id             = $this->general_model->insert_id();
        return $id;
      } else {
        return '';
      }
    }

  }

  public function insert_saldo_code_accounting($id_periode,$code,$sum_debet,$sum_credit,$saldo){
    $sum_debet              = $sum_debet != NULL ? $sum_debet : 0 ;
    $sum_credit             = $sum_credit != NULL ? $sum_credit : 0 ;

    // $check_saldo             = $this->saldo_model->count_rows(array('id_periode'=>$id_periode,'code_accounting'=>$code));
    $id_code_accounting     = $this->general_model->get_data_by_field('zacc_code_accounting','id',array('code'=>$code));
    $proyek                 = $this->general_model->get_data_by_field('zacc_code_accounting','project',array('code'=>$code));
    $data = array(
      'id_periode'          =>  $id_periode,
      'code_accounting'     =>  $code,
      'totdebet'            =>  $sum_debet,
      'totcredit'           =>  $sum_credit,
      'saldo'               =>  $saldo,
      'date_insert'         =>  date('Y-m-d H:i:s'),
      'id_user'             =>  $this->id_user,
      'id_code_accounting'  =>  $id_code_accounting,
      'proyek'              =>  $proyek,
    );

    // if ( $check_saldo > 0 ){
      // $save_saldo = $this->saldo_model->update($data,array('id_periode'=>$id_periode,'code_accounting'=>$code));
    // } else {
      $save_saldo = $this->general_model->do_delete('zacc_saldo',array('id_periode'=>$id_periode,'code_accounting'=>$code));
      $save_saldo = $this->general_model->do_save('zacc_saldo',$data);
    // }
  }

}
