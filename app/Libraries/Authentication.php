<?php

namespace App\Libraries;
use App\Models\General_model;

class Authentication
{

  protected $db;

  public function __construct(){
    date_default_timezone_set("Asia/Jakarta");
    $this->session = session();
    $this->db = db_connect();
    $this->general_model = new General_model();
    $this->id_user = $this->session->get('id_user');
  }

  public function is_login() {
    $session = session();
    if($session->get('is_login')==TRUE) {
      $this->general_model->do_update('user',['last_active'=>date('Y-m-d H:i:s')],['id'=>$this->id_user]);
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function is_login_option() {
    $session = session();
    if($session->get('is_login')==TRUE) {
      return TRUE;
    } else {
      echo '<option value="">- Anda Logout -</option>';
      exit;
    }
  }

  public function is_login_div() {
    $session = session();
    if($session->get('is_login')==TRUE) {
      return TRUE;
    } else {
      echo '<i>Sessi anda habis, silakan login ulang.</i>';
      exit;
    }
  }

  public function is_login_ajax() {
    $session = session();
    if($session->get('is_login')==TRUE) {

    } else {
      echo json_encode(array('response'=>TRUE,'message'=>'Sesi anda habis, silakan login kembali'));
      exit;
    }
  }

}
