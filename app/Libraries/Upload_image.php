<?php

namespace App\Libraries;

class Upload_image {

    protected $ci;

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function upload_for_ajax($path,$name,$resize,$new_name)    {
        $this->ci->load->library('upload');
        $config['allowed_types']    = 'gif|jpg|png|jpeg';
        // $config['allowed_types']    = '*';
        $config['upload_path']      = $path;
        $config['max_size']         = '100000';
        $config['max_width']        = '100000';
        $config['max_height']       = '100000';
        $config['file_name'] = $new_name;
        $this->ci->upload->initialize($config);
        if(!$this->ci->upload->do_upload($name))
        {
            // return $this->ci->upload->display_errors();
            return FALSE;
        }
        else
        {
            $userfile   = $this->ci->upload->data();
            $image=$userfile['file_name'];
            $this->ci->upload->data();
                    //resize image
            $this->ci->load->library('image_lib');
            $config2['image_library'] = 'gd2';
            $config2['source_image'] = $path.$image;
            $config2['create_thumb'] = FALSE;
            $config2['new_image']=$path;
            $config2['maintain_ratio'] = TRUE;
            $config2['width'] = $resize;
            $this->ci->image_lib->clear();
            $this->ci->image_lib->initialize($config2);
            $this->ci->image_lib->resize();
            return $image;
        }
    }



}
