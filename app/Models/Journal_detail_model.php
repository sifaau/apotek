<?php
namespace App\Models;
use CodeIgniter\Model;

class Journal_detail_model extends Model {

  public function __construct()
  {
    $this->db = \Config\Database::connect('default');
  }

  public function sum_balance_journal_debet_by_two_date_by_limit($date_start,$date_end,$code,$limit){
    if($project == ''){
      $con_proyek = "";
    } else {
      $con_proyek = "";
    }

    $date_start = date('Y-m-d',strtotime($date_start));
    $date_end   = date('Y-m-d',strtotime($date_end));
    $query = $this->db->query("SELECT SUM(debet-credit) AS hasil
    FROM(
      SELECT zacc_journal.date_trans,zacc_journal_detail.debet,zacc_journal_detail.credit,zacc_journal_detail.id
      FROM zacc_journal,zacc_journal_detail WHERE
      zacc_journal.id = zacc_journal_detail.id_journal
      AND
      DATE(zacc_journal.`date_trans`) >= '".$date_start."' AND DATE(zacc_journal.`date_trans`) <= '".$date_end."' AND zacc_journal_detail.code_accounting = '".$code."' AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL
      ".$con_proyek."
      ORDER BY UNIX_TIMESTAMP(zacc_journal.date_trans) ASC, zacc_journal_detail.id ASC LIMIT ".$limit."
    ) AS a");
    $hasil=0;
    if ($query->getNumRows() > 0){
      $column = $query->getRowArray();
      $hasil = $column['hasil'];
    }
    return $hasil;
  }

  public function sum_balance_journal($field,$condition){
    $hasil = 0;
    $builder = $this->db->table('zacc_journal_detail,zacc_journal,zacc_code_accounting');
    $builder->select("SUM(".$field.") as hasil");
    $builder->where('zacc_journal_detail.id_journal=zacc_journal.id');
    $builder->where('zacc_journal_detail.code_accounting=zacc_code_accounting.code');
    $builder->where('zacc_journal.status IS NULL');
    $builder->where('zacc_journal_detail.status IS NULL');
    $builder->where('zacc_code_accounting.status IS NULL');
    $builder->where($condition);
    $query= $builder->get();
    if ($query->getNumRows() > 0)
    {
      $column = $query->getRowArray();
      $hasil = $column["hasil"];
    }
    return $hasil;
  }

  public function list_ref_for_ledger($condition){
    $q = $this->db->query("SELECT
    zacc_journal_detail.code_accounting, zacc_journal_detail.debet,zacc_journal_detail.credit,
    zacc_journal_detail.desc as desc_detail
    FROM zacc_journal,zacc_journal_detail WHERE zacc_journal.`id` = zacc_journal_detail.`id_journal`
    AND zacc_journal.status IS NULL AND zacc_journal_detail.status IS NULL ".$condition);
    return $query;
  }


}



?>
