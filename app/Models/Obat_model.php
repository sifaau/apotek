<?php
namespace App\Models;
use CodeIgniter\Model;

class Obat_model extends Model {

  public function __construct()
  {
    $this->db = \Config\Database::connect('default');
  }

  public function get_stok_terbanyak($limit=null){
    if ($limit == null || $limit == ''){
      $con_limit = '';
    } else {
      $con_limit = 'LIMIT '.$limit;
    }
    $query = $this->db->query("SELECT *,((stok1) + ( stok2*k2 ) + ((stok3*k3)*k2) + (((stok4*k4)*k3)*k4) ) as stok_asli FROM (

      select *,
      IFNULL((SELECT SUM(ms.qty) FROM master_stok as ms WHERE ms.id_satuan = sid1),0) as  stok1,
      IFNULL((SELECT SUM(ms.qty) FROM master_stok as ms WHERE ms.id_satuan = sid2),0) as  stok2,
      IFNULL((SELECT SUM(ms.qty) FROM master_stok as ms WHERE ms.id_satuan = sid3),0) as  stok3,
      IFNULL((SELECT SUM(ms.qty) FROM master_stok as ms WHERE ms.id_satuan = sid4),0) as  stok4
      from (
        SELECT o.id,o.nama,
        (select satuan FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '1') as s1,
        IFNULL((select IFNULL(s.konversi,1) FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '1'),1) as k1,
        IFNULL((select IFNULL(s.konversi,1) FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '2'),1) as k2,
        IFNULL((select IFNULL(s.konversi,1) FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '3'),1) as k3,
        IFNULL((SELECT IFNULL(s.konversi,1) FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '4' ),1) as k4,
        (select s.id FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '1') as sid1,
        (select s.id FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '2') as sid2,
        (select s.id FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '3') as sid3,
        (select s.id FROM satuan as s WHERE s.id_reff = o.id AND s.table_reff = 'obat' and ket = '4' ) as sid4

        FROM obat as o, satuan as ss
        WHERE
        o.id = ss.id_reff
        AND ss.table_reff = 'obat'
        AND ss.ket = '1'
        AND o.status IS NULL
      ) as x

    ) AS y

    ORDER by stok_asli DESC
    ".$con_limit);

    return $query;
  }

  public function sering_terjual_dua_bulan_terakhir($limit=null){
    if ($limit == null || $limit == ''){
      $con_limit = '';
    } else {
      $con_limit = 'LIMIT '.$limit;
    }
    $query = $this->db->query("SELECT *,(SELECT o.nama FROM obat as o WHERE o.id = id_reff) as  nama,(SELECT COUNT(pd.id) FROM penjualan_detail AS pd WHERE pd.id_item=id_reff AND pd.table_reff_item=table_reff )  AS kali
FROM (

SELECT DISTINCT pb.id_item AS id_reff,pb.table_reff_item as table_reff FROM penjualan_detail as pb, obat as o,penjualan as p
WHERE o.id = pb.id_item AND pb.table_reff_item = 'obat'
AND p.id = pb.id_penjualan
AND DATE(p.datetime) >= DATE_ADD(CURDATE(), INTERVAL -2 month)

) AS x ".$con_limit);
    return $query;
  }



}
?>
