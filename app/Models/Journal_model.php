<?php
namespace App\Models;
use CodeIgniter\Model;

class Journal_model extends Model {

  public function __construct()
  {
    $this->db = \Config\Database::connect('default');
  }

  public function list_ledger_no_page($condition,$order){
    $builder = $this->db->table('zacc_journal,zacc_journal_detail,zacc_code_accounting');
    $builder->select('zacc_journal.id as id_journal,zacc_journal.auto,zacc_journal.code_ref, zacc_journal.id_periode,zacc_journal.date_trans,zacc_journal.no_bukti,zacc_journal.desc,zacc_journal.date_insert,
    zacc_journal_detail.id as id_journal_detail,zacc_journal_detail.id_journal,zacc_journal_detail.code_accounting, zacc_journal_detail.debet, zacc_journal_detail.credit,zacc_journal_detail.id_user,zacc_journal_detail.desc as desc_detail,
    zacc_journal_detail.id_user_update,zacc_journal_detail.date_update,
    zacc_code_accounting.`balance`');
    $builder->where('zacc_journal.`id` = zacc_journal_detail.`id_journal`');
    $builder->where('zacc_code_accounting.`code` = zacc_journal_detail.`code_accounting`');
    $builder->where('zacc_code_accounting.`status` IS NULL');
    $builder->where('zacc_journal.status IS NULL');
    $builder->where('zacc_journal_detail.status IS NULL');
    $builder->where($condition);
    $builder->orderBy($order);
    // $this->db->limit($per_page,$page);
    $query = $builder->get();
		return $query;
  }

  public function riwayat($array,$condition,$order,$per_page,$page){
    $this->db->select($array);
    $this->db->where($condition);
    $this->db->order_by($order);
    $this->db->limit($per_page,$page);
    return $this->db->get($this->table);
  }

}
?>
