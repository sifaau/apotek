<?php
namespace App\Models;
use CodeIgniter\Model;

class Saldo_model extends Model {

  public function __construct()
  {
    $this->db = \Config\Database::connect('default');
  }

  public function sum_saldo_code_account($field,$condition){
    $this->db->select_sum($field);
    $this->db->from('zacc_saldo, zacc_code_accounting');
    $this->db->where('zacc_saldo.`code_accounting` = zacc_code_accounting.code');
    $this->db->where('zacc_code_accounting.status IS NULL');
    $this->db->where($condition);
    $query=$this->db->get();
    if ($query->getNumRows() > 0)
    {
      $column = $query->getRowArray();
      $hasil = $column["$field"];
    }
    return $hasil;
  }

  public function list_income_statement($condition){
    $this->db->select('zacc_code_accounting.code,zacc_code_accounting.name,zacc_code_accounting.balance, zacc_saldo.saldo,zacc_saldo.id_periode');
    $this->db->from('zacc_code_accounting,zacc_saldo');
    $this->db->where('zacc_code_accounting.code = zacc_saldo.`code_accounting`');
    $this->db->where('zacc_code_accounting.type_report','LR');
    $this->db->where('zacc_code_accounting.level',2);
    $this->db->where('zacc_code_accounting.status IS NULL');
    $this->db->where($condition);
    $this->db->order_by('zacc_code_accounting.code ASC');
    return $this->db->get();
  }

  public function list_detail_income_($condition){
    $this->db->select('zacc_code_accounting.code,zacc_code_accounting.name,zacc_code_accounting.balance,
      (SELECT proyek.proyek FROM proyek WHERE proyek.id_proyek = zacc_code_accounting.project) AS project_name');
    $this->db->from('zacc_code_accounting');
    $this->db->where('zacc_code_accounting.status IS NULL');
    $this->db->where($condition);
    $this->db->order_by('zacc_code_accounting.code ASC');
    return $this->db->get();
  }

  public function list_detail_income_statement($condition){
    $this->db->select('zacc_code_accounting.code,zacc_code_accounting.name,zacc_code_accounting.balance, zacc_saldo.saldo,zacc_saldo.id_periode,
      (SELECT proyek.proyek FROM proyek WHERE proyek.id_proyek = zacc_code_accounting.project) AS project_name');
    $this->db->from('zacc_code_accounting,zacc_saldo');
    $this->db->where('zacc_code_accounting.code = zacc_saldo.`code_accounting`');
    $this->db->where('zacc_code_accounting.status IS NULL');
    $this->db->where($condition);
    $this->db->order_by('zacc_code_accounting.code ASC');
    return $this->db->get();
  }

  public function list_detail_income_statement2($code_parent,$id_periode,$id_periode_before){
    $query = $this->db->query("
      SELECT a.`code`, `a`.`name`,a.balance,a.division_child,
      (SELECT proyek.proyek FROM proyek WHERE proyek.id_proyek = a.project) AS project_name,
      IFNULL((SELECT saldo FROM zacc_saldo WHERE code_accounting = a.code AND id_periode = '".$id_periode."' ),0)
      -
      IFNULL((SELECT saldo FROM zacc_saldo WHERE code_accounting = a.code AND id_periode = '".$id_periode_before."' ),0)
      AS saldo
      FROM `zacc_code_accounting` AS a
      WHERE
      `a`.`code_parent` = '".$code_parent."'
      AND `a`. status IS NULL
      ORDER BY a.code ASC
      ");
    return $query;
  }

  public function list_detail_income_statement_by_division($code_parent,$code_division,$id_periode,$id_periode_before){
    $query = $this->db->query("
      SELECT a.`code`, `a`.`name`,a.balance,
      (SELECT proyek.proyek FROM proyek WHERE proyek.id_proyek = a.project) AS project_name,
      IFNULL((SELECT saldo FROM zacc_saldo WHERE code_accounting = a.code AND id_periode = '".$id_periode."' ),0)
      -
      IFNULL((SELECT saldo FROM zacc_saldo WHERE code_accounting = a.code AND id_periode = '".$id_periode_before."' ),0)
      AS saldo
      FROM `zacc_code_accounting` AS a
      WHERE
      `a`.`code_parent` = '".$code_parent."'
      AND `a`.`division` = '".$code_division."'
      AND `a`.`status` IS NULL
      ORDER BY a.code ASC
      ");
    return $query;
  }

  public function list_detail_income_statement_by_project($code_parent,$id_periode,$id_periode_before,$id_proyek){
    $query = $this->db->query("
      SELECT a.`code`, `a`.`name`,a.balance,
      (SELECT proyek.proyek FROM proyek WHERE proyek.id_proyek = a.project) AS project_name,
      IFNULL((SELECT saldo FROM zacc_saldo WHERE code_accounting = a.code AND id_periode = '".$id_periode."' ),0)
      -
      IFNULL((SELECT saldo FROM zacc_saldo WHERE code_accounting = a.code AND id_periode = '".$id_periode_before."' ),0)
      AS saldo
      FROM `zacc_code_accounting` AS a
      WHERE
      `a`.`code_parent` = '".$code_parent."'
      AND `a`.`project` = '".$id_proyek."'
      AND `a`.`status` IS NULL
      ORDER BY a.code ASC
      ");
    return $query;
  }

  public function list_neraca_statement($condition){
    $this->db->select('zacc_code_accounting.code,zacc_code_accounting.name,zacc_code_accounting.balance, zacc_saldo.saldo,zacc_saldo.id_periode');
    $this->db->from('zacc_code_accounting,zacc_saldo');
    $this->db->where('zacc_code_accounting.code = zacc_saldo.`code_accounting`');
    $this->db->where('zacc_code_accounting.type_report','N');
    $this->db->where('zacc_code_accounting.level',2);
    $this->db->where('zacc_code_accounting.status IS NULL');
    $this->db->where($condition);
    $this->db->order_by('zacc_code_accounting.code ASC');
    return $this->db->get();
  }

  public function list_detail_neraca_statement($condition){
    $this->db->select('zacc_code_accounting.code,zacc_code_accounting.name,zacc_code_accounting.balance, zacc_saldo.saldo,zacc_saldo.id_periode');
    $this->db->from('zacc_code_accounting,zacc_saldo');
    $this->db->where('zacc_code_accounting.code = zacc_saldo.`code_accounting`');
    $this->db->where('zacc_code_accounting.status IS NULL');
    $this->db->where($condition);
    return $this->db->get();
  }


}
?>
