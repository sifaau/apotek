<?php
namespace App\Models;
use CodeIgniter\Model;

class General_model extends Model {


	public function __construct()
  {
		$this->db = \Config\Database::connect('default');
	}


	public function do_save($table,$data){
		$builder = $this->db->table($table);
		return $builder->insert($data);
	}

	public function do_insert_batch($table,$data){
		$builder = $this->db->table($table);
		return $builder->insertBatch($$data);
	}

	public function do_update($table,$data,$condition){
		$builder = $this->db->table($table);
		$builder->where($condition);
		return $builder->update($data);
	}

	public function do_update_batch($table,$data,$index){
		$builder = $this->db->table($table);
		return $builder->update_batch($data,$index);
	}

	public function do_delete($table,$condition){
		$builder = $this->db->table($table);
		$builder->where($condition);
		return $builder->delete();
	}

	public function all_data($table){
		$builder = $this->db->table($table);
		return $builder->get();
	}

	public function select_data($table,$array,$condition){
		$builder = $this->db->table($table);
		$builder->select($array);
		$builder->where($condition);
		return $builder->get();
	}

	public function select_data_by_limit($table,$array,$condition,$limit){
		$builder = $this->db->table($table);
		$builder->select($array);
		$builder->where($condition);
		$builder->limit($limit);
		return $builder->get();
	}

	public function select_distinct($table,$array,$condition){
		$builder = $this->db->table($table);
		$builder->distinct();
		$builder->select($array);
		$builder->where($condition);
		return $builder->get();
	}

	public function select_distinct_order($table,$array,$condition,$order){
		$builder = $this->db->table($table);
		$builder->distinct();
		$builder->select($array);
		$builder->where($condition);
		$builder->orderBy($order);
		return $builder->get();
	}

	public function select_order($table,$array,$condition,$order){
		$builder = $this->db->table($table);
		$builder->select($array);
		$builder->where($condition);
		$builder->orderBy($order);
		return $builder->get();
	}

	public function select_order_limit($table,$array,$condition,$order,$limit,$limit2=null){
		$builder = $this->db->table($table);
		$builder->select($array);
		$builder->where($condition);
		$builder->orderBy($order);
		if ($limit2==null OR $limit2==''){
			$builder->limit($limit);
		} else {
			$builder->limit($limit,$limit2);
		}
		return $builder->get();
	}


	public function select_sum($table,$field,$condition){
		$builder = $this->db->table($table);
		$hasil=0;
		$builder->select('SUM('.$field.') AS hasil');
		$builder->where($condition);
		$query=$builder->get();
		if ($query->getNumRows() > 0)
		{
			$column = $query->getRowArray();
			$hasil = $column["hasil"];
			$hasil = $hasil == '' ? 0 : $hasil;
		}
		return $hasil;
	}

	public function select_max($table,$field,$condition){
		$builder = $this->db->table($table);
		$hasil=0;
		$builder->select('MAX('.$field.') AS '.$field);
		$builder->where($condition);
		$query=$builder->get();
		if ($query->getNumRows() > 0)
		{
			$column = $query->getRowArray();
			$hasil = $column[$field];
		}
		return $hasil;
	}

	public function count_rows($table,$pk,$condition){
		$builder = $this->db->table($table);
		$builder->select($pk);
		$builder->where($condition);
		$query=$builder->get();
		return $query->getNumRows();
	}

	public function insert_id(){
		return $this->db->insertID();
	}

	public function get_data_by_field($table,$field,$condition){
		$builder = $this->db->table($table);
		$hasil=NULL;
		$builder->select($field);
		$builder->where($condition);
		$query=$builder->get();
		if ($query->getNumRows() > 0)
		{
			$column = $query->getRowArray();
			$hasil = $column[$field];
		}
		return $hasil;
	}

	public function do_query($q){
		$q = $this->db->query($q);
		return $q;
	}

	public function for_paging($table,$select,$condition,$order_query,$per_page,$page){
		$builder = $this->db->table($table);
		$builder->select($select);
		$builder->where($condition);
		$builder->orderBy($order_query);
		$builder->limit($per_page,$page);
		$query = $builder->get();
		return $query;
	}

	public function error(){
		return $this->db->error();
	}

	public function last_field($table,$field,$order){
    $hasil=NULL;
		$builder = $this->db->table($table);
		$builder->select($field);
		$builder->orderBy($order);
    $builder->limit(1);
    $query=$builder->get();
    if ($query->getNumRows() > 0)
    {
      $column = $query->getRowArray();
      $hasil = $column[$field];
    }
    return $hasil;
  }

}
?>
