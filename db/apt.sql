-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: 0.0.0.0    Database: apotek
-- ------------------------------------------------------
-- Server version	5.7.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `a_app_setting`
--

DROP TABLE IF EXISTS `a_app_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `a_app_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `param` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alamat`
--

DROP TABLE IF EXISTS `alamat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alamat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `table_reff` varchar(50) DEFAULT NULL,
  `id_reff` int(11) DEFAULT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `blok` varchar(10) DEFAULT NULL,
  `no` varchar(20) DEFAULT NULL,
  `rt` varchar(10) DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `id_province` varchar(11) DEFAULT NULL,
  `id_city` varchar(11) DEFAULT NULL,
  `id_subdistrict` varchar(11) DEFAULT NULL,
  `kd_pos` varchar(12) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_reff` (`id_reff`,`id_province`,`id_city`,`id_subdistrict`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alamat_city`
--

DROP TABLE IF EXISTS `alamat_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alamat_city` (
  `id` int(11) NOT NULL,
  `id_province` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alamat_province`
--

DROP TABLE IF EXISTS `alamat_province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alamat_province` (
  `id` int(11) NOT NULL,
  `province` varchar(100) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alamat_subdistrict`
--

DROP TABLE IF EXISTS `alamat_subdistrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alamat_subdistrict` (
  `id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `subdistrict` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `id_golongan` varchar(45) DEFAULT NULL,
  `spesifikasi` text,
  `merek` varchar(200) DEFAULT NULL,
  `id_perusahaan` varchar(45) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gudang`
--

DROP TABLE IF EXISTS `gudang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gudang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gudang_rak`
--

DROP TABLE IF EXISTS `gudang_rak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gudang_rak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_gudang` int(11) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `master_stok`
--

DROP TABLE IF EXISTS `master_stok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_stok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_satuan` int(11) DEFAULT NULL,
  `id_gudang_rak` int(11) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `no_batch` varchar(200) DEFAULT NULL,
  `date_expired` date DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `id_user_update` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obat`
--

DROP TABLE IF EXISTS `obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `id_perusahaan` int(11) DEFAULT NULL,
  `deskripsi` text,
  `indikasi` text,
  `kandungan` text,
  `zat_aktif` text,
  `efek_samping` text,
  `dosis` varchar(200) DEFAULT NULL,
  `aturan_pakai` varchar(45) DEFAULT NULL,
  `waktu_1` varchar(45) DEFAULT NULL,
  `waktu_2` varchar(45) DEFAULT NULL,
  `waktu_3` varchar(45) DEFAULT NULL,
  `waktu_4` varchar(45) DEFAULT NULL,
  `waktu_5` varchar(45) DEFAULT NULL,
  `syarat_pakai` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obat_gol`
--

DROP TABLE IF EXISTS `obat_gol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obat_gol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `keterangan` text,
  `status` tinyint(1) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obat_perusahaan`
--

DROP TABLE IF EXISTS `obat_perusahaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obat_perusahaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `alamat` text,
  `kota` varchar(45) DEFAULT NULL,
  `telp` varchar(45) DEFAULT NULL,
  `hp` varchar(45) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obat_racikan`
--

DROP TABLE IF EXISTS `obat_racikan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obat_racikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obat_racikan_detail`
--

DROP TABLE IF EXISTS `obat_racikan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obat_racikan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_racikan` int(11) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `table_reff_item` varchar(200) DEFAULT NULL,
  `satuan` varchar(100) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pembayaran`
--

DROP TABLE IF EXISTS `pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reff` int(11) DEFAULT NULL,
  `table_reff` varchar(45) DEFAULT NULL,
  `tgl_bayar` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `id_journal` varchar(45) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `penjualan`
--

DROP TABLE IF EXISTS `penjualan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `datetime` varchar(45) DEFAULT NULL,
  `id_user` varchar(45) DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `ppn` double DEFAULT NULL,
  `tuslah` double DEFAULT NULL,
  `harga_kesepakatan` double DEFAULT NULL,
  `tunai` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `penjualan_detail`
--

DROP TABLE IF EXISTS `penjualan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_penjualan` int(11) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `table_reff_item` varchar(200) DEFAULT NULL,
  `satuan` varchar(200) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `date_expired` date DEFAULT NULL,
  `no_batch` varchar(200) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `stock_awal` double DEFAULT NULL,
  `last_stock` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `po`
--

DROP TABLE IF EXISTS `po`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `po` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier` varchar(45) DEFAULT NULL,
  `no` varchar(200) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `ppn` double DEFAULT NULL,
  `harga_kesepakatan` double DEFAULT NULL,
  `status_bayar` varchar(2) DEFAULT NULL,
  `status_barang` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `po_barang_masuk`
--

DROP TABLE IF EXISTS `po_barang_masuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `po_barang_masuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_po_detail` int(11) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `table_reff_item` varchar(45) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `date_expired` date DEFAULT NULL,
  `no_batch` varchar(200) DEFAULT NULL,
  `date_come` date DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_gudang_rak` int(11) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `stock_awal` double DEFAULT NULL,
  `last_stock` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `po_detail`
--

DROP TABLE IF EXISTS `po_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `po_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_po` varchar(45) DEFAULT NULL,
  `id_item` varchar(45) DEFAULT NULL,
  `table_reff_item` varchar(45) DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL,
  `id_supplier` varchar(45) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `satuan`
--

DROP TABLE IF EXISTS `satuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reff` varchar(200) DEFAULT NULL,
  `table_reff` varchar(50) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `persen_harga_jual` double DEFAULT NULL,
  `persen_diskon` double DEFAULT NULL,
  `ket` varchar(45) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `satuan_master`
--

DROP TABLE IF EXISTS `satuan_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `satuan_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_opname`
--

DROP TABLE IF EXISTS `stock_opname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_opname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) DEFAULT NULL,
  `table_reff_item` varchar(200) DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL,
  `no_batch` varchar(200) DEFAULT NULL,
  `date_expired` date DEFAULT NULL,
  `stock_awal` double DEFAULT NULL,
  `masuk` double DEFAULT NULL,
  `keluar` double DEFAULT NULL,
  `last_stock` double DEFAULT NULL,
  `id_gudang_rak` int(11) DEFAULT NULL,
  `date_opname` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `ket` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `hp` varchar(45) DEFAULT NULL,
  `telp` varchar(45) DEFAULT NULL,
  `bank` varchar(45) DEFAULT NULL,
  `bank_norek` varchar(45) DEFAULT NULL,
  `bank_name` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `last_active` datetime DEFAULT NULL,
  `level` varchar(45) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `jabatan` varchar(45) DEFAULT NULL,
  `alamat` varchar(45) DEFAULT NULL,
  `hp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-19 18:27:34
